**TABLA DE CONTENIDO**

1. ###### SEED 
2. ###### Instalación
3. ###### Descarga e importación
4. ###### Descripción de clases
    - 4.1 Paquete de colecciones
        - 4.1.1 Arbol123
        - 4.1.2 ArbolAVL
        - 4.1.3 ArbolB
        - 4.1.4 ArbolBinario
        - 4.1.5 ArbolBinarioBusqueda
        - 4.1.6 ArbolB+
        - 4.1.7 ArbolEneario
        - 4.1.8 ArbolHeap
        - 4.1.9 ArbolRojiNegro
        - 4.1.10 ArbolSplay
        - 4.1.11 Arista
        - 4.1.12 Cola
        - 4.1.13 ColaP
        - 4.1.14 Componente
        - 4.1.15 Digrafo
        - 4.1.16 ExceptionUFPS
        - 4.1.17 GrafoND
        - 4.1.18 InformacionDeEntrada
        - 4.1.19 IteratorLC
        - 4.1.20 IteratorLCD
        - 4.1.21 IteratorLD
        - 4.1.22IteratorLS
        - 4.1.23 ListaC
        - 4.1.24 istaCD
        - 4.1.25 ListaD
        - 4.1.26 ListaS
        - 4.1.27 Nodo
        - 4.1.28 Nodo123
        - 4.1.29 NodoAVL
        - 4.1.30 NodoBin
        - 4.1.31 NodoD
        - 4.1.32 NodoEneario
        - 4.1.33 NodoP
        - 4.1.34 NodoRN
        - 4.1.35 Pagina
        - 4.1.36 Pila
        - 4.1.37 Secuencia
        - 4.1.38 TablaHash
        - 4.1.39 Vertice
5. ###### Construido con
6. ###### Autores y contribuyentes
7. ###### Vista

## 1. SEED 
SEED, semilla promotora del estudio de estructuras de Datos elementales, que esta compuesta por un componente en lenguaje JAVA y un conjunto de simuladores. Este mediador pedagógico digital ilustra el comportamiento de las estructuras de manera dinámica y se basan en el componente en JAVA para su funcionamiento.

## 2. Instalación
Seed esta desarrollado bajo el software de entorno de desarrollo integrado NetBeans, asi que se debe tener esta herramienta por lo cual debemos dirigirnos a el [sitio oficial](https://netbeans.apache.org/)

  #### Paso 1
  - Descargar el IDE [NetBeans](https://netbeans.apache.org/download/index.html) del sitio oficial Version .12 
  - Descargado el IDE hay que descargar los complementos [Java SE Runtime Environment 8](https://www.oracle.com/co/java/technologies/javase-jre8-downloads.html) para poder ejecutar porgramas Java y [Java SE Development Kit 8](https://www.oracle.com/co/java/technologies/javase/javase-jdk8-downloads.html) el entorno de desarrollo para la creación de aplicaciones.
  - Para la instalacion de los complementos simplemente iniciamos los ejecutadores y damos en siguiente, siguiente hasta finalizar la instalacion, asi sucesivamente con los dos complementos **JRE** y **JDK** siguiendo con el ejecutable de **NetBeans** de igual forma al iniciarlo se procede a dar en siguiente, siguiente eligiendo la carpeta de ubicacion como desee configruarlo y finalizar.

## 3. Descarga e importación
Finalizada la instalacion del programa y sus componentes, se continua con la descarga del repositorio **SEED_UFPS** para ello dentro del repositorio ubicamos el icono de **Downloads** para bajar el proyecto, despues pasamos a importar el proyecto en NetBean desde la opcion File -- nuevo proyecto-- ó --importar--.
 
## 4. Descripción de clases
Se podran identificar el grupo de clases que conforma el proyecto de SEED

## 4.1 Paquete de colecciones
representa un grupo de objetos. Esto objetos son conocidos como elementos. Cuando queremos trabajar con un conjunto de elementos, necesitamos un almacén donde poder guardarlos. En Java, se emplea la interfaz genérica Collection para este propósito.

### 4.1.1 Arbol123
Es un árbol n-ario de orden tres, donde cada Nodo tiene uno o dos elementos. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Arbol123.java)

### 4.1.2 ArbolAVL
Un árbol AVL es un Árbol binario de búsqueda al que se le añade una condición de equilibrio. Esta condición es que para todo nodo la altura de sus subárboles izquierdo y derecho pueden diferir a lo sumo en 1. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolAVL.java)

### 4.1.3 ArbolB
Es un árbol de búsqueda que puede estar vacío o aquel cuyos nodos pueden tener varios hijos, existiendo una relación de orden entre ellos. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolB.java)

### 4.1.4 ArbolBinario
Es un conjunto finito de nodos que puede estar vacío o consistir en un nodo raíz y dos árboles binarios disjuntos, llamados subárbol izquierdo y subárbol derecho. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolBinario.java)

### 4.1.5 ArbolBinarioBusqueda
Es un árbol binario con la propiedad de que todos los elementos almacenados en el subárbol izquierdo de cualquier nodo x son menores que el elemento almacenado en x,y. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolBinarioBusqueda.java)

### 4.1.6 ArbolB+
Los árboles B+ almacenan los registros de datos sólo en sus nodos hoja (agrupados en páginas o bloques), y en los nodos interiores y nodo raíz se construye un índice multinivel mediante un árbol B, para esos bloques de datos. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolBMas.java)

### 4.1.7 ArbolEneario
Es una estructura de datos que se caracterizan porque cada nodo tiene un número indeterminado de hijos. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolEneario.java)

### 4.1.8 ArbolHeap
Es una estructura de datos similar a un árbol binario de búsqueda, pero ordenado de distinta forma por prioridades y además se representa siempre como un árbol binario completo representado como un arreglo. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolHeap.java)

### 4.1.9 ArbolRojiNegro
Es un árbol binario de búsqueda en el que cada nodo almacena un bit adicional de información llamado color, el cual puede ser rojo o negro. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolRojiNegro.java)

### 4.1.10 ArbolSplay
Es un árbol binario de búsqueda que ofrecen en tiempo O(log n) en las operaciones de búsqueda, inserción y eliminación de un nodo. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ArbolSplay.java)

### 4.1.11 Arista
Es la relación entre dos vértices de un grafo de un arbol. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Arista.java)

### 4.1.12 Cola
Un conjunto de elementos caracterizadas porque las operaciones de inserción y borrado se realizan sobre extremos opuestos de la estructura. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Cola.java)

### 4.1.13 ColaP
Es un conjunto de nodos entrelazados entre si, estructura que se origina con la creación de un NodoP al que se le denomina se le denomina “ini” (primer nodo de la Cola) y “fin” (ultimo nodo de la estructura definido como atributo. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ColaP.java)

### 4.1.14 Componente
La refrencia de lo que compone las clases, los nodos de los arboles. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Componente.java)

### 4.1.15 Digrafo
Contiene una lista de vértices y aristas, esta estructura se origina con la creación de un vértice y se va consolidando con la creación de elementos fundamentales. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Digrafo.java)

### 4.1.16 ExceptionUFPS
Clase que identifica alguna anomalia en el codigo de seed. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ExceptionUFPS.java)

### 4.1.17 GrafoND
Es un tipo de grafo en el cual las aristas representan relaciones simétricas y no tienen un sentido definido, a diferencia del grafo dirigido. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/GrafoND.java)

### 4.1.18 InformacionDeEntrada
Son objetos que poseen la clave del objeto y el objeto que será insertado como un objeto general en la tabla. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/InformacionDeEntrada.java)

### 4.1.19 IteratorLC
Hace referencia al recorrido del contenedor en la listaC [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/IteratorLC.java)

### 4.1.20 IteratorLCD 
Hace referencia al recorrido del contenedor en la listaCD [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/IteratorLCD.java)

### 4.1.21 IteratorLD
Hace referencia al recorrido del contenedor en la listaD 
[Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/IteratorLD.java)

### 4.1.22IteratorLS
Permite recorrer de una forma más sencilla la estructura. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/IteratorLS.java)

### 4.1.23 ListaC
Es un contenedor secuencial en el que se pueden insertar y borrar elementos independientemente del tamaño del contenedor. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ListaC.java)

### 4.1.24 ListaCD
la estructura Lista Circular Doble para disminuir coste computacional y complejidad del proceso, esta se almacena en un Iterador. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ListaCD.java)

### 4.1.25 ListaD
la estructura Lista Doble para disminuir coste computacional y complejidad del proceso, esta se almacena en un Iterador. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ListaD.java)

### 4.1.26 ListaS
Es un conjunto de nodos entrelazados entre si, cuyo origen es con la creación de un Nodo al que se le denomina “cabeza”, punto de referencia. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/ListaS.java)

### 4.1.27 Nodo
Tiene caracteristicas, posee atributos de un arbol. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Nodo.java)

### 4.1.28 Nodo123
Referencia para la realización de los diferentes procesos para el funcionamiento de la estructura. Los procesos básicos de la estructura son: insertar, eliminar y buscar los datos en la estructura. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Nodo123.java)

### 4.1.29 NodoAVL
Posee las mismas características de “NodoBin”, por esto “NodoAVL” hereda los atributos y operaciones de “NodoBin”. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/NodoAVL.java)

### 4.1.30 NodoBin
Esta conformado por la información que posee y que tiene la particularidad de que solo puede referenciar a dos Nodos un hijo izquierdo denominado izq y un hijo derecho der. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/NodoBin.java)

### 4.1.31 NodoD
Contiene la información del nodo, referencia a un nodo. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/NodoD.java)

### 4.1.32 NodoEneario
Contiene la información del nodo, referencia a un nodo denominado hijo, el cual es el hijo más cercando y otra al nodo hermano más cercano. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/NodoEneario.java)

### 4.1.33 NodoP
Tiene caracteristicas, posee atributos de un arbol, hereda atributos del Nodo. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/NodoP.java) 

### 4.1.34 NodoRN
Posee similitud de características con “NodoBin” hereda los atributos y operaciones de “NodoBin”. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/NodoRN.java)

### 4.1.35 Pagina
Se utiliza para establecer el enlace con otra página que posee datos menores del dato que posee. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Pagina.java)

### 4.1.36 Pila
Es una coleccion de datos que representa una estructura lineal de datos en que se puede agregar o quitar elementos únicamente por uno de los dos extremos. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Pila.java)

### 4.1.37 Secuencia
Son estructuras de datos que, una vez definidas, no pueden ser cambiadas por otras de otra naturaleza, o de diferente tamaño. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Secuencia.java)

### 4.1.38 TablaHash
Es una estructura de datos que asocia llaves o claves con valores. Colección de elementos, cada uno de los cuales tiene una clave y una información asociada. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/TablaHash.java)

### 4.1.39 Vertice
Es un grafo inducido del grafo, formado por todos los vértices adyacentes. [Ver clase](https://gitlab.com/dcarreno/seed/-/blob/master/SEED_UFPS/src/Colecciones/Vertice.java)


## Construido Con
Herramientas utilizadas

- [NetBeans](https://netbeans.apache.org/) NetBeans es un entorno de desarrollo integrado libre, hecho principalmente para el lenguaje de programación Java. Existe además un número importante de módulos para extenderlo. NetBeans IDE es un producto libre y gratuito sin restricciones de uso.
 Java™ Platform, Standard Edition Development Kit (JDK™). JDK es un entorno de desarrollo para crear aplicaciones, applets y componentes utilizando el lenguaje de programación Java.

- [Java™ Platform, Standard Edition Development Kit (JDK™)](https://www.oracle.com/co/java/technologies/javase/javase-jdk8-downloads.html) JDK es un entorno de desarrollo para crear aplicaciones, applets y componentes utilizando el lenguaje de programación Java.El JDK incluye herramientas útiles para desarrollar y probar programas escritos en el lenguaje de programación Java y que se ejecutan en la plataforma Java. En esta version el **JDK** Contiene el JRE.


## 6. Autores y contribuyentes

Desarrollado por los Ingenieros:
  + Marco Antonio Adarme.
  + Wilfred Uriel García.
  + Yulieth Pabón.

## Vista

### WEB SEED_UFPS

![](https://gitlab.com/dcarreno/seed/-/raw/master/seed.png)

