/**
 * ---------------------------------------------------------------------
 * $Id: ListaC.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;
import java.util.Iterator;


/**
 * Implementación de la Clase para el manejo de Lista Circular.
 * @param <T> Tipo de datos a almacenar en la lista.
 * @author Marco Adarme
 * @version 2.0
 */
public class ListaC<T> implements Iterable<T>
{    
    
    ////////////////////////////////////////////////////////////
    // ListaC - Atributos //////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Representa el Nodo cabecera de la Lista
     */
    private Nodo<T> cabeza;  
    
    /**
     * Representa el tamaño de la Lista
     */
    private int tamanio=0;    
    
    
        
    ////////////////////////////////////////////////////////////
    // ListaC - Implementación de Métodos //////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Constructor de la Clase Lista Circular Enlazada, por defecto la cabeza es NULL. <br>
     * <b>post: </b> Se construyo una lista vacía.
     */
    public ListaC() {
        this.cabeza=new Nodo<T> (null,null);
        this.cabeza.setSig(cabeza);     
    }
    
    /**
     * Método que permite insertar un Elemento al Inicio de la Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento al inicio de la Lista. <br>
     * @param dato Información que desea almacenar en la Lista. La información
     * debe ser un Objeto.
     */
    public void insertarAlInicio(T dato){        
        Nodo<T> x=new Nodo<T>(dato, cabeza.getSig());
        cabeza.setSig(x);
        this.tamanio++;
    }

    /**
     * Inserta un Elemento al Final de la Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento al final de la Lista. <br>
     * @param x Representa el dato a Insertar al final de la Lista.
     */	
    public void insertarAlFinal(T x){
        if(this.esVacia())
            this.insertarAlInicio(x);
        else {            
            try {                
                    Nodo<T> ult=this.getPos(this.tamanio-1);
                    if(ult==null)
                        return;
                    ult.setSig(new Nodo<T>(x, this.cabeza));
                    this.tamanio++;                
                }catch(ExceptionUFPS e){                
                    System.err.println(e.getMensaje());                
                }            
            }
    }
    
    /**
     * Método que inserta un Elemento de manera Ordenada desde la cabeza de la Lista. <br>
     * <b>post: </b> Se inserto un nuevo elemento en la posición según el Orden de la Lista.<br>
     * @param info Información que desea almacenar en la Lista de manera Ordenada.
     */
    public void insertarOrdenado(T info){
        if (this.esVacia())
            this.insertarAlInicio(info);
        else{
            Nodo<T> x=this.cabeza;
            Nodo<T> y=x;
            x = x.getSig();
            while(x!=this.cabeza){
                Comparable comparador=(Comparable)info;
                int rta=comparador.compareTo(x.getInfo());
                if(rta<0)
                    break;
                y=x;
                x=x.getSig();
            }
            if(x==cabeza.getSig())
                this.insertarAlInicio(info);
            else{
                y.setSig(new Nodo<T>(info, x));
                this.tamanio++;
                }
            }
     }

    /**
     * Método que permite eliminar un elemento de la lista dada una posición. <br>
     * <b>post: </b> Se elimino el dato en la posición indicada de la lista. <br>
     * @param i Posición del objeto. Debe ir desde 0 hasta el tamaño de la lista menos uno.  <br>
     * @return Retorna el objeto eliminado de la Lista.
     */
    public T eliminar(int i){
        try{
            Nodo<T> x;
            if(i==0){
                x = this.cabeza.getSig();
                this.cabeza.setSig(x.getSig());
                this.tamanio--;
                return (x.getInfo());
            }
            x=this.getPos(i-1);
            if(x==null)
                return (null);
            Nodo<T> y = x.getSig();
            x.setSig(y.getSig());
            this.tamanio--;
            return(y.getInfo());
           }catch(ExceptionUFPS ex) {
                System.err.println(ex.getMensaje());
            }
        return(null);	    	    
    }
    
    /**
     * Método que elimina todos los datos de la Lista Circular. <br>
     * <b>post: </b> Elimina todos los datos que contenga la lista circular. <br>
     */
    public void vaciar(){        
        this.cabeza.setSig(cabeza);
        this.tamanio=0;
    }

    /**
     * Método que retorna el Objeto de la posición i. <br>
     * <b>post: </b> Se retorno el elemento indicado por la posición recibida i.<br>
     * @param i Posición de un elemento de la lista. <br>
     * @return Devuelve el Objeto de la posición especificada, null en caso contrario.
     */	
    public T get(int i){        
        try {           
                Nodo<T> x=this.getPos(i);
                if(x==null)
                    return (null);
                return(x.getInfo());
            }catch (ExceptionUFPS ex) {
                System.err.println(ex.getMensaje());
            }
            return (null);
    }
    
    /**
     * Modifica el elemento que se encuentre en una posición dada. <br>
     * <b>post: </b> Se edito la información del elemento indicado por la posición recibida. <br>
     * @param i Una Posición dentro de la Lista. <br>
     * @param dato Es el nuevo valor que toma el elemento en la lista
     */        
    public void set(int i, T dato){        
        try{
                Nodo<T> t=this.getPos(i);   
                if(t==null)
                    return;
                t.setInfo(dato);
        }catch(ExceptionUFPS e){
                System.err.println(e.getMensaje());
            }
    }
    
    /**
     * Método que devuelve el tamaño de la lista. <br>
     * <b>post: </b> Se retorno el número de elementos existentes en la Lista. <br>
     * @return Un int con el tamaño de la lista
     */    
    public int getTamanio(){
        return (this.tamanio);        
    }

    /**
     * Método que retorna true si la lista esta vacía, false en caso contrario. <br>
     * <b>post: </b> Se retorno true si la lista se encuentra vacía, false si tiene elementos. <br>
     * @return Un boolean con true o false en caso de que la lista se encuentre vacía.
     */
    public boolean esVacia(){        
        return(cabeza==cabeza.getSig() || this.tamanio==0);        
    }
    
    /**
     * Método que busca un elemento en la lista si lo encuentra retorna true, de lo contrario false. <br>
     * <b>post: </b> Se retorno true si se encontró el elementos buscado, false si no fue así. <br>
     * @param info El cual contiene el valor del parámetro a buscar en la lista. <br>
     * @return Un boolean, si es true encontró el dato en la lista y si es false no lo encontró.
     */
    public boolean esta(T info) {        
        return (this.getIndice(info)!=-1);        
    }
    
    /**
     * Método que crea para la lista circular un elemento Iterator.
     * <b>post: </b> Se retorno un Iterator para la Lista.<br>
     * @return Un iterator tipo &#60;T&#62; de la lista.
     */	      
    @Override
    public Iterator<T> iterator(){
        return (new IteratorLC<T>(this.cabeza));
    }
    
      /**
     * Método que permite retornar la información de una Lista en un Vector. <br>
     * @return Un vector de Objetos con la información de cada posición de la Lista.
     */
    public Object[] aVector(){
         if(this.esVacia())
                return (null);
        Object vector[]=new Object[this.getTamanio()];
        Iterator<T> it=this.iterator();
        int i=0;
        while(it.hasNext())
            vector[i++]=it.next();
        return(vector);
    }
    
    /**
     * Método que retorna toda la información de los elementos en un String de la Lista. <br>
     * <b>post: </b> Se retorno la representación en String de la Lista. 
     * El String tiene el formato "e1-&#62;e2-&#62;e3..-&#62;en", donde e1, e2, ..., en son los los elementos de la Lista. <br>
     * @return Un String con los datos de los elementos de la Lista
     */
    @Override
    public String toString(){ 
        if (this.esVacia())
            return ("Lista Vacia");
        String r="";
        for(Nodo<T> x=this.cabeza.getSig();x.getInfo()!=null;x=x.getSig())
            r+=x.getInfo().toString()+"->";
        return(r);
    }
    
    /**
     * Elemento privado de la clase que devuelve al elemento en la posición. <br>
     * <b>post: </b> Se retorno el Nodo que se encuentra en esa posición indicada. <br> 
     * @param i Es de tipo integer y contiene la posición del elemento en la lista. <br>
     * @return Un tipo NodoD<T> con el nodo de la posición.
     */
    @SuppressWarnings("empty-statement")
    private Nodo<T> getPos(int i)throws ExceptionUFPS{
        if(i<0||i>=this.tamanio){
            System.err.println("Error indice no valido en una Lista Circular!");
            return (null);
        }
        Nodo<T> x=cabeza.getSig();
        for( ; i-->0; x=x.getSig());
            return x;
    }
    
    /**
     * Obtiene la posición de un objeto en la Lista. <br>
     * <b>post: </b> Se retorno la posición en la que se encuentra el dato buscado. 
     * @param dato Representa el dato a encontrar dentro de la Lista. <br>
     * @return Un int con la posición del elemento,-1 si el elemento no se encuentra en la Lista.
     */
    public int getIndice(T dato){
        int i=0;        
        for(Nodo<T> x=this.cabeza.getSig();x!=this.cabeza;x=x.getSig()){
            if(x.getInfo().equals(dato))
            return(i);            
            i++;            
        }    	
        return (-1);
    }    
  

}//Fin de la Clase ListaC
