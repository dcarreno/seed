/**
 * ---------------------------------------------------------------------
 * $Id: IteratorLD.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;
import java.util.Iterator;
/**
 * Implementación de clase para el manejo de Iteradores en una Lista Doble Enlazada&#60;T&#62;.
 * @param <T> Tipo de datos a almacenar en la lista
 * @author Marco Adarme
 * @version 2.0
 */
public class IteratorLD<T> implements Iterator<T>
{
    
    ////////////////////////////////////////////////////////////
    // IteratorLD - Atributos //////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Nodo de la Lista a Iterar 
     */
    private NodoD<T> posicion;   
    
    
    
    
    ////////////////////////////////////////////////////////////
    // IteratorLD - Implementación de Métodos //////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Constructor con parámetros de la clase Iterador de una lista doble <br>
     * <b> post: </b> Se crea un iterador de lista doble. <br>
     * @param posición Es un tipo Nodo<T> que posee un nodo de la lista
     */
    IteratorLD(NodoD<T> posicion){            
        this.posicion=posicion;	            
    }

    /**
     * Método que informa si existe otro elemento en la lista para seguir iterando<br>
     * <b> post: </b> Se retorna si existen aún datos por iterar en la Lista. <br>
     * @return Un tipo boolean que informa si existe o no un dato en la lista, desde la posición 
     * actual del cursor.
     */
    @Override
    public boolean hasNext(){            
        return (posicion!=null);            
    }

    /**
     * Método que retorna un dato de la posición actual del cursor del iterador. <br>
     * <b> post: </b> Se ha retornado el dato en la posición actual de la iteración. <br>
     * El cursor queda en la siguiente posición.
     * @return Un tipo T que contiene el dato actual
     */
    @Override
    public T next(){            
        if(!this.hasNext()){                
            System.err.println("Error no hay mas elementos");
        return null;                
        }            
        NodoD<T> actual=posicion;
        posicion=posicion.getSig();            
        return(actual.getInfo());
    }
    
    /**
     *
     */
    @Override
    public void remove(){}

}//Fin de Clase IteratorLD
