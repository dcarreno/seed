/**
 * ---------------------------------------------------------------------
 * $Id: Vertice.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;



/**
 * Implementación de Clase para el manejo de los Vértices o Nodos de un Grafo; <br>
 * Representa la unidad fundamental de la que están formados los grafos. <br>
 * @param <T> Tipos de datos que se almacenan en el vértice. 
 * @author Uriel García
 * @version 1.0
 */
public class Vertice<T> 
{
    
    ////////////////////////////////////////////////////////////
    // Vertice - Atributos /////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Información almacenada en el vértice
     */
    private T info;
    
    /**
     * Listado de Vértices adyacentes al actual
     */
    private ListaCD<Vertice> vecinos;
    
    /**
     * Representa el vértice predecesor en un recorrido
     */
    private Vertice predecesor;
    
    /**
     * Representa un valor que indica si el vértice ha sido visitado;
     * True=visitado, False=No visitado
     */
    private boolean esVisit;
    
    
    
    ///////////////////////////////////////////////////////////
    // Vertice - Implementacion de Metodos ////////////////////
    ///////////////////////////////////////////////////////////

    /**
     * Método constructor que permite crear un nuevo vértice con la información presente en el mismo. <br>
     * <b>post: </b> Se construyo un nuevo vértice con la información ingresada.
     * @param info Objeto de tipo T que representa la información del vértice.
     */
    public Vertice(T info) {
        this.info = info;
        this.vecinos = new ListaCD<Vertice>();
        this.esVisit = false;
        this.predecesor = null;
    }

    /**
     * Obtiene la información del Vertice. <br>
     * <b>post: </b> Se retorno la información del vértice. <br>
     * @return Un objeto tipo T con la información del vértice.
     */
    public T getInfo() {
        return info;
    }
    
    /**
     * Obtiene el Listado de Vértices adyacentes/vecinos al vértice indicado. <br>
     * <b>post: </b> Se retorno el Listado de Vértices vecinos del vértice seleccionado<br>
     * @return Una ListaCD cada uno de los Vértices adyacentes al vértice indicado.
     */
    public ListaCD<Vertice> getVecinos() {
        return vecinos;
    }
    
    /**
     * Obtiene el vértice predecesor en el recorrido al vértice actual. <br>
     * <b>post: </b> Se retorno un vértice con el predecesor del vértice actual. <br>
     * @return Un vértice que representa el predecesor del vértice actual.
     */
    public Vertice getPredecesor() {
        return predecesor;
    }
    
    /**
     * Método que permite evaluar si un vértice ha sido visitado en un recorrido. <br>
     * <b>post: </b> Se retorno un boolean que evalúa si el vértice ha sido visitado. <br>
     * @return Un Objeto de tipo boolean que evalúa si el vértice ha sido visitado
     */
    public boolean getVisit() {
        return esVisit;
    }
    
    /**
     * Método que permite modificar la información presente en el vértice. <br>
     * <b>post: </b> Se edito la información del vértice. <br>
     * @param info Representa la nueva Información del vértice.
     */
    public void setInfo(T info) {
        this.info = info;
    }
    
    /**
     * Método que permite editar el listado de Vértices adyacentes al vértice actual. <br>
     * <b> post: </b> Se edito el listado de Vértices adyacentes al vértice actual. <br>
     * @param vec Representa el nuevo listado de Vértices adyacentes al vértice actual. <br>
     */
    public void setVecinos(ListaCD<Vertice> vec) {
        this.vecinos = vec;
    }
    
    /**
     * Método que permite editar el vértice predecesor al vértice actual en el recorrido. <br>
     * <b> post: </b> Se edito el vértice predecesor el vértice actual en el recorrido. <br>
     * @param predecesor Representa el vértice predecesor al vértice actual. <br>
     */
    public void setPredecesor(Vertice predecesor) {
        this.predecesor = predecesor;
    }
    
    /**
     * Método que permite marcar la visita de un vértice dentro de un recorrido. <br>
     * <b> post: </b> Se marco la visita de un vértice con un true o false. <br>
     * @param esVisit Es de tipo boolean y representa true=visitado, false=No visitado.
     */
    public void setVisit(boolean esVisit) {
        this.esVisit = esVisit;
    }
    
    /**
     * Método que permite evaluar si dos Vertices son iguales, comparando sus informaciones. <br>
     * <b> post: </b> Se retorno un valor booleano para evaluar la igualdad de los Vértices. <br>
     * @param v Representa el segundo vértice en comparación. <br>
     * @return Un objeto de tipo boolean que evalúa la igualdad de la información de los Vértices.
     */
    public boolean equals(Vertice v){
        return (this.info.equals(v.getInfo()));
    }
    
    /**
     * Método que permite insertar un vértice adyacente/vecino al vértice actual. <br>
     * <b> post: </b> Se inserto un vértice vecino al vértice actual. <br>
     * @param v Representa el vértice que será insertado como vecino del vértice actual.
     */
    public void insertarVecino(Vertice v){
        this.vecinos.insertarAlFinal(v);
    }
    
    /**
     * Método que permite eliminar un vértice adyacente al vértice actual. <br>
     * <b> post: </b> Se elimino un vértice vecino del vértice actual. <br>
     * @param v Un objeto de tipo vértice que representa al vértice a eliminar.
     */
    public void eliminarVecino(Vertice v){
        ListaCD<Vertice> l = new ListaCD<Vertice>();
        for(Vertice vert: this.vecinos){
            if(!vert.equals(v))
                l.insertarAlFinal(vert);
        }
        this.vecinos = l;
    }
    
    /**
     * Método que permite evaluar si un vértice es adyacente a otro vértice indicado. <br>
     * <b> post: </b> Se evaluó la adyacencia de dos vértices dentro del Grafo. <br>
     * @param v Un Objeto de tipo vértice del cual se quiere evaluar si es adyacente al actual. <br>
     * @return Un objeto de tipo boolen con el resultado de la operación realizada.
     */
    public boolean esAdyacente(Vertice v){
        for(Vertice vert: this.vecinos)
            if(vert.getInfo().equals(v.getInfo()))
                return (true);
        return (false);
    }
    
    /**
     * Método que permite evaluar si un vértice es un vértice Aislado; Si no posee Aristas. <br>
     * <b> post: </b> Se evaluó si el vértice se encuentra aislado de los demas Vértices <br>
     * @return Un objeto de tipo boolean que almacena un true=si es aislado y false= en caso contrario
     */
    public boolean esAisladoND() {
        for(Vertice v: this.vecinos){
            if(!v.equals(this))
                return (false);
        }
        return (true);
    }
    
    /**
     * Método que permite evaluar si un vértice es un vértice Hoja; Posee una sola Arista asociada. <br>
     * <b> post: </b> Se evaluó si el vértice es una Hoja del Grafo <br>
     * @return Un objeto de tipo boolean que almacena un true=si es hoja y false= en caso contrario
     */
    public boolean esHojaND(){
        int i=0;
        for(Vertice v: this.vecinos){
            if(!v.equals(this)){
                if(++i>1)
                    return (false);
            }                
        }
        return (i==1);
    }
    /**
     * Método que permite conocer el Grado de un vértice para un Grafo No Dirigido. <br>
     * @return El número de vértices adyacentes a determinado vértice.
     */
    public int getGradoND() {
        return (this.vecinos.getTamanio());
    }
    
    /**
     * Método que sirve de Interfaz del Método toString() del Objeto. <br>
     * @return Una cadena de String con la información del vértice.
     */
    @Override
    public String toString(){
        return (this.info.toString());
    }

    
    
}// Fin de la Clase vértice

