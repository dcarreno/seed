/**
 * ---------------------------------------------------------------------
 * $Id: Nodo123.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de clase para el manejo de Nodos de Arboles 123.
 * @param <T> Tipo de datos que se desea almacenar dentro del Nodo.
 * @author Uriel García
 * @version 1.0
 */
public class Nodo123<T>{
    
    ////////////////////////////////////////////////////////////
    // Nodo123 - Atributos /////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Representa la información del Nodo que es de menor valor
     */
    private T infoMen;
    
    /**
     * Representa la información del Nodo que es de mayor valor
     */
    private T infoMay;
    
    /**
     * Representa el Hijo izquierdo del Nodo
     */
    private Nodo123<T> izq;
    
    /**
     * Representa el Hijo medio del Nodo
     */
    private Nodo123<T> med;
    
    /**
     * Representa el Hijo derecho del Nodo
     */
    private Nodo123<T> der;
    
    
    
    ////////////////////////////////////////////////////////////
    // Nodo123 - Implementación de Métodos /////////////////////
    ////////////////////////////////////////////////////////////

    /**
     * Constructor vacío que genera un nuevo Nodo123 con sus datos en NULL. <br>
     * <b>post: </b> Se construyo un Nodo que contiene la información dada y sus hijos en null. <br>
     */
    public Nodo123() {
        this.infoMen = null;
        this.infoMay = null;
        this.izq = null;
        this.med = null;
        this.der = null;
    }

    /**
     * Constructor con parámetros que genera un nuevo Nodo123 con los datos asignados. <br>
     * <b>post: </b> Se construyo un Nodo que contiene la información recibida pero sus hijos en null. <br>
     * @param i1 Representa la información menor del nuevo Nodo123 creado. <br>
     * @param i2 Representa la información mayor del nuevo Nodo123 creado. <br>
     */
    public Nodo123(T i1, T i2) {
        this.infoMen = i1;
        this.infoMay = i2;
        this.izq = null;
        this.med = null;
        this.der = null;
    }

    /**
     * Método que permite obtener la información menor del Nodo123. <br>
     * <b>post: </b> Se retorno la información menor del Nodo123.<br>
     * @return Un objeto de tipo T que contiene la información menor del Nodo.
     */
    public T getInfoMen() {
        return infoMen;
    }

    /**
     * Método que permite cambiar la información menor contenida en el Nodo123. <br>
     * <b>post: </b> Se edito la información menor del Nodo123. <br>
     * @param info1 Es de tipo T y contiene la información menor del Nodo.
     */
    public void setInfoMen(T info1) {
        this.infoMen = info1;
    }

    /**
     * Método que permite obtener la información mayor del Nodo123. <br>
     * <b>post: </b> Se retorno la información mayor del Nodo123.<br>
     * @return Un objeto de tipo T que contiene la información mayor del Nodo.
     */
    public T getInfoMay() {
        return infoMay;
    }

    /**
     * Método que permite cambiar la información mayor contenida en el Nodo123. <br>
     * <b>post: </b> Se edito la información mayor del Nodo123. <br>
     * @param info2 Es de tipo T y contiene la información mayor del Nodo.
     */
    public void setInfoMay(T info2) {
        this.infoMay = info2;
    }

    /**
     * Método que permite obtener el hijo izquierdo del Nodo123. <br>
     * <b>post: </b> Se retorno el hijo izquierdo del Nodo123.<br>
     * @return Un tipo Nodo123&#60;T&#62; que contiene la información del hijo izquierdo del Nodo.
     */
    public Nodo123<T> getIzq() {
        return izq;
    }

    /**
     * Método que permite modificar el hijo izquierdo del Nodo123. <br>
     * <b>post: </b> Se edito el nuevo hijo izquierdo del Nodo123.<br>
     * @param izq Es de tipo Nodo123&#60;T&#62; que contiene el Nodo Izquierdo.
     */
    public void setIzq(Nodo123<T> izq) {
        this.izq = izq;
    }

    /**
     * Método que permite obtener el hijo en el medio del Nodo123. <br>
     * <b>post: </b> Se retorno el hijo en el medio del Nodo123.<br>
     * @return Un tipo Nodo123&#60;T&#62; que contiene la información del hijo en el medio del Nodo.
     */
    public Nodo123<T> getMed() {
        return med;
    }

    /**
     * Método que permite modificar el hijo en el medio del Nodo123. <br>
     * <b>post: </b> Se edito el nuevo hijo en el medio del Nodo123.<br>
     * @param med Es de tipo Nodo123&#60;T&#62; que contiene el Nodo Hijo en el medio.
     */
    public void setMed(Nodo123<T> med) {
        this.med = med;
    }

    /**
     * Método que permite obtener el hijo derecho del Nodo123. <br>
     * <b>post: </b> Se retorno el hijo derecho del Nodo123.<br>
     * @return Un tipo Nodo123&#60;T&#62; que contiene la información del hijo derecho del Nodo.
     */
    public Nodo123<T> getDer() {
        return der;
    }

    /**
     * Método que permite modificar el hijo derecho del Nodo123. <br>
     * <b>post: </b> Se edito el nuevo hijo derecho del Nodo123.<br>
     * @param der Es de tipo Nodo123&#60;T&#62; que contiene el Nodo derecho.
     */
    public void setDer(Nodo123<T> der) {
        this.der = der;
    }
   
}// Fin de la Clase Nodo123
