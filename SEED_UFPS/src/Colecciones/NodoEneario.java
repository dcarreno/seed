/**
 * ---------------------------------------------------------------------
 * $Id: NodoEneario.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de Clase que contiene la información de los Nodos de Árbol Eneario.
 * @param <T> Tipo de datos a almacenar en el Nodo.
 * @author Uriel García
 * @version 1.0
 */

public class  NodoEneario<T>
{
    
    ////////////////////////////////////////////////////////////
    // NodoEneario - Atributos /////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Representa la información del Nodo
     */
    private T info;
    
    /**
     * Referencia su hijo mas a la izquierda
     */
    private NodoEneario<T> hijo;        
    
    /**
     * Referencia a su hermano mas a su derecha
     */
    private NodoEneario<T> hermano;     
    
    

    ////////////////////////////////////////////////////////////
    // NodoEneario - Implementación de Métodos /////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Constructor con el dato de la clase que genera un nuevo NodoEneario. <br>
     * <b>post: </b> Se construyo un Nodo que contiene la información dada y sus hijos en null. <br>
     * @param info Representa la información del nuevo NodoEneario creado.
     */
    public NodoEneario(T info){
        this.info= info ;
        this.hijo=null;
        this.hermano=null;
    }
    
    /**
     * Constructor con todos los parámetros de la Clase para generar un nuevo NodoEneario. <br>
     * <b>post: </b> Se construyo un NodoEneario con la información de su primer Hijo y Hermano. <br>
     * @param info Es de tipo T el cual posee la información del Nodo del Árbol. <br>
     * @param hi Es de tipo NodoEneario&#60;T&#62; y representa el hijo más a la izquierda del Nodo. <br>
     * @param he Es de tipo NodoEneario&#60;T&#62; y representa el hermano más cercano al Nodo por la derecha.
     */
    public NodoEneario(T info, NodoEneario<T> hi, NodoEneario<T> he){
        this.info = info;
        this.hijo = hi;
        this.hermano = he;
    }
    
    /**
     * Método que permite obtener la información del NodoEneario. <br>
     * <b>post: </b> Se retorno la información del NodoEneario.<br>
     * @return Un objeto de tipo T que contiene la información del NodoEneario.
     */
    public T getInfo() {
        return info;
    }

    /**
     * Método que permite cambiar la información contenida en el NodoEneario. <br>
     * <b>post: </b> Se edito la información del NodoEneario. <br>
     * @param info Es de tipo T y contiene la información del Nodo.
     */
    public void setInfo(T info) {
        this.info = info;
    }
    
    /**
     * Método que permite obtener el hijo más a su izquierda del NodoEneario. <br>
     * <b>post: </b> Se retorno el hijo más a la izquierda del NodoEneario.<br>
     * @return Un objeto de NodoEneario&#60;T&#62; que contiene la informacion del hijo del Nodo.
     */
    public NodoEneario<T> getHijo() {
        return hijo;
    }

    /**
     * Método que permite modificar el hijo más a la izquierda del NodoEneario. <br>
     * <b>post: </b> Se edito el nuevo hijo izquierdo del Nodo Eneario.<br>
     * @param hijo Es de tipo NodoEneario&#60;T&#62; que contiene el nuevo hijo más a la izquierda del Nodo.
     */
    public void setHijo(NodoEneario<T> hijo) {
        this.hijo = hijo;
    }

    /**
     * Método que permite obtener el hermano más cercano por la derecha del NodoEneario. <br>
     * <b>post: </b> Se retorno el hermano más cercano por la derecha del NodoEneario.<br>
     * @return Un objeto de NodoEneario&#60;T&#62; que contiene la información del hermano del Nodo.
     */
    public NodoEneario<T> getHermano() {
        return hermano;
    }

    /**
     * Método que permite modificar el hermano más cercano por la derecha del NodoEneario. <br>
     * <b>post: </b> Se edito el nuevo hermano más cercano por la derecha Eneario.<br>
     * @param hermano Es de tipo NodoEneario&#60;T&#62; que contiene el nuevo hermano a la derecha del Nodo.
     */
    public void setHermano(NodoEneario<T> hermano) {
        this.hermano = hermano;
    }


}// Final de la Clase NodoEneario

