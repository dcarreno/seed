/**
 * ---------------------------------------------------------------------
 * $Id: ExceptionUFPS.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de clase para manejo de las excepciones generadas en algunas Estructuras de Datos. <br>
 * @author Marco A. Adarme J.
 */
public class ExceptionUFPS extends Exception
{
    
    /**
     * Constructor con mensaje
     * @param mensaje Mensaje de error
     */
    public ExceptionUFPS(String mensaje)
    {
        super(mensaje);
    }
    
    /**
     * Método que retorna el mensaje de error para la excepción generada. <br>
     * @return Mensaje de error representativo de la Excepción generada.
     */
    public String getMensaje(){
        return (super.getMessage());
    }
}
