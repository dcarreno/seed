/**
 * ---------------------------------------------------------------------
 * $Id: NodoRN.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de Clase contiene la información de los Nodos de un Árbol RojiNegro.
 * @param <T> Tipo de datos a almacenar en el Árbol RojiNegro
 * @author Yulieth Pabon
 * @version 1.0
 */
public class NodoRN<T> extends NodoBin<T>{
    
    ////////////////////////////////////////////////////////////
    // NodoRN - Atributos //////////////////////////////////////
    ////////////////////////////////////////////////////////////
  
    /**
     * Nodo padre del Nodo
     */
    private NodoRN<T> padre;
    
    /**
     * Color del Nodo, solo posee dos valores 0 ó 1
     * Color=0 (El Nodo es Rojo)
     * Color=1 (El Nodo es Negro)
     */
    private int color;   
    
    ////////////////////////////////////////////////////////////
    // NodoRN - Implementación de Métodos //////////////////////
    ////////////////////////////////////////////////////////////

    /**
     * Crea un Árbol rojinegro vacío. <br>
     * <b>post: </b> Se creo un Árbol RojiNegro vacio. <br>
     */
    public NodoRN() {
        super();
        this.padre=null;
        this.color=0;
    }
    
    /**
     * Constructor con parámetros de la clase. <br>
     * <b>post: </b> Se creo un Árbol RojiNegro con los parámetros establecidos. <br>
     * @param x Es de tipo T el cual posee la información del Nodo del Árbol. <br>
     * @param i Es de tipo Nodo&#60;T&#62; el cual posee el Nodo del lado izquierdo. <br>
     * @param d Es de tipo Nodo&#60;T&#62; el cual posee el Nodo del lado derecho. <br>
     * @param p Variable
     * 
     */
    public NodoRN(T x, NodoRN<T> i, NodoRN<T> d, NodoRN<T> p) {
        super(x,i,d);
        this.padre=p;
        this.color=0;
    }


    /**
     * Constructor con parámetros de la clase que genera una hoja del Árbol. <br>     
     * <b>post: </b> Se genero una hoja del Árbol RojiNegro con la información dada. <br>
     * @param x Es de tipo T el cual posee la información del Nodo del Árbol. <br>
     */
    public NodoRN(T x) {
        super(x);
        this.padre=null;
        this.color=0;
    }
    
    /**
     * Método que retorna la información del Nodo. <br>     
     * <b>post: </b> Se retorna la información del Nodo RojiNegro. <br>
     * @return Un tipo T que contiene la información del Nodo.
     */
    @Override
    public T getInfo() {
        return ((T)super.getInfo());
    }
    
    /**
     * Método el cual retorna el padre del Nodo. <br>     
     * <b>post: </b> Se retorna el padre del Nodo RojiNegro. <br>
     * @return un tipo Nodo&#60;T&#62; el cual contiene el padre del Nodo.
     */
    public NodoRN<T> getPadre() {
        return this.padre;
    }
    
    /**
     * Método el cual retorna el Nodo izquierdo. <br>     
     * <b>post: </b> Se retorna el hijo izquierdo del Nodo RojiNegro. <br>
     * @return Un tipo Nodo&#60;T&#62; el cual contiene el Nodo izquierdo. 
     */
    @Override
    public NodoRN<T> getIzq() {
        return ((NodoRN<T>)super.getIzq());
    }
    
    /**
     * Metodo el cual retorna el Nodo derecho. <br>     
     * <b>post: </b> Se retorna el hijo derecho del Nodo RojiNegro. <br>
     * @return Un tipo Nodo &#60;T&#62; el cual contiene el Nodo derecho.
     */
    @Override
    public NodoRN<T> getDer() {
          return ((NodoRN<T>)super.getDer());
    }
    
    /**
     * Método que permite obtener el color del Nodo. <br>     
     * <b>post: </b> Se retorna el color del Nodo RojiNegro. <br>
     * @return El color del Nodo.
     */
    public int getColor() {
        return color;
    }
    
    /**
     * Método que modifica el contenido del Nodo. <br>     
     * <b>post: </b> Se edito la información del Nodo RojiNegro. <br>
     * @param info De tipo T y contiene la información del Nodo. <br>
     */
    @Override
    public void setInfo(T info) {
       super.setInfo(info);
    }
    
    /**
     * Método que modifica el padre del Nodo. <br>     
     * <b>post: </b> Se edito el Nodo padre del Nodo RojiNegro. <br>
     * @param p es de tipo NodoB&#60;T&#62; que contiene el padre del Nodo. <br>
     */
    public void setPadre(NodoRN<T> p) {
        this.padre=p;
    }
    
    /**
     * Método que modifica el Nodo izquierdo. <br>     
     * <b>post: </b> Se edito el hijo izquierdo del Nodo RojiNegro. <br>
     * @param i Es de tipo NodoB&#60;T&#62; que contiene el Nodo izquierdo. <br>
     */
    public void setIzq(NodoRN<T> i) {
       super.setIzq(i);
    }
    
    /**
     * Método que modifica el Nodo derecho. <br>     
     * <b>post: </b> Se edito el hijo derecho del Nodo RojiNegro. <br>
     * @param d Es de tipo NodoN&#60;T&#62; que contiene el Nodo derecho. <br>
     */
    public void setDer(NodoRN<T> d) {
        super.setDer(d);
    }

    /**
     * Método que permite modificar el color del Nodo. <br>     
     * <b>post: </b> Se edito el color del Nodo RojiNegro. <br>
     * @param color Es de tipo entero ...0 es rojo y 1 es negro. <br>
     */
    public void setColor(int color){
        this.color = color;
    }

    /**
     * Método que permite devolver la información de un Nodo con una cadena de caracteres. <br>
     * @return Un objeto de tipo String con la información del Nodo concatenada.
     */
    @Override
    public String toString() {
        return "info"+super.getInfo()+ " padre=" + padre + ", color=" + color + '}';
    }
    
}//Fin de la Clase NodoRN


