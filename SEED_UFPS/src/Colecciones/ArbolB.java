/**
 * ---------------------------------------------------------------------
 * $Id: ArbolB.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;
import java.util.Iterator;

/**
 * Implementación de Clase para el manejo de un ArbolB.
 * @param <T> Tipo de datos a almacenar en el ArbolB.
 * @author Yulieth Pabon
 * @version 1.0
 */

public class ArbolB <T>
{
    
    ////////////////////////////////////////////////////////////
    // ArbolB - Atributos //////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * orden del árbol
     */
    private int n; 
    
    /**
     *numero maximo de llaves
     */ 
    private int m; 
    
    /**
     * numero máximo de apuntadores
     */
    private int m1;
    
    /**
     * raíz del árbol
     */
    private Pagina raiz;
    
    
   
    ////////////////////////////////////////////////////////////
    // ArbolB - Implementación de Métodos //////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Crea un Árbol B vació con orden por defecto de 2. <br>
     * <b>post: </b> Se creo un Árbol B vacío por defecto. <br>
     */
    public ArbolB(){
        this.raiz = null;
        this.n=2;
         this.m=n*2;
        this.m1= (this.m)+1;
    }
    
    /**
     * Crea un Árbol B vació con orden especifico. <br>
     * <b>post: </b> Se creo un Árbol B vació con orden especifico. <br>
     * @param n De tipo entero que indica el numero el orden del Árbol B. <br>
     */
    public ArbolB(int n){
        if (n<=0){
             System.err.println("Tamano del orden del arbol no es válido");
             return;
         }
        this.raiz = null;
        this.n=n;
        this.m=n*2;
        this.m1= (this.m)+1;
    }

    /**
     * Método que permite conocer la raíz del Árbol B. <br>
     * <b>post: </b> Se obtuvo la raíz del Árbol B. <br>
     * @return Un objeto de tipo NodoB&#60;T&#62; que es la raíz del Árbol B. <br>
     */
    public Pagina getRaiz() {
        return raiz;
    }

    /**
     * Método que permite modificar la raíz del Árbol B. <br>
     * <b>post: </b> Se modifico la raíz del Árbol B. <br>
     * @param raiz Objeto de tipo NodoB&#60;T&#62; que representa la nueva raíz del Árbol. <br>
     */
    protected void setRaiz(Pagina raiz) {
        this.raiz = raiz;
    }
    
    /**
     * Método que permite saber el número de orden del Árbol. <br>
     * <b>post: </b> Se retorno el número de orden del Árbol. <br>
     * @return El número de orden del Árbol.
     */
     public int getN() {
        return n;
    }

    /**
     * Método que permite saber el numero máximo de elementos que puede almacenar el Árbol por página. <br>
     * <b>post: </b> Se retorno el numero máximo de elementos que puede almacenar el Árbol por página. <br>
     * @return El numero máximo de elementos que puede almacenar el Árbol por página.
     */
    public int getM() {
        return m;
    }

    /**
     * Método que permite saber el numero máximo de apuntadores que puede almacenar el Árbol por página. <br>
     * <b>post: </b> Se retorno el numero máximo de apuntadores que puede almacenar el Árbol por página. <br>
     * @return El número máximo de apuntadores que puede almacenar el Árbol por página.
     */
    public int getM1() {
        return m1;
    }

    /**
     * Método que permite modificar el número de orden del Árbol. <br>
     * <b>post: </b> Se modifico el número de orden del Árbol. <br>
     * @param n Nuevo orden del Árbol
     */
    public void setN(int n) {
        this.n = n;
    }

    /**
     * Método que permite insertar un nuevo dato en el Árbol B. <br>
     * <b>post: </b> Se inserto un nuevo dato al Árbol B. <br>
     * @param x Dato a insertar en el Árbol.  <br>
     * @return La página donde se insertó x, o null sino se insertó correctamente
     */
     public boolean insertar(T x){
        //pila para guardar el camino desde la raíz hasta la página donde se inserta x
        Pila<Pagina> pila= new Pila<Pagina>();
        //Para trabajar subir y subir1 por referencia se usa si la página se rompe
        T []subir= (T[]) new Object[1];
        T []subir1= (T[]) new Object[1];
        //variables auxiliares 
        int posicion=0, i=0, terminar, separar;
        Pagina p = null, nuevo=null, nuevo1;
        if(this.raiz==null){ 
            this.raiz=this.crearPagina(x);
        }
        else{
            posicion= buscar(this.raiz,x, pila);
            if(posicion==-1)
                return (false);
            else{
                terminar=separar=0;
                while((!pila.esVacia()) && (terminar==0)){
                    p= pila.desapilar();
                    if(p.getCont()==this.m){
                        if(separar==0){
                            nuevo=romper(p,null,x,subir);
                            separar=1;
                        }
                    else{
                        nuevo1=romper(p,nuevo,subir[0],subir1);
                        subir[0]=subir1[0];
                        nuevo=nuevo1;
                    }
                  }
                    else{
                        if(separar==1){
                            separar=0;
                            i=donde(p,subir[0]);
                            i=insertar(p, subir[0],i);
                            cderechaApunt(p,i+1);
                            p.getApuntadores()[i+1]= nuevo;

                        }
                        else{   
                            posicion=insertar(p,x,posicion);
                        }
                        terminar=1;
                    }
                }
                if((separar==1)&&(terminar==0)){
                    this.setRaiz(this.crearPagina(subir[0]));
                    this.raiz.getApuntadores()[0]=p;
                    this.raiz.getApuntadores()[1]=nuevo;
                }
            }
        }
        return (true);
    }
     
    /**
     *  Método que permite insertar una página en el Árbol B. <br>
     * <b>post: </b> Se inserto un nuevo dato al Árbol B. <br>
     * @param p Página en donde inserta el dato. <br>
     * @param x Apuntador a una página del Árbol. <br>
     * @param i Indicando la posición donde se desea insertar el dato. <br>
     * @return La posición donde se insertó el dato.
     */
      private int insertar(Pagina p, T x, int i){
          int j ;
          if(p.getCont()!=0){
           int compara=((Comparable)p.getInfo()[i]).compareTo(x);
            if(compara<0)
                i++;
            else{
                j=p.getCont()-1;
                while(j>=i){
                     p.getInfo()[j+1] = p.getInfo()[j];
                     j=j-1;
                }
            }                
         }
          p.setCont(p.getCont()+1);
          p.getInfo()[i]= x;          
          return (i);
      }
     
    /**
     * Método que permite eliminar un dato del Árbol B. <br>
     * <b>post: </b> Se elimino el dato del Árbol B. <br>
     * @param x dato que se desea eliminar. <br>
     * @return El dato que se eliminó o null en caso de no haberse eliminado el dato.
     */
    public boolean eliminar(T x){
        int  posicion,i,k;
        Pagina p,q = null,r,t;  
        Pila<Componente> pila=new Pila<Componente>();
        Componente objeto=new Componente();
        posicion=esta(this.raiz,x,pila);
        if(posicion==-1)
           return (false);//la llave no existe en el árbol
        else{
            objeto= pila.desapilar();
            p=objeto.getP();
            i=objeto.getV();
            if(!this.esHoja(p)){
                t=p;
                k=i;
                pila.apilar(new Componente(p, i+1));
                p=p.getApuntadores()[i+1];
                while(p!=null){
                    pila.apilar(new Componente (p,0));
                    p=p.getApuntadores()[0];
                }
                objeto= pila.desapilar();
                p=objeto.getP();// p página que contiene el dato a eliminar
                i=objeto.getV();
                t.getInfo()[k]=p.getInfo()[0];
                x=(T)p.getInfo()[0];
                posicion=0;
            }
            if(p.getCont()>this.n)
                this.retirar(p,posicion);
            else{
                if(!pila.esVacia()){
                    objeto= pila.desapilar();
                    q=objeto.getP();//q página que contiene el puntero de la página donde está el dato
                    i=objeto.getV();
                    if(i<q.getCont()){
                        r=q.getApuntadores()[i+1];
                        if(r.getCont()>this.n){
                            this.retirar(p, posicion);
                            this.cambio(p, q, r, i, x);
                        }
                        else{
                            if(i!=0){
                                r=q.getApuntadores()[i-1];
                                if(r.getCont()>this.n){
                                    this.retirar(p, posicion);
                                    this.cambio(p, q, r, (i-1), x);
                                }
                                else{ 
                                    this.unir(q, r, p, (i-1), pila, x, posicion);
                                }
                            }
                            else{ 
                            this.unir(q, r, p, i, pila, x, posicion);
                            }
                        }
                    }
                    else{ 
                         r=q.getApuntadores()[i-1];
                         if(r.getCont()>this.n){
                              this.retirar(p, posicion);
                              this.cambio(p, q, r, (i-1), x);
                    }
                         else
                             this.unir(q, r, p, (i-1), pila, x, posicion);
                  }
                }
                else{ 
                    this.retirar(p, posicion);
                    if(p.getCont()==0){
                        this.setRaiz(null);
                    }
                }
            }
        }
        return true;
    }
    
    /**
     * Método que permite retirar un dato del Árbol indicada. <br>
     * <b>post: </b> Se elimino el dato del Árbol B. <br>
     * @param p Pagina de la que se desea retirar el dato.  <br>
     * @param i Posición del dato en el Árbol. <br>
     */
    private void retirar(Pagina p, int i){
        while(i<p.getCont()-1){
            p.getInfo()[i]= p.getInfo()[i+1];
            i++;
        }
        p.setCont(p.getCont()-1);
    }
   
    /**
     * Método que permite crear físicamente una página en memoria. <br>
     * <b>post: </b> Se creo físicamente una página en memoria. <br>
     * @param x Información que tendrá la nueva hoja. <br>
     * @return La página creada.
     */
    private Pagina crearPagina(T x){
        Pagina p= new Pagina(n);
        inicializar(p);
        p.setCont(1);
        p.getInfo()[0]= x;
        return (p);
    }
    
     /**
      * Método que permite inicializar una página. <br>
     * <b>post: </b> Se inicializo una página en el arbol. <br>
      * @param p Apuntador a una página aun si inicializar. <br>
      */
    private void inicializar(Pagina p){
        int i =0;
        p.setCont(0);
        while(i < this.m1)
            p.getApuntadores()[i++] = null;
    }
    
    /**
     * Método que permite evaluar la existencia un dato dentro del Árbol B. <br>
     * <b>post: </b> Se evaluó la existencia de un dato dentro del Árbol. <br>
     * @param dato Representa el dato que se quiere localizar dentro del Árbol Eneario. <br>
     * @return Un objeto de tipo boolean que contiene un true si ubico el dato y false en caso contrario.
     */
    public boolean esta(T dato){
        Pila pi=new Pila();
        return(this.esta(this.raiz, dato, pi)!=(-1));
    }
    /**
     * Método| que permite determinar si un elemento se encuentra en el Árbol. <br>
     * <b>post: </b> Se evaluó la existencia de un dato dentro del Árbol. <br>
     * @param p Página que contiene las páginas de la búsqueda. <br>
     * @param x Posición del apuntador de las páginas. <br>
     * @param pi Estructura que almacenara el camino de la búsqueda de X. <br>
     * @return Posición de X dentro de la página donde se encontró, de no ser así retorna -1.
     */
       private int esta(Pagina p, T x,Pila<Componente> pila){
            int i=0;
            boolean encontro=false;
            int posicion=-1;
            while((p!=null) && !encontro){
                i=0;
               int compara=((Comparable)p.getInfo()[i]).compareTo(x);
                while((compara<0) && (i<(p.getCont()-1))){
                    i++;
                    compara=((Comparable)p.getInfo()[i]).compareTo(x);
                }
                if((compara>0)){
                    pila.apilar(new Componente(p,i));
                    p=p.getApuntadores()[i];
                }
                else 
                    if((compara<0)){
                        pila.apilar(new Componente(p,i+1));
                        p=p.getApuntadores()[i+1];
                    }
                    else {
                        pila.apilar(new Componente(p,i));
                        encontro=true;
                    } 
                }
                if(encontro){
                    posicion=i;
                }
            return (posicion);
    }
            
    /**
     * Método que permite encontrar un elemento se encuentra en el Árbol. <br>
     * <b>post: </b> Se realizo una búsqueda de X en el Árbol. <br>
     * @param p Página que contiene las páginas de la búsqueda. <br>
     * @param x Posición del apuntador de las páginas. <br>
     * @param pi Estructura que conteniente el camino de la búsqueda de X. <br>
     * @return Posición de X dentro de la página donde se encontró, de no ser así retorna -1
     */
        private int buscar(Pagina p,T x, Pila pila){
            int i=-1,posicion;
            boolean encontro= false;
            posicion=-1;
            while((p!=null)&&!(encontro)){
                pila.apilar(p);
                i=0;
                int compara=((Comparable)p.getInfo()[i]).compareTo(x);
                while((compara<0)&&(i<(p.getCont()-1))){
                    i++;
                    compara=((Comparable)p.getInfo()[i]).compareTo(x);
                }
                if((compara>0))
                    p=p.getApuntadores()[i];
                else{
                    if(compara<0)
                        p=p.getApuntadores()[i+1];
                    else
                        encontro=true;
                }
            }
            if(!encontro)
                posicion=i;
            return (posicion);
        }
   
    /**
     * Método que indica el lugar físico donde se debe insertar el dato x. <br>
     * <b>post: </b> Se indica la posición de un dato en una página determinada del Árbol. <br>
     * @param p Página posible a insertar. <br>
     * @param x Dato a insertar. <br>
     * @return Índice que indica el lugar físico donde se debe insertar el dato.
     */
    protected int donde(Pagina p, T x){
        int i;
        i=0;
        int compara=((Comparable)p.getInfo()[i]).compareTo(x);
        while((compara<0) && (i<(p.getCont()-1))){
            i++;
            compara=((Comparable)p.getInfo()[i]).compareTo(x);
        }
        return i;
    }
    
    /**
     * Método que permite romper una página del Árbol en dos, para mantener su orden. <br>
     * <b>post: </b> Se dividió una página del Árbol en dos, para mantener sus características. <br>
     * @param p Página a romper. <br>
     * @param t Apuntador de que ser null la página es una hoja del Árbol. <br>
     * @param x Dato a insertar en el Árbol. <br>
     * @param Subir contenedor de la página a ascender en el Árbol. <br>
     * @return La nueva página del Árbol.
     */
     private Pagina romper(Pagina p, Pagina t, T x, T[] subir){
        T[] a= (T[]) new Object[m1];
        int i = 0;
        boolean s= false;
        Pagina []b= new Pagina[this.m1+1];
        while(i<this.m && !s){
            int compara=((Comparable)p.getInfo()[i]).compareTo(x);
            if(compara<0){ //<-- X es mayor que el dato del árbol
                a[i]= (T)p.getInfo()[i];
                b[i]=p.getApuntadores()[i];
                p.getApuntadores()[i++]=null;
            }
            else
                s=true;
        }
        a[i]=x;
        b[i]=p.getApuntadores()[i];
        p.getApuntadores()[i]=null;
        b[++i]=t;
        while((i <= this.m)){
            a[i]=(T) p.getInfo()[i-1];
            b[i+1]=p.getApuntadores()[i];
            p.getApuntadores()[i++]=null;
        }
        Pagina q= new Pagina(this.n);
        inicializar(q);
        p.setCont(n);q.setCont(n);
        i=0;
        while(i < this.n){
            p.getInfo()[i]= a[i];
            p.getApuntadores()[i] = b[i];
            q.getInfo()[i]=a[i+n+1];
            q.getApuntadores()[i]=b[i+n+1];
            i++;
        }
        p.getApuntadores()[n]=b[n];
        q.getApuntadores()[n]=b[m1];
        subir[0]= a[n];
        return (q);
    }
    
     /**
     * Método que permite correr los apuntadores hacia la izquierda. <br>
     * <b>post: </b> Se corrieron los apuntadores de la página hacia la izquierda. <br>
     * @param p Apuntador de la página a recorrer. <br>
     * @param i Variable entera que indica en donde se comienza a correr. <br>
     * @param j Variable 
     */
     protected void cizquierda_apunt(Pagina p, int i, int j){
        while(i<j){
            p.getApuntadores()[i]=p.getApuntadores()[i+1];
            i++;
        }
        p.getApuntadores()[i]=null;
    }
     
    /**
     * Método que permite correr los apuntadores hacia la derecha. <br>
     * <b>post: </b> Se corrieron los apuntadores de la página hacia la derecha. <br>
     * @param p Apuntador de la página a recorrer. <br>
     * @param i Variable entera que indica en donde se comienza a correr. <br>
     */
    protected void cderechaApunt(Pagina p, int i){
        int j;
        j= p.getCont();
        while (j>i){
            p.getApuntadores()[j] = p.getApuntadores()[j-1];
            j--;
        }
    }
    
    /**
     *  Método que permite realizar las modificaciones al eliminar un dato, para que el Árbol conserve sus características. <br>
     * <b>post: </b> Se cambiaron las paginas, para realizar la eliminación de un dato. <br>
     * @param p Apuntador de la página donde se encuentra el dato a eliminar. <br>
     * @param q Página que contiene el apuntador a la página donde se encuentra el dato a eliminar. <br>
     * @param r Apuntador a página hermana del apuntador de la página donde se encuentra el dato a eliminar. <br>
     * @param i Posición en la que se encuentra el dato en la página. <br>
     * @param x Dato que se desea eliminar. <br>
     */
    protected void cambio(Pagina p, Pagina q, Pagina r, int i, T x){
        int k;
        T t ;
        int compara=((Comparable)r.getInfo()[r.getCont()-1 ]).compareTo(x);
        if(compara<0){
            t = (T)q.getInfo()[i];
            retirar(q,i);
            k=0;
            k=insertar(p,t,k);
            t= (T)r.getInfo()[r.getCont()-1];
            retirar(r, r.getCont()-1);
            k=i;
            if(k==-1)
                k=0;
            k=insertar(q,t,k);
        }
        else{
            t= (T)q.getInfo()[i];
            retirar(q,i);
            k=p.getCont()-1;
            if(k==-1)
                k=0;
            k=insertar(p,t,k);
            t= (T)r.getInfo()[0];
            retirar(r,0);
            k=i;
            if(q.getCont()!=0)
                if(k > q.getCont()-1)
                    k=q.getCont()-1;
            k=insertar(q,t,k);
        }
    }
   
    /**
     * Método que permite unir dos páginas, para conservar las características del Árbol B. <br>
     * <b>post: </b> Se unió dos páginas para mantener las características de Árbol B. <br>
     * @param q Página que contiene apuntadores hacia las paginas r y p. <br> 
     * @param r Página que tendrá la información de la paginas. <br>
     * @param p Página que contiene la llave que se desea eliminar
     * @param i Posición en r donde se debe incorporar la llave que se retira de q
     * @param pi Almacena el camino entre q y la raíz del Árbol
     * @param x Dato que se desea eliminar
     * @param posición Posición donde se encuentra la llave que se retirada de la pagina
     */
    private void unir (Pagina q,Pagina r, Pagina p, int i, Pila<Componente> pila, T x, int posicion){
        int terminar=0,j = 0,k;
        Pagina t = null;
        Componente objeto= new Componente();
        retirar(p,posicion);
        int compara=((Comparable)r.getInfo()[0]).compareTo(x);
        if(compara>0){
            t=p;
            p=r;
            r=t;
        }
        while(terminar==0){
     /*1*/  if((r.getCont()<this.n) && (p.getCont()>this.n)){
                    cambio(r,q,p,i,x);
                    r.getApuntadores()[r.getCont()]=p.getApuntadores()[0];
                    this.cizquierda_apunt(p, 0, p.getCont()+1);
                    terminar=1;
                }
     /*2*/ else
                    if((p.getCont()<this.n) && (r.getCont()>this.n)){
                        cambio(p,q,r,i,x);
                        this.cderechaApunt(p, 0);
                        p.getApuntadores()[0]=r.getApuntadores()[r.getCont()+1];
                        r.getApuntadores()[r.getCont()+1]=null;
                        terminar=1;
                    }
                    else  {
                        j=r.getCont();
                        r.getInfo()[j++]=q.getInfo()[i]; 
                        k=0;
                        while(k<=p.getCont()-1){
                            r.getInfo()[j++]=(T)p.getInfo()[k++];
                        }
                        r.setCont(j);
                        this.retirar(q, i);
                        k=0;
                        j=this.m-p.getCont();
                        while(p.getApuntadores()[k]!=null){
                            r.getApuntadores()[j++]=p.getApuntadores()[k++];
                        }
                        p=null;
         /*3*/      if(q.getCont()==0){
                            q.getApuntadores()[i+1]=null;
                /*4*/       if(pila.esVacia()){
                                    q=null;
                            }
                    }
                    else 
                        this.cizquierda_apunt(q, i+1, q.getCont()+1);
        /*5*/  if(q!=null){
        /*6*/       if(q.getCont()>=this.n){
                            terminar=1;
                        }
                        else{
                            t=q;
       /*7*/            if(!pila.esVacia()){
                                objeto= pila.desapilar();
                                q=objeto.getP();
                                i=objeto.getV();
                                compara=((Comparable)q.getInfo()[0]).compareTo(x);
                                if(compara<=0){
                                    p=t;
                                    r=q.getApuntadores()[i-1];
                                    i=i-1;
                                }
                                else{
                                        r=t;
                                        p=q.getApuntadores()[i+1];
                                }
                        }
                        else 
                            terminar=1;
                        }
                 }
                else{
                    terminar=1;
                    this.setRaiz(r);
                }
            }
        }
   }   
     
    /**
     * Método que retorna un iterador con las hojas del Árbol B. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol B. <br>
     * @return Un objeto Iterador con las hojas del Árbol B.
     */
    public Iterator<T> getHojas(){
        ListaCD<T> l=new ListaCD();
        getHojas(this.raiz, l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un iterador con las hojas del Árbol B. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol B.<br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol. <br>
     */
    private void getHojas(Pagina<T> r, ListaCD<T> l){
        if(r==null)
            return;
        if(this.esHoja(r)){
            for(int j=0; j<r.getCont();j++)
                l.insertarAlFinal(r.getInfo()[j]);
        } 
        for(int i=0; i<r.getCont()+1;i++){
             getHojas(r.getApuntadores()[i],l);
        }         
    }
    
    /**
     * Método que retorna un iterador con las hojas del Árbol B. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol B. <br>
     * @return Un objeto Iterador con las hojas del Árbol B.
     */
    public int contarHojas(){
        return contarHojas(this.raiz);
    }

    /**
     * Método de tipo privado que retorna un iterador con las hojas del Árbol B. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol B.<br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol. <br>
     */
    private int contarHojas(Pagina<T> r){
        if(r==null)
            return 0;
        int cont=0;
        if(this.esHoja(r))
            cont++;
        for(int i=0; i<r.getCont()+1;i++){
             cont+=contarHojas(r.getApuntadores()[i]);
         }      
        return (cont);
    }
    
    
    
    /**
     * Método que permite determinar si una página es hoja. <br>
     * <b>post: </b> Se determino si una página es hoja. <br>
     * @param p Página a determinar si es hoja. <br>
     * @return True en caso de que la pagina sea hoja.
     */
    protected  boolean esHoja(Pagina p){
        int j=0;
        while ((p.getApuntadores()[j]==null) && (j < (p.getCont()-1)))
            j++;
        return(p.getApuntadores()[j]==null);
    }
    
    /**
     * Método que permite conocer si un Árbol B se encuentra vació. <br>
     * <b>post: </b> Se evaluó si el Árbol B se encuentra o no vació. <br>
     * @return Un boolean, true si el Árbol se encuentra vació, false en caso contrario
     */
    public boolean esVacio(){
        return (this.raiz==null);
    }
    

     /**
     * Método que permite obtener el peso del Árbol B. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol B.<br>
     * @return Un entero con la cantidad de elementos del Árbol B.
     */
    public int getPeso(){
        if(this.esVacio())
             return (0);
        return(getPeso(this.getRaiz()));
    }

    /**
     * Método de tipo privado que permite conocer el número de elemento del Árbol B. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return El número de elementos que contiene el Árbol B.
     */
    private int getPeso(Pagina<T> r){
        if(r==null)
            return 0;
        int cont=0;
        cont+= r.getCont();
        for(int i=0; i<r.getCont()+1;i++){
             cont+=getPeso(r.getApuntadores()[i]);
         }      
        return (cont);
    }
    
     /**
     * Método que permite obtener la altura del Árbol B. <br>
     * <b>post: </b> Se retorno la altura del Árbol B.<br>
     * @return Un entero con la altura del Árbol B.
     */
    public int getAltura(){
        return(getAltura(this.getRaiz()));
    }

    /**
     * Método de tipo privado que permite conocer la altura del Árbol B. <br>
     * <b>post: </b> Se retorno la altura del Árbol B. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return Un entero con la altura del Árbol B.
     */
    private int getAltura(Pagina<T> r){
       int alt=0;
          while(r!=null){
              alt++;
              r=r.getApuntadores()[0];
          }
          return (alt);
    }
    
    /**
     * Método que permite imprimir el Árbol B. <br>
     * <b>post: </b> Se imprimió los datos que contiene el Árbol. <br>
     * @return msg
     */
    public String imprime(){
        String msg="";
        return (this.imprime(this.raiz,msg));
    }
    
     /**
     * Método que permite imprimir el Árbol B. <br>
     * <b>post: </b> Se imprimió los datos que contiene el Árbol. <br>
     * @param r Raíz o pagina no hoja del Árbol B
     */
     private String  imprime(Pagina r,String msg){
        int i=0;
       while(i<=r.getCont()){
            msg+=r.toString()+"  pagina = "+i+"   ES ="+r.getApuntadores()[i].toString()+"\n" ;
            if(!this.esHoja(r.getApuntadores()[i]))
                 msg+=this.imprime(r.getApuntadores()[i],msg);
            i++;
       }
       return msg;
    }

    /**
     * Método que permite clonar la información de un Árbol B. <br>
     * @return Un nuevo Árbol B con la información del Árbol duplicada. <br>
     */
    public ArbolB<T> clonar() {
        ArbolB<T> clon = new ArbolB<T>(this.getN());
        if(raiz==null)
            return (clon);
        clon.setRaiz(clonar(this.raiz));
        return (clon);
    }

    private Pagina clonar(Pagina r) {
        if(r==null)
            return (null);
        else
        {   
            T info[] = (T[]) new Object[r.getM()];
            for(int i=0; i<r.getCont();i++){
                info[i] = (T) r.getInfo()[i];
            }
            Pagina aux = new Pagina(r.getN());
            aux.setInfo(info);
            aux.setCont(r.getCont());
            for(int i=0; i<aux.getCont()+1;i++){
                aux.getApuntadores()[i] = clonar(r.getApuntadores()[i]);
            }
            return (aux);
        }
    }
    
    /**
     *  Método que retorna un iterador con el recorrido preOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el Árbol. <br>
     * @return Un iterador en preorden para el Árbol AVL.
     */
    public Iterator<T> preOrden(){
        ListaCD<T> l=new ListaCD();
        preOrden(this.raiz, l);
        return (l.iterator());
    }
    
    private void preOrden(Pagina<T> r, ListaCD<T> l){
        if(r==null)
            return;
        for(int j=0; j<r.getCont();j++)
            l.insertarAlFinal(r.getInfo()[j]);
        for(int i=0; i<r.getCont()+1;i++){
             preOrden(r.getApuntadores()[i],l);
        }         
    }
    
    /**
     * Método que retorna un iterador con el recorrido in Orden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el Árbol. <br>
     * @return Un iterador en inOrden para el Árbol AVL. <br>
     */
    public Iterator<T> inOrden(){
        ListaCD<T> l=new ListaCD();
        inOrden(this.raiz, l);
        return (l.iterator());
    }
    
    private void inOrden(Pagina<T> r, ListaCD<T> l){
        if(r==null)
            return;
        inOrden(r.getApuntadores()[0],l);
        for(int j=0; j<r.getCont();j++)
            l.insertarAlFinal(r.getInfo()[j]);
        for(int i=1; i<r.getCont()+1;i++){
             inOrden(r.getApuntadores()[i],l);
        }         
    }
    
   /**
     * Método que retorna un iterador con el recorrido postOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador preOrden para el Árbol. <br>
     * @return Un iterador en postOrden para el Árbol AVL. <br>
     */
    public Iterator<T> postOrden(){
        ListaCD<T> l=new ListaCD();
        postOrden(this.raiz, l);
        return (l.iterator());
    }
    
    private void postOrden(Pagina<T> r, ListaCD<T> l){
        if(r==null)
            return;
        for(int i=0; i<r.getCont()+1;i++){
             postOrden(r.getApuntadores()[i],l);
        }
        for(int j=0; j<r.getCont();j++)
            l.insertarAlFinal(r.getInfo()[j]);
    }
    
    /**
     * Método que permite retornar un iterador con el recorrido por niveles del Árbol. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Árbol AVL. <br>
     * @return Un iterador con el recorrido por niveles del Árbol AVL.
     */
    public Iterator<T> impNiveles(){
        ListaCD<T> l=new ListaCD<T>();
        if(!this.esVacio()){
            Cola<Pagina<T>> c=new Cola<Pagina<T>>();
            c.enColar(this.getRaiz());
            Pagina<T> x;
                while(!c.esVacia()){
                    x=c.deColar();
                    if(x!=null){
                        for(int i=0; i<x.getCont();i++)
                            l.insertarAlFinal(x.getInfo()[i]);
                        for(int j=0;j<x.getCont()+1;j++)
                            c.enColar(x.getApuntadores()[j]);  
                    }                 
                }
        }
        return (l.iterator());
    }
    
    /**
     * Método que permite podar las paginas Hoja de un Árbol B.
     */
    public void podar() {
        if(this.esHoja(raiz))
            this.setRaiz(null);
        podar(this.raiz);
    }

    private void podar(Pagina<T> r) {
        if (r==null)
            return;
        for(int i=0; i<r.getCont()+1; i++){
            Pagina apunt = r.getApuntadores()[i];
            if(this.esHoja(apunt))
                r.getApuntadores()[i]=null;
        }
        for(int j=0; j<r.getCont()+1;j++){
            if(r.getApuntadores()[j]!=null){
                podar(r.getApuntadores()[j]);
            }
        }
        
    }
    
    
}//Fin de la Clase Árbol B

