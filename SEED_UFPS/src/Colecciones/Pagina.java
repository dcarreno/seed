/**
 * ---------------------------------------------------------------------
 * $Id: Pagina.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de Clase para el manejo de los Nodos (denominados PAGINAS) de un ArbolB.
 * @param <T> Tipo de datos a almacenar en el ArbolB.
 * @author Yulieth Pabon
 * @version 1.0
 */
public class Pagina <T>{

    ////////////////////////////////////////////////////////////
    // Pagina - Atributos /////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * orden del arbol
     */
    private int n; 
    
    /**
     * Número maximo de llaves
     */
    private int m; 
    
    /**
     * Número máximo de apuntadores
     */
    private int m1;
    
    /**
     * Número de llaves de pagina
     */
    private int cont;  
    
    /**
     * llaves clasificadas ascendentemente
     */
    private T[] info; 
    
    /**
     * direcciones de los hijos de la pagina 
     */
    private Pagina[] apuntadores; 

    
    
    ////////////////////////////////////////////////////////////
    // Pagina - Implementación de Métodos /////////////////////
    ////////////////////////////////////////////////////////////
      
    /**
     * Crea una página con orden especifico. <br>
     * <b>post: </b> Se creo una página con orden especifico. <br>
     * @param n De tipo entero que indica el numero el orden de la página. <br>
     */
    public Pagina(int n){
        this.n = n;
        this.m = n*2;
        this.m1 = this.m+1;
        this.info= (T[]) new Object[m];
        for(int i=0; i<info.length;i++)
            info[i]=null;
        this.apuntadores = new Pagina[this.m1];
        for(int i=0; i<apuntadores.length;i++)
            apuntadores[i]=null;
    }
    
  
    
    /**
     * Método que permite saber el número de orden de la página. <br>
     * <b>post: </b> Se retorno el número de orden de la página. <br>
     * @return El número de orden de la pagina
     */
     public int getN() {
        return n;
    }

    /**
     * Método que permite saber el número de llaves que puede almacenar la página. <br>
     * <b>post: </b> Se retorno el número de llaves que puede almacenar la página. <br>
     * @return El número de llaves que puede almacenar la pagina
     */
    public int getCont() {
        return cont;
    }

    /**
     * Método que permite saber la información que almacena la página. <br>
     * <b>post: </b> Se retorno de la información que almacena la página. <br>
     * @return la información que esta almacenada en la pagina
     */
    public T[] getInfo() {
        return info;
    }

    /**
     * Método que permite saber el número máximo de elementos que puede almacenar la página. <br>
     * <b>post: </b> Se retorno el número máximo de elementos que puede almacenar la página. <br>
     * @return El número máximo de elementos que puede almacenar la pagina
     */
    public int getM() {
        return m;
    }

    /**
     * Método que permite saber el numero máximo de apuntadores que puede almacenar la página. <br>
     * <b>post: </b> Se retorno el número máximo de apuntadores que puede almacenar la página. <br>
     * @return El número máximo de apuntadores que puede almacenar la pagina
     */
    public int getM1() {
        return m1;
    }

    /**
     * Método que permite obtener los apuntadores de la página. <br>
     * <b>post: </b> Se retorno los apuntadores de la página. <br>
     * @return Un arreglo con los apuntadores de la pagina
     */
    public Pagina[] getApuntadores() {
        return apuntadores;
    }
    
    /**
     * Método que permite modificar el número de orden de la página. <br>
     * <b>post: </b> Se modifico el número de orden de la página. <br>
     * @param n Número del orden de la página. <br>
     */
    public void setN(int n) {
        this.n = n;
    }
   
    /**
     * Método que permite modificar el número de llaves de la página. <br>
     * <b>post: </b> Se modifico el número de llaves de la página. <br>
     * @param cont Numero de llaves de la página. <br>
     */
    public void setCont(int cont) {
        this.cont = cont;
    }

    /**
     * Método que permite modificar los apuntadores de la página. <br>
     * <b>post: </b> Se modifico los apuntadores de la página. <br>
     * @param apuntadores conjunto de apuntadores de la página. <br>
     */
    public void setApuntadores(Pagina[] apuntadores) {
        this.apuntadores = apuntadores;
    }

    /**
     * Método que permite modificar la información de la página. <br>
     * <b>post: </b> Se modifico la información de la página. <br>
     * @param info Conjunto de información que tendrá la página. <br>
     */
    public void setInfo(T[] info) {
        this.info = info;
    }

    @Override
    public String toString(){
        String msg= "  Informacion de la pagina";
        int i=0;
        while(i<this.getCont()){
        msg+=" --> "+this.info[i++].toString();}
        return msg;
    }
    
}//Fin de la clase Pagina
