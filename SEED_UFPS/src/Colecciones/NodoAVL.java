/**
 * ---------------------------------------------------------------------
 * $Id: NodoAVL.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de clase que contiene la información de los Nodos del Árbol AVL.
 * @param <T> Tipo de datos a almacenar en el Nodo.
 * @author Uriel Garcia
 * @version 1.0
 */
public class NodoAVL<T> extends NodoBin<T>{
    
    ////////////////////////////////////////////////////////////
    // NodoAVL - Atributos /////////////////////////////////////
    ////////////////////////////////////////////////////////////
        
    /**
     * Padre del NodoAVL
     */
    private NodoAVL<T> padre;
    
    /**
     * Factor de Balance del NodoAVL
     */
    private int bal;
    
    
    
    ////////////////////////////////////////////////////////////
    // NodoAVL - Implementación de Métodos /////////////////////
    ////////////////////////////////////////////////////////////    
    
    /**
     * Constructor vacío de la clase Nodo AVL. <br>
     * <b>post: </b> Se construyo un Nodo AVL vacío con la información y sus hijos en NULL. <br>
     */
    public NodoAVL() {
        super();
        this.padre=null;
        this.bal=0;
    }
    
    /**
     * Constructor con parámetros de la clase <br>
     * <b>post: </b> Se construyo un Nodo Doble con la información dada. <br>
     * @param x Es de tipo T el cual posee la información del nodo del Árbol
     * @param i Es de tipo NodoAVL&#60;T&#62; el cual posee el nodo del lado izquierdo
     * @param d Es de tipo NodoAVL&#60;T&#62; el cual posee el nodo del lado derecho
     * @param p Es de tipo NodoAVL&#60;T&#62; el cual posee el nodo padre
     * @param b Es de tipo int el cual contiene el factor de Balance del Nodo.
     */
    public NodoAVL(T x, NodoAVL<T> i, NodoAVL<T> d, NodoAVL<T> p, int b) {
        super(x,i,d);
        this.padre=p;
        this.bal=b;
    }
    
    /**
     * Constructor con parámetros de la clase que genera un nuevo elemento. <br>
     * <b>post: </b> Se construyo un Nodo Hoja que contiene la información dada y sus hijos en null. <br>
     * @param x Representa la información del nodo hoja del Árbol
     */
    public NodoAVL(T x) {        
        super(x);
        this.padre=null;
        this.bal=0;
    }
    
    /**
     * Método que permite obtener la información del Nodo AVL. <br>
     * <b>post: </b> Se retorno la información del Nodo AVL. <br>
     * @return Un tipo T que contiene la información del Nodo AVL
     */
    @Override
    public T getInfo(){
        return (super.getInfo());
    }
    
    /**
     * Método que permite obtener el hijo izquierdo del Nodo AVL. <br>
     * <b>post: </b> Se retorno el hijo izquierdo del Nodo AVL. <br>
     * @return Un tipo NodoAVL&#60;T&#62; que contiene la información del hijo izquierdo del Nodo AVL
     */
    @Override
    public NodoAVL<T> getIzq() {
        return ((NodoAVL<T>)super.getIzq());
    }
    
    /**
     * Método que permite obtener el hijo derecho del Nodo AVL. <br>
     * <b>post: </b> Se retorno el hijo derecho del Nodo AVL. <br>
     * @return Un tipo NodoAVL&#60;T&#62; que contiene la información del hijo derecho del Nodo AVL
     */
    @Override
    public NodoAVL<T> getDer() {
        return ((NodoAVL<T>)super.getDer());
    }
    
    /**
     * Método que permite cambiar la información contenida en el Nodo AVL. <br>
     * <b>post: </b> Se edito la información del Nodo AVL. <br>
     * @param info De tipo T y contiene la información del nodo
     */
    @Override
    public void setInfo(T info) {
        super.setInfo(info);
    }
    
    /**
     * Método que permite modificar el hijo izquierdo del Nodo AVL. <br>
     * <b>post: </b> Se retorno el nuevo hijo izquierdo del Nodo AVL. <br>
     * @param i Es de tipo NodoB&#60;T&#62; que contiene el nodo izquierdo
     */
    
    public void setIzq(NodoAVL<T> i) {
        super.setIzq(i);
    }
    
    /**
     * Método que permite modificar el hijo derecho del Nodo AVL. <br>
     * <b>post: </b> Se modifico el hijo derecho del Nodo AVL. <br>
     * @param d Es de tipo NodoAVL&#60;T&#62; que contiene el nodo derecho
     */
    public void setDer(NodoAVL<T> d) {
        super.setDer(d);
    }
    
    /**
     * Método que permite obtener el padre del Nodo AVL. <br>
     * <b>post: </b> Se retorno el padre del Nodo AVL. <br>
     * @return un tipo NodoAVL&#60;T&#62; que contiene la información del padre del Nodo AVL
     */
    public NodoAVL<T> getPadre() {
        return (this.padre);
    }
    
    /**
     * Método que permite modificar el padre del Nodo AVL. <br>
     * <b>post: </b> Se modifico el padre del Nodo AVL. <br>
     * @param p Es de tipo NodoAVL&#60;T&#62; que contiene el padre.
     */
    public void setPadre(NodoAVL<T> p) {
        this.padre=p;
    }
    
    /**
     * Método que permite obtener el factor de Balance del Nodo AVL. <br>
     * <b>post: </b> Se retorno el factor de Balance del Nodo AVL. <br>
     * @return Un objeto de tipo int con el factor de balance del Nodo AVL
     */
    public int getBal() {
        return this.bal;
    }
    
    /**
     * Método que permite modificar el factor de Balance del Nodo AVL. <br>
     * <b>post: </b> Se modifico el factor de Balance del Nodo AVL. <br>
     * @param b Es de tipo int representa el nuevo factor de balance.
     */
    public void setBal(int b) {
        this.bal = b;
    }
    
}//Fin de la Clase NodoAVL


