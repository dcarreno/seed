/**
 * ---------------------------------------------------------------------
 * $Id: ArbolSplay.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

import java.util.Iterator;

/**
 * Implementación de Clase para el manejo de un Árbol Splay.
 * @param <T> Tipo de dato a almacenar en el Árbol Splay
 * @author Uriel García
 * @version 1.0
 */
public class ArbolSplay<T> extends ArbolBinarioBusqueda <T> {
    

    ////////////////////////////////////////////////////////////
    // ArbolSplay - Implementación de Métodos ///////////////////
    ////////////////////////////////////////////////////////////

    /**
     * Crea un Árbol Splay vacío con sus datos Nulos. <br>
     * <b>post: </b> Se creo un Árbol Splay vacio. <br>
     */
     public ArbolSplay()    {
         super();
     }

     /**
      * Crea un Árbol Splay con una raíz predefinida. <br>
      * <b>post: </b> Se creo un Árbol con raíz predeterminada. <br>
      * @param raiz Un objeto de tipo T , almacena la dirección de memoria de un nodo de un Árbol. <br>
      */
      public ArbolSplay(T raiz) {
         super(raiz);
      }
      
     /**
      * Método que permite conocer el objeto almacenado en la raíz del Árbol Splay. <br>
      * <b>post: </b> Se obtuvo la raíz del Árbol Splay. <br>
      * @return La raíz del Árbol Binario.
      */
     @Override
     public T getObjRaiz() {
         return (super.getObjRaiz());
     }

     /**
      * Método que permite insertar un dato en el Árbol Splay de manera que este se ubique en la raíz. <br>
      * <b>post: </b> Se inserto un nuevo dato al Árbol Splay. <br>
      * @param dato Un elemento tipo T que se desea almacenar en el Árbol. <br>
      * @return True si el elemento fue insertado exitosamente o false en caso contrario
      */
     @Override
     public boolean insertar(T dato){
         //Si el arbol se encuentra vacio
         if (esVacio()) {
             super.setRaiz(new NodoBin<T>(dato));
             return (true);
         }        
         super.setRaiz(buscarAS(dato));
         int cmp = ((Comparable)dato).compareTo(super.getRaiz().getInfo());        
         // Si el dato es menor a la raíz
         if (cmp < 0) {
             NodoBin<T> n = new NodoBin<T>(dato);
             n.setIzq(super.getRaiz().getIzq());
             n.setDer(super.getRaiz());
             super.getRaiz().setIzq(null);
             super.setRaiz(n);
             return (true);
         }
         // Si el dato es mayor a la raíz
         else if (cmp > 0) {
             NodoBin<T> n = new NodoBin<T>(dato);
             n.setDer(super.getRaiz().getDer());
             n.setIzq(super.getRaiz());
             super.getRaiz().setDer(null);
             super.setRaiz(n);
             return (true);
         }
         return (false);
     }

    /**
     * Método que permite trasladar un nodo x, que es el nodo al que se accede, a la raíz. <br>
     * <b>post: </b> Se realizo la reestructuración del Árbol Splay de acuerdo al dato accedido recientemente. <br>
     * @param r Representa la raíz del Árbol desde la cual arranca la búsqueda del dato. <br>
     * @param dato Un elemento tipo T que se desea ser accedido y que será ascendido a la raíz. <br>
     * @return Un objeto de tipo NodoBin<T> que representa la nueva raíz del Árbol a cuál debera ser actualizada.
     */
    private NodoBin<T> biselar(NodoBin<T> r, T dato) {
        if (r == null) 
            return (null);
        int cmp1 = ((Comparable)dato).compareTo(r.getInfo());
        //Si el dato es menor a la raiz
        if (cmp1<0){
            if (r.getIzq() == null) {
                return (r);
            }
            int cmp2 = ((Comparable)dato).compareTo(r.getIzq().getInfo());
            //Si es dato es menor que el hijo
            if (cmp2<0){
                r.getIzq().setIzq(biselar(r.getIzq().getIzq(),dato));
                r = rDerecha(r);
            }
            //Si el dato es mayor que el hijo
            else if (cmp2>0) {
                r.getIzq().setDer(biselar(r.getIzq().getDer(), dato));
                if (r.getIzq().getDer()!= null)
                    r.setIzq(rIzquierda(r.getIzq()));
            }            
            if (r.getIzq()== null) 
                return (r);
            else
                return (rDerecha(r));
        }
        //El dato es menor a la raíz
        else if (cmp1>0) { 
            // dato not in tree, so we're done
            if (r.getDer() == null) {
                return (r);
            }
            int cmp2 = ((Comparable)dato).compareTo(r.getDer().getInfo());
            //Si el dato es menor que el hijo
            if (cmp2<0){
                r.getDer().setIzq(biselar(r.getDer().getIzq(), dato));
                if (r.getDer().getIzq() != null)
                    r.setDer(rDerecha(r.getDer()));
            }
            //Si el dato es mayor que el hijo
            else if (cmp2>0) {
                r.getDer().setDer(biselar(r.getDer().getDer(), dato));
                r = rIzquierda(r);
            }
            if (r.getDer()==null) 
                return (r);
            else  
                return (rIzquierda(r));
        }
        else return (r);
    }
    
    /**
     * Método que permite efectuar una rotación simple hacia la derecha de un Nodo. <br>
     * <b>post: </b> Se realizo una rotación simple a la derecha. <br>
     * @param r Nodo que se encuentra desbalanceado y no cumple la propiedad. <br>
     * @return Un objeto de tipo NodoBin<T> con las rotaciones ya realizadas. <br>
     */
    private NodoBin<T> rDerecha(NodoBin<T> r) {
        NodoBin<T> x = r.getIzq();
        r.setIzq(x.getDer());
        x.setDer(r);
        return x;
    }
    
    /**
     * Método que permite efectuar una rotación simple hacia la izquierda de un Nodo. <br>
     * <b>post: </b> Se realizo una rotación simple a la izquierda. <br>
     * @param r Nodo que se encuentra desbalanceado y no cumple la propiedad. <br>
     * @return Un objeto de tipo NodoBin<T> con las rotaciones ya realizadas. <br>
     */
    private NodoBin<T> rIzquierda(NodoBin<T> r) {
        NodoBin<T> x = r.getDer();
        r.setDer(x.getIzq());
        x.setIzq(r);
        return x;
    }
    
    /**
     * Método que permite eliminar un dato del Árbol Splay; El dato más próximo al eliminado se pone en la raíz. <br> 
     * <b>post: </b> Se ha eliminado el dato accedido y el Árbol Splay ha sido biselado de manera correcta. <br>
     * @param dato Un elemento de tipo T que es ascendido a la raíz y posteriormente eliminado. <br>
     * @return Un objeto de tipo boolean con true si el dato ha sido eliminado correctamente.
     */
     @Override
    public boolean eliminar(T dato) {
        if (esVacio())
            return (false);      
        super.setRaiz(buscarAS(dato));
        int cmp = ((Comparable)dato).compareTo(super.getRaiz().getInfo());        
        //Si se encontro el elemento
        if (cmp==0){
            if (super.getRaiz().getIzq()==null){
                super.setRaiz(super.getRaiz().getDer());
            } 
            else {
                    NodoBin<T> x = super.getRaiz().getDer();
                    super.setRaiz(super.getRaiz().getIzq());
                    super.setRaiz(biselar(super.getRaiz(), dato));
                    super.getRaiz().setDer(x);
            }
            return (true);
        }
        //El dato no fue encontrado
        return (false);
    }
    
    /**
     * Método que permite hallar un elemento dentro del Árbol Splay; Una vez ubicado el dato se
     * realiza el proceso de biselación sobre el Árbol de manera que el dato consultado queda en la raíz. <br>
     * <b>post: </b> Se retorno un Objeto de tipo NodoBin<T> que representa la raíz de Árbol Splay una vez biselado. <br>
     * @param dato Representa el elemento el cual se desea evaluar su existencia en el Árbol. <br>
     * @return un Objeto de tipo NodoBin<T> que representa la raíz de Árbol Splay una vez biselado.
     */
    private NodoBin<T> buscarAS(T dato){
        if(esVacio())
            return (null);
        return (biselar(super.getRaiz(), dato));
    }
    
    /**
     * Método que permite evaluar la existencia de un objeto dentro del Árbol Splay; 
     * El elemento es ubicado en la raíz de Árbol y se realiza proceso de biselación. <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el Árbol. <br>
     * @param dato Representa el elemento el cual se desea evaluar su existencia en el Árbol. <br>
     * @return Un boolean , true si el dato esta o false en caso contrario.
     */
    public boolean estaAS(T dato){
        if(esVacio())
            return (false);
        super.setRaiz(buscarAS(dato)); 
        return (super.getRaiz().getInfo().equals(dato));
    }
    
    /**
     * Método que retorna un iterador con las hojas del Árbol Splay. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol. <br>
     * @return Un iterador con las hojas del Árbol.
     */
     @Override
    public Iterator<T> getHojas(){
        return (super.getHojas());
    }
    
    /**
     * Método que permite determinar el número de Nodo hojas dentro del Árbol. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol. <br>
     * @return El número de hojas existentes en el Árbol.
     */
     @Override
    public int contarHojas(){
        return (super.contarHojas());
    }
    
    /**
     *  Método que retorna un iterador con el recorrido preOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el Árbol.<br>
     * @return Un iterador en preOrden (padre-&#62;hijoIzq-&#62;hijoDer) para el Árbol Splay.
     */
     @Override
     public Iterator<T> preOrden(){
         return (super.preOrden());
    }
    
    /**
     * Método que retorna un iterador con el recorrido in Orden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el Árbol. <br>
     * @return un iterador en inOrden (hijoIzq-&#62;padre-&#62;hijoDer) para el Árbol Splay. <br>
     */
     @Override
    public Iterator<T> inOrden(){
        return (super.inOrden());
    }

    /**
     * Método que retorna un iterador con el recorrido postOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador preOrden para el Árbol. <br>
     * @return un iterador en postOrden (hijoIzq-&#62;hijoDer-&#62;padre) para el Árbol Splay. <br>
     */
     @Override
    public Iterator<T> postOrden(){
        return (super.postOrden());
    }
    
    /**
     * Método que permite retornar un iterador con el recorrido por niveles del Árbol. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Árbol Splay.<br>
     * @return Un iterador con el recorrido por niveles del Árbol Splay.
     */
     @Override
    public Iterator<T> impNiveles(){
        return (super.impNiveles());
    }
    
    /**
     * Método que permite obtener el peso del Árbol Splay. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol Splay.<br>
     * @return Un entero con la cantidad de elementos del Árbol Splay.
     */
     @Override
    public int getPeso(){
        return(super.getPeso());
    }
    
    /**
     * Método que permite saber si el Árbol Splay se encuentra vacío. <br>
     * <b>post: </b> Se retorno true si el Árbol no contiene elementos. <br>
     * @return True si no hay datos en el Árbol Splay.
     */
    @Override
    public boolean esVacio(){
        return(super.esVacio());
    }
    
    /**
     * Método que permite obtener la altura del Árbol Splay. <br>
     * <b>post: </b> Se retorno la altura del Árbol Splay.<br>
     * @return Un entero con la altura del Árbol Splay.
     */
     @Override
    public int getAltura(){
        return(super.getAltura());
    }
    
     /**
     * Método que permite clonar la información de un Árbol Splay en un nuevo objeto Árbol Splay. <br>
     * @return Un objeto de tipo Árbol Splay con la información del Árbol duplicada.
     */
    @Override
    public ArbolSplay<T> clonar(){
        ArbolSplay<T> t=new ArbolSplay<T>();
        t.setRaiz(clonarAS(this.getRaiz()));
        return(t);
    }

    /**
     * Método que permite clonar la información de un Árbol Splay en un nuevo objeto Árbol Splay. <br>
     * @param r Representa a raíz del Árbol Splay. <br>
     * @return Un objeto de tipo Árbol Splay con la información del Árbol duplicada.
     */
    private NodoBin<T> clonarAS(NodoBin<T> r){				
        if(r==null)
            return r;
        else
        {
            NodoBin<T> aux=new NodoBin<T>(r.getInfo(), clonarAS(r.getIzq()), clonarAS(r.getDer()));
            return aux;
        }
    }
    
    /**
     * Método que permite conocer por consola la información del Árbol Binario.
     */
     @Override
    public void imprime(){
        System.out.println(" ----- Arbol Splay ----- ");
        imprimeAS(super.getRaiz());
    }
    
    /**
     * Método de tipo privado que permite mostrar por consola la información del Árbol Splay. <br>
     * @param n Representa la raíz del Árbol Splay o de alguno de sus sub_Arboles.
     */
    public void imprimeAS(NodoBin<T> n) {
        int l = 0;
        int r = 0;
        if(n==null)
            return;
        if(n.getIzq()!=null) {
         l = Integer.parseInt(n.getIzq().getInfo().toString());
        }
        if(n.getDer()!=null) {
         r = Integer.parseInt(n.getDer().getInfo().toString());
        }       
        System.out.println("NodoIzq: "+l+"\t Info: "+n.getInfo()+"\t NodoDer: "+r);
        if(n.getIzq()!=null) {
         imprimeAS(n.getIzq());
        }
        if(n.getDer()!=null) {
         imprimeAS(n.getDer());
        }
    }
   
}// Fin de la Clase ArbolSplay.