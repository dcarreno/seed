/**
 * ---------------------------------------------------------------------
 * $Id: ArbolHeap.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;
import java.util.Iterator;


/**
 * Implementación de clase para manejo de Arboles Heaps. <br>
 * @param <T> Tipo de datos a almacenar dentro del Árbol. <br>
 * @author Uriel García
 * @version 1.0
 */
public class ArbolHeap<T>
{
    
    ////////////////////////////////////////////////////////////
    // ArbolHeap - Atributos ///////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Representa el vector con los datos almacenado en el Árbol Heap
     */
    private T[] datos;
    
    /**
     * Representa el peso del Árbol
     */
    private int peso;
    
    /**
     * Tamaño predefinido del Heap
     */
    private final static int def = 200;
    
    
    
    ////////////////////////////////////////////////////////////
    // ArbolHeap - Implementacion de Metodos ///////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Crea un Árbol Heap vacío con un tamaño predefinido. <br>
     * <b>post: </b> Se creo un Árbol Heap vacío. <br>
     */
    public ArbolHeap(){
        //def es un tamaño por defecto para el Árbol.
        T[] temp = (T[]) new Object[def];
        this.datos = temp;
        this.peso = 0;
    }
    
    /**
     * Crea un Árbol Heap vacío, y se le define un tamaño ingresado. <br>
     * <b>post: </b> Se creo un Árbol Heap vacío. <br>
     * @param cap Representa la capacidad para almacenar datos del Heap.
     */
    public ArbolHeap(int cap){
        T[] temp = (T[]) new Object[cap];
        this.datos = temp;
        this.peso = 0;
    } 
    
    /**
     * Método que permite obtener la raíz del Árbol Heap; Es decir, el dato de MAYOR valor dentro del Árbol. <br>
     * <b>post: </b> Se retorno la raíz del Heap, es decir el dato mayor. <br>
     * @return Un objeto de tipo T que representa la raíz del Heap.
     */
    public T getRaiz(){
        T r = null;
        if(!this.esVacio())
            r = this.datos[0];
        return (r);
    }
    
    /**
     * Método que permite insertar un nuevo elemento dentro del Árbol Heap. <br>
     * <b>post: </b> Se ha insertado un nuevo elemento dentro del Heap.<br>
     * @param nuevo Representa el dato a insertar dentro del Heap.
     */
    public void insertar(T nuevo){        
        if(peso>=this.datos.length)
            return; //El Arbol se encuentra lleno.        
        if(this.esVacio()){
            this.datos[peso++] = nuevo;
            return;
        }        
        int indice = peso++;
        this.datos[indice] = nuevo; 
        while ((indice!=0)&&(((Comparable)this.datos[indice]).compareTo(this.datos[this.getPosPadre(indice)]))>0){
            this.datos = intercambiar(this.datos, indice, this.getPosPadre(indice));
            indice = this.getPosPadre(indice);
        }
    }
    
    /**
     * Método que permite eliminar el primer elemento (raíz) de Árbol Heap; El elemento MAYOR. <br>
     * <b>post: </b> Se retorno y elimino el elemento raíz del Árbol. El dato MAYOR. <br>
     * @return El elemento Mayor del Árbol que se ubica en la raíz.
     */
    public T eliminarRaiz(){
        if(peso<=0)
            return (null);
        this.datos = intercambiar(this.datos, 0, --peso);
        if (peso!=0)
          reorganiza(0); 
        T x = this.datos[peso];
        this.datos[peso] = null;
        return (x);
    }
    
    /**
     * Método que permite reorganizar los datos del Árbol después de una eliminación. <br>
     * <b>post: </b> Se reorganizaron los datos del Árbol Heap. 
     */
    private void reorganiza(int pos) {
      if(pos<0 || pos>peso){
          return;
      }
      while (!esHoja(pos)) {
        int j = getHijoIzq(pos);
        if ((j<(peso-1)) && (((Comparable)this.datos[j]).compareTo(this.datos[j+1]) < 0)) 
            j++;
        if (((Comparable)this.datos[pos]).compareTo(this.datos[j]) >= 0) return;
            this.datos =intercambiar(this.datos, pos, j);
            pos = j;
      }
    }
    
    /**
     * Método que permite saber si un elemento ingresado es una hoja del Árbol Heap. <br>
     * @param pos Posición del elemento dentro del Árbol Heap. <br>
     * @return True o false si el elemento es o no una hoja dentro del Árbol Heap.
     */
    private boolean esHoja(int pos){ 
        return ((pos>=peso/2) && (pos<peso)); 
    }
    
    /**
     * Método que permite eliminar un elemento cualquiera del Árbol Heap. <br>
     * <b>post: </b> Se retorno y elimino el elemento eliminado del Heap. <br>
     * @param info Representa el dato que se desea eliminar del Heap. <br>
     * @return El elemento eliminado del Árbol Heap.
     */
    public T eliminar(T info){
        int pos = this.getPos(info);
        T x;
        if(pos==(-1))
            return (null);
        if (pos==(peso-1)){
            x = this.datos[peso-1];
            this.peso--;
            this.datos[peso] = null;
            return (x);
        }            
        else
        {
            this.datos =intercambiar(this.datos, pos, --peso);
            while ((pos>0) && (((Comparable)this.datos[pos]).compareTo(this.datos[this.getPosPadre(pos)])>0)){
                this.datos = intercambiar(this.datos, pos,this.getPosPadre(pos));
                pos = this.getPosPadre(pos);
            }
            if(peso!=0) 
                reorganiza(pos);
        }
        x = this.datos[peso];
        this.datos[peso] = null;
        return (x);
    }
    
    /**
     * Método de tipo privado que permite intercambiar la información de un vector dadas sus posiciones. <br>
     * <b>post: </b> Se cambio la información de cada una de las posiciones del vector indicadas. <br>
     * @param h Vector que representa el Heap que se pretende editar la información. <br>
     * @param p1 Posición del primer elemento al que se le desea cambiar la información. <br>
     * @param p2 Posición del segundo elemento al que se le desea cambiar la información. <br>
     * @return Un objeto de tipo T[] que representa el Heap con la información modificada. <br>
     */
    private T[] intercambiar(T[] h, int p1, int p2) {
        T temp = h[p1];
        h[p1]=h[p2];
        h[p2]=temp;
        return (h);
    }
    
    /**
     * Método que permite obtener los datos existentes dentro del Árbol Heap. <br>
     * <b>post: </b> Se retornaron los datos del Heap en un objeto T[]. <br>
     * @return Un objeto de tipo T[] con los elementos insertados dentro del Heap.
     */
    public T[] getDatos(){
        return (this.datos);
    }
    
    /**
     * Método que permite conocer si un dato se encuentra dentro del Árbol Heap. <br>
     * <b>post: </b> Se evaluó la existencia de un elemento dentro del Heap. <br>
     * @param info Representa el objeto que se desea consultar dentro del Heap. <br>
     * @return True o false dependiendo se si el dato se encuentra dentro del Heap. <br>
     */
    public boolean esta(T info){
        for(int i=0; i<this.peso; i++)
            if(this.datos[i].equals(info))
                return (true);
        return (false);
    }
    
    /**
     * Método que permite obtener un Iterador para recorrer el Árbol Heap por niveles. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Heap por medio de un Iterados. <br>
     * @return Un objeto de tipo Iterator con el recorrido por niveles del Heap.
     */
    public Iterator<T> impNiveles( )
    {
        ListaCD<T> l=new ListaCD<T>();
        for(int i=0; i<this.peso; i++){
            l.insertarAlFinal(this.datos[i]);
        }
        return (l.iterator());
    }
    
    /**
     * Método que permite conocer el peso del Árbol Heap, es decir el número de elementos insertados. <br>
     * <b>post: </b> Se retorno el peso del Heap. <br>
     * @return Un objeto de tipo int con el peso del Heap.
     */
    public int getPeso(){
        return (this.peso);
    }
    
    /**
     * Método que permite conocer si un Árbol Heap se encuentra vacío; no posee elementos. <br>
     * <b>post: </b> Se evaluó si el Heap se encuentra vacío. <br>
     * @return true o false dependiendo de si el Árbol Heap se encuentra vacío o no.
     */
    public boolean esVacio(){
        return (this.peso<1);
    }
    
    /**
     * Método que permite conocer la altura del Árbol Heap. <br>
     * <b>post: </b> Se retorno la altura del Heap. <br>
     * @return Un objeto de tipo int con la altura del Árbol Heap.
     */
    public int getAltura(){
        int alt=0;
        while(Math.pow(2,alt)<=peso)
            alt++;
        return (alt);
    }
    
    /**
     * Método que permite utilizar un Árbol Heap para ordenar un vector por HeapSort. <br>
     * <b>post: </b> Se retorno un vector con los datos ordenados. <br>
     * @return Un objeto de tipo T[] con la información (datos) ordenados.
     */
    public T[] heapSort(){        
        T aux[] = (T[]) new Object[this.datos.length];                
        for(int i=this.peso-1;i>=0;i--){
            aux[i]=this.eliminarRaiz();
        }
        return (aux);
    }
    
    /**
     * Método que permite limpiar la información de los datos ingresados al Heap. <br>
     * <b>post: </b> Se ha limpiado la información del Heap. <br>
     */
    public void limpiar(){
        for( ; this.peso>=0 ; this.peso--)
            this.datos[this.peso]=null;
        this.peso = 0;
    }
    
    /**
     * Método que permite conocer la posición del padre de un elemento dentro del Heap. <br>
     * @param hijo Representa el dato del cual se quiere conocer la posición del padre. <br>
     * @return Un objeto de tipo int con la posición del padre del elemento consultado.
     */
    private int getPosPadre(int hijo){
        if(hijo<=0)
            return (-1);
        return (hijo-1)/2;
    }
    
    /**
     * Método que permite conocer el Hijo izquierdo de un elemento dentro del Heap.
     */
    private int getHijoIzq(int posPadre) {
        return ((2*posPadre)+1);
    }
    
    /**
     * Método que permite conocer el Hijo derecho de un elemento dentro del Heap.
     */
    private int getHijoDer(int posPadre) {
        return ((2*posPadre)+2);
    }
    
    /**
     * Método que permite conocer la posición de un dato dentro del Heap. <br>
     */
    private int getPos(T info){
        for(int i=0; i<this.peso; i++){
            if(this.datos[i].equals(info))
                return (i);
        }            
        return (-1);
    }
    
    /**
     * Método que permite contar la Hojas de un Árbol Heap. <br>
     * @return Un objeto de tipo int con la cantidad de elementos Hoja existente en el Árbol.
     */
    public int contarHojas() {
        int cant = 0;
        for(int i=0; i<this.peso; i++){
            if(this.esHoja(i))
                cant++;
        }
        return (cant);
    }
    
    /**
     * Método que permite conocer la Hojas del Árbol heap y retornarla en un listado. <br>
     * @return Un objeto iterador de la lista con las hojas del Árbol Heap.
     */
    public Iterator getHojas(){
        ListaCD<T> l = new ListaCD<T>();
        for(int i=0; i<this.peso; i++){
            if(this.esHoja(i))
                l.insertarAlFinal(this.datos[i]);
        }
        return (l.iterator());
    }

    /**
     * Método que permite eliminar los elementos Hoja del Árbol heap.
     */
    public void podar() {
        int cant = 0;
        for(int i=0; i<this.peso; i++){
            if(this.esHoja(i)){
                this.datos[i]=null;
                cant++;
            }                
        }
        this.peso= this.peso-cant;
    }
    
    /**
     * Método que permite conocer el recorrido en preOrden del Árbol Heap.
     * @return Un iterador con el listado del recorrido en preOrden del Árbol heap.
     */
    public Iterator<T> preOrden() {
        ListaCD<T> l=new ListaCD<T>();
         preOrden(0,l);
         return (l.iterator());
    }
    
    private void preOrden(int pos, ListaCD<T> l) {
        T r = this.datos[pos];
        if(r!=null){
            l.insertarAlFinal(r);
            preOrden(this.getHijoIzq(pos), l);
            preOrden(this.getHijoDer(pos), l);
        }
    }

    /**
     * Método que permite conocer el recorrido en inOrden del Árbol Heap.
     * @return Un iterador con el listado del recorrido en inOrden del Árbol heap.
     */
    public Iterator<T> inOrden() {
        ListaCD<T> l=new ListaCD<T>();
         inOrden(0,l);
         return (l.iterator());
    }
    
    private void inOrden(int pos, ListaCD<T> l) {
        T r = this.datos[pos];
        if(r!=null){            
            inOrden(this.getHijoIzq(pos), l);
            l.insertarAlFinal(r);
            inOrden(this.getHijoDer(pos), l);
        }
    }
    
    /**
     * Método que permite conocer el recorrido en postOrden del Árbol Heap.
     * @return Un iterador con el listado del recorrido en postOrden del Árbol heap.
     */
    public Iterator<T> postOrden() {
        ListaCD<T> l=new ListaCD<T>();
         postOrden(0,l);
         return (l.iterator());
    }
    
    private void postOrden(int pos, ListaCD<T> l) {
        T r = this.datos[pos];
        if(r!=null){            
            postOrden(this.getHijoIzq(pos), l);
            postOrden(this.getHijoDer(pos), l);
            l.insertarAlFinal(r);
        }
    }
    
    /**
     * Convierte el Heap a una cadena de String. <br>
     * <b>post: </b> Se retorno la representación en String del Árbol heap. 
     * @return La representación en String del Heap.
     */
    @Override
    public String toString(){
        String cad = "";
        for(int i=0; i<this.peso; i++){
            cad+=this.datos[i].toString()+"-";
        }
        return (cad);
    }
    
}// Fin de la clase ArbolHeap
