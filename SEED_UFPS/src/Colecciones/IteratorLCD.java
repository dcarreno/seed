/**
 * ---------------------------------------------------------------------
 * $Id: IteratorLCD.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;
import java.util.Iterator;
/**
 * Implementación de Clase para el manejo de Iteradores en una Lista Circular Doble Enlazada&#60;T&#62; 
 * con nodo cabecera
 * @param <T> Tipo de datos sobre los que se iteran.
 * @author Marco Adarme
 * @version 2.0
 */
public class IteratorLCD<T> implements Iterator<T>
{
    
    ////////////////////////////////////////////////////////////
    // IteratorLCD - Atributos /////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /*
     * Nodo cabecera de la Lista
     */
    private NodoD<T> cab;            
    
    /*
     * Nodo de la Lista a Iterar
     */
    private NodoD<T> posicion;   
    
    
    
	
    ////////////////////////////////////////////////////////////
    // IteratorLCD - Implementación de Métodos /////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Constructor con parámetros de la clase <br>
     * <b> post: </b> Se crea un iterador de lista circular doble. <br>
     * @param cab Es de tipo Nodo<T> que contiene el nodo cabeza de la lista
     */
    IteratorLCD(NodoD<T> cab) {

        this.cab=cab;                       
        this.posicion=this.cab.getSig();

    }
    
    /**
     * Método que informa si existe otro elemento en la lista para seguir iterando<br>
     * <b> post: </b> Se retorna si existen aún datos por iterar en la Lista. <br>
     * @return Un tipo boolean que informa si existe o no un dato en la lista, desde la posición 
     * actual del cursor.
     */
    @Override
    public boolean hasNext() {
            return (this.posicion!=this.cab);
    }
    
    /**
     * Método que retorna un dato de la posición actual del cursor del iterador. <br>
     * <b> post: </b> Se ha retornado el dato en la posición actual de la iteración. <br>
     * El cursor queda en la siguiente posición.
     * @return Un tipo T que contiene el dato actual
     */
    @Override
    public T next() {
        if(!this.hasNext())
            return (null);
        this.posicion=this.posicion.getSig();
        return(this.posicion.getAnt().getInfo());
    }
    
    /**
     *
     */
    @Override
    public void remove() {}

}//Fin  de la clase IteratorLCD