/**
 * ---------------------------------------------------------------------
 * $Id: ArbolBinarioBusqueda.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

import java.util.Iterator;

/**
 * Implementación de Clase para el manejo de un Árbol Binario de Búsqueda (ordenado). <br>
 * @param <T> Tipo de dato a almacenar en el Árbol Binario de Búsqueda. <br>
 * @author Marco Adarme
 * @version 2.0
 */
public class ArbolBinarioBusqueda <T> extends ArbolBinario<T> {

    
    ////////////////////////////////////////////////////////////
    // ArbolBinarioDeBusqueda  - Implementación de Métodos /////
    ////////////////////////////////////////////////////////////
    
   /**
    * Crea un Árbol Binario de Búsqueda vacío. <br>
    * <b>post: </b> Se creo un Árbol Binario de Búsqueda vacío. <br>
    */
    public ArbolBinarioBusqueda(){
        super();
    }
    
    /**
     * Crea un Árbol con una raíz predefinida. <br>
     * <b>post: </b> Se creo un Árbol Binario de Búsqueda con raíz predeterminada. <br>
     * @param raiz Un tipo T, almacena la dirección de memoria de un nodo de un Árbol Binario de Búsqueda. <br>
     */
     public ArbolBinarioBusqueda(T raiz){
         super(raiz);
     }
     
     @Override
     public NodoBin<T> getRaiz(){
         return (super.getRaiz());
     }
     
     /**
     * Método que permite conocer el objeto raíz del Árbol AVL. <br>
     * <b>post: </b> Se retorno el objeto raíz del Árbol. <br>
     * @return Un objeto de tipo T que representa el dato en la raíz del Árbol.
     */
    @Override
    public T getObjRaiz() {
        return super.getObjRaiz();
    }
   
    /**
     * Método que permite insertar un dato en el Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se inserto un nuevo dato al Árbol Binario de Búsqueda. <br>
     * @param dato Un elemento tipo T que se desea almacenar en el Árbol. <br>
     * @return True si el elemento fue insertado o false en caso contrario
     */
     public boolean insertar(T dato){
        NodoBin<T> rr=this.esta(dato)?null:insertar(this.getRaiz(), dato);
        if(rr!=null)
             this.setRaiz(rr);
        return (rr!=null);
     }

    /**
     * Método que permite insertar un dato en el Árbol Binario de Búsqueda según factor de ordenamiento. <br>
     * <b>post: </b> Se inserto ordenado un nuevo dato al Árbol Binario de Búsqueda. <br>
     * @param r de tipo NoboBin<T> que representa la raíz del Árbol. <br>
     * @param dato Elemento a insertar en el Árbol de forma ordenada. <br>
     * @return True si el elemento fue insertado o false en caso contrario
     */
    private NodoBin<T> insertar(NodoBin<T> r, T dato){
        if(r==null)
            return(new NodoBin<T>(dato,null, null));
        int compara=((Comparable)r.getInfo()).compareTo(dato);
        if(compara>0) 
            r.setIzq(insertar(r.getIzq(), dato));
        else
            if(compara<0)  
                    r.setDer(insertar(r.getDer(), dato));
            else{
            System.err.println("Error dato duplicado:"+dato.toString());
            }
        return r;
    }

    /**
     * Método que permite borrar un elemento del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se elimino el elemento en el Árbol Binario de Búsqueda. <br>
     * @param x Dato que se desea eliminar. <br>
     * @return El dato borrado o null si no lo encontró
     */
    @Override
    public boolean eliminar(T x){
        if(!this.esta(x)){
            return (false);
        }
        NodoBin<T> z=eliminarABB(this.getRaiz(),x);
        this.setRaiz(z);
        return (true);
    }
    
     /**
      * Método de tipo privado que permite eliminar un dato en el Árbol Binario de Búsqueda según factor de ordenamiento, manteniendo su propiedad de orden,
      * para esto se busca el menor de los derechos y lo intercambia por el dato que desea eliminar. La idea del algoritmo es que el dato a eliminar 
      * se coloque en una hoja o en un nodo que no tenga una de sus ramas. <br>
      * <b>post: </b> Se elimino el elemento Árbol Binario de Búsqueda. <br>
      * @param r De tipo NoboBin<T> que representa la raíz del Árbol. <br>
      * @param dato Elemento que se desea eliminar del Árbol. <br>
      * @return El dato borrado o null si no lo encontró
      */
    private NodoBin<T> eliminarABB(NodoBin<T> r, T x){
        if (r==null)
                return null;//<--Dato no encontrado		
        int compara=((Comparable)r.getInfo()).compareTo(x);
        if(compara>0)
                r.setIzq(eliminarABB(r.getIzq(), x));
        else
            if(compara<0)
                    r.setDer(eliminarABB(r.getDer(), x));
            else{
                if(r.getIzq()!=null && r.getDer()!=null){
                     NodoBin<T> cambiar=this.masIzquierda(r.getDer());
                     T aux=cambiar.getInfo();
                     cambiar.setInfo(r.getInfo());
                     r.setInfo(aux);
                     r.setDer(eliminarABB(r.getDer(),x));
                    }
                else{
                    r=(r.getIzq()!=null)? r.getIzq():r.getDer();
                 }
            }
        return r;
    }

    /**
     * Método que busca el menor dato del Árbol. El menor dato del Árbol se encuentra en el nodo más izquierdo. <br>
     * <b>post: </b> Se retorno el nodo más izquierdo del Árbol. <br>
     * @param r Representa la raíz del Árbol. <br>
     * @return El nodo más izquierdo del Árbol
     */
    @SuppressWarnings("empty-statement")
    protected NodoBin<T> masIzquierda(NodoBin<T> r){
        for(; r.getIzq()!=null; r=r.getIzq());
        return(r);
    }
    
    /**
     * Método que permite evaluar la existencia de un dato dentro del Árbol Binario de Búsqueda es necesario para que el método funcione 
     * que los objetos almacenados en el Árbol tengan sobre escrito el método equals. <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el Árbol. <br>
     * @param x Representa la información del elemento que se encontrar en el Árbol. <br>
     * @return Un boolean, true si el dato esta o false en caso contrario.
     */
    public boolean estaABB(T x){
        return(esta(this.getRaiz(), x));
    }
    
    /**
     * Método que permite conocer si un elemento especifico se encuentra en el Árbol. <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el Árbol. <br>
     * @param r Representa la raíz del Árbol. <br>
     * @param x Representa la información del elemento que se encontrar en el Árbol. <br>
     * @return Un boolean , true si el dato esta o false en caso contrario.
     */
    private boolean esta(NodoBin<T> r, T x){
        if (r==null)
            return (false);
        int compara=((Comparable)r.getInfo()).compareTo(x);
        if(compara>0)
            return(esta(r.getIzq(),x));
        else
            if(compara<0)
                return(esta(r.getDer(),x));
            else
                return (true);
    }
    
    /**
     * Método que permite consultar un elemento existente dentro del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se retorno un NodoBin&#60;T&#62; perteneciente al dato buscado. <br>
     * @param info Elemento a ubicar dentro del Árbol Binario de Búsqueda. <br>
     * @return Un objeto de tipo NodoBin&#60;T&#62; que representa el objeto buscado.
     */
    protected NodoBin<T> buscar(T info){
        return (buscar(this.getRaiz(),info));
    }
   
    /**
     * Método que permite consultar un elemento existente dentro del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se retorno un NodoBin&#60;T&#62; perteneciente al dato buscado. <br>
     * @param info Elemento a ubicar dentro del Árbol Binario de Búsqueda. <br>
     * @param r Representa la raíz del Árbol. <br>
     * @return Un objeto de tipo NodoBin&#60;T&#62; que representa el objeto buscado.
     */
    protected NodoBin<T> buscar(NodoBin<T> r, T info){
        if(r==null)
            return (null);
        if(r.getInfo().equals(info))
            return r;
        else
        {
            NodoBin<T> aux = (r.getIzq()==null)?null:buscar(r.getIzq(),info);
            if(aux!=null)
                return (aux);
            else
                return (r.getDer()==null)?null:buscar(r.getDer(),info);
        }
    }
    
    /**
     * Método que retorna un iterador con las hojas del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol Binario de Búsqueda. <br>
     * @return Un iterador con las hojas del Árbol Binario de Búsqueda.
     */
    @Override
    public Iterator<T> getHojas(){
        return (super.getHojas());
    }
        
    /**
     * Método que permite determinar el número de Nodo hojas dentro del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol de Búsqueda. <br>
     * @return El número de hojas existentes en el Árbol Binario de Búsqueda.
     */
    @Override
    public int contarHojas(){
        return (super.contarHojas());
    }
    
    /**
     *  Método que retorna un iterador con el recorrido preOrden del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el arbol. <br>
     * @return Un iterador en preOrden (primero la raíz luego los hijos) para el  Árbol Binario de Búsqueda.
     */
    @Override
     public Iterator<T> preOrden(){
         return (super.preOrden());
    }

   /**
     * Método que retorna un iterador con el recorrido in Orden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el Árbol. <br>
     * @return Un iterador en inOrden (primero el hijo izquierdo luego la raíz y después el hijo derecho) para el  Árbol Binario de Búsqueda. <br>
     */
    @Override
    public Iterator<T> inOrden(){
        return (super.inOrden());
    }
    /**
     * Método que retorna un iterador con el recorrido postOrden del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se retorno un iterador postOrden para el Árbol. <br>
     * @return Un iterador en postOrden (primero los hijos y luego la raíz) para el Árbol Binario de Búsqueda. <br>
     */
    @Override
    public Iterator<T> postOrden(){
        return (super.postOrden());
    }
  
    /**
     * Método que permite retornar un iterador con el recorrido por niveles del Árbol Binario de Busqueda. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Árbol Binario de Búsqueda. <br>
     * @return Un iterador con el recorrido por niveles del Árbol Binario de Búsqueda.
     */
    @Override
    public Iterator<T> impNiveles(){
        return (super.impNiveles());
    }
    
    /**
     * Método que permite obtener el peso del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol Binario de Búsqueda. <br>
     * @return Un entero con la cantidad de elementos del Árbol Binario de Búsqueda.
     */
    @Override
    public int getPeso(){
        return(super.getPeso());
    }

    /**
     * Método que permite saber si el Árbol se encuentra vacío. <br>
     * <b>post: </b> Se retorno true si el Árbol no contiene elementos. <br>
     * @return True si no hay datos en el Árbol
     */
    @Override
    public boolean esVacio(){
        return(super.esVacio());
    }
    
    /**
     * Método que permite obtener la altura del Árbol Binario de Búsqueda. <br>
     * <b>post: </b> Se retorno la altura del Árbol Binario de Búsqueda. <br>
     * @return Un entero con la altura del Árbol Binario de Búsqueda.
     */
    @Override
    public int getAltura(){
        return(super.getAltura());
    }
    
     /**
     * Método que permite clonar la información de un Árbol Binario de Búsqueda y retornarla en un nuevo Árbol. <br>
     * @return Un nuevo Árbol Binario Búsqueda con la información clonada del actual Árbol.
     */
    @Override
    public ArbolBinarioBusqueda<T> clonar(){
        ArbolBinarioBusqueda<T> t=new ArbolBinarioBusqueda<T>();
        t.setRaiz(clonarABB(this.getRaiz()));
        return(t);
    }


    private NodoBin<T> clonarABB(NodoBin<T> r){				
        if(r==null)
            return r;
        else
        {
            NodoBin<T> aux=new NodoBin<T>(r.getInfo(), clonarABB(r.getIzq()), clonarABB(r.getDer()));
            return aux;
        }
    }
    
    /**
     * Método que permite conocer por consola la información del Árbol Binario.
     */
    @Override
    public void imprime(){
        System.out.println(" ----- Arbol Binario de Busqueda ----- ");
        this.imprimeABB(super.getRaiz());
    }
    
    /**
     * Método de tipo privado que permite mostrar por consola la información del Árbol Binario. <br>
     * @param n Representa la raíz del Árbol Binario o de alguno de sus sub_Arboles.
     */
    public void imprimeABB(NodoBin<T> n) {
        T l = null;
        T r = null;
        if(n==null)
            return;
        if(n.getIzq()!=null) {
         l = n.getIzq().getInfo();
        }
        if(n.getDer()!=null) {
         r =n.getDer().getInfo();
        }       
        System.out.println("NodoIzq: "+l+"\t Info: "+n.getInfo()+"\t NodoDer: "+r);
        if(n.getIzq()!=null) {
         imprimeABB(n.getIzq());
        }
        if(n.getDer()!=null) {
         imprimeABB(n.getDer());
        }
    }
   
}// Fin de la Clase ArbolBinarioBusqueda.