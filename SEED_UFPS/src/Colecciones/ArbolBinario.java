/**
 * ---------------------------------------------------------------------
 * $Id: ArbolBinario.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;
import java.util.Iterator;

/**
 * Implementación de Clase para el manejo de un Árbol Binario.
 * @param <T> Tipo de datos a almacenar en el Árbol Binario.
 * @author Marco Adarme
 * @version 2.0
 */
public class ArbolBinario<T>
{
    
    ////////////////////////////////////////////////////////////
    // ArbolBinario - Atributos ////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Nodo raiz del Arbol Binario
     */
    private NodoBin<T> raiz;        
    
    

    ////////////////////////////////////////////////////////////
    // ArbolBinario - Implementación de Métodos ////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Crea un Árbol Binario vacío. <br>
     * <b>post: </b> Se creo un Árbol Binario vacío. <br>
     */
     public ArbolBinario(){
        this.raiz=null;
     }
     
    /**
     * Crea un Árbol Binario con una raíz predefinida. <br>
     * <b>post: </b> Se creo un nuevo Árbol con su raíz definida. <br>
     * @param raiz Un objeto de tipo T que representa del dato en la raíz del Árbol. <br>
     */
     public ArbolBinario(T raiz) {
        this.raiz = new NodoBin(raiz);
     }
     
    /**
     * Método que permite conocer el objeto de la raíz del Árbol Binario. <br>
     * <b>post: </b> Se obtuvo la raíz del Árbol Binario. <br>
     * @return La raíz del Árbol Binario.
     */
    public T getObjRaiz() {
        return (raiz.getInfo());
    }
     
    /**
     * Método que permite conocer la raíz del Árbol Binario. <br>
     * <b>post: </b> Se obtuvo la raíz del Árbol Binario. <br>
     * @return La raíz del Árbol Binario.
     */
    public NodoBin<T> getRaiz() {
        return raiz;
    }
    
    /**
     * Método que permite modificar la raíz del Árbol Binario. <br>
     * <b>post: </b> Se modifico la raíz del Árbol Binario. <br>
     * @param raiz Representa la nueva raíz del Árbol Binario.
     */
    public void setRaiz(NodoBin<T> raiz) {
        this.raiz = raiz;
    }
        
    /**
     * Método que permite insertar un Hijo izquierdo al elemento. <br>
     * <b>post: </b> Se inserto un hijo a la izquierda del elemento. <br>
     * @param padre Padre del nuevo elemento. <br>
     * @param hijo Elemento nuevo a insertar. <br>
     * @return False
     */
    public boolean insertarHijoIzq(T padre, T hijo){
        if(this.esVacio()){
            this.setRaiz(new NodoBin<T>(hijo));
            return (true);
        }   
        NodoBin<T> p = this.buscar(padre);
        if(p!=null){
            if(p.getIzq()==null){
                p.setIzq(new NodoBin<T>(hijo));
                return (true);
            }
            return (false);            
        }
        return (false);        
    }
    
    /**
     * Método que permite insertar un Hijo derecho al elemento. <br>
     * <b>post: </b> Se inserto un hijo a la derecha del elemento. <br>
     * @param padre Padre del nuevo elemento. <br>
     * @param hijo Elemento nuevo a insertar. <br>
     * @return false
     */
    public boolean insertarHijoDer(T padre, T hijo){
        if(this.esVacio()){
            this.setRaiz(new NodoBin<T>(hijo));
            return (true);
        }   
        NodoBin<T> p = this.buscar(padre);
        if(p!=null){
            if(p.getDer()==null){
                p.setDer(new NodoBin<T>(hijo));
                return (true);
            }
            return (false);            
        }
        return (false);  
    }
    
    /**
     * Método que permite eliminar un elemento del Árbol Binario, dada su información. <br>
     * <b>post: </b> Se elimino el elemento del Árbol Binario. <br>
     * @param info Dato que se desea eliminar del Árbol Binario. <br>
     * @return True o false dependiendo si se pudo o no eliminar el dato.
     */
    public boolean eliminar(T info){        
        NodoBin<T> r = this.buscar(info);
        if(r==null)
            return (false);        
        boolean tnd = r.getDer()!=null?true:false;
        boolean tni = r.getIzq()!=null?true:false; 
        //Caso 1: No tiene hijos
        if (!tnd && !tni)
            return eliminarC1(r);
        //Caso 2: Tiene solo hijo derecho
        if ( tnd && !tni)
            return eliminarC2(r); 
        //Caso 3: Tiene solo hijo izquierdo
        if ( !tnd && tni )
            return eliminarC2(r); 
        //Caso 4: Tiene ambos hijos
        if ( tnd && tni )
            return eliminarC3(r); 
        return false;
    } 
    
    /**
     * Método de tipo privado que permite eliminar de un Árbol Binario para el Caso 1. <br>
     * <b>post: </b> Se elimino el elemento del Árbol Binario. <br>
     * @param r Nodo que se desea eliminar del Árbol Binario. <br>
     * @return True o false dependiendo si se pudo o no eliminar el dato.
     */
    private boolean eliminarC1(NodoBin<T> r){
        NodoBin<T> p = this.getPadre(r);
        if(p==null){
            if(this.getRaiz()!=r)
                return (false);
            this.setRaiz(null);
            return (true);
        }            
        NodoBin<T> hi = p.getIzq();
        NodoBin<T> hd = p.getDer();
        if(hi==r){
            this.getPadre(r).setIzq(null);
            return true;
        } 
        if (hd==r){
            this.getPadre(r).setDer(null);
            return true;
        } 
        return (false);
    }
    
    /**
     * Método de tipo privado que permite eliminar de un Árbol Binario para el Caso 2. <br>
     * <b>post: </b> Se elimino el elemento del Árbol Binario. <br>
     * @param r Nodo que se desea eliminar del Árbol Binario. <br>
     * @return True o false dependiendo si se pudo o no eliminar el dato.
     */
    private boolean eliminarC2(NodoBin<T> r){
        NodoBin<T> p = this.getPadre(r);
        NodoBin<T> ha = r.getIzq()!=null?r.getIzq():r.getDer(); 
        if(p==null){
            this.setRaiz(ha);
            return (true);
        }
        NodoBin<T> hi = p.getIzq();
        NodoBin<T> hd = p.getDer();
        if (hi==r){
            this.getPadre(r).setIzq(ha);
            r.setDer(null);
            r.setIzq(null); 
            return true;
        } 
        if (hd==r) {
            this.getPadre(r).setDer(ha);
            r.setDer(null);
            r.setIzq(null); 
            return true;
        } 
        return false;
    }
    
    /**
     * Método de tipo privado que permite eliminar de un Árbol Binario para el Caso 3. <br>
     * <b>post: </b> Se elimino el elemento del Árbol Binario. <br>
     * @param r Nodo que se desea eliminar del Árbol Binario. <br>
     * @return True o false dependiendo si se pudo o no eliminar el dato.
     */
    private boolean eliminarC3(NodoBin<T> r){
        NodoBin<T> masIzq = this.masIzquierda(r.getDer());
        if (masIzq!=null){            
            this.eliminar((T) masIzq.getInfo());
            r.setInfo(masIzq.getInfo());            
            return (true);
        }
        return (false);
    }
    
    /**
     * Método de tipo privado que permite conocer el Nodo más a la izquierda, Caso eliminación 3. <br>
     * <b>post: </b> Se retorno el Nodo más a la izquierda que se desea eliminar. <br>
     * @param r Nodo que se desea eliminar del Árbol Binario. <br>
     * @return Nodo Binario ubicado más a la izquierda del Nodo indicado.
     */
    private NodoBin<T> masIzquierda(NodoBin<T> r) {
        if (r.getIzq()!=null){
            return (masIzquierda(r.getIzq()));
        }
        return (r);
    }
    
    /**
     * Método que retorna true si existe un dato en el Árbol binario, o false en caso contrario. <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el Árbol. <br>
     * @param info Representa la información del elemento que se desea buscar. <br>
     * @return Un boolean, true si el dato esta o false en caso contrario.
     */
     public boolean esta(T info){
        return (esta(this.raiz,info));
    }

     /**
      * Método de tipo privado que retorna true si existe un dato en el Árbol binario, o false en caso contrario. <br>
      * Es necesario para que el método funcione que los objetos almacenados en el Árbol tengan sobre escrito el método equals.<br>
      * <b>post: </b> Se retorno true si el elemento se encuentra en el Árbol.<br>
      * @param r Representa la raíz del Árbol binario, o raiz de sub_Árbol. <br>
      * @param info Representa la información del elemento que se desea buscar. <br>
      * @return True si el dato esta o false en caso contrario.
      */
    private boolean esta(NodoBin<T> r, T info){
        if(r==null)
            return (false);
        if(r.getInfo().equals(info))
            return (true);
        return(esta(r.getIzq(),info) || esta(r.getDer(),info));
    }
    
    /**
     * Método que permite consultar un elemento existente dentro del Árbol Binario. <br>
     * <b>post: </b> Se retorno un NodoBin<T> perteneciente al dato buscado. <br>
     * @param info Elemento a ubicar dentro del Árbol Binario.
     * @return Un objeto de tipo NodoBin<T> que representa el objeto buscado.
     */
    private NodoBin<T> buscar(T info){
        return (buscar(this.raiz,info));
    }
   
    /**
     * Método que permite consultar un elemento existente dentro del Árbol Binario. <br>
     * <b>post: </b> Se retorno un NodoBin<T> perteneciente al dato buscado. <br>
     * @param info Elemento a ubicar dentro del Árbol Binario. <br>
     * @param r Representa la raíz del Árbol. <br>
     * @return Un objeto de tipo NodoBin<T> que representa el objeto buscado.
     */
    private NodoBin<T> buscar(NodoBin<T> r, T info){
        if(r==null)
            return (null);
        if(r.getInfo().equals(info))
            return (r);
        else
        {
            NodoBin<T> aux=(r.getIzq()==null)?null:buscar(r.getIzq(),info);
            if(aux!=null)
                return (aux);
            else
                return (r.getDer()==null)?null:buscar(r.getDer(),info);
        }
    }
    
    /**
     * Modificar la información de un Nodo dentro del Árbol Binario. <br>
     * <b>post: </b> Se edito la información de un Nodo del Árbol. <br>
     * @param info1 Elemento dentro del Árbol Binario que se quiere modificar. <br>
     * @param info2 Nueva información del elemento que se desea modificar. <br>
     * @return Un boolean si se puede editar la información del elemento.
     */
    public boolean setDato(T info1, T info2){
        if(!this.esta(info1) || this.esta(info2))
            return (false);
        return (setDato(this.raiz, info1,info2));
    }

    /**
     * Modificar la información de un Nodo dentro del Árbol Binario. <br>
     * <b>post: </b> Se edito la información de un Nodo del Árbol. <br>
     * @param info1 Elemento dentro del Árbol Binario que se quiere modificar. <br>
     * @param info2 Nueva información del elemento que se desea modificar. <br>
     * @param r Representa la raíz del Árbol Binario. <br>
     * @return Un boolean si se puede editar la información del elemento.
     */
    private boolean setDato(NodoBin<T> r, T info1, T info2){
        if(r==null)
            return (false);        
        if(r.getInfo().equals(info1)){
            r.setInfo(info2);
            return (true);
        }
        return(setDato(r.getIzq(),info1,info2) || setDato(r.getDer(),info1,info2));
    }
    
    /**
     * Método que dado un dato almacenado en el Árbol, retorna el padre de ese dato. <br>
     * <b>post: </b> Se retorno el padre del nodo que contiene la información dada. <br>
     * @param r Dato que se desea buscar. <br>
     * @return El padre del dato almacenado en el Árbol, null en caso no existir el dato
     */ 
     protected NodoBin<T> getPadre(NodoBin<T> r){
        if(r==null || this.raiz==null)
            return null;
        NodoBin<T> x=getPadre(this.raiz,r.getInfo());
        if(x==null)
            return null;
        return(x);
     }
     
     /**
      * Método de tipo privado que dado un dato almacenado en el Árbol, retorna el padre de ese dato. Se parte
      * de la premisa que el Árbol no contiene elementos repetidos. <br>
      * <b>post: </b> Se retorno el padre del nodo que contiene la información dada. <br>
      * @param x Representa la raíz del Árbol, o raíz de sub_Árbol
      * @param info Dato que se desea buscar
      * @return El padre del dato almacenado en el Árbol, null en caso no existir el dato
      */
    private NodoBin<T> getPadre(NodoBin<T> x, T info){
        if(x==null)
            return null;
        if((x.getIzq()!=null && x.getIzq().getInfo().equals(info))|| (x.getDer()!=null && x.getDer().getInfo().equals(info)))
            return (x);
        NodoBin<T> y=getPadre(x.getIzq(),info);
        if(y==null)
            return(getPadre(x.getDer(),info));
        else
            return(y);
    }
    
    /**
     * Método que retorna un iterador con las hojas del Árbol binario. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol binario. <br>
     * @return Un iterador con las hojas del Árbol binario 
     */
    public Iterator<T> getHojas(){
        ListaCD<T> l=new ListaCD<T>();
        getHojas(this.raiz, l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un iterador con las hojas del Árbol binario. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol binario. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol. <br>
     */
    private void getHojas(NodoBin<T> r, ListaCD<T> l){
        if (r!=null){
            if(this.esHoja(r))
                l.insertarAlFinal(r.getInfo());
            getHojas(r.getIzq(), l);
            getHojas(r.getDer(), l);
        }
    }
    
    /**
     * Método de tipo privado que permite saber si un elemento es una hoja. <br>
     * <b>post: </b> Se retorno true si es nodo hoja del Árbol binario. <br>
     * @param x Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return True si sus dos hijos son null. <br>
     */
    private boolean esHoja(NodoBin<T> x){
        return (x!=null && x.getIzq()==null && x.getDer()==null);
    }
    
    /**
     * Método que permite determinar el número de Nodo hojas dentro del Árbol Binario. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol. <br>
     * @return El número de hojas existentes en el Árbol Binario.
     */
    public int contarHojas(){
        return (contarHojas(this.raiz));
    }
    
    /**
     * Método que permite determinar el número de Nodo hojas dentro del Árbol Binario. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return El número de hojas existentes en el Árbol Binario.
     */
    private int contarHojas(NodoBin<T> r){
        if(r==null)
            return (0);
        if(this.esHoja(r))
            return (1);
        int chi = contarHojas(r.getIzq());
        int chd = contarHojas(r.getDer());
        return (chi+chd);
    }
    
    /**
     *  Método que retorna un iterador con el recorrido preOrden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el Árbol. <br>
     * @return Un iterador en preorden (padre-&#62;hijoIzq-&#62;hijoDer) para el Árbol binario.
     */
     public Iterator<T> preOrden(){
         ListaCD<T> l=new ListaCD<T>();
         preOrden(this.getRaiz(),l);
         return (l.iterator());
        }

     /**
      * Método que tipo privado que retorna un iterador con el recorrido preOrden del Árbol Binario. <br>
      * <b>post: </b> Se retorno un iterador en preOrden para el Árbol. <br>
      * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
      * @param l Lista para el almacenamiento de los datos del Árbol. <br>
      */
    private void  preOrden(NodoBin<T> r, ListaCD<T> l){
        if(r!=null){
            l.insertarAlFinal(r.getInfo());
        preOrden(r.getIzq(), l);
        preOrden(r.getDer(), l);
        }
    }

   /**
     * Método que retorna un iterador con el recorrido in Orden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el Árbol. <br>
     * @return Un iterador en inOrden (hijoIzq-&#62;padre-&#62;hijoDer) para el Árbol binario. <br>
     */
    public Iterator<T> inOrden(){
        ListaCD<T> l=new ListaCD<T>();
        inOrden(this.getRaiz(),l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un iterador con el recorrido in Orden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador inOrdenpara el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol. <br>
     */
    private void  inOrden(NodoBin<T> r, ListaCD<T> l){
        if(r!=null){
            inOrden(r.getIzq(), l);
        l.insertarAlFinal(r.getInfo());
        inOrden(r.getDer(), l);
        }
    }

    /**
     * Método que retorna un iterador con el recorrido postOrden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador postOrden para el arbol.<br>
     * @return Un iterador en postOrden (hijoIzq-&#62;hijoDer-&#62;padre) para el Árbol binario. <br>
     */
    public Iterator<T> postOrden(){
        ListaCD<T> l=new ListaCD<T>();
        postOrden(this.getRaiz(),l);
        return (l.iterator());
    }

    /**
     * Metodo de tipo privado que retorna un iterador con el recorrido postOrden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador postOrden para el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol
     */
    private void  postOrden(NodoBin<T> r, ListaCD<T> l){
        if(r!=null){
            postOrden(r.getIzq(), l);
            postOrden(r.getDer(), l);
            l.insertarAlFinal(r.getInfo());
        }
    }

    /**
     * Método que permite retornar en un String el recorrido preOrden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el Árbol. <br>
     * @return Un String el recorrido preOrden del arbol
     */
    public String preOrden_Iterativo(){
        return(preOrden_Iterativo(this.raiz));
    }

    /**
     * Método de tipo privado que permite retornar en un String el recorrido preOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el arbol.<br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol
     * @return Un String el recorrido preOrden del Árbol
     */
    private String preOrden_Iterativo(NodoBin<T> r){
        Pila<NodoBin> p=new Pila<NodoBin>();
        String rr="";
        while(r!=null){
            p.apilar(r);
            rr+=r.getInfo().toString()+"-";
            r=r.getIzq();
        }
        while(!p.esVacia()){
            r=p.desapilar();
            r=r.getDer();
            while(r!=null){
                rr+=r.getInfo().toString()+"-";
                p.apilar(r);
                r=r.getIzq();
            }
        }
        return(rr);
    }

    /**
     * Método que permite retornar en un String el recorrido inOrden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador en inOrden para el Árbol. <br>
     * @return Un String el recorrido inOrden del Árbol
     */
    public String inOrden_Iterativo(){
        return(inOrden_Iterativo(this.raiz));
    }

    /**
     * Método de tipo privado que permite retornar en un String el recorrido inOrden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador en inOrden para el Árbol. <br>
     * @param r representa la raíz del Árbol, o raíz de sub_Árbol
     * @return Un String el recorrido inOrden del Árbol
     */
    private String inOrden_Iterativo(NodoBin<T> r){
        Pila<NodoBin> p=new Pila<NodoBin>();
        String rr="";
        while(r!=null){
            p.apilar(r);
            r=r.getIzq();
        }
        while(!p.esVacia()){
            r=p.desapilar();
            rr+=r.getInfo().toString()+"-";
            r=r.getDer();
            while(r!=null){
                p.apilar(r);
                r=r.getIzq();
            }
        }   
        return(rr);
    }
    
    /**
     * Método que permite retornar en un String el recorrido postOrden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador en postOrden para el Árbol. <br>
     * @return Un String el recorrido inOrden del Árbol
     */
    public String postOrden_Iterativo(){
        return(postOrden_Iterativo(this.raiz));
    }
    
    /**
     * Método de tipo privado que permite retornar en un String el recorrido postOrden del Árbol Binario. <br>
     * <b>post: </b> Se retorno un iterador en postOrden para el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return Un String el recorrido postOrden del Árbol
     */
    private String postOrden_Iterativo(NodoBin<T> r){
        Pila<NodoBin> pila = new Pila<NodoBin>();
        NodoBin<T> tope = null;
        String rr="";
        while(r!=null)
        {
            if(r.getIzq()!=null && r.getIzq()!=tope && r.getDer()!=tope)
            { 
                pila.apilar(r);
                r=r.getIzq();
            }
            else if(r.getIzq()==null && r.getDer()==null && r!=tope)
            {
                rr+=r.getInfo().toString()+"-";
                tope=r;
                if(!pila.esVacia())
                    r = pila.desapilar();
                else r =null;
            }
            else if(r.getDer()!=null && tope!=r.getDer())
            {
                pila.apilar(r);
                r=r.getDer();
            }
            else if(r.getDer()!=null && tope==r.getDer())
            {
                rr+=r.getInfo().toString()+"-";
                tope=r;                
                if(!pila.esVacia())
                    r = pila.desapilar();
                else r =null;               
            }
        }
        return (rr);
    }
    
    /**
     * Método que permite retornar un iterador con el recorrido por niveles del Árbol Binario. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Árbol Binario. <br>
     * @return Un iterador con el recorrido por niveles del Árbol Binario.
     */
    public Iterator<T> impNiveles(){
        ListaCD<T> l=new ListaCD<T>();
        if(!this.esVacio()){
            Cola<NodoBin<T>> c=new Cola<NodoBin<T>>();
            c.enColar(this.getRaiz());
            NodoBin<T> x;
                while(!c.esVacia()){
                    x=c.deColar();
                    l.insertarAlFinal(x.getInfo());
                    if(x.getIzq()!=null)
                    c.enColar(x.getIzq());
                    if(x.getDer()!=null)
                    c.enColar(x.getDer());
                }
        }
        return (l.iterator());
    }
    
    /**
     * Método que permite obtener el peso del Árbol Binario. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol Binario. <br>
     * @return Un entero con la cantidad de elementos del Árbol Binario.
     */
    public int getPeso(){
        return(getPeso(this.getRaiz()));
    }

    /**
     * Método de tipo privado que permite conocer el número de elemento del Árbol Binario. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return El número de elementos que contiene el Árbol Binario.
     */
    private int getPeso(NodoBin<T> r){
        if(r==null)
            return 0;
        return (getPeso(r.getIzq())+1+getPeso(r.getDer()));
    }
    
    /**
     * Método que permite saber si el Árbol Binario se encuentra vacío. <br>
     * <b>post: </b> Se retorno true si el Árbol no contiene elementos. <br>
     * @return True su no hay datos en el Árbol Binario.
     */
    public boolean esVacio(){
        return(this.raiz==null);
    }
    
    /**
     * Método que permite obtener la altura del Árbol Binario. <br>
     * <b>post: </b> Se retorno la altura del Árbol Binario. <br>
     * @return Un entero con la altura del Árbol Binario.
     */
    public int getAltura(){
        if(this.raiz==null)
            return (0);
        return(getAltura(this.getRaiz()));
    }

    /**
     * Método de tipo privado que permite conocer la altura del Árbol Binario. <br>
     * <b>post: </b> Se retorno la altura del Árbol Binario. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return Un entero con la altura del Árbol Binario.
     */
    private int getAltura(NodoBin<T> r){
        int ai=0, ad=0;
        if(r.getIzq()!=null)
            ai = getAltura(r.getIzq());
        if(r.getDer()!=null)
            ad = getAltura(r.getDer());
        if(ai>=ad)
            return (ai+1);
        return (ad+1);        
    }
    
    /**
     * Método de tipo privado que permite el grado del Nodo de un Árbol. <br>
     * <b>post: </b> Se retorno el grado del Nodo de Árbol Binario. <br>
     * @param info Representa la información del Nodo a consultar el Grado. <br>
     * @return Un entero con el grado del Nodo consultado.
     */
    public int getGrado(T info){
        NodoBin nodo = this.buscar(info);
        if(nodo==null)
            return (-1);
        if(this.esHoja(nodo)) 
            return (0);
        int rta = 0;
        if(nodo.getIzq()!=null)
            rta++;
        if(nodo.getDer()!=null)
            rta++;
        return (rta);
    }
    
    /**
     * Indica si el Árbol es completo. <br>
     * <b>post: </b> Se retorno true si el Árbol esta completo o false de lo contrario. <br>
     * @return True si el Árbol es completo o false de lo contrario
     */
    public boolean esCompleto( )
    {
        return(esCompleto(this.raiz));
    }
    
    /**
     * Indica si el Árbol es completo. <br>
     * <b>post: </b> Se retorno true si el Árbol esta completo o false de lo contrario. <br>
     * @return True si el Árbol es completo o false de lo contrario
     */
    private boolean esCompleto(NodoBin<T> r)
    {
        if(this.esHoja(r))
            return true;
        else if(r.getIzq()!=null && r.getDer()!=null)
            return esCompleto(r.getIzq()) && esCompleto(r.getDer());
        else
            return false;
    }
    
    /**
     * Indica si el Árbol se encuentra lleno. <br>
     * <b>post: </b> Se retorno true si el Árbol está lleno o false de lo contrario. <br>
     * @return True si el Árbol está lleno o false de lo contrario
     */
    public boolean estaLleno()
    {
        return (estaLleno(this.raiz, this.getAltura()));
    }
    
    /**
     * Indica si el Árbol se encuentra lleno. <br>
     * <b>post: </b> Se retorno true si el Árbol está lleno o false de lo contrario. <br>
     * @param alt La altura para verificar si el Árbol está lleno
     * @return True si el Árbol está lleno o false de lo contrario
     */
    private boolean estaLleno(NodoBin<T> r, int alt)
    {
        if(this.esHoja(r))
            return (alt==1);
        else if(r.getIzq()==null || r.getDer()==null)
            return false;
        else
            return estaLleno(r.getIzq(),alt-1) && estaLleno(r.getDer(),alt-1);
    }
    
    /**
     * Método que elimina las hojas (nodos terminales) del Árbol binario. <br>
     * <b>post: </b> Se eliminaron las hojas del Árbol binario. <br>
     */
    public void podar(){
        if(this.esHoja(raiz))
            this.setRaiz(null);
        podar(this.raiz);
    }

    /**
     * Método de tipo privado que elimina las hojas (nodos terminales) del Árbol binario. <br>
     * <b>post: </b> Se eliminaron las hojas del Árbol binario. <br>
     * @param x Representa la raíz del Árbol, o raíz de sub_Árbol
     */
    private void podar(NodoBin<T> x){
        if (x==null)
            return;
        if(this.esHoja(x.getIzq()))
            x.setIzq(null);
        if(this.esHoja(x.getDer()))
            x.setDer(null);
        podar(x.getIzq());
        podar(x.getDer());
    }
    
    /**
     * Método que retorna el código  Łukasiewicz del árbol binario; Este código etiqueta los nodos internos con "a" 
     * y los externos con una "b" y realiza el recorrido en preOrden con estas convenciones. <br>
     * <b>post: </b> Se retorno el código  Łukasiewicz del Árbol Binario.<br>
     * @return Un String con el código Łukasiewicz del Árbol binario
     */ 
    public String Luca()
    {
        return(Luca(this.raiz));
    }

    /**
     * Método que retorna el código Łukasiewicz del Árbol binario; Este código etiqueta los nodos internos con "a" 
     * y los externos con una "b" y realiza el recorrido en preIOrden con estas convenciones. <br>
     * <b>post: </b> Se retorno el código Łukasiewicz del Árbol binario. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol
     * @return  Un String con el código  Łukasiewicz del Árbol binario
     */
    private String Luca(NodoBin<T> r){
        if(r==null)
        return("b");
        return("a"+Luca(r.getIzq())+Luca(r.getDer()));
    }
    
    /**
     * Método que permite saber si dos Arboles Binarios son iguales de Información y estructura. <br>
     * <b>post: </b> Se retorno true si los árboles son Iguales. <br>
     * @param a2 Segundo Árbol a evaluar su igualdad con el Árbol actual. <br>
     * @return Un boolean dependiendo de si los Árboles son iguales o no.
     */
    public boolean esIgual(ArbolBinario<T> a2){
        return (esIgual(this.raiz, a2.getRaiz()));
    }
    
    /**
     * Método que permite saber si dos Arboles Binarios son iguales de Información y estructura. <br>
     * <b>post: </b> Se retorno true si los árboles son Iguales. <br>
     * @param r1 Representa el NodoBin del primer Árbol evaluado. <br>
     * @param r2 Representa el NodoBin del segundo Árbol evaluado. <br>
     * @return Un boolean dependiendo de si los Árboles son iguales o no.
     */
    private boolean esIgual(NodoBin<T> r1, NodoBin<T> r2){
        if(r1==null && r2== null)
            return (true);
        if(r1==null || r2== null)
            return (false);
        if(r1.getInfo()==r2.getInfo())
            return (esIgual(r1.getIzq(),r2.getIzq())&&esIgual(r1.getDer(),r2.getDer()));
        else 
        return (false);
    }
    
    /**
     * Método que permite saber si dos Arboles Binarios son isomorfos; Misma estructura o forma. <br>
     * <b>post: </b> Se retorno true si los árboles son isomorfos. <br>
     * @param a2 Segundo Árbol a evaluar su igualdad con el Árbol actual. <br>
     * @return Un boolean dependiendo de si los Árboles son isomorfos o no.
     */
    public boolean esIsomorfo(ArbolBinario<T> a2){
        return (esIsomorfo(this.raiz, a2.getRaiz()));
    }
    
    /**
     * Método que permite saber si dos Arboles Binarios son isomorfos; Misma estructura o forma. <br>
     * <b>post: </b> Se retorno true si los árboles son isomorfos. <br>
     * @param r1 Representa el NodoBin del primer Árbol evaluado. <br>
     * @param r2 Representa el NodoBin del segundo Árbol evaluado. <br>
     * @return Un boolean dependiendo de si los Árboles son Isomorfos o no.
     */
    private boolean esIsomorfo(NodoBin<T> r1, NodoBin<T> r2){
        if(r1==null && r2==null)
            return (true);
        if(r1==null || r2==null)
            return (false);
        return (esIsomorfo(r1.getIzq(),r2.getIzq())&&esIsomorfo(r1.getDer(),r2.getDer()));
    }
    
    /**
     * Método que permite saber si dos Arboles Binarios son semejantes; Misma información, diferente forma. <br>
     * <b>post: </b> Se retorno true si los árboles son Semejantes. <br>
     * @param a2 Segundo Árbol a evaluar su igualdad con el Árbol actual. <br>
     * @return Un boolean dependiendo de si los Árboles son semejantes o no.
     */
    public boolean esSemejante(ArbolBinario<T> a2){
        if(this.getPeso()!=a2.getPeso())
            return (false);
        return (esSemejante(a2.getRaiz()));
    }

    /**
     * Método que permite saber si dos Arboles Binarios son semejantes; Misma información, diferente forma. <br>
     * <b>post: </b> Se retorno true si los árboles son Semejantes. <br>
     * @param r1 Representa el NodoBin del primer Árbol evaluado. <br>
     * @param r2 Representa el NodoBin del segundo Árbol evaluado. <br>
     * @return Un boolean dependiendo de si los Árboles son Semejantes o no.
     */
    private boolean esSemejante(NodoBin<T> r) {
        if(r==null)
            return (true);
        if(!this.esta(r.getInfo()))
            return (false);
        return (esSemejante(r.getIzq())&&esSemejante(r.getDer()));
    }
    
     /**
     * Método que permite conocer por consola la información del Árbol Binario.
     */
    public void imprime(){
        System.out.println(" ----- Arbol Binario ----- ");
        this.imprime(this.raiz);
    }
    
    /**
     * Método de tipo privado que permite mostrar por consola la información del Árbol Binario. <br>
     * @param n Representa la raíz del Árbol Binario o de alguno de sus sub_Árboles.
     */
    public void imprime(NodoBin<T> n) {
        T l = null;
        T r = null;
        if(n==null)
            return;
        if(n.getIzq()!=null) {
         l = n.getIzq().getInfo();
        }
        if(n.getDer()!=null) {
         r =n.getDer().getInfo();
        }       
        System.out.println("NodoIzq: "+l+"\t Info: "+n.getInfo()+"\t NodoDer: "+r);
        if(n.getIzq()!=null) {
         imprime(n.getIzq());
        }
        if(n.getDer()!=null) {
         imprime(n.getDer());
        }
    }
    
    /**
     * Método que permite clonar la información de un Árbol Binario Búsqueda. <br>
     * @return Un objeto de tipo Árbol_Binario_Busqueda con la información duplicada del Árbol.
     */
    public ArbolBinarioBusqueda<T> clonar(){
        ArbolBinarioBusqueda<T> t=new ArbolBinarioBusqueda<T>();
        t.setRaiz(clonarAB(this.getRaiz()));
        return(t);
    }


    private NodoBin<T> clonarAB(NodoBin<T> r){				
        if(r==null)
            return r;
        else
        {
            NodoBin<T> aux=new NodoBin<T>(r.getInfo(), clonarAB(r.getIzq()), clonarAB(r.getDer()));
            return aux;
        }
    }
   
}//Fin de la Clase Árbol_Binario
