/**
 * ---------------------------------------------------------------------
 * $Id: ArbolBin.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de Clase que contiene la información de los Nodos de Árbol Binario.
 * @param <T> Tipo de datos a almacenar en el Nodo.
 * @author Marco Adarme
 * @version 2.0
 */
public class NodoBin<T>{
    
    ////////////////////////////////////////////////////////////
    // NodoBin - Atributos /////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Información de nodo binario 
     */
    private T info;
    
    /**
     * Hijo izquierdo del nodo binario
     */
    private NodoBin<T> izq;
    
    /**
     * Hijo derecho del nodo binario
     */
    private NodoBin<T> der;
    
    ////////////////////////////////////////////////////////////
    // NodoBin - Atributos /////////////////////////////////////
    ////////////////////////////////////////////////////////////  
    
    /**
     * Constructor vacío de la clase. <br>
     * <b>post: </b> Se construyo un Nodo Binario vacío con la información y sus hijos en NULL. <br>
     */
    public NodoBin() {
        this.info=null;
        this.der=null;
        this.izq=null;
    }
    
    /**
     * Constructor con parámetros de la clase. <br>
     * <b>post: </b> Se construyo un NodoBin con la información dada. <br>
     * @param x Es de tipo T el cual posee la información del nodo del Árbol. <br>
     * @param i Es de tipo Nodo&#60;T&#62; el cual posee el nodo del lado izquierdo. <br>
     * @param d Es de tipo Nodo&#60;T&#62; el cual posee el nodo del lado derecho
     */
    public NodoBin(T x, NodoBin<T> i, NodoBin<T> d) {
        this.info=x;
        this.izq=i;
        this.der=d;
    }
    
    /**
     * Constructor con parámetros de la clase que genera una hoja del Árbol. <br>
     * <b>post: </b> Se construyo un Nodo que contiene la información dada y sus hijos en null. <br>
     * @param x Representa la información del nodo hoja del Árbol
     */
    public NodoBin(T x){        
        this.info=x;
        this.izq=this.der=null;        
    }
    
    /**
     * Método que permite obtener la información del Nodo Binario. <br>
     * <b>post: </b> Se retorno la información del Nodo Binario. <br>
     * @return un tipo T que contiene la información del Nodo Binario
     */
    public T getInfo(){
        return this.info;
    }
    
    /**
     * Método que permite obtener el hijo izquierdo del Nodo Binario. <br>
     * <b>post: </b> Se retorno el hijo izquierdo del Nodo Binario. <br>
     * @return Un tipo NodoBin&#60;T&#62; que contiene la información del hijo izquierdo del Nodo Binario
     */
    public NodoBin<T> getIzq() {
        return this.izq;
    }
    
    /**
     * Método que permite obtener el hijo derecho del Nodo Binario. <br>
     * <b>post: </b> Se retorno el hijo derecho del Nodo Binario. <br>
     * @return un tipo NodoBin&#60;T&#62; que contiene la información del hijo derecho del Nodo Binario
     */
    public NodoBin<T> getDer() {
        return this.der;
    }
    
    /**
     * Método que permite cambiar la información contenida en el Nodo Binario. <br>
     * <b>post: </b> Se edito la información del Nodo Doble. <br>
     * @param info De tipo T y contiene la información del nodo
     */
    public void setInfo(T info) {
        this.info=info;
    }
    
    /**
     * Método que permite modificar el hijo izquierdo del Nodo Binario. <br>
     * <b>post: </b> Se retorno el nuevo hijo izquierdo del Nodo Binario. <br>
     * @param i Es de tipo NodoB&#60;T&#62; que contiene el Nodo izquierdo
     */
    public void setIzq(NodoBin<T> i) {
        this.izq=i;
    }
    
    /**
     * Método que permite modificar el hijo derecho del Nodo Binario. <br>
     * <b>post: </b> Se retorno el nuevo hijo derecho del Nodo Binario. <br>
     * @param d Es de tipo NodoN&#60;T&#62; que contiene el nodo derecho
     */
    public void setDer(NodoBin<T> d) {
        this.der=d;
    }
    
}//Fin de la Clase NodoBin

