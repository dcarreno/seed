/**
 * ---------------------------------------------------------------------
 * $Id: TablaHash.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de Clase para el manejo de Tablas Hash. <br>
 * @param <Clave> variable
 * @param <T> Tipo de datos a almacenar dentro de la Tabla Hash. <br>
 * @author Yulieth Pabon
 * @version 1.0
 */

public class TablaHash <Clave, T > {
    
    ////////////////////////////////////////////////////////////
    // TablaHash - Atributos ////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Número de datos almacenados en la tabla
     */
    private int numeroDatos;
    
    /**
     * El tamaño de slots de la tabla
     */
    private int numeroSlots;
    
    /**
     * Lista de objetos a almacenar en la tabla
     */
    private ListaCD<InformacionDeEntrada <Clave,T>> [ ] informacionEntrada;

    ////////////////////////////////////////////////////////////
    // TablaHash - Implementación de Métodos ////////////////////
    ////////////////////////////////////////////////////////////  
    
    /**
     * Constructor de una tabla hash con 23 slots. <br>
     * <b> post:</b> Se creo una tabla hash con 23 slots. <br>
     */
    public TablaHash() {
        this.numeroDatos = 0;
        this.numeroSlots = 11;
         this.informacionEntrada = new ListaCD [this.numeroSlots ];
         // inicializar los Slots de la tabla 
         this.inicializarListas( );
    }
    
    /**
     * Constructor de una tabla hash con numero Slots de la tabla específicos. <br>
     * <b> post:</b> Se creo tabla hash con número_Slots de la tabla específicos. <br>
     * @param numeroSlots Numero que representa los slots de la tabla. <br>
     */
    public TablaHash( int numeroSlots) {
        this.numeroDatos =0;
        this.numeroSlots = numeroSlots;
        this.informacionEntrada =  new ListaCD [numeroSlots];
         // inicializar los Slots de la tabla 
         this.inicializarListas( );
    }
     /**
      * Método que permite insertar o modificar en la tabla un objeto con su respectiva clave. <br>
     * <b> post:</b> Se inserto o modifico un objeto en la tabla fragmentada. <br>
     * @param clave Representa la clave del objeto a insertar o modificar. <br>
      * @param objeto Representa el objeto a insertar en la tabla. <br>
      * @return El objeto insertado.
      */ 
    public T insertar( Clave clave, T objeto ) {
        int indice=0;
        InformacionDeEntrada<Clave,T> objetoAnterior=null;
        if(clave==null){
            throw new RuntimeException("La Clave de Objeto no puede ser vacia!!!");
        }
        else{
            indice =index(clave);
            objetoAnterior = this.registrarEntrada(indice, clave );
            if( objetoAnterior== null ){ // Si la clave del objeto no se encuentra en la tabla lo insertamos
                InformacionDeEntrada<Clave,T> nuevoObjeto = new InformacionDeEntrada( clave, objeto );
                this.informacionEntrada[ indice ].insertarAlFinal(nuevoObjeto);
                this.numeroDatos+=1;
                return nuevoObjeto.getObjeto();
            }
            else  // si la clave esta se encuentra en la tabla modificamos el objeto
                 objetoAnterior.setObjeto( objeto);
        }
            return (T)objetoAnterior.getObjeto();
    }

    /**
     * Método que permite eliminar un objeto entrada de la tabla fragmentada. <br>
     * <b> post:</b> Se elimino el objeto en la tabla fragmentada. <br>
     * @param clave Representa la clave del objeto que se desea eliminar. <br>
     * @return El objeto que se eliminó o null en caso de que no exista en la tabla un objeto con esa clave.
     */
    public T eliminar( Clave clave ) {
        int i=0;
        InformacionDeEntrada objeto;
           if(clave==null){
               throw new RuntimeException("La Clave de Objeto no puede ser vacia!!!");
           }
           else{
                int indice =index(clave);
               ListaCD<InformacionDeEntrada<Clave,T>> listaObjeto = this.informacionEntrada[ indice ];
               objeto = new InformacionDeEntrada( clave );
               i=listaObjeto.getIndice(objeto);
                    if(i==-1)
                        objeto=null;
                    else{
                       objeto = ( InformacionDeEntrada )listaObjeto.eliminar(i);
                       this.numeroDatos--;
                    }
               }
               return (T)objeto.getObjeto();
       }
    
    /**
     * Método que permite conocer si se encuentra un objeto asociado con la clave dada. <br>
     * <b>post:</b> Se consulto si se encuentra un objeto asociado con la clave dada. <br>
     * @param clave Dato a verificar si se encuentra en la tabla. <br>
     * @return true De encontrar un objeto asociado con la clave dada. <br>
     */
    public boolean esta(Clave clave){
        return(this.getObjeto(clave)!=null);
    }

    /**
     * Método que permite obtener el objeto asociado con la clave especificada. <br>
     * <b> post:</b> Se obtuvo el objeto de la tabla fragmentada, que posee esa clave . <br>
     * @param clave Representa la clave del objeto que se desea obtener. <br>
     * @return El objeto asociado con la clave o null si no existe objeto con esa clave.
     */
     public Object getObjeto( Clave clave ) {
         InformacionDeEntrada objeto;
        if ( clave == null )
            throw new IllegalArgumentException("Clave null no permitida" );
        else{
            int indice =index(clave);
            ListaCD<InformacionDeEntrada<Clave,T>> listaObjeto = this.informacionEntrada[ indice ];
            objeto= new InformacionDeEntrada( clave );
            int i=listaObjeto.getIndice(objeto);
             if(i==-1)
                    return null;
             else{
                objeto = listaObjeto.get(i);
              }
          }
          return objeto.getObjeto();
      }
     
    /**
    * Método que permite registrar una entrada con la clave especificada dentro del slot indicado en la tabla. <br>
    * <b>pre: </b> clave!=null, indice!=null, indice>=0. <br>
    * <b>post: </b> Se registro la entrada en el slot de la tabla indicado. <br>
    * @param índice Representa el índice de la tabla, donde se registrará la clave con su respectivo objeto. <br>
    * @param clave Representa la clave del objeto a registrar en la tabla. <br>
    * @return La nueva entrada a la tabla o null de no encontrar una entrada asociada a la clave del objeto.
    */
    private InformacionDeEntrada registrarEntrada( int indice, Clave clave ){
        InformacionDeEntrada<Clave,T> objeto = new InformacionDeEntrada( clave );
        ListaCD<InformacionDeEntrada<Clave,T>> listaEntradas = this.informacionEntrada[ indice ];//Slot de la tabla donde deberia encontrarse el objeto
        int i=listaEntradas.getIndice(objeto);
        if(i==-1)
            objeto=null;
        else
            objeto = listaEntradas.get(i);
        return objeto;
    }
    
    /**
    * Método que permite dispersar el código hash de la clave específica, para garantizar una mejor distribución en la asignación de las entradas. <br>
    * <b>pre: </b> hcode!=null. <br>
    * <b> post:</b> Se obtuvo el índice de asignación de slot en la tabla para la clave de la entrada. <br>
    * @param clave Variable
    * @return Índice de asignación de slot en la tabla para la clave de la entrada. <br>
    */
    public int index( Clave clave ){
        int hcode=clave.hashCode();
        double num= ((Math.sqrt(5.0) - 1.0)/2.0);//numero que se utiliza para mejor distribucion de las entradas en la tabla hash.
        double t = (Math.abs( hcode ) * num);//(this.numeroDatos+1); //valor absoluto de hash de objeto 
        return ((int) ((t - (int)t) *( this.numeroSlots) ));
    }
        
    /**
     * Método que permite eliminar las entradas de la tabla hash. <br>
     * <b> post:</b> Se eliminaron todos los datos antes almacenados en la tabla. <br>
     */
    public void eliminarTodo(){
        this.numeroDatos = 0;
        for ( int i = 0; i < this.informacionEntrada.length; i++ )
            this.informacionEntrada[ i ] = null;
    }

    /**
     * Método que permite obtener el numero de objetos almacenados en la tabla hash. <br>
     * <b> post:</b> Se obtuvo el numero de objetos almacenados en la tabla hash. <br>
     * @return El numero de objetos almacenados en la tabla hash.
     */
    public int getNumeroDatos() {
        return numeroDatos;
    }

    /**
     * Método que permite obtener el numero de slots de la tabla hash. <br>
     * <b> post:</b> Se obtuvo el numero de slots de la tabla hash. <br>
     * @return El numero de slots de la tabla hash.
     */
    public int getNumeroSlots() {
        return numeroSlots;
    }

    /**
     * Método que permite obtener el listado de los objetos de entrada de la tabla hash. <br>
     * <b> post:</b> Se obtuvo el listado de los objetos de entrada de la tabla hash. <br>
     * @return El listado de los objetos de entrada de la tabla hash.
     */
    public ListaCD<InformacionDeEntrada<Clave, T>>[] getInformacionEntrada() {
        return informacionEntrada;
    }

    /**
     * Método que permite determinar el número de Slots ocupados en la Tabla. <br>
     * @return El numero de slots ocupados en la tabla en un entero.
     */
    public int numSlotOcupados(){
        int i=0;
        int cant=0;
        while(i<this.numeroSlots){
            if(!this.informacionEntrada[i].esVacia())
                cant++;
                i++;
        }
        return cant;
    }
    
    /**
     * método que modificar el numero de slots de la tabla hash. <br>
     * <b> post:</b> Se modifico el numero de slots de la tabla hash. <br>
     * @param numeroSlots Nuevo tamanio de la tabla hash. <br>
     */
    public void setNumeroSlots(int numeroSlots) {
        this.numeroSlots = numeroSlots;
    }

     /**
     * Método que permite modificar el listado de los objetos de entrada de la tabla hash. <br>
     * <b> post:</b> Se modifico el listado de los objetos de entrada de la tabla hash. <br>
     * @param informacionEntrada el nuevo listado de los objetos de entrada de la tabla hash.
     */
    public void setInformacionEntrada(ListaCD<InformacionDeEntrada<Clave, T>>[] informacionEntrada) {
        this.informacionEntrada = informacionEntrada;
    }
    
    /**
     * Inicializa las listas que representan los Slots de la tabla de hashing. <br>
     * <b>pre: </b> informaciónEntradas!=null. <br>
     * <b>post: </b> Se inicializaron las listas que representan los slots.
     */
    private void inicializarListas( ){
        for( int i = 0; i < this.informacionEntrada.length; i++ ){
            this.informacionEntrada[i] = new ListaCD( );
        }
    }
 
    /**
     * Método que permite obtener un numero primo cercano al valor dado. <br>
     * <b>post: </b> Se obtuvo un numero primo cercano al valor dado.
     * @param numero Representa el valor a obtener el numero primo más cercano
     * @return Un numero primo cercano al valor dado.
     */
    private int obtenerPrimo( int numero ) {
        int primo = numero -1;
        while( !esPrimo( primo ) ){
            primo+=2;
        }
        return primo;
    }
    
   /**
    *  método que permite verificar si el numero dado es un numero primo. <br>
    * <b>pre: </b> Todos los números deben tener un divisor primo menor que su raíz cuadrada. <br>
    * <b>post: </b> Se verifico si el número dado es primo. <br>
    * @param numero Valor a verificar si es primo. <br>
    * @return true Si el número es primo.
    */
    public boolean esPrimo( int numero ){
        boolean esPrimo = false;
        int raizCuadrada = ( int )Math.sqrt( numero );
        for( int i = 3; i <= raizCuadrada && !(esPrimo); i+= 2 )   {
            if( numero % i != 0 ){
                esPrimo = true;
            }
        }
        return esPrimo;
    }

    /**
     * método que permite imprimir los datos almacenados en la tabla. <br>
     *  <b>post: </b> Se retorno una cadena de caracteres que representan los datos almacenados en la tabla. <br>
     * @return Cadena de caracteres que representan los datos almacenados en la tabla.
     */
    public String imprimir(){
        String msg="";
         for ( int i = 0; i < this.informacionEntrada.length; i++ )
            if(this.informacionEntrada[ i ] != null)
                msg+="Slot de la tabla numero"+i+" ==>"+this.informacionEntrada[ i ].toString()+"\n";
         return msg;
    }

    /**
     * Método que permite determinar si la Tabla se encuentra vacía. <br>
     * @return Un objeto de tipo boolean con true si la tabla se encuentra vacía.
     */
    public boolean esVacia() {
        return (this.numeroDatos==0);
    }

}//Fin de la clase TablaHash



