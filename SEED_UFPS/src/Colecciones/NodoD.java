/**
 * ---------------------------------------------------------------------
 * $Id: NodoD.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de clase que contiene la información de los nodos de una lista Doble
 * @param <T> Tipo de datos a almacenar dentro del Nodo Doble.
 * @author Marco Adarme
 * @version 2.0
 */

public class NodoD<T> implements java.io.Serializable
{
    
    ////////////////////////////////////////////////////////////
    // NodoD - Atributos ///////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Información del Nodo
     */
    private T info;      
    
    /**
     * Nodo Anterior 
     */
    private NodoD<T> ant;   
    
    /**
     * Nodo Siguiente 
     */
    private NodoD<T> sig;   

       
    
    ////////////////////////////////////////////////////////////
    // NodoD - Implementación de Métodos ///////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Constructor de un Nodo Doble vacío. <br>
     * <b>post: </b> Se construyo un Nodo Doble vacío con la información en NULL.
     */
    public NodoD() {
        this.info=null;
        this.ant=null;
        this.sig=null; 
    }

    /**
     * Constructor con parámetros de la clase NodoD <br>
     * <b>post: </b> Se construyo un nodo doble con los datos especificados. <br>
     * @param info De tipo T y contiene la información del nodo
     * @param sig Es del tipo NodoD&#60;T&#62; y contiene la dirección del Nodo siguiente
     * @param ant Es de tipo NodoD&#60;T&#62; y contiene la dirección del Nodo anterior
     */
    public NodoD(T info, NodoD<T> sig, NodoD<T> ant){        
        this.info=info;
        this.sig=sig;
        this.ant=ant;        
    }
    
    /**
     * Método que permite obtener el contenido del Nodo doble<br>
     * <b>post: </b> Se retorno la información del Nodo doble. <br>
     * @return un tipo T que contiene la información del Nodo doble
     */
    protected T getInfo(){        
        return(this.info);        
    }
    
    /**
     * Método que permite obtener la dirección del Nodo anterior al actual<br>
     * <b>post: </b> Se retorno la información del Nodo anterior al actual. <br>
     * @return un Nodo&#60;T&#62; que contiene 
     */
    protected NodoD<T> getAnt(){        
        return (this.ant);        
    }
    
    /**
     * Método que permite devolver el nodo siguiente al que apunta el Nodo doble<br>
     * <b>post: </b> Se retorno la información del Nodo siguiente al actual. <br>
     * @return un tipo NodoD&#60;T&#62; que contiene el nodo siguiente
     */
    protected NodoD<T> getSig(){        
        return (this.sig);        
    }
    
    /**
     * Método que permite cambiar la información contenida en el Nodo doble<br>
     * <b>post: </b> Se edito la información del Nodo Doble. <br>
     * @param info Es de tipo T y contiene la información nueva del nodo doble
     */
    protected void setInfo(T info){        
        this.info = info;            
    }

    /**
     * Método que permite editar la dirección del nodo anterior por una nueva<br>
     * <b>post: </b> Se edito la información del Nodo anterior del nodo actual. <br>
     * @param ant Es de tipo NodoD&#60;T&#62; y contiene la nueva dirección del nodo anterior
     */
    protected void setAnt(NodoD<T> ant){        
        this.ant=ant;        
    }
    
    /**
     * Método que permite cambiar el nodo siguiente del Nodo doble actual<br>
     * <b>post: </b> Se edito la información del Nodo siguiente del nodo actual. <br>
     * @param sig Es de tipo NodoD&#60;T&#62; y contiene la información del nuevo nodo siguiente
     */
    protected void setSig(NodoD<T> sig){        
        this.sig=sig;	        
    }

} //Fin de la Clase NodoD
