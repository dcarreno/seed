/**
 * ---------------------------------------------------------------------
 * $Id: ArbolEnerio.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

import java.util.Iterator;

/**
 * Implementación de Clase para el manejo de un Árbol Eneario.
 * @param <T> Tipo de datos a almacenar en el Árbol Eneario.
 * @author Uriel García
 * @version 1.0
 */
public class ArbolEneario<T>{    
    
    ////////////////////////////////////////////////////////////
    // ArbolEneario - Atributos ////////////////////////////////
    ////////////////////////////////////////////////////////////
 
    /**
     * Nodo raiz del Arbol Eneario
     */
    private NodoEneario<T> raiz;
    
    
    
    ////////////////////////////////////////////////////////////
    // ArbolEneario - Implementación de Métodos ////////////////
    ////////////////////////////////////////////////////////////

    /**
     * Crea un Árbol Eneario vacío. <br>
     * <b>post: </b> Se creo un Árbol Eneario vacío. <br>
     */
    public ArbolEneario(){
        this.raiz=null;
    }
    
    /**
     * Crea un Árbol Eneario con una raíz predefinida. <br>
     * <b>post: </b> Se creo un nuevo Árbol con su raíz definida. <br>
     * @param raiz Un objeto de tipo T que representa del dato en la raíz del Árbol.
     */
    public ArbolEneario(NodoEneario<T> raiz){
        this.raiz=raiz;
    }
    
    /**
     * Método que permite conocer el objeto en la raíz del Árbol Eneario. <br>
     * <b>post: </b> Se obtuvo la raíz del Árbol Eneario.<br>
     * @return Un objeto de tipo NodoEneario&#60;T&#62; que es la raíz del Árbol Eneario.
     */
    public T getObjRaiz() {
        return raiz.getInfo();
    }

    /**
     * Método que permite conocer la raíz del Árbol Eneario. <br>
     * <b>post: </b> Se obtuvo la raíz del Árbol Eneario.<br>
     * @return Un objeto de tipo NodoEneario&#60;T&#62; que es la raíz del Árbol Eneario.
     */
    public NodoEneario<T> getRaiz() {
        return raiz;
    }

    /**
     * Método que permite modificar la raíz del Árbol Eneario. <br>
     * <b>post: </b> Se modifico la raíz del Árbol Eneario. <br>
     * @param raiz Objeto de tipo NodoEneario&#60;T&#62; que representa la nueva raíz del Árbol.
     */
    public void setRaiz(NodoEneario<T> raiz) {
        this.raiz = raiz;
    }
    
    /**
     * Método que permite insertar un nuevo NodoEneario como hijo de un NodoEneario. <br>
     * <b>post: </b> Se inserto un nuevo dato como hijo del Nodo indicado. <br>
     * @param padre Es de tipo T y representa el dato del padre del nuevo NodoEneario. <br>
     * @param dato Es de tipo T y representa el nuevo dato a insertar en el Árbol Eneario. <br>
     * @return Un valor de tipo boolean que representa el éxito de la operación indicada o la razón del error.
     */
    public boolean insertarHijo(T padre, T dato){
       NodoEneario<T> nuevo = new NodoEneario(dato); 
       //El arbol se encuentra vacio
       if(this.esVacio()){
           this.setRaiz(nuevo);
           return (true); 
       }
       NodoEneario p = this.buscar(padre);
       NodoEneario n = this.buscar(dato);
       if(n!=null || p==null)
           return (false); //Ya existe dato o padre no existe
       if(this.esHoja(p)){
           p.setHijo(nuevo);
           return (true); 
       }
       NodoEneario<T> q = p.getHijo();
       p.setHijo(nuevo);
       nuevo.setHermano(q);
       return (true);
    }
    
    /**
     * Método que permite insertar un nuevo NodoEneario como hermano de un NodoEneario. <br>
     * <b>post: </b> Se inserto un nuevo dato como hermano del Nodo indicado. <br>
     * @param hermano Es de tipo T y representa el dato del hermano del nuevo NodoEneario. <br>
     * @param dato Es de tipo T y representa el nuevo dato a insertar en el Árbol Eneario. <br>
     * @return Un valor de tipo boolean que representa el éxito de la operación indicada o la razón del error.
     */
    public boolean insertarHermano(T hermano, T dato){
       NodoEneario<T> nuevo = new NodoEneario(dato); 
       //El arbol se encuentra vacio
       if(this.esVacio()){
           this.raiz = nuevo;
           return (true); 
       }
       NodoEneario h = this.buscar(hermano);
       NodoEneario n = this.buscar(dato);
       //Si es la raiz
       if(this.raiz==h || h==null || n!=null)
           return (false); //Hermano de la raiz, hermano Null o ya existe
       NodoEneario<T> sigH = h.getHermano();
       h.setHermano(nuevo);
       nuevo.setHermano(sigH);
       return (true);
    }
    
    /**
     * Método que permite eliminar un dato del Árbol Eneario. <br>
     * <b>post: </b> Se elimino un dato del Árbol Eneario. <br>
     * @param dato Representa la información del dato que se desea eliminar del Árbol. <br>
     * @return Un objeto de tipo boolean con true si se eliminó el dato y false en caso contrario.
     */
    public boolean eliminar(T dato){
        if(!this.esta(dato))
            return (false);
        return (elimina(dato));
    }
    
    /**
     * Método de tipo privado que permite eliminar un dato del Árbol Eneario. <br>
     * <b>post: </b> Se elimino un dato del Árbol Eneario. <br>
     * @param dato Representa la información del dato que se desea eliminar del Árbol. <br>
     * @return Un objeto de tipo boolean con true si se eliminó el dato y false en caso contrario.
     */
    private boolean elimina(T dato){
        NodoEneario<T> n,h,p,s;
        n = this.getPadre(dato); 
        //Es un hijo (Primer hijo a la izquierda)
        if(n!=null){
            h = n.getHijo();
            //Si tiene hijos, subo al primer hijo
            if(h.getHijo()!=null){
                s = h.getHijo();
                n.setHijo(s);
                p = s;
                while(s!=null){
                    p = s;
                    s = s.getHermano();
                }
                p.setHermano(h.getHermano()); 
            }else{
                n.setHijo(h.getHermano());
            }           
            return (true);
        }
        //Si es un hermano (Es un hijo pero no el primero)
        n = this.getHermano(dato);
        if(n!=null){
            h = n.getHermano();
            //Si tiene hijos, subo al primer hijo
            if(h.getHijo()!=null){
                s = h.getHijo();
                n.setHermano(s);
                p = s;
                while(s!=null){
                    p = s;
                    s = s.getHermano();
                }
                p.setHermano(h.getHermano());
            }else{
                n.setHermano(n.getHermano().getHermano());
            }
            return (true);
        }
        //Si es la raíz del Árbol
        return (eliminaR(dato));
    }
    
    /**
     * Método de tipo privado que permite eliminar la raíz del Árbol Eneario, estructurando sus datos. <br>
     * <b>post: </b> Se elimino un dato que es la raíz del Árbol Eneario. <br>
     * @param dato Representa la información del dato que se desea eliminar del Árbol. <br>
     * @return Un objeto de tipo boolean con true si se eliminó el dato y false en caso contrario.
     */
    private boolean eliminaR(T dato){
        if(this.raiz.getInfo()!=dato)
            return (false);
        NodoEneario<T> h,p,s;
        this.setRaiz(this.raiz.getHijo());
        //Si tenia hijos
        if(this.raiz!=null){
            h = this.raiz.getHijo();
            s = this.raiz.getHermano();
            //Si no tiene hijos
            if(h==null){
                this.raiz.setHijo(s);
            }else{
                p = h;
                while(h!=null){
                    p = h;
                    h = h.getHermano();
                }
                p.setHermano(s);
            }
        }
        return (true);
    }
    
    /**
     * Método que permite conocer el NodoEneario padre de un dato dentro del Árbol. <br>
     * <b>post: </b> Se retorno el padre del elemento indicado. <br>
     * @param info Representa el dato del cual se quiere conocer el NodoEneario padre. <br>
     * @return Un objeto de tipo NodoEneario<T> que representa el padre del dato consultado.
     */
    private NodoEneario<T> getPadre(T info){
        return(gPadre(this.raiz,null,info));
    }
    
    /**
     * Método de tipo privado que permite conocer el NodoEneario padre de un dato dentro del Árbol. <br>
     * <b>post: </b> Se retorno el padre del elemento indicado. <br>
     * @param r Representa la raíz del Árbol Eneario o un sub_Árbol del mismo. <br>
     * @param t Representa el padre del NodoEneario evaluado. Si es la raíz es NULL. <br>
     * @param dato Representa el dato del cual se quiere conocer el NodoEneario padre. <br>
     * @return Un objeto de tipo NodoEneario<T> que representa el padre del dato consultado.
     */
    private NodoEneario<T> gPadre(NodoEneario<T> r, NodoEneario<T> t, T dato){
        NodoEneario<T> q, s;
        if(r==null)
            return (null);
        if(r.getInfo().equals(dato)){
            return (t);
        }
        q = r.getHijo();
        while(q != null){
            s = gPadre(q,r,dato);
            if(s != null){
                return (s);
            } 
            r = null;
            q = q.getHermano();
        }
        return (null);
    }
    
    /**
     * Método que permite conocer el NodoEneario hermano a la izquierda de un dato dentro del Árbol. <br>
     * <b>post: </b> Se retorno el hermano del elemento indicado. <br>
     * @param info Representa el dato del cual se quiere conocer el NodoEneario hermano. <br>
     * @return Un objeto de tipo NodoEneario<T> que representa el hermano del dato consultado.
     */
    private NodoEneario<T> getHermano(T info){
        return(gHermano(this.raiz,null,info));
    }
    
    /**
     * Método de tipo privado que permite conocer el NodoEneario hermano de un dato dentro del Árbol. <br>
     * <b>post: </b> Se retorno el hermano del elemento indicado. <br>
     * @param r Representa la raíz del Árbol Eneario o un sub_Árbol del mismo. <br>
     * @param h Representa el hermano del NodoEneario evaluado. Si es la raíz es NULL. <br>
     * @param dato Representa el dato del cual se quiere conocer el NodoEneario hermano. <br>
     * @return Un objeto de tipo NodoEneario<T> que representa el hermano del dato consultado.
     */
    private NodoEneario<T> gHermano(NodoEneario<T> r, NodoEneario<T> h, T dato){
        NodoEneario<T> p=null, q, s;
        if(r==null)
            return (null);
        if(r.getInfo().equals(dato)){
            return (h);
        }
        q = r.getHijo();
        while(q != null){
            s = gHermano(q,p,dato);
            if(s != null){
                return (s);
            }
            p = q;
            q = q.getHermano();
        }
        return (null);
    }
    
    /**
     * Método que permite evaluar la existencia un dato dentro del Árbol Eneario. <br>
     * <b>post: </b> Se evaluó la existencia de un dato dentro del Árbol. <br>
     * @param dato Representa el dato que se quiere localizar dentro del Árbol Eneario. <br>
     * @return Un objeto de tipo boolean que contiene un true si ubico el dato y false en caso contrario.
     */
    public boolean esta(T dato){
        if(this.esVacio())
            return (false);
        boolean rta = (this.esta(this.raiz,dato));
        return rta;
    }
    
    /**
     * Método que permite evaluar la existencia un dato dentro del Árbol Eneario. <br>
     * <b>post: </b> Se evaluó la existencia de un dato dentro del Árbol. <br>
     * @param r Representa la raíz del Árbol Eneario en el que se buscara el dato. <br>
     * @param dato Representa el dato que se quiere localizar dentro del Árbol Eneario. <br>
     * @return Un objeto de tipo boolean que contiene un true si ubico el dato y false en caso contrario.
     */
    private boolean esta(NodoEneario<T> r, T dato){        
       NodoEneario<T> q;
       boolean s;
       if(r==null)
           return (false);
       if(r.getInfo().equals(dato))
           return (true);
       q = r.getHijo();
       while(q != null){
           s = esta(q, dato);
           if(s)
               return (true);
           q = q.getHermano();
       }
       return (false);
    }
    
    /**
     * Método que permite buscar un dato dentro del Árbol Eneario y retornar el Nodo que lo contiene. <br>
     * <b>post: </b> Se retorno el NodoEneario<T> que representa la ubicación del dato en el Árbol. <br>
     * @param dato Representa el dato que se quiere localizar dentro del Árbol Eneario. <br>
     * @return Un objeto de tipo NodoEneario<T> que representa la ubicación del dato dentro del Árbol.
     */
    private NodoEneario<T> buscar(T dato){
        if(this.esVacio())
            return (null);
        return (this.buscar(this.raiz,dato));
    }
    
    /**
     * Método que permite buscar un dato dentro del Árbol Eneario y retornar el Nodo que lo contiene. <br>
     * <b>post: </b> Se retorno el NodoEneario<T> que representa la ubicación del dato en el Árbol. <br>
     * @param r Representa la raíz del Árbol Eneario en el que se buscara el dato. <br>
     * @param dato Representa el dato que se quiere localizar dentro del Árbol Eneario. <br>
     * @return Un objeto de tipo NodoEneario<T> que representa la ubicación del dato dentro del Árbol.
     */
    private NodoEneario<T> buscar(NodoEneario<T> r, T dato){
       NodoEneario<T> q, s;
       if(r==null)
           return (r);
       if(r.getInfo().equals(dato))
           return (r);
       q = r.getHijo();
       while(q != null){
           s = buscar(q, dato);
           if(s != null){
               return (s);
           }
           q = q.getHermano();
       }
       return (null);
    }
    
    /**
     * Método que permite conocer los hijos de un dato insertado dentro del Árbol y retornarlos en un Iterator. <br>
     * <b>post: </b> Se retorno un Iterador con los hijos del dato evaluado. <br>
     * @param padre Representa el dato del cual se quieren conocer los hijos insertados en dicho sub_Árbol. <br>
     * @return Un objeto de tipo Iterator con los hijos del dato evaluado.
     */
    public Iterator<T> getHijos(T padre){
       ListaCD<T> l=new ListaCD<T>();
       NodoEneario p = this.buscar(this.raiz, padre);
       if(p==null)
           return (l.iterator()); //Este Nodo no existe
       NodoEneario q = p.getHijo();
       while(q!=null){
           l.insertarAlFinal((T)q.getInfo());
           q = q.getHermano();
       }
       return(l.iterator());
   }
   
    
    /**
     * Método que retorna un iterador con las hojas del Árbol Eneario. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol Eneario. <br>
     * @return Un objeto Iterador con las hojas del Árbol Eneario.
     */
    public Iterator<T> getHojas(){
        ListaCD<T> l=new ListaCD();
        getHojas(this.raiz, l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un iterador con las hojas del Árbol Eneario. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol Eneario.<br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol. <br>
     */
    private void getHojas(NodoEneario<T> r, ListaCD<T> l){
        NodoEneario<T> q;
        if(r==null)
            return ;
        q = r.getHijo();
        if(q==null){
            l.insertarAlFinal(r.getInfo());
            return ;
        }
        while(q != null){
        getHojas(q,l);
        q = q.getHermano();
        }
    }
    
    /**
     * Método que permite evaluar si un NodoEneario es una Hoja del Árbol Eneario. <br>
     * <b>post: </b> Se evaluó si el NodoEneario<T> es o no una hoja del Árbol. <br>
     * @param r Representa el Nodo a ser evaluado si es o no una Hoja del Árbol. <br>
     * @return Un objeto de tipo boolean que es true si el Nodo es una Hoja y false en caso contrario.
     */
    private boolean esHoja(NodoEneario<T> r){
        return (r.getHijo()==null);
    }
    
    /**
     * Método que permite determinar el número de Nodo hojas dentro del Árbol Eneario. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol. <br>
     * @return El número de hojas existentes en el Árbol Eneario.
     */
    public int contarHojas(){
        return (contarHojas(this.raiz));
    }
    
    /**
     * Método de tipo privado que permite determinar el número de Nodo hojas dentro del Árbol Eneario. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return El número de hojas existentes en el Árbol Eneario.
     */
    private int contarHojas(NodoEneario<T> r){
        NodoEneario q;
        if(r==null)
            return (0);
        q = r.getHijo();
        if(q==null){
            return (1);
        }
        int acum = 0;
        while(q != null){
            acum += contarHojas(q);
            q = q.getHermano();
        }
        return (acum);
    }
    
    /**
     *  Método que retorna un iterador con el recorrido preOrden del Árbol Eneario. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el Árbol. <br>
     * @return Un Iterador en preorden (padre-&#62;Primer Hijo en preorden -&#62; Hermano en preorden) para el Árbol Eneario.
     */
     public Iterator<T> preOrden(){
         ListaCD<T> l=new ListaCD<T>();
         preOrden(this.getRaiz(),l);
         return (l.iterator());
    }

    /**
    * Método que tipo privado que retorna un Iterador con el recorrido preOrden del Árbol Eneario. <br>
    * <b>post: </b> Se retorno un iterador en preOrden para el Árbol. <br>
    * @param r Representa la raíz del Árbol, o raiz de sub_Árbol. <br>
    * @param l Lista para el almacenamiento de los datos del Árbol. <br>
    */
    private void  preOrden(NodoEneario<T> r, ListaCD<T> l){
        NodoEneario<T> q;
        if(r!=null){
            l.insertarAlFinal(r.getInfo());
            q = r.getHijo();
            if(q != null){
                preOrden(q,l);
                q = q.getHermano();
                while(q != null){
                    preOrden(q,l);
                    q = q.getHermano();
                }
            }
        }
    }

   /**
     * Método que retorna un iterador con el recorrido in Orden del Árbol Eneario. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el Árbol. <br>
     * @return Un Iterador en inOrden (Primer hijo en InOrden-&#62;padre-&#62;Hermano en inOrden) para el Árbol Eneario. <br>
     */
    public Iterator<T> inOrden(){
        ListaCD<T> l=new ListaCD<T>();
        inOrden(this.getRaiz(),l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un Iterador con el recorrido in Orden del Árbol Eneario. <br>
     * <b>post: </b> Se retorno un iterador inOrdenpara el Arbol.<br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol. <br>
     */
    private void  inOrden(NodoEneario<T> r, ListaCD<T> l){
        NodoEneario<T> q;
        if(r!=null){
            q = r.getHijo();
            if(q == null){
                l.insertarAlFinal(r.getInfo());
            }else{
                inOrden(q,l);
                l.insertarAlFinal(r.getInfo());
                q = q.getHermano();
                while(q != null){
                    inOrden(q,l);
                    q = q.getHermano();
                }
            }
        }
    }

    /**
     * Método que retorna un iterador con el recorrido postOrden del Árbol Eneario. <br>
     * <b>post: </b> Se retorno un iterador postOrden para el Arbol.<br>
     * @return Un Iterador en postOrden (Hijo en postOrden-&#62;Hermano en postOrden-&#62;padre) para el Árbol Eneario. <br>
     */
    public Iterator<T> postOrden(){
        ListaCD<T> l=new ListaCD<T>();
        postOrden(this.getRaiz(),l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un iterador con el recorrido postOrden del Árbol Eneario. <br>
     * <b>post: </b> Se retorno un iterador postOrden para el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol.
     */
    private void  postOrden(NodoEneario<T> r, ListaCD<T> l){        
        NodoEneario<T> q;
        if(r!=null){
            q = r.getHijo();
            while(q != null){
                postOrden(q,l);
                q = q.getHermano();
            }
            l.insertarAlFinal(r.getInfo());
        }
    }
    
    /**
     * Método que permite retornar un Iterador con el recorrido por niveles del Árbol Eneario. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Árbol Eneario.<br>
     * @return Un iterador con el recorrido por niveles del Árbol Eneario.
     */
    public Iterator<T> impNiveles(){
        Cola<NodoEneario<T>> c = new Cola();
        ListaCD<T> l = new ListaCD();
        if(this.esVacio())
            return (l.iterator());
        NodoEneario<T> s, q;
        c.enColar(this.raiz);
        while(!c.esVacia()){
            q = c.deColar();
            if(q!=null){
                l.insertarAlFinal(q.getInfo());
                s = q.getHijo();
                while(s!=null){
                    c.enColar(s);
                    s = s.getHermano();
                }
            }            
        }
        return (l.iterator());
    }
    
    /**
     * Método que permite obtener el peso del Árbol Eneario. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol Eneario.<br>
     * @return Un entero con la cantidad de elementos del Árbol Eneario.
     */
    public int getPeso(){
        return(getPeso(this.getRaiz()));
    }

    /**
     * Método de tipo privado que permite conocer el número de elemento del Árbol Eneario. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return El número de elementos que contiene el Árbol Eneario.
     */
    private int getPeso(NodoEneario<T> r){
        NodoEneario<T> q;
        int cant = 0;
        if(r!=null){
            cant++;
            q = r.getHijo();
            if(q != null){
                cant+=getPeso(q);
                q = q.getHermano();
                while(q != null){
                    cant+=getPeso(q);
                    q = q.getHermano();
                }
            }
        }
        return (cant);
    }
    
    /**
     * Método que permite conocer si un Árbol Eneario se encuentra vacío. <br>
     * <b>post: </b> Se evaluó si el Árbol Eneario se encuentra o no vacío. <br>
     * @return Un objeto de tipo boolean, true si el Árbol se encuentra vacío, false en caso contrario
     */
    public boolean esVacio(){
        return (this.raiz==null);
    }
    
    /**
     * Método que permite conocer el grado del Árbol Eneario, que es su misma gordura. <br>
     * <b>post: </b> Se retorno la gordura del Árbol Eneario, es decir el mayor número de hijos de un Nodo. <br>
     * @return Un objeto de tipo int con la gordura del Árbol Eneario.
     */
    public int gordura(){
        if(this.esVacio())
            return (0);
       int masGordo = -1;
       Cola<NodoEneario<T>> cola = new Cola<NodoEneario<T>>();
       Cola<Integer> c = new Cola<Integer>();
       NodoEneario<T> s, q;
       int i=0;
       int cont=1,ant=-1;       
       cola.enColar(this.getRaiz());
       c.enColar(i);
       while(!cola.esVacia()){
           q = cola.deColar();
           i = c.deColar();
           if(i!=ant){
               if(masGordo < cont){
                   masGordo = cont;
               }
               cont=0;
               ant=i;
           }
           cont++;
           s = q.getHijo();
           while(s != null){
               cola.enColar(s);
               c.enColar(i+1);
               s = s.getHermano();
           }
       }
       return ((masGordo<cont)?cont:masGordo);
   }
    
    /**
     * Método que permite obtener la altura del Árbol Eneario. <br>
     * <b>post: </b> Se retorno la altura del Árbol Eneario.<br>
     * @return Un entero con la altura del Árbol Eneario.
     */
    public int getAltura(){
        if(this.esVacio())
            return (0);
        return(getAltura(this.getRaiz()));
    }

    /**
     * Método de tipo privado que permite conocer la altura del Árbol Eneario. <br>
     * <b>post: </b> Se retorno la altura del Árbol Eneario. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return Un entero con la altura del Árbol Eneario.
     */
    private int getAltura(NodoEneario<T> r){
        if(this.esHoja(r))
            return (1);
        int maxAltura = 0;
        NodoEneario<T> q;
        if(r!=null){
            q = r.getHijo();            
            if(q != null){
                while(q != null){
                    int auxAltura = getAltura(q);
                    if(auxAltura>maxAltura)
                        maxAltura = auxAltura;
                    q = q.getHermano();
                }
            }
        }
        return (maxAltura+1);
    }

   
}//Final de la Clase Árbol Eneario


    

    
