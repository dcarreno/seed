/**
 * ---------------------------------------------------------------------
 * $Id: Componente.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de Clase que permite almacenar la ruta para buscar un determinado dato en el Árbol B.
 * @author Yuieth Pabon
 * @version 1.0
 */
public class Componente {
    
    ////////////////////////////////////////////////////////////
    // Componente - Atributos /////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Pagina que contiene el apuntador a la próxima pagina
     */
    private Pagina p; 
    
    /**
     * Índice del apuntador de la próxima pagina
     */
    private int v; 
    
    

    ////////////////////////////////////////////////////////////
    // Componente - Implementación de Métodos /////////////////
    ////////////////////////////////////////////////////////////
   
    /**
     * Crea un componente vacío. <br>
     * <b>post: </b> Se creo un componente vacío. <br>
     */
    public Componente() {
    }

    /**
     * Crea un componente con datos específicos. <br>
     * <b>post: </b> Se creo un componente con un apuntador y un índice en específico. <br>
     * @param p Apuntador del componente a la página. <br>
     * @param v Índice del apuntador a la próxima página. <br>
     */
    public Componente(Pagina p, int v) {
        this.p = p;
        this.v = v;
    }

    /**
     * Método que permite obtener página que contiene el apuntador a la próxima página. <br>
     * <b>post: </b> Se retorno el apuntador de la página. <br>
     * @return el Apuntador de la página.
     */
    public Pagina getP() {
        return p;
    }

    /**
     * Método que permite retornar el índice del apuntador de la próxima página. <br>
     * <b>post: </b> Se retorno el índice del apuntador de la próxima página. <br>
     * @return El índice del apuntador de la próxima página.
     */
    public int getV() {
        return v;
    }

    /**
     * Método que permite modificar página que contiene el apuntador a la próxima página. <br>
     * <b>post: </b> Se modifico página que contiene el apuntador a la próxima página. <br>
     * @param p Página que contiene el apuntador a la próxima página. <br>
     */
    public void setP(Pagina p) {
        this.p = p;
    }

    /**
     * Método que permite modificar el índice del apuntador de la próxima página<br>
     * <b>post: </b> Se modifico el índice del apuntador de la próxima página. <br>
     * @param v Variable
     */
    public void setV(int v) {
        this.v = v;
    }
    
}//Fin de la clase Componente

