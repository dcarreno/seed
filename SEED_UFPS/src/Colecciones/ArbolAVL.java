/**
 * ---------------------------------------------------------------------
 * $Id: ArbolAVL.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

import java.util.Iterator;

/**
 * Implementación de Clase para el manejo de un Árbol AVL.
 * @param <T> Tipo de datos a almacenar en el Árbol AVL.
 * @author Uriel García
 * @version 1.0
 */
public class ArbolAVL<T> extends ArbolBinarioBusqueda<T> {
 
    
    ////////////////////////////////////////////////////////////
    // ArbolAVL - Implementación de Métodos ////////////////////
    ////////////////////////////////////////////////////////////  
    
    /**
     * Crea un Árbol AVL vacío. <br>
     * <b>post: </b> Se creo un Árbol AVL vacío. <br>
     */
    public ArbolAVL(){
        super();
    }
    
    /**
     * Crea un Árbol AVL con una raíz predefinida. <br>
     * <b>post: </b> Se creo un nuevo Árbol AVL con raíz predeterminada. <br>
     * @param r Un tipo &#60;T&#62; , almacena la dirección de memoria de un nodo de un Árbol AVL<br>
     */
    public ArbolAVL(T r) {
        super.setRaiz(new NodoAVL<T>(r));
    }
    
    /**
     * Método que permite conocer el objeto raíz del Árbol AVL. <br>
     * <b>post: </b> Se retorno el objeto raíz del Árbol. <br>
     * @return Un objeto de tipo T que representa el dato en la raíz del Árbol.
     */
    @Override
    public T getObjRaiz() {
        return (super.getObjRaiz());
    }
        
    /**
     * Método que permite insertar un nuevo dato dentro del Árbol AVL sin que se pierda el balance. <br>
     * <b>post: </b> Se inserto un nuevo dato dentro del Árbol AVL. <br>
     * @param nuevo Representa el nuevo que se pretende ingresar al Árbol AVL. <br>
     * @return True o false dependiendo si se pudo o no insertar el nuevo elemento dentro del Árbol
     */
    @Override
    public boolean insertar(T nuevo){
        NodoAVL<T> n = new NodoAVL<T>(nuevo);
        return (insertaAVL((NodoAVL<T>)super.getRaiz(),n));
    }
    
    /**
     * Método que permite insertar un nuevo dato dentro del Árbol AVL sin que se pierda el balance. <br>
     * <b>post: </b> Se inserto un nuevo dato dentro del Árbol AVL. <br>
     * @param p Representa la raíz del Árbol AVL en el cual se inserta el nuevo dato. <br>
     * @param q Representa el Nodo<T> que será insertado dentro del Árbol AVL. <br>
     * @return True o false dependiendo si se pudo o no insertar el nuevo elemento dentro del Árbol
     */
    private boolean insertaAVL(NodoAVL<T> p, NodoAVL<T> q){
        //Si el Arbol se encuentra vacio
        if(this.esVacio()) {
            setRaiz(q);
            return (true);
        }
        int comp = ((Comparable)q.getInfo()).compareTo(p.getInfo());
        if(comp==0)
            return (false); //Esta nodo ya existe
        if(comp<0){
            if(p.getIzq()==null){
                p.setIzq(q);
                q.setPadre(p);
                balancear(p);
                return (true);
            }else{
                return (insertaAVL(p.getIzq(),q));
            }
        }else 
        if(comp>0){
            if(p.getDer()==null){
                p.setDer(q);
                q.setPadre(p);
                //El nodo ha sido insertado, ahora se balancea.
                balancear(p);
                return (true);
            } else {
                return(insertaAVL(p.getDer(),q));
            }
        }
        return false;
    }
    
    /**
     * Método que permite balancear el Árbol AVL de manera que siga manteniendo sus propiedades. <br>
     * <b>post: </b> EL Árbol AVL ha sido balanceado, por lo que sigue cumpliendo con sus propiedades. <br>
     * @param r Representa el Nodo del Árbol desde el cual se quiere realizar el balance.
     */
    private void balancear(NodoAVL<T> r){ 
        // Se actualiza el factor de balance del Nodo
        setBalance(r);
        int balance = r.getBal();
        // Se evaua el balance
        if(balance==-2){
            if(getAlturaNodo(r.getIzq().getIzq())>=getAlturaNodo(r.getIzq().getDer())) {
                r = rDerecha(r);
            } else {
                r = drIzqDer(r);
            }
        } 
        else 
            if(balance==2){
                if(getAlturaNodo(r.getDer().getDer())>=getAlturaNodo(r.getDer().getIzq())) {
                    r = rIzquierda(r);} 
                else{
                    r = drDerIzq(r);}
            }
        // Se modifica el padre
        if(r.getPadre()!=null){
            balancear(r.getPadre());
        }else{
            this.setRaiz(r);
        }
    }
    
    /**
     * Método que permite Balancear el Árbol AVL.
     */
    public void balancearAltura(){
        balancearAltura((NodoAVL<T>)super.getRaiz());
    }
    
    /**
     * Método de tipo privado que permite balancear la altura del Árbol AVL.
     * @param r Representa la raíz del Árbol o sub_Árbol.
     */
    private void balancearAltura(NodoAVL<T> r){
        if(r==null)
            return;
        this.setBalance(r);
        balancearAltura(r.getIzq());
        balancearAltura(r.getDer());        
    }
    
    /**
     * Método que permite modificar el factor de balance de un Nodo de acuerdo a sus nuevas condiciones. <br>
     * <b>post: </b> Se ha modificado el factor de balance del NodoAVL<T> indicado. <br>
     * @param r Representa el NodoAVL<T> el cual será recalculado su nuevo factor de balance.
     */
    private void setBalance(NodoAVL<T> r) {
        r.setBal(getAlturaNodo(r.getDer())-getAlturaNodo(r.getIzq()));;
    }
    
    /**
     * Método que permite conocer la altura de un Nodo dentro del Árbol AVL para determinar su balance. <br>
     * <b>post: </b> Se retorno la altura del Nodo dentro del Árbol AVL. <br>
     * @param r Representa el NodoAVL<T> del cual se pretende conocer su altura. <br>
     * @return Un objeto de tipo int con la altura del Nodo dentro del Árbol AVL.
     */
    private int getAlturaNodo(NodoAVL<T> r) {
        if(r==null)
            return -1;        
        if(r.getIzq()==null && r.getDer()==null)
            return 0;        
        if(r.getIzq()==null)
            return 1+getAlturaNodo(r.getDer());        
        if(r.getDer()==null)
            return 1+getAlturaNodo(r.getIzq());        
        return 1+getMax(getAlturaNodo(r.getIzq()),getAlturaNodo(r.getDer()));        
    }
    
    /**
     * Método que permite obtener el valor máximo entre dos valores a evaluar. <br>
     * <b>post: </b> Se retorno el valor máximo de dos datos evaluados. <br>
     * @param a Representa el primer valor a evaluar. <br>
     * @param b Representa el segundo valor a evaluar. <br>
     * @return Un objeto de tipo int con el dato de Mayor valor entre los datos evaluados.
     */
   private int getMax(int a, int b) {
        if(a>=b)
         return a;
        return b;
   }
   
    /**
     * Método que permite efectuar una doble rotación hacia la derecha de un Nodo. <br>
     * <b>post: </b> Se realizo una doble rotación a la derecha. <br>
     * @param r Nodo que se encuentra desbalanceado y no cumple la propiedad. <br>
     * @return Un objeto de tipo NodoAVL<T> con las rotaciones ya realizadas. <br>
     */
    private NodoAVL<T> drIzqDer(NodoAVL<T> r) {
        r.setIzq(rIzquierda(r.getIzq()));
        return rDerecha(r);
    }
    
    /**
     * Método que permite efectuar una doble rotación hacia la izquierda de un Nodo. <br>
     * <b>post: </b> Se realizo una doble rotación a la izquierda. <br>
     * @param r Nodo que se encuentra desbalanceado y no cumple la propiedad. <br>
     * @return Un objeto de tipo NodoAVL<T> con las rotaciones ya realizadas. <br>
     */
    private NodoAVL<T> drDerIzq(NodoAVL<T> r) {
        r.setDer(rDerecha(r.getDer()));
        return rIzquierda(r);
    }
    
    /**
     * Método que permite efectuar una rotación simple hacia la izquierda de un Nodo. <br>
     * <b>post: </b> Se realizo una rotación simple a la izquierda. <br>
     * @param r Nodo que se encuentra desbalanceado y no cumple la propiedad. <br>
     * @return Un objeto de tipo NodoAVL<T> con las rotaciones ya realizadas. <br>
     */
    private NodoAVL<T> rIzquierda(NodoAVL<T> r) { 
        NodoAVL<T> v = r.getDer();
        v.setPadre(r.getPadre());
        r.setDer(v.getIzq());
        if(r.getDer()!=null){
            r.getDer().setPadre(r);
        }
        v.setIzq(r);
        r.setPadre(v);
        if(v.getPadre()!=null){
            if(v.getPadre().getDer()==r){
                v.getPadre().setDer(v);
            } else 
            if(v.getPadre().getIzq()==r){
                v.getPadre().setIzq(v);
            }
        }
        setBalance(r);
        setBalance(v);
        return (v);
    }
    
    /**
     * Método que permite efectuar una rotación simple hacia la derecha de un Nodo. <br>
     * <b>post: </b> Se realizo una rotación simple a la derecha. <br>
     * @param r Nodo que se encuentra desbalanceado y no cumple la propiedad. <br>
     * @return Un objeto de tipo NodoAVL<T> con las rotaciones ya realizadas. <br>
     */
    private NodoAVL<T> rDerecha(NodoAVL<T> r){ 
        NodoAVL<T> v = r.getIzq();
        v.setPadre(r.getPadre());

        r.setIzq(v.getDer());
        if(r.getIzq()!=null) {
            r.getIzq().setPadre(r);
        }

        v.setDer(r);
        r.setPadre(v);
        if(v.getPadre()!=null){
            if(v.getPadre().getDer()==r){
                v.getPadre().setDer(v);
            } else 
                if(v.getPadre().getIzq()==r){
                    v.getPadre().setIzq(v);
                }
        }
        setBalance(r);
        setBalance(v);
        return (v);
    }
    
    /**
     * Método que permite eliminar un dato del Árbol AVL; manteniendo el Árbol sus propiedades de balanceado. <br>
     * <b>post: </b> Se elimino un elemento del Árbol AVL y este ha mantenido sus propiedades.
     * @param dato Representa el Objeto de tipo T que se desea eliminar del Árbol.
     * @return Un objeto de tipo boolean con true si el dato ha sido eliminado correctamente.
     */
    @Override
    public boolean eliminar(T dato) {
        if(this.esVacio() || !this.esta(dato))
            return (false);
        return(eliminarAVL((NodoAVL<T>)super.getRaiz(),dato));
    }
    
    /**
     * Método que permite eliminar un dato del Árbol AVL; manteniendo el Árbol sus propiedades de balanceado. <br>
     * <b>post: </b> Se elimino un elemento del Árbol AVL y este ha mantenido sus propiedades. <br>
     * @param p Representa la raíz del Árbol AVL sobre el cual se va a realizar la eliminación. <br>
     * @param q Representa el Objeto de tipo T que desea ser eliminado del Árbol. <br>
     * @return True o false dependiendo se si se puedo eliminar el dato del Árbol.
     */
    private boolean eliminarAVL(NodoAVL<T> p, T q) {
        int comp = ((Comparable)p.getInfo()).compareTo(q);
        if(comp==0)
            return(eliminaAVL(p));
        if(comp>0)
            return (eliminarAVL(p.getIzq(),q));
        else 
            return (eliminarAVL(p.getDer(),q));
    }
    
    /**
     * Método que permite eliminar un dato del Árbol AVL; manteniendo el Árbol sus propiedades de balanceado. <br>
     * <b>post: </b> Se elimino un elemento del Árbol AVL y este ha mantenido sus propiedades. <br>
     * @param q Representa el NodoAVL<T> que debe ser eliminado del Árbol. <br>
     * @return True o false dependiendo se si se puedo eliminar el dato del Árbol.
     */
    private boolean eliminaAVL(NodoAVL<T> q){
        NodoAVL<T> s;
        //Si el Nodo es una hoja
        if(q.getIzq()==null || q.getDer()==null){        
            //Si el Nodo es la raiz
            if(q.getPadre()==null){
                if(q.getIzq()!=null){
                    q.getIzq().setPadre(null);
                    this.setRaiz(q.getIzq());
                }else{
                    if(q.getDer()!=null){
                        q.getDer().setPadre(null);
                        this.setRaiz(q.getDer());
                    }else
                        setRaiz(null);
                }
                return (true);
            }
            s = q;
        }
        else{
            // Se recupera el hijo sucesor al Nodo
            s = getSucesor(q);
            q.setInfo(s.getInfo());
        }
        NodoAVL<T> p;
        if(s.getIzq()!=null){
            p = s.getIzq();
        } 
        else{
            p = s.getDer();
        }
        if(p!=null){
            p.setPadre(s.getPadre());
        }
        if(s.getPadre()==null){
            this.setRaiz(p);
        }else{
            if(s==s.getPadre().getIzq()){
                s.getPadre().setIzq(p);
            }
            else{
                s.getPadre().setDer(p);
            }
            // Se realiza el balanceo del Arbol
            balancear(s.getPadre());        
        }
        s = null;
        return (true);
    }
    
    /**
     * Método que permite encontrar el Nodo sucesor al Nodo que se pretende eliminar. <br>
     * <b>post: </b> Se retorno el sucesor al NodoAVL<T> que se desea eliminar de Árbol. <br>
     * @param q Representa el NodoAVL<T> sobre el cual se desea evaluar su sucesor. <b>
     * @return Un objeto de tipo NodoAVL<T> que representa el sucesor al Nodo que se pretende eliminar.
     */
    private NodoAVL<T> getSucesor(NodoAVL<T> q) {
        if(q.getDer()!=null){
            NodoAVL<T> r = q.getDer();
            while(r.getIzq()!=null){
                r = r.getIzq();
            }
            return r;
        } 
        else{
            NodoAVL<T> p = q.getPadre();
            while(p!=null && q==p.getDer()){
                q = p;
                p = q.getPadre();
            }
            return p;
        }
    }
    
    /**
     * Método que permite evaluar la existencia de un objeto dentro del Árbol AVL. <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el Árbol. <br>
     * @param x Representa el elemento el cual se desea evaluar su existencia en el Árbol. <br>
     * @return Un boolean , true si el dato esta o false en caso contrario.
     */
    @Override
    public boolean esta(T x){
        return(super.estaABB(x));
    }
    
    /**
     * Método que retorna un iterador con las hojas del Árbol AVL. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol. <br>
     * @return Un iterador con las hojas del Árbol.
     */
    @Override
    public Iterator<T> getHojas(){
        return (super.getHojas());
    }
    
    /**
     * Método que permite determinar el número de Nodo hojas dentro del Árbol. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol. <br>
     * @return El número de hojas existentes en el Árbol.
     */
    @Override
    public int contarHojas(){
        return (super.contarHojas());
    }
    
    /**
     *  Método que retorna un iterador con el recorrido preOrden del árbol. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el Árbol. <br>
     * @return Un iterador en preorden (padre-&#62;hijoIzq-&#62;hijoDer) para el Árbol AVL.
     */
    @Override
     public Iterator<T> preOrden(){
         return (super.preOrden());
    }
    
    /**
     * Método que retorna un iterador con el recorrido in Orden del Arbol. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el Árbol. <br>
     * @return Un iterador en inOrden (hijoIzq-&#62;padre-&#62;hijoDer) para el Árbol AVL. <br>
     */
    @Override
    public Iterator<T> inOrden(){
        return (super.inOrden());
    }

    /**
     * Método que retorna un iterador con el recorrido postOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador preOrden para el Árbol. <br>
     * @return Un iterador en postOrden (hijoIzq-&#62;hijoDer-&#62;padre) para el Árbol AVL. <br>
     */
    @Override
    public Iterator<T> postOrden(){
        return (super.postOrden());
    }
    
    /**
     * Método que permite retornar un iterador con el recorrido por niveles del Árbol. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Árbol AVL. <br>
     * @return Un iterador con el recorrido por niveles del Árbol AVL.
     */
    @Override
    public Iterator<T> impNiveles(){
        return (super.impNiveles());
    }
    
    /**
     * Método que permite obtener el peso del Árbol AVL. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol AVL. <br>
     * @return Un entero con la cantidad de elementos del Árbol AVL.
     */
    @Override
    public int getPeso(){
        return(super.getPeso());
    }
    
    /**
     * Método que permite saber si el Árbol AVL se encuentra vacío. <br>
     * <b>post: </b> Se retorno true si el Árbol no contiene elementos. <br>
     * @return true si no hay datos en el Árbol AVL.
     */
    @Override
    public boolean esVacio(){
        return (super.esVacio());
    }
    
    /**
     * Método que permite obtener la altura del Árbol AVL. <br>
     * <b>post: </b> Se retorno la altura del Árbol AVL. <br>
     * @return Un entero con la altura del Árbol AVL.
     */
    @Override
    public int getAltura(){
        return(super.getAltura());
    }
    
    /**
     * Método que permite conocer por consola la información del Árbol Binario.
     */
    @Override
    public void imprime(){
        System.out.println(" ----- Arbol AVL ----- ");
        imprimeAVL((NodoAVL<T>) super.getRaiz());
    }
    
    /**
     * Método de tipo privado que permite mostrar por consola la información del Árbol AVL. <br>
     * @param n Representa la raíz del Árbol AVL o de alguno de sus sub_Arboles.
     */
    public void imprimeAVL(NodoAVL<T> n) {
        int l = 0;
        int r = 0;
        int p = 0;
        if(n==null)
            return;
        if(n.getIzq()!=null) {
         l = Integer.parseInt(n.getIzq().getInfo().toString());
        }
        if(n.getDer()!=null) {
         r = Integer.parseInt(n.getDer().getInfo().toString());
        }
        if(n.getPadre()!=null) {
         p = Integer.parseInt(n.getPadre().getInfo().toString());
        }        
        System.out.println("NodoIzq: "+l+"\t Info: "+n.getInfo()+"\t NodoDer: "+r+"\t Padre: "+p+"\t Balance: "+n.getBal());
        if(n.getIzq()!=null) {
         imprimeAVL(n.getIzq());
        }
        if(n.getDer()!=null) {
         imprimeAVL(n.getDer());
        }
    }
    
    
} //Fin de la clase Árbol AVL

