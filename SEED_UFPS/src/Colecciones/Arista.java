/**
 * ---------------------------------------------------------------------
 * $Id: Arista.java,v 2.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

/**
 * Implementación de la Clase para el manejo de las Aristas/Arcos del Grafo;
 * Una Arista corresponde a una relación entre dos vértices de un Grafo.
 * @param <T> Tipo de objetos a almacenar dentro de la Arista.
 * @author Uriel García
 * @version 1.0
 */

public class Arista<T> 
{
    
    ////////////////////////////////////////////////////////////
    // Arista - Atributos //////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Representa el Vértice de un lado de la Arista
     */
    private Vertice<T> vertA;
    
    /**
     * Representa el Vértice del otro de la Arista
     */
    private Vertice<T> vertB;
    
    /**
     * Representa el peso de la Arista; Puede representar costos, tiempos, distancias
     */
    private int peso;
    
    /**
     * Representa un valor que indica si el Vértice ha sido visitado;
     * True=visitado, False=No visitado
     */
    private boolean esVisit;
    
    
    
    ////////////////////////////////////////////////////////////
    // Arista - Implementación de Métodos //////////////////////
    ////////////////////////////////////////////////////////////

    /**
     * Método constructor que permite crear una nueva Arista con sus 2 vértices definidos. <br>
     * <b> post: </b> Se creo una nueva Arista con la información de los 2 vértices. <br>
     * @param vA Objeto de tipo Vértice que representa uno de los extremos de la Arista. <br>
     * @param vB Objeto de tipo Vértice que representa uno de los extremos de la Arista. <br>
     * @param p Objeto de tipo T que representa el peso de la Arista. Para los recorridos debe ser de tipo int.
     */
    public Arista(Vertice<T> vA, Vertice<T> vB, int p) {
        this.vertA = vA;
        this.vertB = vB;
        this.peso = p;
        this.esVisit = false;
    }

    /**
     * Obtiene el Vértice de uno de los extremos de la Arista. <br>
     * <b> post </b> Se retorno el Vértice en uno de los extremos de la Arista. <br>
     * @return Un objeto de tipo Vértice que representa un extremo de la Arista.
     */
    public Vertice<T> getVertA() {
        return vertA;
    }

    /**
     * Método que permite editar uno de los Vértices extremos de la Arista. <br>
     * <b> post: </b> Se edito uno de los Vértices que se encuentra en un extremo de la Arista. <br>
     * @param vertA Representa el nuevo Vértice que se ubicara en un extremo de la Arista. <br>
     */
    public void setVertA(Vertice<T> vertA) {
        this.vertA = vertA;
    }

    /**
     * Obtiene el Vértice de uno de los extremos de la Arista. <br>
     * <b> post </b> Se retorno el Vértice en uno de los extremos de la Arista. <br>
     * @return Un objeto de tipo Vértice que representa un extremo de la Arista.
     */
    public Vertice<T> getVertB() {
        return vertB;
    }

    /**
     * Método que permite editar uno de los Vértices extremos de la Arista. <br>
     * <b> post: </b> Se edito uno de los Vértices que se encuentra en un extremo de la Arista. <br>
     * @param vertB Representa el nuevo Vértice que se ubicara en un extremo de la Arista. <br>
     */
    public void setVertB(Vertice<T> vertB) {
        this.vertB = vertB;
    }

    /**
     * Método que permite conocer el peso de la Arista; Puede representar costos, tiempos, distancias. <br>
     * <b> post: </b> Se retorno un objeto de tipo T que representa el peso de la Arista. <br>
     * @return Un objeto de tipo T que representa el peso de la Arista, costo, tiempo o distancia.
     */
    public int getPeso() {
        return peso;
    }

    /**
     * Método que permite editar el peso de una Arista, reemplazarlo con un nuevo valor. <br>
     * <b> post: </b> Se edito el peso de la Arista con un nuevo valor. <br>
     * @param peso Representa el nuevo peso de la Arista.
     */
    public void setPeso(int peso) {
        this.peso = peso;
    }
    
    /**
     * Método que permite evaluar si una Arista ha sido visitada en un recorrido. <br>
     * <b>post: </b> Se retorno un boolean que evalúa si la Arista ha sido visitada. <br>
     * @return Un Objeto de tipo boolean que evalúa si la Arista ha sido visitada.
     */
    public boolean getVisit() {
        return esVisit;
    }

    /**
     * Método que permite marcar la visita de una Arista dentro de un recorrido. <br>
     * <b> post: </b> Se marco la visita de una Arista con un true o false. <br>
     * @param esVisit Es de tipo boolean y representa true=visitado, false=No visitado.
     */
    public void setVisit(boolean esVisit) {
        this.esVisit = esVisit;
    }
    
    /**
     * Método que permite evaluar si una Arista incide sobre un Vértice. <br>
     * <b> post: </b> Se retorno un valor booleano con el resultado de la operación. <br>
     * @param v Vértice del que se desea si la Arista actual incide sobre él. <br>
     * @return Un objeto de tipo boolean true=si la Arista incide en el Vértice.
     */
    public boolean incideEnND(Vertice v){
        return (this.vertA.equals(v)||this.vertB.equals(v));
    }
    
    /**
     * Método que permite evaluar si dos Aristas son iguales, comparando sus extremos. <br>
     * <b> post: </b> Se retorno un valor booleano para evaluar la igualdad de las Aristas. <br>
     * @param ar Representa la segunda Arista en comparación. <br>
     * @return Un objeto de tipo boolean que evalúa la igualdad de los extremos de la Arista.
     */
    public boolean equalsND(Arista ar){
        return ((ar.getVertA().equals(this.vertA))||(ar.getVertA().equals(this.vertB)))&&
                ((ar.getVertB().equals(this.vertA))||(ar.getVertB().equals(this.vertB)));
    }
    
    /**
     * Método que permite evaluar si dos Aristas son iguales, comparando sus extremos. <br>
     * <b> post: </b> Se retorno un valor booleano para evaluar la igualdad de las Aristas. <br>
     * @param ar Representa la segunda AristaD en comparación. <br>
     * @return Un objeto de tipo boolean que evalúa la igualdad de los extremos de la AristaD.
     */
    public boolean equalsD(Arista ar){
        return ((ar.getVertA().equals(this.vertA))&&(ar.getVertB().equals(this.vertB)));
    }
    
    /**
     * Método que sirve de Interfaz del Método toString() del Objeto. <br>
     * @return Una cadena de String con la información del Vértice.
     */
    @Override
    public String toString(){
        return ("("+this.vertA.getInfo()+"-"+this.vertB.getInfo()+")");
    }
    
}//Fin de la Clase Arista
