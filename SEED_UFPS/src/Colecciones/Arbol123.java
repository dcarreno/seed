/**
 * ---------------------------------------------------------------------
 * $Id: Arbol123.java,v 1.0 2013/08/23 
 * Universidad Francisco de Paula Santander 
 * Programa Ingeniería de Sistemas
 *
 * Proyecto: SEED_UFPS
 * ----------------------------------------------------------------------
 */

package Colecciones;

import java.util.Iterator;

/**
 * Implementación de Clase para el manejo de un Árbol Binario Ordenado 1-2-3. <br>
 * @param <T> Tipo de datos a almacenar en el Árbol 1-2-3. <br>
 * @author Uriel García
 * @version 1.0
 */

public class Arbol123 <T>{
    
    ////////////////////////////////////////////////////////////
    // Arbol123 - Atributos ////////////////////////////////////
    ////////////////////////////////////////////////////////////
    
    /**
     * Nodo raíz del Árbol 1-2-3
     */
    private Nodo123<T> raiz;
    
    ////////////////////////////////////////////////////////////
    // Arbol123 - Implementación de Métodos ////////////////////
    ////////////////////////////////////////////////////////////  
    
    /**
     * Crea un Arbol1-2-3 vacío.
     * <b>post: </b> Se creo un Árbol 1-2-3 vacío.<br>
     */

    public Arbol123(){
        this.raiz= null;
    }

    /**
     * Método que permite conocer la raíz del Árbol 1-2-3. <br>
     * <b>post: </b> Se obtuvo la raíz del Árbol 1-2-3.<br>
     * @return Un objeto de tipo Nodo1-2-3 &#60;T&#62; que es la raíz del Árbol 1-2-3.
     */
    public Nodo123<T> getRaiz() {
        return raiz;
    }
    
    /**
     * Método que permite conocer el dato menor en la raíz del Árbol 1-2-3. <br>
     * <b>post: </b> Se obtuvo la raíz del Árbol 1-2-3. <br>
     * @return Un objeto de tipo Nodo1-2-3&#60;T&#62; que es la raíz del Árbol 1-2-3.
     */
    public T getInfoMenRaiz() {
        return raiz.getInfoMen();
    }
    
    /**
     * Método que permite conocer el dato mayor en la raíz del Árbol 1-2-3. <br>
     * <b>post: </b> Se obtuvo la raíz del Árbol 1-2-3.<br>
     * @return Un objeto de tipo Nodo1-2-3&#60;T&#62; que es la raíz del Árbol 1-2-3.
     */
    public T getInfoMayRaiz() {
        return raiz.getInfoMay();
    }

    /**
     * Método que permite modificar la raíz del Árbol 1-2-3. <br>
     * <b>post: </b> Se modifico la raíz del Árbol 1-2-3. <br>
     * @param raíz Objeto de tipo Nodo123&#60;T&#62; que representa la nueva raíz del Árbol.
     */

    private void setRaiz(Nodo123<T> raiz) {
        this.raiz = raiz;
    }
    
    /**
     * Método que permite insertar un nuevo dato dentro del Árbol 1-2-3 y que este mantenga sus propiedades. <br>
     * <b>post: </b> Se inserto un nuevo dato dentro del Árbol 1-2-3. <br>
     * @param nuevo Representa el nuevo que se pretende ingresar al Árbol 1-2-3. <br>
     * @return true o false dependiendo si se pudo o no insertar el nuevo elemento dentro del Árbol.
     */
    public boolean insertar(T nuevo){
        if(this.esta(nuevo))
            return (false);
        if(this.esVacio()){
            this.setRaiz(new Nodo123(nuevo,null));
            return (true);
        }            
        return (this.insertar(this.raiz,nuevo));
    }
    
    /**
     * Método de tipo privado que permite insertar un nuevo dato dentro del Árbol 1-2-3 y que este mantenga
     * sus propiedades como tal de ordenamiento de los datos. <br>
     * @param r Representa la raíz del Árbol o del sub_Árbol. <br>
     * @param dato Representa el nuevo dato de tipo T a insertar dentro del Árbol. <br>
     * @return Un objeto de tipo boolean que permite evaluar el éxito de la operación realizada.
     */
    private boolean insertar(Nodo123<T> r, T dato) {
        int comp1=0, comp2=0;
        boolean hayInfoI=false, hayInfoF=false;
        //Si existe el InfoMenor
        if(r.getInfoMen()!=null){
            hayInfoI = true;
            comp1 = ((Comparable)dato).compareTo(r.getInfoMen());
        }
        //Si existe el InfoMayor
        if(r.getInfoMay()!=null){
            hayInfoF = true;
            comp2 = ((Comparable)dato).compareTo(r.getInfoMay());
        }        
        if(r.getInfoMen()==null && r.getInfoMay()==null){
            r.setInfoMen(dato);
            return (true);
        } 
        if(r.getInfoMay()==null){
            if(hayInfoI&&comp1>0){
                r.setInfoMay(dato);
                return (true);
            }
            if(hayInfoI&&comp1<0){
                //cambio de posiciones los infos
                if(r.getIzq()==null){
                    r.setInfoMay(r.getInfoMen());
                    r.setInfoMen(dato);
                    return (true);
                }else{
                    return (insertar(r.getIzq(),dato));
                }
            }
        }
        //Si es menor que InfoMenor
        if(hayInfoI&&comp1<0){
            if(r.getIzq()==null)
                r.setIzq(new Nodo123());
            return (this.insertar(r.getIzq(),dato));
        }
        //Si es mayor que InfoMenor pero menor que InfoMayor
        if((hayInfoI&&comp1>0) && (hayInfoF&&comp2<0)){
            //Insertar por el medio
            if(r.getMed()==null)
                r.setMed(new Nodo123());
            return (this.insertar(r.getMed(),dato));
        }
        //Si es mayor que InfoMayor
        if((hayInfoI&&comp1>0) && (hayInfoF&&comp2>0)){
            //Insertar por la derecha
            if(r.getDer()==null)
                r.setDer(new Nodo123());
            return (this.insertar(r.getDer(),dato));
        }
        return (false);
    }
    
    /**
     * Método que permite eliminar un dato del Arbol1-2-3; manteniendo el Árbol sus propiedades de ordenamiento. <br>
     * <b>post: </b> Se elimino un elemento del Arbol1-2-3 y este ha mantenido sus propiedades.
     * @param dato Representa el Objeto de tipo T que se desea eliminar del Árbol.
     * @return true
     */
    public boolean eliminar(T dato){
        if(this.esVacio() || !this.esta(dato))
            return (false);
        this.setRaiz(eliminar(this.raiz,dato));
        return (true);
    }
    
     /**
     * Método de tipo privado que permite desconectar un Nodo del Arbol1-2-3; manteniendo el Árbol sus propiedades de ordenamiento. <br>
     * @param r Representa la raíz del Árbol o del sub_Árbol sobre el cual se elimina el dato. <br>
     * @param dato Es un objeto de tipo T y representa el dato que se desea eliminar del Árbol. <br>
     * @return Un objeto de tipo Nodo123&#60;T&#62; que representa la raíz del Árbol una vez eliminado el objeto del Árbol.
     */
    private Nodo123<T> eliminar(Nodo123<T> r, T dato) {
        int comp1=0, comp2=0;
        boolean hayInfoI=false, hayInfoF=false;
        //Si existe el InfoMenor
        if(r.getInfoMen()!=null){
            hayInfoI = true;
            comp1 = ((Comparable)dato).compareTo(r.getInfoMen());
        }
        //Si existe el InfoMayor
        if(r.getInfoMay()!=null){
            hayInfoF = true;
            comp2 = ((Comparable)dato).compareTo(r.getInfoMay());
        }
        //Si lo ubica como InfoMenor
        if(hayInfoI && comp1==0){
            if(r.getIzq()!=null){
                r.setInfoMen(getMayor(r.getIzq()));
                r.setIzq(eliminar(r.getIzq(),r.getInfoMen()));
                return (r);
            }
            if(r.getInfoMay()!=null){
                r.setInfoMen(r.getInfoMay());
                r.setInfoMay(r.getDer()!=null?getMayor(r.getDer()):null);
                if(r.getInfoMay()!=null )
                    r.setDer(eliminar(r.getDer(),r.getInfoMay()));
                r.setIzq(r.getMed());
                r.setMed(r.getDer());
                r.setDer(null);
                return (r);
            }
            if(r.getMed()!=null){
                return r.getMed();
            }
            return (null);
        } 
        else
            //Si lo ubico como InfoMayor
        if((hayInfoF&&comp2==0) && r.getInfoMay()!=null){
            if( r.getDer() != null ){
                r.setInfoMay(getMenor(r.getDer()));
                r.setDer(eliminar(r.getDer(), r.getInfoMay()));
                return (r);
            }          
            if(r.getMed()!=null){
                r.setInfoMay(getMayor(r.getMed()));
                r.setMed(eliminar(r.getMed(),r.getInfoMay()));
            }   
            else
            {
                r.setInfoMay(null);
            }
            return (r); 
        }
        //Buscar por la Izquierda
        if((hayInfoI&&comp1<0) && r.getIzq()!=null){
            r.setIzq(eliminar(r.getIzq(),dato));
            return (r);
        } 
        //Buscar por el medio
        if((r.getInfoMay()==null || (hayInfoF&&comp2<0)) && r.getMed()!=null){
            r.setMed(eliminar(r.getMed(),dato));
            return (r);
        } 
        //Buscar por la derecha
        if( r.getDer()!=null ){
            r.setDer(eliminar(r.getDer(),dato));
            return (r);
        }
        return (r);        
    }
     
    /**
     * Método de tipo privado que permite conocer el dato de mayor valor teniendo un Nodo123<T> como referencia. <br>
     * @param r Representa la raíz del Árbol o sub_Árbol sobre el cual se quiere conocer el dato mayor. <br>
     * @return Un objeto de tipo T que representa el dato de mayor valor a la derecha de un Nodo. <br>
     */
    private T getMayor(Nodo123<T> r){
        if(r.getDer()!=null)
            return(getMayor(r.getDer()));
        if(r.getInfoMay()!=null)
            return (r.getInfoMay());
        if(r.getMed()!=null)
            return(getMayor(r.getMed()));
        return (r.getInfoMen());
    }
     
    /**
     * Método de tipo privado que permite conocer el dato de menor valor teniendo un Nodo123<T> como referencia. <br>
     * @param r Representa la raíz del Árbol o sub_Árbol sobre el cual se quiere conocer el dato menor. <br>
     * @return Un objeto de tipo T que representa el dato de menor valor a la izquierda de un Nodo. <br>
     */
    private T getMenor(Nodo123<T> r){
        Nodo123<T> aux = r;
        while(aux.getIzq()!=null)
            aux = aux.getIzq();
        return (aux.getInfoMen());
    }
    
    /**
     * Método que permite evaluar la existencia de un objeto dentro del Árbol 1-2-3. <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el Árbol. <br>
     * @param dato Representa el elemento el cual se desea evaluar su existencia en el Árbol. <br>
     * @return Un boolean , true si el dato esta o false en caso contrario.
     */
    public boolean esta(T dato){
        if(this.esVacio())
            return (false);
        return (this.esta(this.raiz,dato));
    }

    /**
     * Método privado que permite conocer si un elemento especifico se encuentra en el Árbol. <br>
     * <b>post: </b> Se retorno true si el elemento se encuentra en el Árbol. <br>
     * @param r Representa la raíz del Árbol o del sub_Árbol. <br>
     * @param dato Representa el elemento que se desea encontrar del Árbol <br>
     * @return Un boolean, true si el dato esta o false en caso contrario.
     */
    private boolean esta(Nodo123<T> r, T dato) {        
        int comp1=0, comp2=0;
        boolean hayInfoI=false, hayInfoF=false;
        //Existe el InfoMenor
        if(r.getInfoMen()!=null){
            hayInfoI = true;
            comp1 = ((Comparable)dato).compareTo(r.getInfoMen());
        }
        //Existe el InfoMayor
        if(r.getInfoMay()!=null){
            hayInfoF = true;
            comp2 = ((Comparable)dato).compareTo(r.getInfoMay());
        } 
        if((hayInfoI&&comp1==0) || (hayInfoF&&comp2==0))
            return (true);   
        //Busca por izquierda
        if(hayInfoI&&comp1<0){
            if(r.getIzq()==null)
                return (false);
            return (this.esta(r.getIzq(),dato));
        }
        //Busca por el medio
        if(hayInfoF&&comp2<0){
            if(r.getMed()==null)
                return (false);
            return (this.esta(r.getMed(), dato));
        }
        //Busca por la derecha
        if(r.getDer()!=null)            
        return (this.esta(r.getDer(), dato));
        //El objeto no ha sido encontrado
        return (false);
    }
    
    /**
     * Método que permite buscar un dato dentro del Árbol 1-2-3 y retornar el Nodo que lo contiene. <br>
     * <b>post: </b> Se retorno el Nodo123&#60;T&#62; que representa la ubicación del dato en el Árbol. <br>
     * @param dato Representa el dato que se quiere localizar dentro del Árbol 1-2-3. <br>
     * @return Un objeto de tipo Nodo123&#60;T&#62; que representa la ubicación del dato dentro del Árbol.
     */
    public Nodo123<T> buscar(T dato){
        if(this.esVacio())
            return (null);
        return (this.buscar(this.raiz,dato));
    }

    /**
     * Método que permite buscar un dato dentro del Árbol 1-2-3 y retornar el Nodo que lo contiene. <br>
     * <b>post: </b> Se retorno el Nodo123<T> que representa la ubicación del dato en el Árbol. <br>
     * @param r Representa la raíz del Árbol 1-2-3 en el que se buscara el dato. <br>
     * @param dato Representa el dato que se quiere localizar dentro del Árbol 1-2-3. <br>
     * @return Un objeto de tipo Nodo123<T> que representa la ubicación del dato dentro del Árbol.
     */
    private Nodo123<T> buscar(Nodo123<T> r, T dato) {
        int comp1=0, comp2=0;
        boolean hayInfoI=false, hayInfoF=false;
        //Si existe el InfoMenor
        if(r.getInfoMen()!=null){
            hayInfoI = true;
            comp1 = ((Comparable)dato).compareTo(r.getInfoMen());
        }
        //Si existe el InfoMAyor
        if(r.getInfoMay()!=null){
            hayInfoF = true;
            comp2 = ((Comparable)dato).compareTo(r.getInfoMay());
        }         
        if((hayInfoI&&comp1==0) || (hayInfoF&&comp2==0))
            return (r);     
        //Buscar por la izquierda
        if(hayInfoI&&comp1<0){
            if(r.getIzq()==null)
                return (null);
            return (this.buscar(r.getIzq(),dato));
        }
        //Buscar por el medio
        if(hayInfoF&&comp2<0){
            if(r.getMed()==null)
                return (null);
            return (this.buscar(r.getMed(), dato));
        }
        //Buscar por la derecha
        if(r.getDer()!=null)            
        return (this.buscar(r.getDer(), dato));
        //El objeto no ha sido encontrado
        return (null);
    }
    
    /**
     * Método que retorna un iterador con las hojas del Árbol 1-2-3. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol. <br>
     * @return Un iterador con las hojas del Árbol.
     */
    public Iterator<T> getHojas(){
        ListaCD<T> l=new ListaCD<T>();
        this.getHojas(this.raiz,l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un iterador con las hojas del Árbol 1-2-3. <br>
     * <b>post: </b> Se retorno un iterador con las hojas del Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol. <br>
     */
    private void getHojas(Nodo123<T> r, ListaCD<T> l){
        if (r!=null){
            if(this.esHoja(r)){
                if(r.getInfoMen()!=null)
                    l.insertarAlFinal(r.getInfoMen());
                if(r.getInfoMay()!=null)
                l.insertarAlFinal(r.getInfoMay());
            }
            this.getHojas(r.getIzq(), l);
            this.getHojas(r.getMed(), l);
            this.getHojas(r.getDer(), l);
        }
    }
    
    /**
     * Método de tipo privado que permite saber si un elemento es una hoja. <br>
     * <b>post: </b> Se retorno true si es nodo hoja del Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @return true Si es una hoja; es decir, sus dos hijos son null. <br>
     */
    private boolean esHoja(Nodo123<T> r) {
        return(r!=null&&r.getIzq()==null&&r.getMed()==null&&r.getDer()==null);
    }
    
    /**
     * Método que permite determinar el número de Nodo hojas dentro del Árbol. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol. <br>
     * @return El número de hojas existentes en el Árbol.
     */
    public int contarHojas(){
        return (this.contarHojas(this.raiz));
    }
    
    /**
     * Método que permite determinar el número de Nodo hojas dentro del Árbol. <br>
     * <b>post: </b> Se retorno el número de hojas del Árbol. <br>
     * @return Un objeto de tipo int con el número de hojas existentes en el Árbol.
     */
    private int contarHojas(Nodo123<T> r){
        if(r==null)
            return (0);
        if(this.esHoja(r))
            return (1);
        int chi = contarHojas(r.getIzq());
        int chm = contarHojas(r.getMed());
        int chd = contarHojas(r.getDer());
        return (chi+chm+chd);
    }
    
    /**
     * Método que retorna un iterador con el recorrido preOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador en preOrden para el Árbol.<br>
     * @return Un iterador en preorden (padre1-&#62;hijoIzq-&#62;padre2-&#62;hijoMed-&#62;hijoDer) para el Árbol 1-2-3.
     */
    public Iterator<T> preOrden(){
        ListaCD<T> l=new ListaCD<T>();
        preOrden(this.getRaiz(),l);
        return (l.iterator());
    }

    /**
      * Método que tipo privado que retorna un iterador con el recorrido preOrden del Árbol. <br>
      * <b>post: </b> Se retorno un iterador en preOrden para el Árbol. <br>
      * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
      * @param l Lista para el almacenamiento de los datos del Árbol. <br>
      */
    private void  preOrden(Nodo123<T> r, ListaCD<T> l){
        if(r!=null){
            if(r.getInfoMen()!=null)
            l.insertarAlFinal(r.getInfoMen());            
            if(r.getInfoMay()!=null)
            l.insertarAlFinal(r.getInfoMay());
            preOrden(r.getIzq(), l);          
            preOrden(r.getMed(),l);            
            preOrden(r.getDer(), l);
        }
    }
    
        /**
     * Método que retorna un iterador con el recorrido in Orden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el Árbol. <br>
     * @return Un iterador en inOrden (hijoIzq-&#62;padre1-&#62;hijoMed-&#62;padre2-&#62;hijoDer) para el Árbol 1-2-3. <br>
     */
    public Iterator<T> inOrden(){
        ListaCD<T> l=new ListaCD<T>();
        inOrden(this.getRaiz(),l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un iterador con el recorrido in Orden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador inOrden para el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol. <br>
     */
    private void  inOrden(Nodo123<T> r, ListaCD<T> l){
        if(r!=null){
            inOrden(r.getIzq(), l);
            if(r.getInfoMen()!=null)
            l.insertarAlFinal(r.getInfoMen());            
            if(r.getInfoMay()!=null)
            l.insertarAlFinal(r.getInfoMay());
            inOrden(r.getMed(),l);                                  
            inOrden(r.getDer(), l);
        }
    }
    
    /**
     * Método que retorna un iterador con el recorrido postOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador preOrden para el Árbol. <br>
     * @return Un iterador en postOrden (hijoIzq-hijoMed-&#62;hijoDer-&#62;padre1-&#62;padre2) para el Árbol 1-2-3. <br>
     */
    public Iterator<T> postOrden(){
        ListaCD<T> l=new ListaCD<T>();
        postOrden(this.getRaiz(),l);
        return (l.iterator());
    }

    /**
     * Método de tipo privado que retorna un iterador con el recorrido postOrden del Árbol. <br>
     * <b>post: </b> Se retorno un iterador preOrden para el Árbol. <br>
     * @param r Representa la raíz del Árbol, o raíz de sub_Árbol. <br>
     * @param l Lista para el almacenamiento de los datos del Árbol
     */
    private void  postOrden(Nodo123<T> r, ListaCD<T> l){
        if(r!=null){            
            postOrden(r.getIzq(), l);            
            postOrden(r.getMed(),l);            
            postOrden(r.getDer(), l);
            if(r.getInfoMen()!=null)
            l.insertarAlFinal(r.getInfoMen());            
            if(r.getInfoMay()!=null)
            l.insertarAlFinal(r.getInfoMay());
        }
    }
    
    /**
     * Método que permite retornar un iterador con el recorrido por Niveles del Árbol. <br>
     * <b>post: </b> Se retorno el recorrido por niveles del Árbol 1-2-3. <br>
     * @return Un iterador con el recorrido por niveles del Árbol 1-2-3.
     */
    public Iterator<T> impNiveles(){
        ListaCD<T> l=new ListaCD<T>();
        if(!this.esVacio()){
            Cola<Nodo123<T>> c=new Cola<Nodo123<T>>();
            c.enColar(this.getRaiz());
            Nodo123<T> x;
                while(!c.esVacia()){
                    x=c.deColar();
                    l.insertarAlFinal(x.getInfoMen());
                    l.insertarAlFinal(x.getInfoMay());
                    if(x.getIzq()!=null)
                    c.enColar(x.getIzq());
                    if(x.getMed()!=null)
                    c.enColar(x.getMed());
                    if(x.getDer()!=null)
                    c.enColar(x.getDer());
                }
        }
        return (l.iterator());
    }
    
    /**
     * Método que permite obtener el peso del Árbol 1-2-3. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol 1-2-3. <br>
     * @return Un entero con la cantidad de elementos del Árbol 1-2-3.
     */
    public int getPeso(){
        return(getPeso(this.getRaiz()));
    }

    /**
     * Método de tipo privado que permite conocer el número de elemento del Árbol 1-2-3. <br>
     * <b>post: </b> Se retorno el número de elementos en el Árbol. <br>
     * @param r Representa la raíz del árbol, o raíz de sub_Árbol. <br>
     * @return El número de elementos que contiene el Árbol 1-2-3.
     */
    private int getPeso(Nodo123<T> r){
        if(r==null)
            return 0;
        int n=1;
        if(r.getInfoMay()!=null)
            n=2;
        return (getPeso(r.getIzq())+n+getPeso(r.getMed())+getPeso(r.getDer()));
    }
    
    /**
     * Método que permite saber si el Árbol 1-2-3 se encuentra vacío. <br>
     * <b>post: </b> Se retorno true si el árbol no contiene elementos. <br>
     * @return true Si no hay datos en el Árbol 1-2-3.
     */
    public boolean esVacio() {
        return (this.raiz==null);
    }
    
    /**
     * Método que permite obtener la altura del Árbol 1-2-3. <br>
     * <b>post: </b> Se retorno la altura del Árbol 1-2-3. <br>
     * @return Un entero con la altura del Árbol 1-2-3.
     */
    public int getAltura(){
        if(this.raiz==null)
            return (0);
        return(getAltura(this.getRaiz()));
    }

    /**
     * Método de tipo privado que permite conocer la Altura del Árbol 1-2-3. <br>
     * <b>post: </b> Se retorno la altura del Árbol 1-2-3. <br>
     * @param r Representa la raíz del árbol, o raíz de sub_Árbol. <br>
     * @return Un entero con la altura del Árbol 1-2-3.
     */
    public int getAltura(Nodo123<T> r){
        int ai=0, am=0, ad=0, alt;
        if(r.getIzq()!=null)
            ai = getAltura(r.getIzq());
        if(r.getMed()!=null)
            am = getAltura(r.getMed());
        if(r.getDer()!=null)
            ad = getAltura(r.getDer());
        alt = ai;
        if(am>=ai)
            alt = am;
        if(ad>=alt)
            alt = ad;
        return (alt+1);        
    }
    
    /**
     * Método que permite eliminar los elementos Hoja de un Arbol123.
     */
    public void podar() {
        if(this.esHoja(raiz))
            this.setRaiz(null);
        podar(this.raiz);
    }

    private void podar(Nodo123<T> r) {
        if (r==null)
            return;
        if(this.esHoja(r.getIzq()))
            r.setIzq(null);
        if(this.esHoja(r.getMed()))
            r.setMed(null);
        if(this.esHoja(r.getDer()))
            r.setDer(null);
        podar(r.getIzq());
        podar(r.getMed());
        podar(r.getDer());
    }
    
    /**
     * Método que permite conocer por consola la información del Árbol.
     */
    public void imprime(){
        imprime(this.raiz);
    }
    
    /**
     * Método de tipo privado que permite mostrar por consola la información del Árbol 1-2-3. <br>
     * @param r Representa la raíz del Arbol1-2-3 o de alguno de sus sub_Arboles.
     */
    public void imprime(Nodo123<T> r){
        if(this.esVacio())
            return;
        String cad= "";
        cad+="Info:"+r.getInfoMen()+"."+r.getInfoMay()+"";;
        if(r.getIzq()!=null)
            cad+="\t Izq"+r.getIzq().getInfoMen()+"."+r.getIzq().getInfoMay()+"";
        if(r.getMed()!=null)
            cad+="\t Med"+r.getMed().getInfoMen()+"."+r.getMed().getInfoMay()+"";
        if(r.getDer()!=null) 
            cad+="\t Der"+r.getDer().getInfoMen()+"."+r.getDer().getInfoMay()+"";
        System.out.println(cad);
        if(r.getIzq()!=null)
            imprime(r.getIzq());
        if(r.getMed()!=null)
            imprime(r.getMed());
        if(r.getDer()!=null) 
            imprime(r.getDer());
        
    }
    
} //Fin de Clase Arbol1-2-3

