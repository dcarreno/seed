/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ArbolSplay;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ArbolSplay.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolSplayTest extends TestCase{
    
    private ArbolSplay miAS;
    
    public ArbolSplayTest() {
        super();
    }
    
    public void setUp() {
        this.miAS=new ArbolSplay();
    }

    /**
     * Prueba Metodo getObjRaiz(), de la clase  ArbolSplay.
     */
    @Test
    public void testGetObjRaiz() {
        System.out.println("Prueba... getObjRaiz()");
        assertTrue(this.miAS.insertar(8));
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.imprime();
        assertNotNull(this.miAS.getRaiz());
        assertEquals(6,this.miAS.getObjRaiz());
    }

    /**
     *  Prueba Metodo insertar(), de la clase  ArbolSplay.
     */
    @Test
    public void testInsertar() {
        System.out.println("Prueba... insertar()");
        assertTrue(this.miAS.insertar(8));
        assertTrue(this.miAS.insertar(9));
        assertTrue(this.miAS.insertar(1));
        assertTrue(this.miAS.insertar(3));
        assertTrue(this.miAS.insertar(4));
        assertTrue(this.miAS.insertar(6));
        assertTrue(this.miAS.insertar(7));
        assertTrue(this.miAS.insertar(10));
        assertTrue(this.miAS.insertar(12));
        assertEquals(12, this.miAS.getObjRaiz());
    }

    /**
     *  Prueba Metodo eliminar(), de la clase  ArbolSplay.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        assertTrue(this.miAS.eliminar(1));
        assertTrue(this.miAS.eliminar(7));
        assertTrue(this.miAS.eliminar(9));
    }

    /**
     *  Prueba Metodo estaAS(), de la clase  ArbolSplay.
     */
    @Test
    public void testEstaAS() {
        System.out.println("Prueba... estaAS()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        assertTrue(this.miAS.esta(8));
        assertTrue(this.miAS.esta(3));
    }

    /**
     *  Prueba Metodo getHojas(), de la clase  ArbolSplay.
     */
    @Test
    public void testGetHojas() {
        System.out.println("Prueba... getHojas()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        String cad="1-";
         String cad2="";
          assertNotNull(this.miAS.getHojas());
           Iterator<Integer> it= this.miAS.getHojas();
            while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     *  Prueba Metodo contarHojas(), de la clase  ArbolSplay.
     */
    @Test
    public void testContarHojas() {
        System.out.println("Prueba... contarHojas()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        assertTrue(this.miAS.contarHojas()==1);
    }

    /**
     *  Prueba Metodo preOrden(), de la clase  ArbolSplay.
     */
    @Test
    public void testPreOrden() {
        System.out.println("Prueba... preOrden()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        String cad2="";
         assertNotNull(this.miAS.preOrden());
         String cad="12-10-9-8-7-6-4-3-1-";
          Iterator<Integer> it= this.miAS.preOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     *  Prueba Metodo inOrden(), de la clase  ArbolSplay.
     */
    @Test
    public void testInOrden() {
        System.out.println("Prueba... inOrden()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
         String cad="1-3-4-6-7-8-9-10-12-";
         assertNotNull(this.miAS.inOrden());
         String cad2="";
          Iterator<Integer> it= this.miAS.inOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    
    }

    /**
     *  Prueba Metodo postOrden(), de la clase  ArbolSplay.
     */
    @Test
    public void testPostOrden() {
        System.out.println("Prueba... postOrden()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        String cad="1-3-4-6-7-8-9-10-12-";
         assertNotNull(this.miAS.postOrden());
         String cad2="";
         Iterator<Integer> it= this.miAS.postOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     *  Prueba Metodo impNiveles(), de la clase  ArbolSplay.
     */
    @Test
    public void testImpNiveles() {
        System.out.println("Prueba... impNiveles()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
    }

    /**
     *  Prueba Metodo getPeso(), de la clase  ArbolSplay.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        assertTrue(this.miAS.getPeso()==9);
    }

    /**
     *  Prueba Metodo esVacio(), de la clase  ArbolSplay.
     */
    @Test
    public void testEsVacio() {
        System.out.println("Prueba... esVacio()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        assertFalse(this.miAS.esVacio());
    }

    /**
     *  Prueba Metodo getAltura(), de la clase  ArbolSplay.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
         this.miAS.insertar(8);
        this.miAS.insertar(9);
        this.miAS.insertar(1);
        this.miAS.insertar(3);
        this.miAS.insertar(4);
        this.miAS.insertar(6);
        this.miAS.insertar(7);
        this.miAS.insertar(10);
        this.miAS.insertar(12);
        assertTrue(this.miAS.getAltura()==9);
    }


}