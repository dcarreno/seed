/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.TablaHash;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Tabla Hash.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */
public class TablaHashTest extends TestCase{
    
    private TablaHash<Integer,String> miTH;
    
    public TablaHashTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miTH=new TablaHash(31);
    }
    
    /**
     * Prueba Metodo insertar(), de la clase TablaHash.
     */
    @Test
    public void testInsertar() {
        System.out.println("Prueba... insertar()");
        assertTrue(this.miTH.insertar(89, "yuli")!=null);
        assertTrue(this.miTH.insertar(12, "pepe")!=null);
        assertTrue(this.miTH.insertar(9, "pablo")!=null);
        assertTrue(this.miTH.insertar(49, "maria")!=null);
        assertTrue(this.miTH.insertar(79, "mery")!=null);
        assertTrue(this.miTH.insertar(19, "carol")!=null);
        assertTrue(this.miTH.insertar(22, "pipe")!=null);
        assertTrue(this.miTH.insertar(11, "lili")!=null);
        assertTrue(this.miTH.insertar(34, "cristi")!=null);
        assertTrue(this.miTH.insertar(66, "andres")!=null);
        assertTrue(this.miTH.insertar(99, "lucia")!=null);
        assertTrue(this.miTH.insertar(11, "jose")!=null);
        assertTrue(this.miTH.insertar(13, "juan")!=null);
    }

    /**
     * Prueba Metodo eliminar(), de la clase  TablaHash.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
        this.miTH.insertar(89, "yuli");
        this.miTH.insertar(12, "pepe");
        this.miTH.insertar(9, "pablo");
        this.miTH.insertar(49, "maria");
        this.miTH.insertar(79, "mery");
        this.miTH.insertar(19, "carol");
        this.miTH.insertar(22, "pipe");
        this.miTH.insertar(11, "lili");
        this.miTH.insertar(34, "cristi");
        this.miTH.insertar(66, "andres");
        this.miTH.insertar(99, "lucia");
        this.miTH.insertar(11, "jose");
        this.miTH.insertar(13, "juan");
        assertTrue(this.miTH.eliminar(79)!=null);
        assertTrue(this.miTH.eliminar(19)!=null);
        assertTrue(this.miTH.eliminar(22)!=null);
        assertTrue(this.miTH.eliminar(11)!=null);
    }

    /**
     * Prueba Metodo esta(), de la clase  TablaHash.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
        this.miTH.insertar(89, "yuli");
        this.miTH.insertar(12, "pepe");
        this.miTH.insertar(9, "pablo");
        this.miTH.insertar(49, "maria");
        this.miTH.insertar(79, "mery");
        this.miTH.insertar(19, "carol");
        this.miTH.insertar(22, "pipe");
        this.miTH.insertar(11, "lili");
        this.miTH.insertar(34, "cristi");
        this.miTH.insertar(66, "andres");
        this.miTH.insertar(99, "lucia");
        this.miTH.insertar(11, "jose");
        this.miTH.insertar(13, "juan");
        assertTrue(this.miTH.esta(66));
        assertFalse(this.miTH.esta(16));
        assertTrue(this.miTH.esta(11));
        assertTrue(this.miTH.esta(13));
    }

     /**
     *Prueba Metodo eliminarTodo(), de la clase  TablaHash.
     */
    @Test
    public void testEliminarTodo() {
        System.out.println("Prueba... EliminarTodo()");
        this.miTH.insertar(89, "yuli");
        this.miTH.insertar(12, "pepe");
        this.miTH.insertar(9, "pablo");
        this.miTH.insertar(49, "maria");
        this.miTH.insertar(79, "mery");
        this.miTH.insertar(19, "carol");
        this.miTH.insertar(22, "pipe");
        this.miTH.insertar(11, "lili");
        this.miTH.insertar(34, "cristi");
        this.miTH.insertar(66, "andres");
        this.miTH.insertar(99, "lucia");
        this.miTH.insertar(11, "jose");
        this.miTH.insertar(13, "juan");
        this.miTH.eliminarTodo();
        assertTrue(this.miTH.esVacia());
    }

    /**
     * Prueba Metodo getNumeroDatos(), de la clase  TablaHash.
     */
    @Test
    public void testGetNumeroDatos() {
        System.out.println("Prueba... getNumeroDatos()");
        this.miTH.insertar(89, "yuli");
        this.miTH.insertar(12, "pepe");
        this.miTH.insertar(9, "pablo");
        this.miTH.insertar(49, "maria");
        this.miTH.insertar(79, "mery");
        this.miTH.insertar(19, "carol");
        this.miTH.insertar(22, "pipe");
        this.miTH.insertar(11, "lili");
        this.miTH.insertar(34, "cristi");
        this.miTH.insertar(66, "andres");
        this.miTH.insertar(99, "lucia");
        this.miTH.insertar(11, "jose");
        this.miTH.insertar(13, "juan");
        assertTrue(this.miTH.getNumeroDatos()==12);
    }

    /**
     * Prueba Metodo getNumeroSlots(), de la clase  TablaHash.
     */
    @Test
    public void testGetNumeroSlots() {
        System.out.println("Prueba... getNumeroSlots()");
        assertTrue(this.miTH.getNumeroSlots()==31);
    }

    /**
     * Prueba Metodo getInformacionEntrada(), de la clase  TablaHash.
     */
    @Test
    public void testGetInformacionEntrada() {
        System.out.println("Prueba... getInformacionEntrada()");
        this.miTH.insertar(89, "yuli");
        this.miTH.insertar(12, "pepe");
        this.miTH.insertar(9, "pablo");
        this.miTH.insertar(49, "maria");
        this.miTH.insertar(79, "mery");
        this.miTH.insertar(19, "carol");
        this.miTH.insertar(22, "pipe");
        this.miTH.insertar(11, "lili");
        this.miTH.insertar(34, "cristi");
        this.miTH.insertar(66, "andres");
        this.miTH.insertar(99, "lucia");
        this.miTH.insertar(11, "jose");
        this.miTH.insertar(13, "juan");
        System.out.println(this.miTH.getInformacionEntrada().toString());
    }



}