/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ArbolAVL;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ArbolAVL.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolAVLTest  extends TestCase{
    
    private ArbolAVL miAVL;
    
    public ArbolAVLTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miAVL=new ArbolAVL();
    }

    /**
     * Prueba Metodo getObjRaiz(), de la clase ArbolAVL.
     */
    @Test
    public void testGetObjRaiz() {
        System.out.println("Prueba... getObjRaiz()");
       this.miAVL.insertar(2);
       this.miAVL.insertar(3);
       this.miAVL.insertar(8);
       this.miAVL.insertar(23);
       this.miAVL.insertar(12);
        assertNotNull(this.miAVL.getRaiz());
        assertEquals(this.miAVL.getObjRaiz(),3);
    }

    /**
     * Prueba Metodo insertar(), de la clase ArbolAVL.
     */
    @Test
    public void testInsertar() {
        System.out.println("Prueba...  insertar()");
        assertTrue(this.miAVL.insertar(23));
        assertTrue(this.miAVL.insertar(7));
        assertTrue(this.miAVL.insertar(25));
        assertTrue(this.miAVL.insertar(3));
        assertTrue(this.miAVL.insertar(18));
        assertTrue(this.miAVL.insertar(24));
        assertTrue(this.miAVL.insertar(30));
        assertTrue(this.miAVL.insertar(1));
        assertTrue(this.miAVL.insertar(5));
        assertTrue(this.miAVL.insertar(8));
        assertTrue(this.miAVL.insertar(19));
        assertTrue(this.miAVL.insertar(28));
        assertTrue(this.miAVL.insertar(35));
    }

    /**
     * Prueba Metodo eliminar(), de la clase ArbolAVL.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba...  eliminar()");
        this.miAVL.insertar(23);
        this.miAVL.insertar(7);
        this.miAVL.insertar(25);
        this.miAVL.insertar(3);
        this.miAVL.insertar(18);
        this.miAVL.insertar(24);
        this.miAVL.insertar(30);
        this.miAVL.insertar(1);
        this.miAVL.insertar(5);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(28);
        this.miAVL.insertar(35);
        assertFalse(this.miAVL.eliminar(6));
        assertTrue(this.miAVL.eliminar(7));
        assertTrue(this.miAVL.eliminar(30));
        assertTrue(this.miAVL.eliminar(24));
        assertFalse(this.miAVL.esta(7));
        assertFalse(this.miAVL.esta(30));
        assertFalse(this.miAVL.esta(24));
    }

    /**
     * Prueba Metodo esta(), de la clase ArbolAVL.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba...  esta()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(28);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
         assertNotNull(this.miAVL.getRaiz().getDer());
        assertFalse(this.miAVL.estaABB(34));
        assertTrue(this.miAVL.estaABB(23));
    }

    /**
     * Prueba Metodo getHojas(), de la clase ArbolAVL.
     */
    @Test
    public void testGetHojas() {
        System.out.println("Prueba...  getHojas()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
        String cad="2-5-11-13-19-23-28-";
        String cad2="";
        assertNotNull(this.miAVL.getHojas());
         Iterator<Integer> it= this.miAVL.getHojas();
         while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo contarHojas(), de la clase ArbolAVL.
     */
    @Test
    public void testContarHojas() {
        System.out.println("Prueba...  contarHojas()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
        assertNotNull(this.miAVL.getHojas());
        assertTrue(this.miAVL.contarHojas()==7);
    }

    /**
     * Prueba Metodo preOrden(), de la clase ArbolAVL.
     */
    @Test
    public void testPreOrden() {
        System.out.println("Prueba...  preOrden()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
         String cad2="";
         assertNotNull(this.miAVL.preOrden());
         String cad="15-8-4-2-5-12-11-13-20-17-19-25-23-28-";
          Iterator<Integer> it= this.miAVL.preOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo inOrden(), de la clase ArbolAVL.
     */
    @Test
    public void testInOrden() {
        System.out.println("Prueba...  inOrden()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
         String cad2="";
         assertNotNull(this.miAVL.inOrden());
         String cad="2-4-5-8-11-12-13-15-17-19-20-23-25-28-";
          Iterator<Integer> it= this.miAVL.inOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo posOrden(), de la clase ArbolAVL.
     */
    @Test
    public void testPosOrden() {
        System.out.println("Prueba...  posOrden()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
         String cad="2-5-4-11-13-12-8-19-17-23-28-25-20-15-";
         String cad2="";
         assertNotNull(this.miAVL.postOrden());
          Iterator<Integer> it= this.miAVL.postOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo impNiveles(), de la clase ArbolAVL.
     */
    @Test
    public void testImpNiveles() {
        System.out.println("Prueba...  impNiveles()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
        String cad="15-8-20-4-12-17-25-2-5-11-13-19-23-28-";
         String cad2="";
         assertNotNull(this.miAVL.impNiveles());
          Iterator<Integer> it= this.miAVL.impNiveles();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo getPeso(), de la clase ArbolAVL.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba...  getPeso()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
        assertTrue(this.miAVL.getPeso()==14);
    }

    /**
     * Prueba Metodo vacio(), de la clase ArbolAVL.
     */
    @Test
    public void testEsVacio() {
        System.out.println("Prueba...  esVacio()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
        assertFalse(this.miAVL.esVacio());
    }

    /**
     * Prueba Metodo getAltura(), de la clase ArbolAVL.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba...  getAltura()");
        this.miAVL.insertar(2);
        this.miAVL.insertar(17);
        this.miAVL.insertar(15);
        this.miAVL.insertar(13);
        this.miAVL.insertar(28);
        this.miAVL.insertar(4);
        this.miAVL.insertar(20);
        this.miAVL.insertar(11);
        this.miAVL.insertar(25);
        this.miAVL.insertar(8);
        this.miAVL.insertar(19);
        this.miAVL.insertar(5);
        this.miAVL.insertar(23);
        this.miAVL.insertar(12);
        assertTrue(this.miAVL.getAltura()==4);
    }
}