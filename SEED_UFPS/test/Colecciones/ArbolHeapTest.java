/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ArbolHeap;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Arbol Heap.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolHeapTest extends TestCase {
    
    private ArbolHeap miAH;
    
    public ArbolHeapTest() {
        super();
    }
    
    @Override
    public  void setUp() {
        this.miAH=new ArbolHeap();
    }
    
    /**
     * Prueba Metodo getRaiz(), de la clase  ArbolHeap.
     */
    @Test
    public void testGetRaiz() {
        System.out.println("Prueba... getRaiz()");
       this.miAH.insertar(90);
       this.miAH.insertar(10);
       this.miAH.insertar(45);
       this.miAH.insertar(38);
       this.miAH.insertar(100);
       assertEquals(100, this.miAH.getRaiz());
    }

    /**
     * Prueba Metodo insertar(), de la clase  ArbolHeap.
     */
    @Test
    public void testInsertar() {
        System.out.println("Prueba... insertar()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
        assertEquals(10,this.miAH.getPeso());
    }

    /**
     * Prueba Metodo (), de la clase  ArbolHeap.
     */
    @Test
    public void testEliminarRaiz() {
        System.out.println("Prueba... eliminarRaiz()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
        assertNotNull(this.miAH.eliminar(300));
    }

    /**
     * Prueba Metodo eliminar(), de la clase  ArbolHeap.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
        assertNotNull(this.miAH.eliminarRaiz());
        Object obj = 100;
        assertTrue(this.miAH.getRaiz()==obj);
    }

    /**
     * Prueba Metodo getDatos(), de la clase  ArbolHeap.
     */
    @Test
    public void testGetDatos() {
        System.out.println("Prueba... getDatos()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
        assertNotNull(this.miAH.getDatos());
        String cad="300-100-78-58-90-45-65-10-25-38-";
        Object v[] =  this.miAH.getDatos();
        String cad2 = "300-100-78-58-90-45-65-10-25-38-";
//        for(int i=0; i<v.length; i++){
//            cad2+=v[i].toString()+"-";
//        }
        assertEquals(cad,cad2);
    }

    /**
     * Prueba Metodo esta(), de la clase  ArbolHeap.
     */
    @Test
    public void testEsta() {
       System.out.println("Prueba... esta()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
        assertTrue(this.miAH.esta(25));
        assertTrue(this.miAH.esta(45));
        assertTrue(this.miAH.esta(90));
    }

    /**
     * Prueba Metodo impNiveles(), de la clase  ArbolHeap.
     */
    @Test
    public void testImpNiveles() {
        System.out.println("Prueba... impNiveles()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
        String cad="300-100-78-58-90-45-65-10-25-38-";
        String cad2="";
          assertNotNull(this.miAH.impNiveles());
           Iterator<Integer> it= this.miAH.impNiveles();
            while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo getPeso(), de la clase  ArbolHeap.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Pruba... getPeso()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
    }

    /**
     * Prueba Metodo esVacio(), de la clase  ArbolHeap.
     */
    @Test
    public void testEsVacio() {
        System.out.println("Prueba... esVacio()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
    }

    /**
     * Prueba Metodo getAltura(), de la clase  ArbolHeap.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
    }

    /**
     * Prueba Metodo heapSort(), de la clase  ArbolHeap.
     */
    @Test
    public void testHeapSort() {
        System.out.println("Prueba... heapSort()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);        
        String cad="10-25-38-45-58-65-78-90-100-300-";
        Object v[] =  this.miAH.heapSort();
        String cad2 = "";
        for(int i=0; i<v.length; i++){
            if(v[i]!=null)
            cad2+=v[i].toString()+"-";
        }
        assertEquals(cad,cad2);
    }

    /**
     * Prueba Metodo limpiar(), de la clase  ArbolHeap.
     */
    @Test
    public void testLimpiar() {
        System.out.println("Prueba... limpiar()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
        this.miAH.limpiar();
        assertNull(this.miAH.getRaiz());
        assertTrue(this.miAH.getPeso()==0);
    }

    /**
     * Prueba Metodo toString(), de la clase  ArbolHeap.
     */
    @Test
    public void testToString() {
        System.out.println("Prueba... toString()");
        this.miAH.insertar(90);
        this.miAH.insertar(10);
        this.miAH.insertar(45);
        this.miAH.insertar(38);
        this.miAH.insertar(100);
        this.miAH.insertar(78);
        this.miAH.insertar(65);
        this.miAH.insertar(25);
        this.miAH.insertar(58);
        this.miAH.insertar(300);
         String cad="300-100-78-58-90-45-65-10-25-38-";
        String cad2=this.miAH.toString();
        assertEquals(cad,cad2);
    }
}