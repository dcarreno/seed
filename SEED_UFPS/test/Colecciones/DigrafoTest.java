/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.Vertice;
import Colecciones.ListaCD;
import Colecciones.Arista;
import Colecciones.Digrafo;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Digrafo.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class DigrafoTest extends TestCase {
    
    private Digrafo miDigrafo;
    
    public DigrafoTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miDigrafo = new Digrafo();
    }
    
    /**
     * Prueba de Metodo insertarVertice de la clase Digrafo.
     */
    @Test
    public void testInsertarVertice() {
        System.out.println("Prueba... insertarVertice()");
        Object vertice = 87 ;
        boolean expResult = true;
        boolean result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 90;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 23;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(true, result);
        vertice = 87;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(false, result);
        vertice = 77;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 77;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(false, result);
    }

    /**
     * Prueba de Metodo insertarArista de la clase Digrafo.
     */
    @Test
    public void testInsertarArista() {
        System.out.println("Prueba... insertarArista()");
        Object verticeA = 87 ;
        Object verticeB = 15 ;
        boolean result = this.miDigrafo.insertarArista(verticeA, verticeB);
        assertEquals(false, result);
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        result = this.miDigrafo.insertarArista(verticeA, verticeB);
        assertEquals(true, result);
        result = this.miDigrafo.insertarArista(verticeA, verticeA);
        assertEquals(true, result);
        Object verticeC = 59 ;
        this.miDigrafo.insertarVertice(verticeC);
        result = this.miDigrafo.insertarArista(verticeA, verticeC);
        assertEquals(true, result);
        result = this.miDigrafo.insertarArista(verticeC, verticeB);
        assertEquals(true, result);
    }

    /**
     * Prueba de Metodo getVertices de la clase Digrafo.
     */
    @Test
    public void testGetVertices() {
        System.out.println("Prueba... getVertices()");
        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(23);
        expResult.insertarAlFinal(87);
        expResult.insertarAlFinal(10);
        expResult.insertarAlFinal(48);
        expResult.insertarAlFinal(31);
        this.miDigrafo.insertarVertice(23);
        this.miDigrafo.insertarVertice(87);
        this.miDigrafo.insertarVertice(10);
        this.miDigrafo.insertarVertice(48);
        this.miDigrafo.insertarVertice(31);        
        ListaCD result = this.miDigrafo.getVertices();
        for(int i=0; i<result.getTamanio();i++){
             assertEquals(result.get(i).toString(), expResult.get(i).toString());
        }
    }

    /**
     * Prueba de Metodo getAristas de la clase Digrafo.
     */
    @Test
    public void testGetAristas() {
        System.out.println("Prueba... getAristas()");
        Object v1 = 23;
        Object v2 = 278;
        Object v3 = 72;
        Object v4 = 60;
        this.miDigrafo.insertarVertice(v1);
        this.miDigrafo.insertarVertice(v2);
        this.miDigrafo.insertarVertice(v3);
        this.miDigrafo.insertarVertice(v4);
        this.miDigrafo.insertarArista(v1, v2);
        this.miDigrafo.insertarArista(v3, v2);
        this.miDigrafo.insertarArista(v3, v4);
        this.miDigrafo.insertarArista(v1, v4);
        ListaCD result = this.miDigrafo.getAristas();
        assertTrue(result.get(0).toString().equals("("+v1.toString()+"-"+v2.toString()+")"));
        assertTrue(result.get(1).toString().equals("("+v3.toString()+"-"+v2.toString()+")"));
        assertTrue(result.get(2).toString().equals("("+v3.toString()+"-"+v4.toString()+")"));
        assertTrue(result.get(3).toString().equals("("+v1.toString()+"-"+v4.toString()+")"));
    }

    /**
     * Prueba de Metodo eliminarVertice de la clase Digrafo.
     */
    @Test
    public void testEliminarVertice() {
        System.out.println("Prueba... eliminarVertice()");
        Object vertice = 87 ;
        boolean expResult = true;
        boolean result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 90;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 23;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(true, result);
        vertice = 87;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(false, result);
        vertice = 77;
        result = this.miDigrafo.insertarVertice(vertice);
        assertEquals(expResult, result);
        assertFalse(this.miDigrafo.insertarVertice(77));
        this.miDigrafo.eliminarVertice(77);
        assertTrue(this.miDigrafo.insertarVertice(77));
    }

    /**
     * Prueba de Metodo eliminarArista de la clase Digrafo.
     */
    @Test
    public void testEliminarArista() {
        System.out.println("Prueba... eliminarArista()");
        Object verticeA = 87 ;
        Object verticeB = 15 ;
        boolean result = this.miDigrafo.insertarArista(verticeA, verticeB);
        assertEquals(false, result);
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        result = this.miDigrafo.insertarArista(verticeA, verticeB);
        assertEquals(true, result);
        result = this.miDigrafo.insertarArista(verticeA, verticeA);
        assertEquals(true, result);
        Object verticeC = 59 ;
        this.miDigrafo.insertarVertice(verticeC);
        result = this.miDigrafo.insertarArista(verticeA, verticeC);
        assertEquals(true, result);
        result = this.miDigrafo.insertarArista(verticeC, verticeB);
        assertEquals(true, result);
        verticeA = 44;
        assertFalse(this.miDigrafo.eliminarArista(verticeC, verticeA));
        verticeA = 87;
        assertTrue(this.miDigrafo.eliminarArista(verticeA, verticeC));
        assertTrue(this.miDigrafo.eliminarArista(verticeA, verticeA));
        verticeB = 33;
        assertFalse(this.miDigrafo.eliminarArista(verticeB, verticeB));
        verticeB = 15;
        assertTrue(this.miDigrafo.eliminarArista(verticeC, verticeB));
    }

    /**
     * Prueba de Metodo buscarVertice de la clase Digrafo.
     */
    @Test
    public void testBuscarVertice() {
        System.out.println("Prueba.. buscarVertice()");
        Object vertice = 6;
        Object vertice1 = 4;
        Object vertice2 = 89;
        Object vertice3 = 123;
        Vertice expResult = null;
        Vertice result = this.miDigrafo.buscarVertice(vertice);
        assertEquals(expResult, result);
        this.miDigrafo.insertarVertice(vertice);
        this.miDigrafo.insertarVertice(vertice1);
        this.miDigrafo.insertarVertice(vertice2);
        this.miDigrafo.insertarVertice(vertice3);
        assertNotNull(this.miDigrafo.buscarVertice(vertice3));
        assertNotNull(this.miDigrafo.buscarVertice(vertice2));
        assertEquals(this.miDigrafo.buscarVertice(vertice).toString(),vertice.toString());
        assertEquals(this.miDigrafo.buscarVertice(vertice1).toString(),vertice1.toString());
    }

    /**
     * Prueba de Metodo buscarArista de la clase Digrafo.
     */
    @Test
    public void testBuscarArista() {
        System.out.println("Prueba... buscarArista()");
        Object vertice = 6;
        Object vertice1 = 4;
        Object vertice2 = 89;
        Object vertice3 = 123;
        this.miDigrafo.insertarVertice(vertice);
        this.miDigrafo.insertarVertice(vertice1);
        this.miDigrafo.insertarVertice(vertice2);
        this.miDigrafo.insertarVertice(vertice3);
        this.miDigrafo.insertarArista(vertice, vertice1);
        this.miDigrafo.insertarArista(vertice1, vertice2);
        this.miDigrafo.insertarArista(vertice2, vertice3);
        this.miDigrafo.insertarArista(vertice3, vertice);
        Arista expResult = null;
        Arista result = this.miDigrafo.buscarArista(vertice,vertice2);
        assertEquals(expResult, result);
        result = this.miDigrafo.buscarArista(vertice1,vertice3);
        assertEquals(expResult, result);
        assertNotNull(this.miDigrafo.buscarArista(vertice1, vertice2));
        assertNotNull(this.miDigrafo.buscarArista(vertice2, vertice3));
    }

    /**
     * Prueba de Metodo esta de la clase Digrafo.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
        Object verticeA = 87 ;
        Object verticeB = 15 ;
        Object verticeC = 33 ;
        Object verticeD = 98 ;
        Object verticeE = 67 ;
        Object verticeF = 76 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeE);
        assertTrue(this.miDigrafo.esta(new Vertice(verticeA)));
        assertTrue(this.miDigrafo.esta(new Vertice(verticeB)));
        assertTrue(this.miDigrafo.esta(new Vertice(verticeC)));
        assertFalse(this.miDigrafo.esta(new Vertice(verticeD)));
        assertTrue(this.miDigrafo.esta(new Vertice(verticeE)));
        assertFalse(this.miDigrafo.esta(new Vertice(verticeF)));
    }

    /**
     * Prueba de Metodo getMatrizAdyacencia de la clase Digrafo.
     */
    @Test
    public void testGetMatrizAdyacencia() {
        System.out.println("Prueba... getMatrizAdyacencia()");
        Object verticeA = 87 ;
        Object verticeB = 15 ;
        Object verticeC = 33 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);        
        Object[][] expResult = new Object[4][4];
        expResult[1][1]=0;  expResult[1][2]=1;  expResult[1][3]=1;
        expResult[2][1]=0;  expResult[2][2]=0;  expResult[2][3]=0;
        expResult[3][1]=0;  expResult[3][2]=0;  expResult[3][3]=0;        
        Object[][] result = this.miDigrafo.getMatrizAdyacencia();
        for(int i=1; i<4; i++)
            for(int j=1; j<4; j++)
                assertEquals(result[i][j].toString(),expResult[i][j].toString());
    }

    /**
     * Prueba de Metodo getMatrizIncidencia de la clase Digrafo.
     */
    @Test
    public void testGetMatrizIncidencia() {
        System.out.println("Prueba... getMatrizIncidencia()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);  
        Object[][] expResult = new Object[4][3];
        expResult[1][1]=1;  expResult[1][2]=1;  
        expResult[2][1]=-1;  expResult[2][2]=0;  
        expResult[3][1]=0;  expResult[3][2]=-1;  
        Object[][] result = this.miDigrafo.getMatrizIncidencia();
        for(int i=1; i<4; i++)
            for(int j=1; j<3; j++)
                assertEquals(result[i][j].toString(),expResult[i][j].toString());
    }

    /**
     * Prueba de Metodo esMultigrafo de la clase Digrafo.
     */
    @Test
    public void testEsMultigrafo() {
        System.out.println("Prueba... esMultigrafo()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        assertFalse(this.miDigrafo.esMultigrafo());
        this.miDigrafo.insertarArista(verticeA, verticeB);
        assertTrue(this.miDigrafo.esMultigrafo());
        
    }

    /**
     * Prueba de Metodo esPseudoGrafo de la clase Digrafo.
     */
    @Test
    public void testEsPseudoGrafo() {
        System.out.println("Prueba.. esPseudoGrafo()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        assertFalse(this.miDigrafo.esPseudoGrafo());
        this.miDigrafo.insertarArista(verticeC, verticeC);
        assertTrue(this.miDigrafo.esPseudoGrafo());
    }


    /**
     * Prueba de Metodo esGrafoNulo de la clase Digrafo.
     */
    @Test
    public void testEsGrafoNulo() {
        System.out.println("Prueba... esGrafoNulo()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        assertEquals(this.miDigrafo.esGrafoNulo(),-1);
        this.miDigrafo.eliminarArista(verticeA, verticeB);
        this.miDigrafo.eliminarArista(verticeA, verticeC);
        assertEquals(this.miDigrafo.esGrafoNulo(),3);
    }

    /**
     * Prueba de Metodo esConexo de la clase Digrafo.
     */
    @Test
    public void testEsConexo() {
        System.out.println("Prueba... esConexo()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        assertFalse(this.miDigrafo.esConexo());
        this.miDigrafo.insertarArista(verticeA, verticeC);
        assertTrue(this.miDigrafo.esConexo());
    }


    /**
     * Prueba de Metodo esGrafoRegular de la clase Digrafo.
     */
    @Test
    public void testEsGrafoRegular() {
        System.out.println("Prueba... esGrafoRegular()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        assertFalse(this.miDigrafo.esGrafoRegular());
        this.miDigrafo.insertarArista(verticeB, verticeC);
        assertTrue(this.miDigrafo.esGrafoRegular());
    }

    /**
     * Prueba de Metodo esCompleto de la clase Digrafo.
     */
    @Test
    public void testEsCompleto() {
        System.out.println("Prueba... esCompleto()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        assertEquals(this.miDigrafo.esCompleto(),-1);
        this.miDigrafo.insertarArista(verticeB, verticeC);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeC, verticeA);
        this.miDigrafo.insertarArista(verticeC, verticeB);
        assertEquals(this.miDigrafo.esCompleto(),3);
        
    }

   
    /**
     * Prueba de Metodo esGrafoHamiltoniano de la clase Digrafo.
     */
    @Test
    public void testEsGrafoHamiltoniano() {
        System.out.println("Prueba... esGrafoHamiltoniano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        assertFalse(this.miDigrafo.esGrafoHamiltoniano());
        this.miDigrafo.insertarArista(verticeD, verticeB);
        assertTrue(this.miDigrafo.esGrafoHamiltoniano());
    }

    /**
     * Prueba de Metodo getCicloHamiltoniano de la clase Digrafo.
     */
    @Test
    public void testGetCicloHamiltoniano() {
        System.out.println("Prueba... getCicloHamiltoniano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeB);

        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeB);
        ListaCD result = this.miDigrafo.getCicloHamiltoniano();
        
        for(int i=0; i<expResult.getTamanio(); i++){
            assertEquals(expResult.get(i).toString(),result.get(i).toString());
        }        
    }

    /**
     * Prueba de Metodo hayCaminoHamiltoniano de la clase Digrafo.
     */
    @Test
    public void testHayCaminoHamiltoniano() {
        System.out.println("Prueba... hayCaminoHamiltoniano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        assertTrue(this.miDigrafo.hayCaminoHamiltoniano());
    }

    /**
     * Prueba de Metodo getCaminoHamiltoniano de la clase Digrafo.
     */
    @Test
    public void testGetCaminoHamiltoniano() {
        System.out.println("Prueba... getCaminoHamiltoniano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeB);

        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeB);
        ListaCD result = this.miDigrafo.getCaminoHamiltoniano();
        
        for(int i=0; i<expResult.getTamanio(); i++){
            assertEquals(expResult.get(i).toString(),result.get(i).toString());
        }        
    }

    /**
     * Prueba de Metodo esGrafoEuleriano de la clase Digrafo.
     */
    @Test
    public void testEsGrafoEuleriano() {
        System.out.println("Prueba... esGrafoEuleriano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeB);
        assertTrue(this.miDigrafo.esGrafoEuleriano());
    }

    /**
     * Prueba de Metodo getCicloEuleriano de la clase Digrafo.
     */
    @Test
    public void testGetCicloEuleriano() {
        System.out.println("Prueba... getCicloEuleriano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeB);

        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeB);
        expResult.insertarAlFinal(verticeA);
        ListaCD result = this.miDigrafo.getCicloEuleriano();
        
        for(int i=0; i<expResult.getTamanio(); i++){
            assertEquals(expResult.get(i).toString(),result.get(i).toString());
        }        
    }

    /**
     * Prueba de Metodo hayCaminoEuleriano de la clase Digrafo.
     */
    @Test
    public void testHayCaminoEuleriano() {
        System.out.println("Prueba... hayCaminoEuleriano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeB);
        assertTrue(this.miDigrafo.hayCaminoEuleriano());
    }

    /**
     * Prueba de Metodo getCaminoEuleriano de la clase Digrafo.
     */
    @Test
    public void testGetCaminoEuleriano() {
        System.out.println("Prueba... getCaminoEuleriano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeB);

        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeB);
        ListaCD result = this.miDigrafo.getCaminoEuleriano();
        
        for(int i=0; i<expResult.getTamanio(); i++){
            assertEquals(expResult.get(i).toString(),result.get(i).toString());
        }        
    }

    /**
     * Prueba de Metodo existeRutaEntre de la clase Digrafo.
     */
    @Test
    public void testExisteRutaEntre() {
        System.out.println("Prueba... existeRutaEntre()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeB, verticeA);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);

        assertTrue(this.miDigrafo.existeRutaEntre(verticeA, verticeC));
        assertTrue(this.miDigrafo.existeRutaEntre(verticeB, verticeC));
        assertFalse(this.miDigrafo.existeRutaEntre(verticeD, verticeC));
    }

    /**
     * Prueba de Metodo getLongitudDeCamino de la clase Digrafo.
     */
    @Test
    public void testGetLongitudDeCamino() {
        System.out.println("Prueba... getLongitudDeCamino()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        assertEquals(this.miDigrafo.getLongitudDeCamino(verticeA, verticeC),1);
        assertEquals(this.miDigrafo.getLongitudDeCamino(verticeA, verticeD),2);
        assertEquals(this.miDigrafo.getLongitudDeCamino(verticeD, verticeB),-1);
    }

    /**
     * Prueba de Metodo longRutaMinimaDijkstra de la clase Digrafo.
     */
    @Test
    public void testLongRutaMinimaDijkstra() {
        System.out.println("Prueba... longRutaMinimaDijkstra()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarAristaP(verticeA, verticeB, 6);
        this.miDigrafo.insertarAristaP(verticeA, verticeC, 23);
        this.miDigrafo.insertarAristaP(verticeC, verticeD, 90);
        this.miDigrafo.insertarAristaP(verticeB, verticeD, 12);
        assertEquals(this.miDigrafo.longRutaMinimaDijkstra(verticeA, verticeD), 18);
        this.miDigrafo.insertarAristaP(verticeC, verticeA, 56);
        assertEquals(this.miDigrafo.longRutaMinimaDijkstra(verticeC, verticeD), 74);
    }


    /**
     * Prueba de Metodo rutaMasCorta de la clase Digrafo.
     */
    @Test
    public void testRutaMasCorta() {
        System.out.println("Prueba... rutaMasCorta()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarAristaP(verticeA, verticeB, 6);
        this.miDigrafo.insertarAristaP(verticeA, verticeC, 23);
        this.miDigrafo.insertarAristaP(verticeC, verticeD, 90);
        this.miDigrafo.insertarAristaP(verticeB, verticeD, 12);
        this.miDigrafo.insertarAristaP(verticeC, verticeA, 56);
        assertEquals(this.miDigrafo.rutaMasCorta(verticeA, verticeC),2);
        assertEquals(this.miDigrafo.rutaMasCorta(verticeA, verticeD),3);
    }

    /**
     * Prueba de Metodo getBEP de la clase Digrafo.
     */
    @Test
    public void testGetBEP_GenericType() {
        System.out.println("Prueba... getBEP()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        Object verticeE = 911 ;
        
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarVertice(verticeE);
        
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeE);
        
        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeE);
        ListaCD result = this.miDigrafo.getBEP(verticeA.toString());
        for(int i=0; i<result.getTamanio();i++)
            assertEquals(result.get(i).toString(),expResult.get(i).toString());
    }

    /**
     * Prueba de Metodo getBEA de la clase Digrafo.
     */
    @Test
    public void testGetBEA_GenericType() {
        System.out.println("Prueba... getBEA()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        Object verticeE = 911 ;
        
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarVertice(verticeE);
        
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeE);
        
        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeB);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeE);
        ListaCD result = this.miDigrafo.getBEA(verticeA.toString());
        for(int i=0; i<result.getTamanio();i++)
            assertEquals(result.get(i).toString(),expResult.get(i).toString());
    }

    /**
     * Prueba de Metodo sonVerticesAdyacentes de la clase Digrafo.
     */
    @Test
    public void testSonVerticesAdyacentes() {
        System.out.println("Prueba... sonVerticesAdyacentes()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        Object verticeE = 911 ;
        
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarVertice(verticeE);
        
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeE);
        
        assertTrue(this.miDigrafo.sonVerticesAdyacentes(verticeA, verticeB));
        assertFalse(this.miDigrafo.sonVerticesAdyacentes(verticeC, verticeB));
        assertTrue(this.miDigrafo.sonVerticesAdyacentes(verticeC, verticeD));
        assertFalse(this.miDigrafo.sonVerticesAdyacentes(verticeD, verticeB));
        assertTrue(this.miDigrafo.sonVerticesAdyacentes(verticeD, verticeE));
    }

    /**
     * Prueba de Metodo getGradoSalidaVert de la clase Digrafo.
     */
    @Test
    public void testGetGradoSalidaVert() {
        System.out.println("Prueba.. getGradoSalidaVert()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarVertice(verticeE);
        
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeE);
        
        assertEquals(this.miDigrafo.getGradoSalidaVert(verticeA),2);
        assertEquals(this.miDigrafo.getGradoSalidaVert(verticeB),0);
        assertEquals(this.miDigrafo.getGradoSalidaVert(verticeC),1);
        assertEquals(this.miDigrafo.getGradoSalidaVert(verticeD),1); 
        assertEquals(this.miDigrafo.getGradoSalidaVert(verticeE),0);  
    }

    /**
     * Prueba de Metodo getGradoEntradaVert de la clase Digrafo.
     */
    @Test
    public void testGetGradoEntradaVert() {
        System.out.println("Prueba... getGradoEntradaVert()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarVertice(verticeE);
        
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        this.miDigrafo.insertarArista(verticeD, verticeE);
        
        assertEquals(this.miDigrafo.getGradoEntradaVert(verticeA),0);
        assertEquals(this.miDigrafo.getGradoEntradaVert(verticeB),1);
        assertEquals(this.miDigrafo.getGradoEntradaVert(verticeC),1);
        assertEquals(this.miDigrafo.getGradoEntradaVert(verticeD),1);
    }

    /**
     * Prueba de Metodo esVerticeAislado de la clase Digrafo.
     */
    @Test
    public void testEsVerticeAislado() {
        System.out.println("Prueba... esVerticeAislado()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miDigrafo.insertarVertice(verticeA);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarVertice(verticeE);
        
        this.miDigrafo.insertarArista(verticeA, verticeB);
        this.miDigrafo.insertarArista(verticeA, verticeC);
        this.miDigrafo.insertarArista(verticeC, verticeD);
        
        assertTrue(this.miDigrafo.esVerticeAislado(verticeA));
        assertFalse(this.miDigrafo.esVerticeAislado(verticeB));
        assertFalse(this.miDigrafo.esVerticeAislado(verticeC));
        assertFalse(this.miDigrafo.esVerticeAislado(verticeD));
        assertTrue(this.miDigrafo.esVerticeAislado(verticeE));
    }

    /**
     * Prueba de Metodo getPeso de la clase Digrafo.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miDigrafo.insertarVertice(verticeA);
        assertEquals(this.miDigrafo.getPeso(),1);
        this.miDigrafo.insertarVertice(verticeB);
        this.miDigrafo.insertarVertice(verticeC);
        assertEquals(this.miDigrafo.getPeso(),3);
        this.miDigrafo.insertarVertice(verticeD);
        this.miDigrafo.insertarVertice(verticeE);
        assertEquals(this.miDigrafo.getPeso(),5);        
    }

}