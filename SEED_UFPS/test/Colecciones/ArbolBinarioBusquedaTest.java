/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ArbolBinarioBusqueda;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ArbolBinarioBusqueda.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolBinarioBusquedaTest extends TestCase{
    
    private ArbolBinarioBusqueda miABB;
    
    public ArbolBinarioBusquedaTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miABB= new ArbolBinarioBusqueda();
    }
    
    /**
     * Prueba Metodo getObjRaiz(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testGetObjRaiz() {        
        System.out.println("Prueba... getObjRaiz()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        assertNotNull(this.miABB.getRaiz());
        assertEquals(this.miABB.getObjRaiz(),23);
    }
  /**
     * Prueba Metodo insertar(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testInsertar() {
        System.out.println("Prueba... insertar()");
        assertTrue(this.miABB.insertar(23));
        assertTrue(this.miABB.insertar(7));
        assertTrue(this.miABB.insertar(25));
        assertTrue(this.miABB.insertar(3));
        assertTrue(this.miABB.insertar(18));
        assertTrue(this.miABB.insertar(24));
        assertTrue(this.miABB.insertar(30));
        assertTrue(this.miABB.insertar(1));
        assertTrue(this.miABB.insertar(5));
        assertTrue(this.miABB.insertar(8));
        assertTrue(this.miABB.insertar(19));
        assertTrue(this.miABB.insertar(28));
        assertTrue(this.miABB.insertar(35));
        this.miABB.imprime();
    }
    
    /**
     * Prueba Metodo eliminar(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testEliminar() {        
        System.out.println("Prueba... eliminar()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
        assertFalse(this.miABB.eliminar(6));
        assertTrue(this.miABB.eliminar(7));
        assertTrue(this.miABB.eliminar(30));
    }

    /**
     * Prueba Metodo estaABB(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... estaABB()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
        assertNotNull(this.miABB.getRaiz().getDer());
        assertFalse(this.miABB.estaABB(34));
        assertTrue(this.miABB.estaABB(23));
    }

    /**
     * Prueba Metodo getHojas(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testGetHojas() {
        System.out.println("Prueba... getHojas()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
         String cad="1-5-8-19-24-28-35-";
         String cad2="";
          assertNotNull(this.miABB.getHojas());
           Iterator<Integer> it= this.miABB.getHojas();
            while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo contarHojas(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testContarHojas() {
        System.out.println("Prueba... contarHojas()");
         this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
         assertNotNull(this.miABB.getHojas());
         assertTrue(this.miABB.contarHojas()==7);
    }

    /**
     *  Prueba Metodo preOrden(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testPreOrden() {
        System.out.println("Prueba... preOrden()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
         String cad2="";
         assertNotNull(this.miABB.preOrden());
         String cad="23-7-3-1-5-18-8-19-25-24-30-28-35-";
          Iterator<Integer> it= this.miABB.preOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo inOrden(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testInOrden() {
        System.out.println("Prueba... inOrden()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
         String cad="1-3-5-7-8-18-19-23-24-25-28-30-35-";
         assertNotNull(this.miABB.inOrden());
         String cad2="";
          Iterator<Integer> it= this.miABB.inOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo posOrden(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testPosOrden() {
        System.out.println("Prueba... posOrden()");
         this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
        String cad="1-5-3-8-19-18-7-24-28-35-30-25-23-";
         assertNotNull(this.miABB.postOrden());
         String cad2="";
         Iterator<Integer> it= this.miABB.postOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo impNiveles(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testImpNiveles() {
        System.out.println("Prueba... impNiveles()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
        String cad="23-7-25-3-18-24-30-1-5-8-19-28-35-";
        String cad2="";
         Iterator<Integer> it= this.miABB.impNiveles();
          assertNotNull(it);
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo getPeso(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
        assertTrue(this.miABB.getPeso()==13);
    }

    /**
     * Prueba Metodo esVacio(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testEsVacio() {
        System.out.println("Prueba... esVacio()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
        assertFalse(this.miABB.esVacio());
    }

    /**
     * Prueba Metodo getAltura(), de la clase ArbolBinarioBusqueda.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
        this.miABB.insertar(23);
        this.miABB.insertar(7);
        this.miABB.insertar(25);
        this.miABB.insertar(3);
        this.miABB.insertar(18);
        this.miABB.insertar(24);
        this.miABB.insertar(30);
        this.miABB.insertar(1);
        this.miABB.insertar(5);
        this.miABB.insertar(8);
        this.miABB.insertar(19);
        this.miABB.insertar(28);
        this.miABB.insertar(35);
        assertTrue(this.miABB.getAltura()==4);
    }   
    
}