/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.Pila;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Pila.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class PilaTest extends TestCase{
    
    private Pila miPila;
    
    public PilaTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miPila= new Pila();
    }
    
    /**
     * Prueba Metodo apilar(), de la clase Pila.
     */
    @Test
    public void testApilar() {
        System.out.println("Prueba... apilar()");
        Object info = 0;
        this.miPila.apilar(info);
        info = 1;
        this.miPila.apilar(info);
        info = 2;
        this.miPila.apilar(info);
        info = 3;
        this.miPila.apilar(info);
        info = 4;
        this.miPila.apilar(info);
        info = 5;
        this.miPila.apilar(info);
        info = 6;
        this.miPila.apilar(info);
        info = 7;
        this.miPila.apilar(info);
        assertEquals(this.miPila.getTope(),info);
    }

    /**
     * Prueba Metodo desapilar(), de la clase Pila.
     */
    @Test
    public void testDesapilar() {
        System.out.println("Prueba... desapilar()");
        Object info = 0;
        this.miPila.apilar(info);
        info = 1;
        this.miPila.apilar(info);
        info = 2;
        this.miPila.apilar(info);
        info = 3;
        this.miPila.apilar(info);
        info = 4;
        this.miPila.apilar(info);
        info = 5;
        this.miPila.apilar(info);
        info = 6;
        this.miPila.apilar(info);
        
        this.miPila.desapilar();
        this.miPila.desapilar();
        this.miPila.desapilar();
        
        assertEquals(this.miPila.getTope(),3);
    }

    /**
     * Prueba Metodo vaciar(), de la clase Pila.
     */
    @Test
    public void testVaciar() {
        Object info = 0;
        this.miPila.apilar(info);
        info = 1;
        this.miPila.apilar(info);
        info = 2;
        this.miPila.apilar(info);
        info = 3;
        this.miPila.apilar(info);
        info = 4;
        this.miPila.apilar(info);
        info = 5;
        this.miPila.apilar(info);
        info = 6;
        this.miPila.apilar(info);
        info = 7;
        this.miPila.apilar(info);
        info = 8;
        this.miPila.apilar(info);
        info = 9;
        this.miPila.apilar(info);
        info = 10;
        this.miPila.apilar(info);
        info = 11;
        this.miPila.apilar(info);
        info = 12;
        this.miPila.apilar(info);
        info = 13;
        this.miPila.apilar(info);
        System.out.println("Prueba... vaciar()");
        this.miPila.vaciar();
        assertTrue(this.miPila.esVacia());
    }

    /**
     * Prueba Metodo getTope(), de la clase Pila.
     */
    @Test
    public void testGetTope() {
        Object info = 0;
        this.miPila.apilar(info);
        info = 1;
        this.miPila.apilar(info);
        info = 2;
        this.miPila.apilar(info);
        info = 3;
        this.miPila.apilar(info);
        info = 4;
        this.miPila.apilar(info);
        info = 5;
        this.miPila.apilar(info);
        info = 6;
        this.miPila.apilar(info);
        info = 7;
        this.miPila.apilar(info);
        info = 8;
        this.miPila.apilar(info);
        info = 9;
        this.miPila.apilar(info);
        info = 10;
        this.miPila.apilar(info);
        info = 11;
        this.miPila.apilar(info);
        info = 12;
        this.miPila.apilar(info);
        info = 13;
        this.miPila.apilar(info);
        System.out.println("Prueba... getTope()");
        assertNotNull(this.miPila.getTope());
    }

    /**
     * Prueba Metodo getTamanio(), de la clase Pila.
     */
    @Test
    public void testGetTamanio() {
        Object info = 0;
        this.miPila.apilar(info);
        info = 1;
        this.miPila.apilar(info);
        info = 2;
        this.miPila.apilar(info);
        info = 3;
        this.miPila.apilar(info);
        info = 4;
        this.miPila.apilar(info);
        info = 5;
        this.miPila.apilar(info);
        info = 6;
        this.miPila.apilar(info);
        info = 7;
        this.miPila.apilar(info);
        info = 8;
        this.miPila.apilar(info);
        info = 9;
        this.miPila.apilar(info);
        info = 10;
        this.miPila.apilar(info);
        info = 11;
        this.miPila.apilar(info);
        info = 12;
        this.miPila.apilar(info);
        info = 13;
        this.miPila.apilar(info);
        System.out.println("Prueba... getTamanio()");
        assertTrue(this.miPila.getTamanio()==14);
    }

    /**
     * Prueba Metodo esVacia(), de la clase Pila.
     */
    @Test
    public void testEsVacia() {
        Object info = 0;
        this.miPila.apilar(info);
        info = 1;
        this.miPila.apilar(info);
        info = 2;
        this.miPila.apilar(info);
        info = 3;
        this.miPila.apilar(info);
        info = 4;
        this.miPila.apilar(info);
        info = 5;
        this.miPila.apilar(info);
        System.out.println("Prueba... esVacia()");
        assertFalse(this.miPila.esVacia());
    }

    /**
     * Prueba Metodo toString(), de la clase Pila.
     */
    @Test
    public void testToString() {
         Object info = 23;
        this.miPila.apilar(info);
        info = 8;
        this.miPila.apilar(info);
        info = 2;
        this.miPila.apilar(info);
        info = 4;
        this.miPila.apilar(info);
        info = 20;
        this.miPila.apilar(info);
        info = 10;
        this.miPila.apilar(info);
        System.out.println("Prueba... toString()");
        String cad="10->20->4->2->8->23->";
        assertEquals(cad,this.miPila.toString());
        
    }
}