/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ListaD;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ListaD.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ListaDTest extends TestCase {
    
    private ListaD miListaD;
    
    public ListaDTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miListaD= new ListaD();
    }
    
    /**
     * Prueba Metodo insertarAlInicio(), de la clase ListaD.
     */
    @Test
    public void testInsertarAlInicio() {
        System.out.println("Prueba... insertarAlInicio()");
        String cadena = ("9<->21<->73<->22<->24<->16<->18<->46<->");
        Object x = 46;
        this.miListaD.insertarAlInicio(x);
        x=18;
        this.miListaD.insertarAlInicio(x);
         x=16;
        this.miListaD.insertarAlInicio(x);
         x=24;
        this.miListaD.insertarAlInicio(x);
         x=22;
        this.miListaD.insertarAlInicio(x);
         x=73;
        this.miListaD.insertarAlInicio(x);
         x=21;
        this.miListaD.insertarAlInicio(x);
         x=9;
        this.miListaD.insertarAlInicio(x);
        assertEquals(cadena, this.miListaD.toString());
    }

    /**
     * Prueba Metodo insertarAlFinal(), de la clase ListaD.
     */
    @Test
    public void testInsertarAlFinal() {
        String cadena = ("2<->1<->0<->13<->29<->3<->33<->22<->15<->");
         Object x = 1;
        this.miListaD.insertarAlInicio(x);
        x=2;
        this.miListaD.insertarAlInicio(x);
        System.out.println("Prueba... insertarAlFinal()");
         x = 0;
        this.miListaD.insertarAlFinal(x);
         x = 13;
        this.miListaD.insertarAlFinal(x);
         x = 29;
        this.miListaD.insertarAlFinal(x);
         x = 3;
        this.miListaD.insertarAlFinal(x);
         x = 33;
        this.miListaD.insertarAlFinal(x);
         x = 22;
        this.miListaD.insertarAlFinal(x);
         x = 15;
        this.miListaD.insertarAlFinal(x);
        assertEquals(cadena, this.miListaD.toString());
    }

    /**
     * Prueba Metodo eliminar(), de la clase ListaD.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
         Object x = 40;
        this.miListaD.insertarAlInicio(x);
        x=18;
        this.miListaD.insertarAlInicio(x);
        x=3;
        this.miListaD.insertarAlInicio(x);
        int i = this.miListaD.getIndice(18);
        Object result = this.miListaD.eliminar(i);
        assertNotNull( result);
    }

    /**
     * Prueba Metodo vaciar(), de la clase ListaD.
     */
    @Test
    public void testVaciar() {
        System.out.println("Prueba... vaciar()");
         Object x = 24;
        this.miListaD.insertarAlInicio(x);
        x=80;
        this.miListaD.insertarAlInicio(x);
        x=18;
        this.miListaD.insertarAlFinal(x);
       this.miListaD.vaciar();
       assertTrue(this.miListaD.esVacia());
    }

    /**
     * Prueba Metodo get(), de la clase ListaD.
     */
    @Test
    public void testGet() {
        System.out.println("Prueba... get()");
         Object x = 9;
        this.miListaD.insertarAlInicio(x);
        x=2;
        this.miListaD.insertarAlInicio(x);
         x=0;
        this.miListaD.insertarAlFinal(x);
         x=12;
        this.miListaD.insertarAlFinal(x);
        int i = this.miListaD.getIndice(2);
        Object result = this.miListaD.get(i);
       assertNotNull(result);
    }

    /**
     * Prueba Metodo set(), de la clase ListaD.
     */
    @Test
    public void testSet() {
        System.out.println("Prueba... set()");
         Object x = 17;
        this.miListaD.insertarAlInicio(x);
        x=10;
        this.miListaD.insertarAlInicio(x);
         x=27;
        this.miListaD.insertarAlFinal(x);
         x=8;
        this.miListaD.insertarAlFinal(x);
        int i = 2;
        Object dato = 5;
        this.miListaD.set(i, dato);
        assertTrue(this.miListaD.get(i)==dato);
    }

    /**
     * Prueba Metodo getTamanio(), de la clase ListaD.
     */
    @Test
    public void testGetTamanio() {
         Object x = 34;
        this.miListaD.insertarAlInicio(x);
        x=15;
        this.miListaD.insertarAlInicio(x);
         x=88;
        this.miListaD.insertarAlFinal(x);
         x=98;
        this.miListaD.insertarAlFinal(x);
         x=19;
        this.miListaD.insertarAlFinal(x);
        System.out.println("Prueba... getTamanio()");
        int result = this.miListaD.getTamanio();
        assertTrue(result==5);
    }

    /**
     * Prueba Metodo esVacia(), de la clase ListaD.
     */
    @Test
    public void testEsVacia() {
        System.out.println("Prueba... esVacia()");
         assertTrue(this.miListaD.esVacia());
    }

    /**
     * Prueba Metodo esta(), de la clase ListaD.
     */
    @Test
    public void testEsta() {
          Object x = 4;
        this.miListaD.insertarAlInicio(x);
        x=5;
        this.miListaD.insertarAlInicio(x);
         x=68;
        this.miListaD.insertarAlFinal(x);
         x=87;
        this.miListaD.insertarAlFinal(x);
         x=24;
        this.miListaD.insertarAlFinal(x);
        System.out.println("Prueba... esta()");
        Object info = 87;
        boolean result =this.miListaD.esta(info);
        assertNotNull( result);
    }

    /**
     * Prueba Metodo iterator(), de la clase ListaD.
     */
    @Test
    public void testIterator() {
         Object x = 4;
        this.miListaD.insertarAlInicio(x);
        x=5;
        this.miListaD.insertarAlInicio(x);
         x=58;
        this.miListaD.insertarAlFinal(x);
         x=81;
        this.miListaD.insertarAlFinal(x);
         x=11;
        this.miListaD.insertarAlFinal(x);
        System.out.println("Prueba... iterator()");
        String cad = "";
        Iterator result = this.miListaD.iterator();
        while(result.hasNext()){
            cad+= result.next().toString()+"<->";
        }
        String expResult = ("5<->4<->58<->81<->11<->");
        assertEquals(expResult, cad);
    }

    /**
     * TPrueba Metodo toString(), de la clase ListaD.
     */
    @Test
    public void testToString() {
         Object x = 19;
        this.miListaD.insertarAlInicio(x);
        x=51;
        this.miListaD.insertarAlInicio(x);
         x=16;
        this.miListaD.insertarAlFinal(x);
         x=10;
        this.miListaD.insertarAlFinal(x);
         x=71;
        this.miListaD.insertarAlFinal(x);
         x=21;
        this.miListaD.insertarAlFinal(x);
        System.out.println("Prueba... toString()");
        String expResult = "51<->19<->16<->10<->71<->21<->";
        String result = this.miListaD.toString();
        assertEquals(expResult, result);
    }

    /**
     * Prueba Metodo getIndice(), de la clase ListaD.
     */
    @Test
    public void testGetIndice() {
         Object x = 9;
        this.miListaD.insertarAlInicio(x);
        x=5;
        this.miListaD.insertarAlInicio(x);
        x=61;
        this.miListaD.insertarAlFinal(x);
        x=12;
        this.miListaD.insertarAlFinal(x);
        x=1;
        this.miListaD.insertarAlFinal(x);
        x=3;
        this.miListaD.insertarAlFinal(x);
        System.out.println("Prueba... getIndice()");
        Object info = 12;
        int result = this.miListaD.getIndice(info);
        assertTrue( result==3);
    }
}