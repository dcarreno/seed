/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;


import Colecciones.Secuencia;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Secuencia.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class SecuenciaTest extends TestCase {
    
    private static Secuencia<Object> miSec;
    
    public SecuenciaTest() {
        super();
    }
    
    @Override
    public void setUp() {
        miSec = new Secuencia(20);
    }

    /**
     * Prueba Metodo Insertar(), de la clase Secuencia.
     */
    @Test
    public void testInsertar() {
        System.out.println("Prueba... insertar()");
        Object elem = 32;
        miSec.insertar(elem);        
        assertTrue(miSec.getTamanio()==1);
        assertTrue(miSec.get(0).equals(32));
        elem = 68;
        miSec.insertar(elem);
        elem = 76;
        miSec.insertar(elem);
        assertFalse(miSec.getTamanio()==2);
        assertNotNull(miSec.get(2));
        assertTrue(miSec.get(0).equals(32)&&
                miSec.get(1).equals(68)&&miSec.get(2).equals(76));    
        assertNull(miSec.get(3));
    }

    /**
     * Prueba Metodo Eliminar(), de la clase Secuencia.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
        Object elem = 25;
        miSec.insertar(elem);   
        elem = 32;
        miSec.insertar(elem);   
        elem = 49;
        miSec.insertar(elem);   
        elem = 11;
        miSec.insertar(elem);   
        assertNotNull(miSec.get(3));
        assertTrue(miSec.get(1).equals(32)&&miSec.getTamanio()==4);
        miSec.eliminar(elem);
        assertTrue(miSec.get(2).equals(49)&&miSec.getTamanio()==3);
        elem = 25;
        miSec.eliminar(elem);
        assertFalse(miSec.get(0).equals(25));
        assertEquals(32,miSec.get(0));
        elem = 49;
        miSec.eliminar(elem);
        assertTrue(miSec.getTamanio()==1);
    }

    /**
     * Prueba Metodo EliminarP(), de la clase Secuencia.
     */
    @Test
    public void testEliminarP() {
        System.out.println("Prueba... eliminarP()");
        Object elem = 25;
        miSec.insertar(elem);   
        elem = 32;
        miSec.insertar(elem);   
        elem = 49;
        miSec.insertar(elem);   
        elem = 11;
        miSec.insertar(elem);   
        assertNotNull(miSec.get(3));
        assertTrue(miSec.get(1).equals(32)&&miSec.getTamanio()==4);
        miSec.eliminarP(3);
        assertTrue(miSec.get(2).equals(49)&&miSec.getTamanio()==3);
        assertNull(miSec.get(3));
        miSec.eliminarP(0);
        assertFalse(miSec.get(0).equals(25));
        assertEquals(32,miSec.get(0));
        miSec.eliminarP(1);
        assertTrue(miSec.getTamanio()==1);
    }

    /**
     * Prueba Metodo vaciar(), de la clase Secuencia.
     */
    @Test
    public void testVaciar() {
        System.out.println("Prueba... vaciar()");
        Object elem = 25;
        miSec.insertar(elem);   
        elem = 32;
        miSec.insertar(elem);   
        elem = 49;
        miSec.insertar(elem);   
        elem = 11;
        miSec.insertar(elem);   
        assertEquals(49,miSec.get(2));
        miSec.vaciar();
        assertFalse(miSec.getTamanio()==4);
    }

    /**
     * Prueba Metodo get(), de la clase Secuencia.
     */
    @Test
    public void testGet() {
        System.out.println("Prueba... get()");
        Object elem = 85;
        miSec.insertar(elem);   
        elem = 26;
        miSec.insertar(elem);   
        elem = 67;
        miSec.insertar(elem);   
        elem = 19;
        miSec.insertar(elem);   
        assertEquals(67,miSec.get(2));
        assertEquals(85,miSec.get(0));
        assertEquals(26,miSec.get(1));
        assertEquals(19,miSec.get(3));
        assertNull(miSec.get(4));
    }

    /**
     * Prueba Metodo set(), de la clase Secuencia.
     */
    @Test
    public void testSet() {
        System.out.println("Prueba... set()");
        Object elem = 85;
        miSec.insertar(elem);   
        elem = 26;
        miSec.insertar(elem);   
        elem = 67;
        miSec.insertar(elem);   
        elem = 19;
        miSec.insertar(elem);   
        assertEquals(67,miSec.get(2));
        assertEquals(85,miSec.get(0));
        assertEquals(26,miSec.get(1));
        assertEquals(19,miSec.get(3));
        assertNull(miSec.get(4));
        miSec.set(0, 89);
        miSec.set(3, 14);
        miSec.set(2, 28);
        miSec.set(1, 74);
        assertEquals(28,miSec.get(2));
        assertEquals(89,miSec.get(0));
        assertEquals(74,miSec.get(1));
        assertEquals(14,miSec.get(3));
    }

    /**
     * Pruba Metodo esta(), de la clase Secuencia.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
        Object elem = "Cindy";
        miSec.insertar(elem);   
        elem = "Carmen";
        miSec.insertar(elem);   
        elem = "Yolanda";
        miSec.insertar(elem);   
        elem = "Paulina";
        miSec.insertar(elem);   
        assertTrue(miSec.esta("Carmen"));
        assertTrue(miSec.esta("Paulina"));
        assertFalse(miSec.esta("Maria"));
        assertTrue(miSec.esta("Yolanda"));
        assertTrue(miSec.esta("Cindy"));
    }

    /**
     * Prueba Metodo getTamanio(), de la clase Secuencia.
     */
    @Test
    public void testGetTamanio() {
        System.out.println("Prueba... getTamanio()");
        assertTrue(miSec.getTamanio()==0);
        Object elem = 'B';
        miSec.insertar(elem);   
        elem = 'H';
        miSec.insertar(elem);   
        elem = 'P';
        miSec.insertar(elem);   
        elem = 'S';
        miSec.insertar(elem);   
        assertTrue(miSec.getTamanio()==4);
        miSec.eliminar('H');
        miSec.eliminarP(0);
        assertTrue(miSec.getTamanio()==2);        
    }

    /**
     * Prueba Metodo esVacia(), de la clase Secuencia.
     */
    @Test
    public void testEsVacia() {
        System.out.println("Prueba... esVacia()");
        assertTrue(miSec.esVacia());
        Object elem = 8;
        miSec.insertar(elem);
        assertFalse(miSec.esVacia());
        miSec.eliminarP(0);
        assertTrue(miSec.esVacia());
    }

    /**
     * Prueba Metodo getCapacidad(), de la clase Secuencia.
     */
    @Test
    public void testGetCapacidad() {
        System.out.println("Prueba... getCapacidad()");
        assertTrue(miSec.getCapacidad()==20);
    }

    /**
     * Prueba Metodo toString(), de la clase Secuencia.
     */
    @Test
    public void testToString() {
        System.out.println("Prueba... toString()");
        String val = "Secuencia vacia!";
        assertEquals(val,miSec.toString());
        Object elem = 85;
        miSec.insertar(elem);   
        elem = 26;
        miSec.insertar(elem);   
        elem = 67;
        miSec.insertar(elem);   
        elem = 19;
        miSec.insertar(elem);  
        val = "Secuencia -> | 85 | 26 | 67 | 19 | ";
        assertEquals(val,miSec.toString());
    }
}