/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ArbolB;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ArbolB.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolBTest extends TestCase {
    
    private ArbolB miArbolB;
    
    public ArbolBTest() {
        super();
    }
    
    @Override
    public  void setUp() {
        this.miArbolB=new ArbolB();
    }

    /**
     * Prueba Metodo getRaiz(), de la clase ArbolB.
     */
    @Test
    public void testGetRaiz() {
        System.out.println("Prueba... getRaiz()");
        assertTrue(this.miArbolB.insertar(5));
        assertTrue(this.miArbolB.insertar(33));
        assertTrue(this.miArbolB.insertar(3));
        assertTrue(this.miArbolB.insertar(15));
        assertTrue(this.miArbolB.insertar(25));
        assertTrue(this.miArbolB.insertar(11));
        assertTrue(this.miArbolB.insertar(8));
        Object obj = 15;
        assertTrue(this.miArbolB.getRaiz().getInfo()[0]==obj);
    }

    /**
     * Prueba Metodo getN(), de la clase ArbolB.
     */
    @Test
    public void testGetN() {
        System.out.println("Prueba... getN()");
        assertEquals(2,this.miArbolB.getN());
    }

    /**
     * Prueba Metodo getM(), de la clase ArbolB.
     */
    @Test
    public void testGetM() {
        System.out.println("Prueba... getM()");
        assertEquals(4,this.miArbolB.getM());
    }
    
    /**
     *Prueba Metodo getM1(), de la clase ArbolB.
     */
    @Test
    public void testGetM1() {
        System.out.println("Prueba... getM1()");
        assertEquals(5,this.miArbolB.getM1());
    }

    /**
     * Prueba Metodo setN(), de la clase ArbolB.
     */
    @Test
    public void testSetN() {
        System.out.println("Prueba... setN()");
        this.miArbolB.setN(6);
        assertTrue(this.miArbolB.getN()==6);
    }

    /**
     * Prueba Metodo insertar(), de la clase ArbolB.
     */
    @Test
    public void testInsertar() {
        System.out.println("Prueba... insertar()");
        assertTrue(this.miArbolB.insertar(5));
        assertTrue(this.miArbolB.insertar(33));
        assertTrue(this.miArbolB.insertar(3));
        assertTrue(this.miArbolB.insertar(15));
        assertTrue(this.miArbolB.insertar(25));
        assertTrue(this.miArbolB.insertar(11));
        assertTrue(this.miArbolB.insertar(8));
        System.out.println(this.miArbolB.imprime());
    }

    /**
     * Prueba Metodo eliminar(), de la clase ArbolB.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
        assertTrue(this.miArbolB.insertar(5));
        assertTrue(this.miArbolB.insertar(33));
        assertTrue(this.miArbolB.insertar(3));
        assertTrue(this.miArbolB.insertar(15));
        this.miArbolB.insertar(25);
        this.miArbolB.insertar(11);
        this.miArbolB.insertar(8);
        this.miArbolB.eliminar(15);
        Object obj = 11;
        assertTrue(this.miArbolB.getRaiz().getInfo()[0]==obj);
    }

    /**
     *Prueba Metodo esta(), de la clase ArbolB.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
        this.miArbolB.insertar(5);
        this.miArbolB.insertar(33);
        this.miArbolB.insertar(3);
       this.miArbolB.insertar(15);
        this.miArbolB.insertar(25);
        this.miArbolB.insertar(11);
        this.miArbolB.insertar(8);
        this.miArbolB.insertar(23);
       this.miArbolB.insertar(45);
        this.miArbolB.insertar(12);
        this.miArbolB.insertar(31);
        this.miArbolB.insertar(28);
        this.miArbolB.eliminar(15);
        assertFalse(this.miArbolB.esta(15));
        assertTrue(this.miArbolB.esta(33));
        assertTrue(this.miArbolB.esta(8));
        assertTrue(this.miArbolB.esta(23));
    }

    /**
     * Prueba Metodo getHojas(), de la clase ArbolB.
     */
    @Test
    public void testGetHojas() {
        System.out.println("Prueba... getHojas()");
        this.miArbolB.insertar(5);
        this.miArbolB.insertar(33);
        this.miArbolB.insertar(3);
       this.miArbolB.insertar(15);
        this.miArbolB.insertar(25);
        this.miArbolB.insertar(11);
        this.miArbolB.insertar(18);
        this.miArbolB.insertar(23);
       this.miArbolB.insertar(45);
        this.miArbolB.insertar(12);
        this.miArbolB.insertar(31);
        assertTrue(this.miArbolB.insertar(28));
        String cad="3-5-11-12-18-23-28-31-33-45-";
        String cad2="";
          assertNotNull(this.miArbolB.getHojas());
           Iterator<Integer> it= this.miArbolB.getHojas();
            while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo esVacio(), de la clase ArbolB.
     */
    @Test
    public void testEsVacio() {
        System.out.println("esVacio");
        this.miArbolB.insertar(5);
        this.miArbolB.insertar(33);
        this.miArbolB.insertar(3);
       this.miArbolB.insertar(15);
        this.miArbolB.insertar(25);
        this.miArbolB.insertar(11);
        this.miArbolB.insertar(18);
        this.miArbolB.insertar(23);
       this.miArbolB.insertar(45);
        this.miArbolB.insertar(12);
        this.miArbolB.insertar(31);
        this.miArbolB.insertar(28);
        this.miArbolB.eliminar(15);
        assertFalse(this.miArbolB.esVacio());
    }

    /**
     * Prueba Metodo inOrden(), de la clase ArbolB.
     */
    @Test
    public void testInOrden() {
        System.out.println("Prueba... inOrden()");
        this.miArbolB.insertar(5);
        this.miArbolB.insertar(33);
        this.miArbolB.insertar(3);
       this.miArbolB.insertar(15);
        this.miArbolB.insertar(25);
        this.miArbolB.insertar(11);
        this.miArbolB.insertar(18);
        this.miArbolB.insertar(23);
       this.miArbolB.insertar(45);
        this.miArbolB.insertar(12);
        this.miArbolB.insertar(31);
        this.miArbolB.insertar(28);
        String cad="3-5-11-12-15-25-18-23-28-31-33-45-";
        String cad2="";
          assertNotNull(this.miArbolB.inOrden());
           Iterator<Integer> it= this.miArbolB.inOrden();
            while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo getPeso(), de la clase ArbolB.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
        this.miArbolB.insertar(5);
        this.miArbolB.insertar(33);
        this.miArbolB.insertar(3);
       this.miArbolB.insertar(15);
        this.miArbolB.insertar(25);
        this.miArbolB.insertar(11);
        this.miArbolB.insertar(18);
        this.miArbolB.insertar(23);
       this.miArbolB.insertar(45);
        this.miArbolB.insertar(12);
        this.miArbolB.insertar(31);
        this.miArbolB.insertar(28);
        assertTrue(this.miArbolB.getPeso()==12);
    }

    /**
     * Prueba Metodo getAltura(), de la clase ArbolB.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
        this.miArbolB.insertar(5);
        this.miArbolB.insertar(33);
        this.miArbolB.insertar(3);
       this.miArbolB.insertar(15);
        this.miArbolB.insertar(25);
        this.miArbolB.insertar(11);
        this.miArbolB.insertar(18);
        this.miArbolB.insertar(23);
       this.miArbolB.insertar(45);
        this.miArbolB.insertar(12);
        this.miArbolB.insertar(31);
        this.miArbolB.insertar(28);
        this.miArbolB.eliminar(15);
        assertEquals(2, this.miArbolB.getAltura());
    }



}