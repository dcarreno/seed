/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ListaS;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ListaS
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ListaSTest  extends TestCase{
    
    private  ListaS miLista;
    
    public ListaSTest() {
        super();         
    }
    
    @Override
    public void setUp() {
        this.miLista= new ListaS();
    }

    /**
     * Prueba Metodo insertarAlInicio(), de la clase ListaS.
     */
    @Test
    public void testInsertarAlInicio() {
        System.out.println("Prueba... insertarAlInicio()");
        String cadena = ("25->2->7->12->23->6->8->4->");
        Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=8;
        this.miLista.insertarAlInicio(x);
         x=6;
        this.miLista.insertarAlInicio(x);
         x=23;
        this.miLista.insertarAlInicio(x);
         x=12;
        this.miLista.insertarAlInicio(x);
         x=7;
        this.miLista.insertarAlInicio(x);
         x=2;
        this.miLista.insertarAlInicio(x);
         x=25;
        this.miLista.insertarAlInicio(x);
        assertEquals(cadena, this.miLista.toString());
    }

    /**
     * Prueba Metodo insertarAlFinal(), de la clase ListaS.
     */
    @Test
    public void testInsertarAlFinal() {
        String cadena = ("8->4->9->13->21->11->5->2->45->");
         Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=8;
        this.miLista.insertarAlInicio(x);
        System.out.println("Prueba... insertarAlFinal()");
         x = 9;
        this.miLista.insertarAlFinal(x);
         x = 13;
        this.miLista.insertarAlFinal(x);
         x = 21;
        this.miLista.insertarAlFinal(x);
         x = 11;
        this.miLista.insertarAlFinal(x);
         x = 5;
        this.miLista.insertarAlFinal(x);
         x = 2;
        this.miLista.insertarAlFinal(x);
         x = 45;
        this.miLista.insertarAlFinal(x);
        assertEquals(cadena, this.miLista.toString());
    }

    /**
     * Prueba Metodo eliminar(), de la clase ListaS.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
         Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=8;
        this.miLista.insertarAlInicio(x);
        int i = this.miLista.getIndice(x);
        Object result = this.miLista.eliminar(i);
        assertNotNull( result);
    }

    /**
     * Prueba Metodo vaciar(), de la clase ListaS.
     */
    @Test
    public void testVaciar() {
        System.out.println("Prueba... vaciar()");
         Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=8;
        this.miLista.insertarAlInicio(x);
       this.miLista.vaciar();
       assertTrue(this.miLista.esVacia());
    }

    /**
     * Prueba Metodo get(), de la clase ListaS.
     */
    @Test
    public void testGet() {
        System.out.println("Prueba... get()");
         Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=8;
        this.miLista.insertarAlInicio(x);
        int i = this.miLista.getIndice(8);
        Object result = this.miLista.get(i);
       assertNotNull(result);
    }

    /**
     * Prueba Metodo set(), de la clase ListaS.
     */
    @Test
    public void testSet() {
        System.out.println("Prueba... set()");
         Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=8;
        this.miLista.insertarAlInicio(x);
        int i = 0;
        Object dato = 5;
        this.miLista.set(i, dato);
        assertTrue(this.miLista.get(i)==dato);
    }

    /**
     * Prueba Metodo getTamanio(), de la clase ListaS.
     */
    @Test
    public void testGetTamanio() {
         Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=5;
        this.miLista.insertarAlInicio(x);
        System.out.println("Prueba... getTamanio()");
        int result = this.miLista.getTamanio();
        assertTrue(result==2);
    }

    /**
     * Prueba Metodo esVacia(), de la clase ListaS.
     */
    @Test
    public void testEsVacia() {
        Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=8;
        this.miLista.insertarAlInicio(x);
         x=6;
        this.miLista.insertarAlInicio(x);
         x=23;
        this.miLista.insertarAlInicio(x);
         x=12;
        this.miLista.insertarAlInicio(x);
         x=7;
        this.miLista.insertarAlInicio(x);
        System.out.println("Prueba... esVacia()");
         assertFalse(this.miLista.esVacia());
    }

    /**
     * Prueba Metodo esta(), de la clase ListaS.
     */
    @Test
    public void testEsta() {
          Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=5;
        this.miLista.insertarAlInicio(x);
        System.out.println("Prueba... esta()");
        Object info = 5;
        boolean result =this.miLista.esta(info);
        assertNotNull( result);
    }

    /**
     * Prueba Metodo iterator(), de la clase ListaS.
     */
    @Test
    public void testIterator() {
         Object x = 4;
        this.miLista.insertarAlInicio(x);
        x=5;
        this.miLista.insertarAlInicio(x);
        System.out.println("Prueba... iterator()");
        String cad = "";
        Iterator result = this.miLista.iterator();
        while(result.hasNext()){
            cad+= result.next().toString()+"->";
        }
        String expResult = ("5->4->");
        assertEquals(expResult, cad);
    }

    /**
     * Prueba Metodo toString(), de la clase ListaS.
     */
    @Test
    public void testToString() {
         Object x = 9;
        this.miLista.insertarAlInicio(x);
        x=5;
        this.miLista.insertarAlInicio(x);
        System.out.println("Prueba... toString()");
        String expResult = "5->9->";
        String result = this.miLista.toString();
        assertEquals(expResult, result);
    }

    /**
     * Prueba Metodo getIndice(), de la clase ListaS.
     */
    @Test
    public void testGetIndice() {
         Object x = 9;
        this.miLista.insertarAlInicio(x);
        x=5;
        this.miLista.insertarAlInicio(x);
        System.out.println("Prueba... getIndice()");
        Object info = 9;
        int result = this.miLista.getIndice(info);
        assertTrue( result==1);
    }
}