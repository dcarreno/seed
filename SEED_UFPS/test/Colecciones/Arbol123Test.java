/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ListaCD;
import Colecciones.Arbol123;
import java.util.Iterator;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Arbol1-2-3.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class Arbol123Test extends TestCase {
    
    private Arbol123 miArbol123;
    
    public Arbol123Test() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miArbol123 = new Arbol123();
    }
    
    /**
     * Prueba de Metodo getInfoMenRaiz de la clase Arbol123.
     */
    @Test
    public void testGetInfoMenRaiz() {
        System.out.println("Prueba... getInfoMenRaiz()");
        
        this.miArbol123.insertar(112);
        this.miArbol123.insertar(45);
        this.miArbol123.insertar(54);
        this.miArbol123.insertar(22);
        this.miArbol123.insertar(93);
        this.miArbol123.insertar(11);
        this.miArbol123.insertar(3);
        this.miArbol123.insertar(30);
        
        assertEquals(this.miArbol123.getInfoMenRaiz(), 45);
        
    }

    /**
     * Prueba de Metodo getInfoMayRaiz de la clase Arbol123.
     */
    @Test
    public void testGetInfoMayRaiz() {
        System.out.println("Prueba... getInfoMayRaiz()");
        this.miArbol123.insertar(45);
        this.miArbol123.insertar(112);
        this.miArbol123.insertar(54);
        this.miArbol123.insertar(22);
        this.miArbol123.insertar(93);
        this.miArbol123.insertar(11);
        this.miArbol123.insertar(3);
        this.miArbol123.insertar(30);
        
        assertEquals(this.miArbol123.getInfoMayRaiz(), 112);
    }

    /**
     * Prueba de Metodo insertar de la clase Arbol123.
     */
    @Test
    public void testInsertar() {
        System.out.println("Prueba... insertar()");
        
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        
        assertEquals(this.miArbol123.getInfoMenRaiz(), 11);
        assertEquals(this.miArbol123.getInfoMayRaiz(), 90);
    }

    /**
     * Prueba de Metodo eliminar de la clase Arbol123.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
        
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertEquals(this.miArbol123.getInfoMenRaiz(), 11);
        assertEquals(this.miArbol123.getInfoMayRaiz(), 90);
        assertTrue(this.miArbol123.eliminar(90));
        assertTrue(this.miArbol123.eliminar(11));
        assertFalse(this.miArbol123.eliminar(55));
        assertEquals(this.miArbol123.getInfoMenRaiz(), 3);
        assertEquals(this.miArbol123.getInfoMayRaiz(), 88);
    }

    /**
     * Prueba de Metodo esta de la clase Arbol123.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        
        assertEquals(this.miArbol123.esta(3),true);
        assertEquals(this.miArbol123.esta(11),true);
        assertEquals(this.miArbol123.esta(77),false);
        assertEquals(this.miArbol123.esta(32),false);
        assertEquals(this.miArbol123.esta(49),true);
    }

    /**
     * Prueba de Metodo getHojas de la clase Arbol123.
     */
    @Test
    public void testGetHojas() {
        System.out.println("Prueba... getHojas()");
        
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertTrue(this.miArbol123.insertar(89));
        assertTrue(this.miArbol123.insertar(9));
                
        ListaCD l = new ListaCD();
        l.insertarAlFinal(3);
        l.insertarAlFinal(9);
        l.insertarAlFinal(40);
        l.insertarAlFinal(88);
        l.insertarAlFinal(89);
        
        Iterator it = this.miArbol123.getHojas();
        Iterator it2 = l.iterator();
        
        while(it.hasNext()){
            Object obj = it.next();
            if(obj!=null)
                assertEquals(obj.toString(),it2.next().toString());
        }
    }

    /**
     * Prueba de Metodo contarHojas de la clase Arbol123.
     */
    @Test
    public void testContarHojas() {
        System.out.println("Prueba... contarHojas()");
        
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertTrue(this.miArbol123.insertar(89));
        assertTrue(this.miArbol123.insertar(9));
        
        assertEquals(this.miArbol123.contarHojas(), 3);
    }

    /**
     * Prueba de Metodo preOrden de la clase Arbol123.
     */
    @Test
    public void testPreOrden() {
        System.out.println("Prueba... preOrden()");
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertTrue(this.miArbol123.insertar(89));
        assertTrue(this.miArbol123.insertar(9));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(11);
        l.insertarAlFinal(90);
        l.insertarAlFinal(3);
        l.insertarAlFinal(9);
        l.insertarAlFinal(41);
        l.insertarAlFinal(49);
        l.insertarAlFinal(40);
        l.insertarAlFinal(88);
        l.insertarAlFinal(89);
        
        Iterator it = this.miArbol123.preOrden();
        Iterator it2 = l.iterator();
        
        while(it.hasNext()){
            Object obj = it.next();
            if(obj!=null)
                assertEquals(obj.toString(),it2.next().toString());
        }
    }

    /**
     * Prueba de Metodo inOrden de la clase Arbol123.
     */
    @Test
    public void testInOrden() {
        System.out.println("Prueba... inOrden()");
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertTrue(this.miArbol123.insertar(89));
        assertTrue(this.miArbol123.insertar(9));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(3);
        l.insertarAlFinal(9);
        l.insertarAlFinal(11);
        l.insertarAlFinal(90);
        l.insertarAlFinal(40);
        l.insertarAlFinal(41);
        l.insertarAlFinal(49);
        l.insertarAlFinal(88);
        l.insertarAlFinal(89);
        
        Iterator it = this.miArbol123.inOrden();
        Iterator it2 = l.iterator();
        
        while(it.hasNext()){
            Object obj = it.next();
            if(obj!=null)
                assertEquals(obj.toString(),it2.next().toString());
        }
    }

    /**
     * Prueba de Metodo postOrden de la clase Arbol123.
     */
    @Test
    public void testPosOrden() {
        System.out.println("Prueba... postOrden()");
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertTrue(this.miArbol123.insertar(89));
        assertTrue(this.miArbol123.insertar(9));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(3);
        l.insertarAlFinal(9);
        l.insertarAlFinal(40);
        l.insertarAlFinal(88);
        l.insertarAlFinal(89);
        l.insertarAlFinal(41);
        l.insertarAlFinal(49);
        l.insertarAlFinal(11);
        l.insertarAlFinal(90);
        
        Iterator it = this.miArbol123.postOrden();
        Iterator it2 = l.iterator();
        
        while(it.hasNext()){
            Object obj = it.next();
            if(obj!=null)
                assertEquals(obj.toString(),it2.next().toString());
        }
    }

    /**
     * Prueba de Metodo impNiveles de la clase Arbol123.
     */
    @Test
    public void testImpNiveles() {
        System.out.println("Prueba... impNiveles()");
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertTrue(this.miArbol123.insertar(89));
        assertTrue(this.miArbol123.insertar(9));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(11);
        l.insertarAlFinal(90);
        l.insertarAlFinal(3);
        l.insertarAlFinal(9);
        l.insertarAlFinal(41);
        l.insertarAlFinal(49);
        l.insertarAlFinal(40);
        l.insertarAlFinal(88);
        l.insertarAlFinal(89);
        
        Iterator it = this.miArbol123.impNiveles();
        Iterator it2 = l.iterator();
        
        while(it.hasNext()){
            Object obj = it.next();
            if(obj!=null)
                assertEquals(obj.toString(),it2.next().toString());
        }
    }

    /**
     * Prueba de Metodo getPeso de la clase Arbol123.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
        
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertTrue(this.miArbol123.insertar(89));
        assertTrue(this.miArbol123.insertar(9));
        
        assertEquals(this.miArbol123.getPeso(), 9);
    }

    /**
     * Prueba de Metodo getAltura de la clase Arbol123.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
        assertTrue(this.miArbol123.insertar(90));
        assertTrue(this.miArbol123.insertar(11));        
        assertTrue(this.miArbol123.insertar(49));
        assertTrue(this.miArbol123.insertar(41));
        assertTrue(this.miArbol123.insertar(40));
        assertTrue(this.miArbol123.insertar(88));
        assertTrue(this.miArbol123.insertar(3));
        assertTrue(this.miArbol123.insertar(89));
        assertTrue(this.miArbol123.insertar(9));
        
        assertEquals(this.miArbol123.getAltura(), 3);
        
        assertTrue(this.miArbol123.insertar(20));
        assertTrue(this.miArbol123.insertar(30));
        
        assertEquals(this.miArbol123.getAltura(), 4);
    }

}