/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.Cola;
import junit.framework.TestCase;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Cola.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ColaTest extends TestCase{
    
    private Cola miCola;
    
    public ColaTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miCola = new Cola();
    }
    
    /**
     * Prueba Metodo enColar(), de la clase Cola.
     */
    @Test
    public void testEnColar() {
        System.out.println("Prueba... enColar()");
        Object info = 0;
        this.miCola.enColar(info);
        info = 1;
        this.miCola.enColar(info);
        info = 2;
        this.miCola.enColar(info);
        info = 3;
        this.miCola.enColar(info);
        info = 4;
        this.miCola.enColar(info);
        info = 5;
        this.miCola.enColar(info);
        info = 6;
        this.miCola.enColar(info);
        info = 7;
        this.miCola.enColar(info);
        info = 0;
        assertEquals(this.miCola.getInfoInicio(),info);
    }

    /**
     * Prueba Metodo deColar(), de la clase Cola.
     */
    @Test
    public void testDeColar() {
        System.out.println("Prueba... deColar()");
        Object info = 0;
        this.miCola.enColar(info);
        info = 1;
        this.miCola.enColar(info);
        info = 2;
        this.miCola.enColar(info);
        info = 3;
        this.miCola.enColar(info);
        info = 4;
        this.miCola.enColar(info);
        info = 5;
        this.miCola.enColar(info);
        info = 6;
        this.miCola.enColar(info);
        
        this.miCola.deColar();
        this.miCola.deColar();
        this.miCola.deColar();
        
        assertEquals(this.miCola.getInfoInicio(),3);
    }

    /**
     * Prueba Metodo vaciar(), de la clase Cola.
     */
    @Test
    public void testVaciar() {
        Object info = 0;
        this.miCola.enColar(info);
        info = 1;
        this.miCola.enColar(info);
        info = 2;
        this.miCola.enColar(info);
        info = 3;
        this.miCola.enColar(info);
        info = 4;
        this.miCola.enColar(info);
        info = 5;
        this.miCola.enColar(info);
        info = 6;
        this.miCola.enColar(info);
        info = 7;
        this.miCola.enColar(info);
        info = 8;
        this.miCola.enColar(info);
        info = 9;
        this.miCola.enColar(info);
        info = 10;
        this.miCola.enColar(info);
        info = 11;
        this.miCola.enColar(info);
        info = 12;
        this.miCola.enColar(info);
        info = 13;
        this.miCola.enColar(info);
        System.out.println("Prueba... vaciar()");
        this.miCola.vaciar();
        assertTrue(this.miCola.esVacia());
    }

    /**
     * Prueba Metodo getIni(), de la clase Cola.
     */
    @Test
    public void testGetIni() {
        Object info = 0;
        this.miCola.enColar(info);
        info = 1;
        this.miCola.enColar(info);
        info = 2;
        this.miCola.enColar(info);
        info = 3;
        this.miCola.enColar(info);
        info = 4;
        this.miCola.enColar(info);
        info = 5;
        this.miCola.enColar(info);
        info = 6;
        this.miCola.enColar(info);
        info = 7;
        this.miCola.enColar(info);
        info = 8;
        this.miCola.enColar(info);
        info = 9;
        this.miCola.enColar(info);
        info = 10;
        this.miCola.enColar(info);
        info = 11;
        this.miCola.enColar(info);
        info = 12;
        this.miCola.enColar(info);
        info = 13;
        this.miCola.enColar(info);
        System.out.println("Prueba... getIni()");
        assertNotNull(this.miCola.getInfoInicio());
        assertEquals(this.miCola.getInfoInicio(),0);
    }

    /**
     * Prueba Metodo getTamanio(), de la clase Cola.
     */
    @Test
    public void testGetTamanio() {
        Object info = 0;
        this.miCola.enColar(info);
        info = 1;
        this.miCola.enColar(info);
        info = 2;
        this.miCola.enColar(info);
        info = 3;
        this.miCola.enColar(info);
        info = 4;
        this.miCola.enColar(info);
        info = 5;
        this.miCola.enColar(info);
        info = 6;
        this.miCola.enColar(info);
        info = 7;
        this.miCola.enColar(info);
        info = 8;
        this.miCola.enColar(info);
        info = 9;
        this.miCola.enColar(info);
        info = 10;
        this.miCola.enColar(info);
        info = 11;
        this.miCola.enColar(info);
        info = 12;
        this.miCola.enColar(info);
        info = 13;
        this.miCola.enColar(info);
        System.out.println("Prueba... getTamanio()");
        assertTrue(this.miCola.getTamanio()==14);
    }

    /**
     * Prueba Metodo esVacia(), de la clase Cola.
     */
    @Test
    public void testEsVacia() {
        Object info = 0;
        this.miCola.enColar(info);
        info = 1;
        this.miCola.enColar(info);
        info = 2;
        this.miCola.enColar(info);
        info = 3;
        this.miCola.enColar(info);
        info = 4;
        this.miCola.enColar(info);
        info = 5;
        this.miCola.enColar(info);
        System.out.println("Prueba... esVacia()");
        assertFalse(this.miCola.esVacia());
    }

    /**
     * Prueba Metodo toString(), de la clase Cola.
     */
    @Test
    public void testToString() {
         Object info = 23;
        this.miCola.enColar(info);
        info = 8;
        this.miCola.enColar(info);
        info = 2;
        this.miCola.enColar(info);
        info = 4;
        this.miCola.enColar(info);
        info = 20;
        this.miCola.enColar(info);
        info = 10;
        this.miCola.enColar(info);
        System.out.println("Prueba... toString()");
        String cad="23->8->2->4->20->10->";
        assertEquals(cad,this.miCola.toString());
        
    }
}