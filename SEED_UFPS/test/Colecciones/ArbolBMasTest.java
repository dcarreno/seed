/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ArbolBMas;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ArbolB+.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolBMasTest extends TestCase {
    
    private ArbolBMas<Integer> miArbolBMas;
    
    public ArbolBMasTest() {
        super();
    }
    
    @Override
    public  void setUp() {
        this.miArbolBMas=new ArbolBMas();
    }

    /**
     * Prueba Metodo insertarBMas(), de la clase ArbolBMas.
     */
    @Test
    public void testInsertarBMas() {
        System.out.println("Prueba... insertarBMas");
        assertTrue(this.miArbolBMas.insertarBMas(5));
        assertTrue(this.miArbolBMas.insertarBMas(33));
        assertTrue(this.miArbolBMas.insertarBMas(3));
        assertTrue(this.miArbolBMas.insertarBMas(15));
        assertTrue(this.miArbolBMas.insertarBMas(25));
        assertTrue(this.miArbolBMas.insertarBMas(11));
        assertTrue(this.miArbolBMas.insertarBMas(8));
        Object obj = 15;
        assertTrue(this.miArbolBMas.getRaiz().getInfo()[0]==obj);
    }

    /**
     * Prueba Metodo eliminarBMas(), de la clase ArbolBMas.
     */
    @Test
    public void testEliminarBMas() {
        System.out.println("Prueba... eliminarBMas()");
        this.miArbolBMas.insertarBMas(11);
        this.miArbolBMas.insertarBMas(3);
        this.miArbolBMas.insertarBMas(44);
        this.miArbolBMas.insertarBMas(1);
        this.miArbolBMas.insertarBMas(22);
        this.miArbolBMas.insertarBMas(32);
        this.miArbolBMas.insertarBMas(23);
        this.miArbolBMas.insertarBMas(8);
        this.miArbolBMas.insertarBMas(31);
        this.miArbolBMas.insertarBMas(15);
        assertTrue(this.miArbolBMas.eliminarBMas(32));
        assertTrue(this.miArbolBMas.eliminarBMas(3));
        assertTrue(this.miArbolBMas.eliminarBMas(15));
        assertFalse(this.miArbolBMas.eliminarBMas(32));
    }

    /**
     * Prueba Metodo estaBMasr(), de la clase ArbolBMas.
     */
    @Test
    public void testEstaBMas() {
        System.out.println("Prueba... estaBMas()");
        this.miArbolBMas.insertarBMas(11);
        this.miArbolBMas.insertarBMas(3);
        this.miArbolBMas.insertarBMas(44);
        this.miArbolBMas.insertarBMas(1);
        this.miArbolBMas.insertarBMas(22);
        this.miArbolBMas.insertarBMas(32);
        this.miArbolBMas.insertarBMas(23);
        this.miArbolBMas.insertarBMas(8);
        this.miArbolBMas.insertarBMas(31);
        this.miArbolBMas.insertarBMas(15);
        assertTrue(this.miArbolBMas.estaBMas(23));
        assertTrue(this.miArbolBMas.estaBMas(11));
        assertFalse(this.miArbolBMas.estaBMas(4));
        assertTrue(this.miArbolBMas.estaBMas(8));
    }

    /**
     *Prueba Metodo getHojas(), de la clase ArbolB.
     */
    @Test
    public void testGetHojas() {
        System.out.println("Prueba... getHojas()");
        this.miArbolBMas.insertarBMas(11);
        this.miArbolBMas.insertarBMas(3);
        this.miArbolBMas.insertarBMas(44);
        this.miArbolBMas.insertarBMas(1);
        this.miArbolBMas.insertarBMas(22);
        this.miArbolBMas.insertarBMas(32);
        this.miArbolBMas.insertarBMas(23);
        this.miArbolBMas.insertarBMas(8);
        this.miArbolBMas.insertarBMas(31);
        this.miArbolBMas.insertarBMas(15);
        String cad="1-3-8-11-15-22-23-31-32-44-";//corregir
        String cad2="";
          assertNotNull(this.miArbolBMas.getHojas());
           Iterator<Integer> it= this.miArbolBMas.getHojas();
            while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo esVacio(), de la clase ArbolBMas.
     */
    @Test
    public void testEsVacio() {
        System.out.println("Prueba... esVacio()");
        this.miArbolBMas.insertarBMas(11);
        this.miArbolBMas.insertarBMas(3);
        this.miArbolBMas.insertarBMas(44);
        this.miArbolBMas.insertarBMas(1);
        this.miArbolBMas.insertarBMas(22);
        this.miArbolBMas.insertarBMas(32);
        this.miArbolBMas.insertarBMas(23);
        this.miArbolBMas.insertarBMas(8);
        this.miArbolBMas.insertarBMas(31);
        this.miArbolBMas.insertarBMas(15);
        assertFalse(this.miArbolBMas.esVacio());
    }

    /**
     * Prueba Metodo getPeso(), de la clase ArbolBMas.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
        this.miArbolBMas.insertarBMas(11);
        this.miArbolBMas.insertarBMas(3);
        this.miArbolBMas.insertarBMas(44);
        this.miArbolBMas.insertarBMas(1);
        this.miArbolBMas.insertarBMas(22);
        this.miArbolBMas.insertarBMas(32);
        this.miArbolBMas.insertarBMas(23);
        this.miArbolBMas.insertarBMas(8);
        this.miArbolBMas.insertarBMas(31);
        this.miArbolBMas.insertarBMas(15);
        assertTrue(this.miArbolBMas.getPesoBMas()==10);
    }

    /**
     * Test of getAltura method, of class ArbolBMas.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
        this.miArbolBMas.insertarBMas(11);
        this.miArbolBMas.insertarBMas(3);
        this.miArbolBMas.insertarBMas(44);
        this.miArbolBMas.insertarBMas(1);
        this.miArbolBMas.insertarBMas(22);
        this.miArbolBMas.insertarBMas(32);
        this.miArbolBMas.insertarBMas(23);
        this.miArbolBMas.insertarBMas(8);
        this.miArbolBMas.insertarBMas(31);
        this.miArbolBMas.insertarBMas(15);
         assertEquals(2, this.miArbolBMas.getAltura());
    }

    /**
     * Test of listar_vsam method, of class ArbolBMas.
     */
    @Test
    public void testListar_vsam() {
        System.out.println("Prueba... listar_vsam()");
        this.miArbolBMas.insertarBMas(11);
        this.miArbolBMas.insertarBMas(3);
        this.miArbolBMas.insertarBMas(44);
        this.miArbolBMas.insertarBMas(1);
        this.miArbolBMas.insertarBMas(22);
        this.miArbolBMas.insertarBMas(32);
        this.miArbolBMas.insertarBMas(23);
        this.miArbolBMas.insertarBMas(8);
        this.miArbolBMas.insertarBMas(31);
        this.miArbolBMas.insertarBMas(15);
        String cad="1-->3-->8-->11-->15-->22-->23-->31-->32-->44-->";
        assertEquals(cad, this.miArbolBMas.listar_vsam());
    }

}