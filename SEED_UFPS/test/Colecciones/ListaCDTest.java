/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ListaCD;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ListaCD.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ListaCDTest extends TestCase {
    
    private ListaCD miListaCD;
    
    public ListaCDTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miListaCD= new ListaCD();
    }
    
    /**
     * Prueba Metodo insertarAlInicio(), de la clase ListaCD.
     */
    @Test
    public void testInsertarAlInicio() {
        System.out.println("Prueba... insertarAlInicio()");
        String cadena = ("7<->6<->5<->4<->3<->2<->1<->0<->");
        Object x = 0;
        this.miListaCD.insertarAlInicio(x);
        x=1;
        this.miListaCD.insertarAlInicio(x);
         x=2;
        this.miListaCD.insertarAlInicio(x);
         x=3;
        this.miListaCD.insertarAlInicio(x);
         x=4;
        this.miListaCD.insertarAlInicio(x);
         x=5;
        this.miListaCD.insertarAlInicio(x);
         x=6;
        this.miListaCD.insertarAlInicio(x);
         x=7;
        this.miListaCD.insertarAlInicio(x);
        assertEquals(cadena, this.miListaCD.toString());
    }

    /**
     * Prueba Metodo insertarAlFinal(), de la clase ListaCD.
     */
    @Test
    public void testInsertarAlFinal() {
        String cadena = ("28<->27<->26<->25<->24<->23<->22<->21<->20<->");
         Object x = 27;
        this.miListaCD.insertarAlInicio(x);
        x=28;
        this.miListaCD.insertarAlInicio(x);
        System.out.println("Prueba... insertarAlFinal()");
         x = 26;
        this.miListaCD.insertarAlFinal(x);
         x = 25;
        this.miListaCD.insertarAlFinal(x);
         x = 24;
        this.miListaCD.insertarAlFinal(x);
         x = 23;
        this.miListaCD.insertarAlFinal(x);
         x = 22;
        this.miListaCD.insertarAlFinal(x);
         x = 21;
        this.miListaCD.insertarAlFinal(x);
         x = 20;
        this.miListaCD.insertarAlFinal(x);
        assertEquals(cadena, this.miListaCD.toString());
    }

    /**
     * Prueba Metodo eliminar(), de la clase ListaCD.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
         Object x =21;
        this.miListaCD.insertarAlInicio(x);
        x=11;
        this.miListaCD.insertarAlInicio(x);
        x=23;
        this.miListaCD.insertarAlInicio(x);
        int i = this.miListaCD.getIndice(21);
        Object result = this.miListaCD.eliminar(i);
        assertNotNull( result);
    }

    /**
     * Prueba Metodo vaciar(), de la clase ListaCD.
     */
    @Test
    public void testVaciar() {
        System.out.println("Prueba... vaciar()");
         Object x = 12;
        this.miListaCD.insertarAlInicio(x);
        x=22;
        this.miListaCD.insertarAlInicio(x);
        x=1;
        this.miListaCD.insertarAlFinal(x);
        x=4;
        this.miListaCD.insertarAlFinal(x);
        x=6;
        this.miListaCD.insertarAlFinal(x);
       this.miListaCD.vaciar();
       assertTrue(this.miListaCD.esVacia());
    }

    /**
     * Prueba Metodo get(), de la clase ListaCD.
     */
    @Test
    public void testGet() {
        System.out.println("Prueba... get()");
         Object x = 90;
        this.miListaCD.insertarAlInicio(x);
        x=27;
        this.miListaCD.insertarAlInicio(x);
         x=30;
        this.miListaCD.insertarAlFinal(x);
         x=72;
        this.miListaCD.insertarAlFinal(x);
        x=10;
        this.miListaCD.insertarAlFinal(x);
        x=19;
        this.miListaCD.insertarAlFinal(x);
        x=13;
        this.miListaCD.insertarAlFinal(x);
        int i = this.miListaCD.getIndice(10);
        Object result = this.miListaCD.get(i);
       assertNotNull(result);
    }

    /**
     * Prueba Metodo set(), de la clase ListaCD.
     */
    @Test
    public void testSet() {
        System.out.println("Prueba... set()");
         Object x = 11;
        this.miListaCD.insertarAlInicio(x);
        x=14;
        this.miListaCD.insertarAlInicio(x);
         x=17;
        this.miListaCD.insertarAlFinal(x);
         x=20;
        this.miListaCD.insertarAlFinal(x);
        x=18;
        this.miListaCD.insertarAlFinal(x);
        x=81;
        this.miListaCD.insertarAlFinal(x);
        x=12;
        this.miListaCD.insertarAlFinal(x);
        int i = 3;
        Object dato = 5;
        this.miListaCD.set(i, dato);
        assertTrue(this.miListaCD.get(i)==dato);
    }

    /**
     * Prueba Metodo getTamanio(), de la clase ListaCD.
     */
    @Test
    public void testGetTamanio() {
         Object x = 3;
        this.miListaCD.insertarAlInicio(x);
        x=1;
        this.miListaCD.insertarAlInicio(x);
         x=0;
        this.miListaCD.insertarAlFinal(x);
         x=8;
        this.miListaCD.insertarAlFinal(x);
         x=4;
        this.miListaCD.insertarAlFinal(x);
        x=19;
        this.miListaCD.insertarAlFinal(x);
        x=21;
        this.miListaCD.insertarAlFinal(x);
        x=31;
        this.miListaCD.insertarAlFinal(x);
        System.out.println("Prueba... getTamanio()");
        int result = this.miListaCD.getTamanio();
        assertTrue(result==8);
    }

    /**
     * Prueba Metodo esVacia(), de la clase ListaCD.
     */
    @Test
    public void testEsVacia() {
        System.out.println("Prueba... esVacia()");
         assertTrue(this.miListaCD.esVacia());
    }

    /**
     * Prueba Metodo esta(), de la clase ListaCD.
     */
    @Test
    public void testEsta() {
          Object x = 14;
        this.miListaCD.insertarAlInicio(x);
        x=59;
        this.miListaCD.insertarAlInicio(x);
         x=18;
        this.miListaCD.insertarAlFinal(x);
         x=3;
        this.miListaCD.insertarAlFinal(x);
         x=0;
        this.miListaCD.insertarAlFinal(x);
        x=77;
        this.miListaCD.insertarAlFinal(x);
        x=33;
        this.miListaCD.insertarAlFinal(x);
        System.out.println("Prueba... esta()");
        Object info = 3;
        boolean result =this.miListaCD.esta(info);
        assertNotNull( result);
    }

    /**
     * Prueba Metodo iterator(), de la clase ListaCD.
     */
    @Test
    public void testIterator() {
         Object x = 34;
        this.miListaCD.insertarAlInicio(x);
        x=48;
        this.miListaCD.insertarAlInicio(x);
         x=0;
        this.miListaCD.insertarAlFinal(x);
         x=1;
        this.miListaCD.insertarAlFinal(x);
         x=4;
        this.miListaCD.insertarAlFinal(x);
        x=19;
        this.miListaCD.insertarAlFinal(x);
        System.out.println("Prueba... iterator()");
        String cad = "";
        Iterator result = this.miListaCD.iterator();
        while(result.hasNext()){
            cad+= result.next().toString()+"<->";
        }
        String expResult = ("48<->34<->0<->1<->4<->19<->");
        assertEquals(expResult, cad);
    }

    /**
     * Prueba Metodo toString(), de la clase ListaCD.
     */
    @Test
    public void testToString() {
        Object x = 0;
        this.miListaCD.insertarAlInicio(x);
        x=1;
        this.miListaCD.insertarAlInicio(x);
         x=2;
        this.miListaCD.insertarAlFinal(x);
         x=3;
        this.miListaCD.insertarAlFinal(x);
         x=4;
        this.miListaCD.insertarAlFinal(x);
         x=5;
        this.miListaCD.insertarAlFinal(x);
        x=6;
        this.miListaCD.insertarAlFinal(x);
        x=7;
        this.miListaCD.insertarAlFinal(x);
        x=8;
        this.miListaCD.insertarAlFinal(x);
        System.out.println("Prueba... toString()");
        String expResult = "1<->0<->2<->3<->4<->5<->6<->7<->8<->";
        String result = this.miListaCD.toString();
        assertEquals(expResult, result);
    }

    /**
     * Prueba Metodo getIndice(), de la clase ListaCD.
     */
    @Test
    public void testGetIndice() {
         Object x = 4;
        this.miListaCD.insertarAlInicio(x);
        x=8;
        this.miListaCD.insertarAlInicio(x);
        x=23;
        this.miListaCD.insertarAlFinal(x);
        x=20;
        this.miListaCD.insertarAlFinal(x);
        x=7;
        this.miListaCD.insertarAlFinal(x);
        x=22;
        this.miListaCD.insertarAlFinal(x);
        System.out.println("Prueba... getIndice()");
        Object info = 23;
        int result = this.miListaCD.getIndice(info);
        assertTrue(result==2);
    }
}