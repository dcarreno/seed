/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ListaC;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ListaC.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ListaCTest extends TestCase {
    
    private ListaC miListaC;
    
    public ListaCTest() {
        super();
    }
    
    @Override
    public void setUp() {
        miListaC = new ListaC();
    }
    
    /**
     * Prueba Metodo insertarAlInicio(), de la clase ListaC.
     */
    @Test
    public void testinsertarAlInicio() {
        System.out.println("Prueba... insertarAlInicio()");
        String cadena = ("1->2->3->4->53->60->38->49->");
        Object x = 49;
        this.miListaC.insertarAlInicio(x);
        x=38;
        this.miListaC.insertarAlInicio(x);
         x=60;
        this.miListaC.insertarAlInicio(x);
         x=53;
        this.miListaC.insertarAlInicio(x);
         x=4;
        this.miListaC.insertarAlInicio(x);
         x=3;
        this.miListaC.insertarAlInicio(x);
         x=2;
        this.miListaC.insertarAlInicio(x);
         x=1;
        this.miListaC.insertarAlInicio(x);
        assertEquals(cadena, this.miListaC.toString());
    }

    /**
     * Prueba Metodo insertarAlFinal(), de la clase ListaC.
     */
    @Test
    public void testInsertarAlFinal() {
        String cadena = ("2->0->79->14->12->76->53->24->50->");
         Object x = 0;
        this.miListaC.insertarAlInicio(x);
        x=2;
        this.miListaC.insertarAlInicio(x);
         x =79;
        this.miListaC.insertarAlFinal(x);
         x = 14;
        this.miListaC.insertarAlFinal(x);
         x = 12;
        this.miListaC.insertarAlFinal(x);
         x = 76;
        this.miListaC.insertarAlFinal(x);
         x = 53;
        this.miListaC.insertarAlFinal(x);
         x = 24;
        this.miListaC.insertarAlFinal(x);
         x = 50;
        this.miListaC.insertarAlFinal(x);
        System.out.println("Prueba... insertarAlFinal()");
        assertEquals(cadena, this.miListaC.toString());
    }

    /**
     * Prueba Metodo eliminar(), de la clase ListaC.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
         Object x = 4;
        this.miListaC.insertarAlInicio(x);
        x=8;
        this.miListaC.insertarAlInicio(x);
        x = 0;
        this.miListaC.insertarAlFinal(x);
        x = 1;
        this.miListaC.insertarAlFinal(x);
        x = 2;
        this.miListaC.insertarAlFinal(x);
        x = 3;
        this.miListaC.insertarAlFinal(x);
        int i = this.miListaC.getIndice(0);
        Object result = this.miListaC.eliminar(i);
        assertNotNull( result);
    }

    /**
     * Prueba Metodo vaciar(), de la clase ListaC.
     */
    @Test
    public void testVaciar() {
        System.out.println("Prueba... vaciar()");
         Object x = 90;
        this.miListaC.insertarAlInicio(x);
        x=72;
        this.miListaC.insertarAlInicio(x);
        x = 33;
        this.miListaC.insertarAlFinal(x);
        x = 37;
        this.miListaC.insertarAlFinal(x);
        x =43;
        this.miListaC.insertarAlFinal(x);
        x = 47;
        this.miListaC.insertarAlFinal(x);
       this.miListaC.vaciar();
       assertTrue(this.miListaC.esVacia());
    }

    /**
     * Prueba Metodo get(), de la clase ListaC.
     */
    @Test
    public void testGet() {
        System.out.println("Prueba... get()");
         Object x = 0;
        this.miListaC.insertarAlInicio(x);
        x=3;
        this.miListaC.insertarAlInicio(x);
        x = 13;
        this.miListaC.insertarAlFinal(x);
        x = 22;
        this.miListaC.insertarAlFinal(x);
        x = 25;
        this.miListaC.insertarAlFinal(x);
        x = 31;
        this.miListaC.insertarAlFinal(x);
        x = 44;
        this.miListaC.insertarAlFinal(x);
        int i = this.miListaC.getIndice(22);
        Object result = this.miListaC.get(i);
       assertNotNull(result);
    }

    /**
     * Prueba Metodo set(), de la clase ListaC.
     */
    @Test
    public void testSet() {
        System.out.println("Prueba... set()");
         Object x = 9;
        this.miListaC.insertarAlInicio(x);
        x=8;
        this.miListaC.insertarAlInicio(x);
        x = 7;
        this.miListaC.insertarAlFinal(x);
        x = 6;
        this.miListaC.insertarAlFinal(x);
        x = 21;
        this.miListaC.insertarAlFinal(x);
        x = 4;
        this.miListaC.insertarAlFinal(x);
        x = 3;
        this.miListaC.insertarAlFinal(x);
        int i = 4;
        Object dato = 5;
        this.miListaC.set(i, dato);
        assertTrue(this.miListaC.get(i)==dato);
    }

    /**
     * Prueba Metodo getTamanio(), de la clase ListaC.
     */
    @Test
    public void testGetTamanio() {
         Object x = 0;
        this.miListaC.insertarAlInicio(x);
        x=1;
        this.miListaC.insertarAlInicio(x);
        x = 2;
        this.miListaC.insertarAlFinal(x);
        x = 3;
        this.miListaC.insertarAlFinal(x);
        x = 4;
        this.miListaC.insertarAlFinal(x);
        x = 5;
        this.miListaC.insertarAlFinal(x);
        x = 6;
        this.miListaC.insertarAlFinal(x);
        System.out.println("Prueba... getTamanio()");
        int result = this.miListaC.getTamanio();
        assertTrue(result==7);
    }

    /**
     * Prueba Metodo esVacia(), de la clase ListaC.
     */
    @Test
    public void testEsVacia() {
        Object x = 23;
        this.miListaC.insertarAlInicio(x);
        x=18;
        this.miListaC.insertarAlInicio(x);
         x=62;
        this.miListaC.insertarAlInicio(x);
         x=26;
        this.miListaC.insertarAlInicio(x);
         x=32;
        this.miListaC.insertarAlInicio(x);
         x=71;
        this.miListaC.insertarAlInicio(x);
        System.out.println("Prueba... esVacia()");
         assertFalse(this.miListaC.esVacia());
    }

    /**
     * Prueba Metodo esta(), de la clase ListaC.
     */
    @Test
    public void testEsta() {
          Object x = 92;
        this.miListaC.insertarAlInicio(x);
        x=56;
        this.miListaC.insertarAlInicio(x);
        x = 75;
        this.miListaC.insertarAlFinal(x);
        x = 66;
        this.miListaC.insertarAlFinal(x);
        x = 82;
        this.miListaC.insertarAlFinal(x);
        System.out.println("Prueba... esta()");
        Object info = 56;
        boolean result =this.miListaC.esta(info);
        assertNotNull( result);
    }

    /**
     * Prueba Metodo iterator(), de la clase ListaC.
     */
    @Test
    public void testIterator() {
         Object x = 7;
        this.miListaC.insertarAlInicio(x);
        x=8;
        this.miListaC.insertarAlInicio(x);
        x = 27;
        this.miListaC.insertarAlFinal(x);
        x = 63;
        this.miListaC.insertarAlFinal(x);
        x = 4;
        this.miListaC.insertarAlFinal(x);
        x = 29;
        this.miListaC.insertarAlFinal(x);
        System.out.println("Prueba... iterator()");
        String cad = "";
        Iterator result = this.miListaC.iterator();
        while(result.hasNext()){
            cad+= result.next().toString()+"->";
        }
        String expResult = ("8->7->27->63->4->29->");
        assertEquals(expResult, cad);
    }

    /**
     * Prueba Metodo toString(), de la clase ListaC.
     */
    @Test
    public void testToString() {
         Object x = 9;
        this.miListaC.insertarAlInicio(x);
        x=5;
        this.miListaC.insertarAlInicio(x);
        x = 76;
        this.miListaC.insertarAlFinal(x);
        x = 33;
        this.miListaC.insertarAlFinal(x);
        x = 2;
        this.miListaC.insertarAlFinal(x);
        x = 0;
        this.miListaC.insertarAlFinal(x);
        x = 1;
        this.miListaC.insertarAlFinal(x);
        System.out.println("Prueba... toString()");
        String expResult = "5->9->76->33->2->0->1->";
        String result = this.miListaC.toString();
        assertEquals(expResult, result);
    }

    /**
     * Prueba Metodo getIndice(), de la clase ListaC.
     */
    @Test
    public void testGetIndice() {
         Object x = 91;
        this.miListaC.insertarAlInicio(x);
        x=53;
        this.miListaC.insertarAlInicio(x);
        x = 11;
        this.miListaC.insertarAlFinal(x);
        x = 12;
        this.miListaC.insertarAlFinal(x);
        x = 31;
        this.miListaC.insertarAlFinal(x);
        x = 14;
        this.miListaC.insertarAlFinal(x);
        System.out.println("Prueba... getIndice()");
        Object info = 31;
        int result = this.miListaC.getIndice(info);
        assertTrue( result==4);
    }
}