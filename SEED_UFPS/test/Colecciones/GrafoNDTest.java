/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.Vertice;
import Colecciones.ListaCD;
import Colecciones.Arista;
import Colecciones.GrafoND;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para GrafoND.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class GrafoNDTest extends TestCase{
    
    private GrafoND miGrafoND;
    
    public GrafoNDTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miGrafoND = new GrafoND();
    }
    
    /**
     * Prueba de Metodo insertarVertice de la clase GrafoND.
     */
    @Test
    public void testInsertarVertice() {
        System.out.println("Prueba... insertarVertice()");
        Object vertice = 87 ;
        boolean expResult = true;
        boolean result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 90;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 23;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(true, result);
        vertice = 87;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(false, result);
        vertice = 77;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 77;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(false, result);
    }

    /**
     * Prueba de Metodo insertarArista de la clase GrafoND.
     */
    @Test
    public void testInsertarArista() {
        System.out.println("Prueba... insertarArista()");
        Object verticeA = 87 ;
        Object verticeB = 15 ;
        boolean result = this.miGrafoND.insertarArista(verticeA, verticeB);
        assertEquals(false, result);
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        result = this.miGrafoND.insertarArista(verticeA, verticeB);
        assertEquals(true, result);
        result = this.miGrafoND.insertarArista(verticeA, verticeA);
        assertEquals(true, result);
        Object verticeC = 59 ;
        this.miGrafoND.insertarVertice(verticeC);
        result = this.miGrafoND.insertarArista(verticeA, verticeC);
        assertEquals(true, result);
        result = this.miGrafoND.insertarArista(verticeC, verticeB);
        assertEquals(true, result);
    }

    /**
     * Prueba de Metodo getVertices de la clase GrafoND.
     */
    @Test
    public void testGetVertices() {
        System.out.println("Prueba... getVertices()");
        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(23);
        expResult.insertarAlFinal(87);
        expResult.insertarAlFinal(10);
        expResult.insertarAlFinal(48);
        expResult.insertarAlFinal(31);
        this.miGrafoND.insertarVertice(23);
        this.miGrafoND.insertarVertice(87);
        this.miGrafoND.insertarVertice(10);
        this.miGrafoND.insertarVertice(48);
        this.miGrafoND.insertarVertice(31);        
        ListaCD result = this.miGrafoND.getVertices();
        for(int i=0; i<result.getTamanio();i++){
             assertEquals(result.get(i).toString(), expResult.get(i).toString());
        }
    }

    /**
     * Prueba de Metodo getAristas de la clase GrafoND.
     */
    @Test
    public void testGetAristas() {
        System.out.println("Prueba... getAristas()");
        Object v1 = 23;
        Object v2 = 278;
        Object v3 = 72;
        Object v4 = 60;
        this.miGrafoND.insertarVertice(v1);
        this.miGrafoND.insertarVertice(v2);
        this.miGrafoND.insertarVertice(v3);
        this.miGrafoND.insertarVertice(v4);
        this.miGrafoND.insertarArista(v1, v2);
        this.miGrafoND.insertarArista(v3, v2);
        this.miGrafoND.insertarArista(v3, v4);
        this.miGrafoND.insertarArista(v1, v4);
        ListaCD result = this.miGrafoND.getAristas();
        assertTrue(result.get(0).toString().equals("("+v1.toString()+"-"+v2.toString()+")"));
        assertTrue(result.get(1).toString().equals("("+v3.toString()+"-"+v2.toString()+")"));
        assertTrue(result.get(2).toString().equals("("+v3.toString()+"-"+v4.toString()+")"));
        assertTrue(result.get(3).toString().equals("("+v1.toString()+"-"+v4.toString()+")"));
    }

    /**
     * Prueba de Metodo eliminarVertice de la clase GrafoND.
     */
    @Test
    public void testEliminarVertice() {
        System.out.println("Prueba... eliminarVertice()");
        Object vertice = 87 ;
        boolean expResult = true;
        boolean result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 90;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(expResult, result);
        vertice = 23;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(true, result);
        vertice = 87;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(false, result);
        vertice = 77;
        result = this.miGrafoND.insertarVertice(vertice);
        assertEquals(expResult, result);
        assertFalse(this.miGrafoND.insertarVertice(77));
        this.miGrafoND.eliminarVertice(77);
        assertTrue(this.miGrafoND.insertarVertice(77));
    }

    /**
     * Prueba de Metodo eliminarArista de la clase GrafoND.
     */
    @Test
    public void testEliminarArista() {
        System.out.println("Prueba... eliminarArista()");
        Object verticeA = 87 ;
        Object verticeB = 15 ;
        boolean result = this.miGrafoND.insertarArista(verticeA, verticeB);
        assertEquals(false, result);
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        result = this.miGrafoND.insertarArista(verticeA, verticeB);
        assertEquals(true, result);
        result = this.miGrafoND.insertarArista(verticeA, verticeA);
        assertEquals(true, result);
        Object verticeC = 59 ;
        this.miGrafoND.insertarVertice(verticeC);
        result = this.miGrafoND.insertarArista(verticeA, verticeC);
        assertEquals(true, result);
        result = this.miGrafoND.insertarArista(verticeC, verticeB);
        assertEquals(true, result);
        verticeA = 44;
        assertFalse(this.miGrafoND.eliminarArista(verticeC, verticeA));
        verticeA = 87;
        assertTrue(this.miGrafoND.eliminarArista(verticeA, verticeC));
        assertTrue(this.miGrafoND.eliminarArista(verticeA, verticeA));
        verticeB = 33;
        assertFalse(this.miGrafoND.eliminarArista(verticeB, verticeB));
        verticeB = 15;
        assertTrue(this.miGrafoND.eliminarArista(verticeC, verticeB));
    }

    /**
     * Prueba de Metodo buscarVertice de la clase GrafoND.
     */
    @Test
    public void testBuscarVertice() {
        System.out.println("Prueba.. buscarVertice()");
        Object vertice = 6;
        Object vertice1 = 4;
        Object vertice2 = 89;
        Object vertice3 = 123;
        Vertice expResult = null;
        Vertice result = this.miGrafoND.buscarVertice(vertice);
        assertEquals(expResult, result);
        this.miGrafoND.insertarVertice(vertice);
        this.miGrafoND.insertarVertice(vertice1);
        this.miGrafoND.insertarVertice(vertice2);
        this.miGrafoND.insertarVertice(vertice3);
        assertNotNull(this.miGrafoND.buscarVertice(vertice3));
        assertNotNull(this.miGrafoND.buscarVertice(vertice2));
        assertEquals(this.miGrafoND.buscarVertice(vertice).toString(),vertice.toString());
        assertEquals(this.miGrafoND.buscarVertice(vertice1).toString(),vertice1.toString());
    }

    /**
     * Prueba de Metodo buscarArista de la clase GrafoND.
     */
    @Test
    public void testBuscarArista() {
        System.out.println("Prueba... buscarArista()");
        Object vertice = 6;
        Object vertice1 = 4;
        Object vertice2 = 89;
        Object vertice3 = 123;
        this.miGrafoND.insertarVertice(vertice);
        this.miGrafoND.insertarVertice(vertice1);
        this.miGrafoND.insertarVertice(vertice2);
        this.miGrafoND.insertarVertice(vertice3);
        this.miGrafoND.insertarArista(vertice, vertice1);
        this.miGrafoND.insertarArista(vertice1, vertice2);
        this.miGrafoND.insertarArista(vertice2, vertice3);
        this.miGrafoND.insertarArista(vertice3, vertice);
        Arista expResult = null;
        Arista result = this.miGrafoND.buscarArista(vertice,vertice2);
        assertEquals(expResult, result);
        result = this.miGrafoND.buscarArista(vertice1,vertice3);
        assertEquals(expResult, result);
        assertNotNull(this.miGrafoND.buscarArista(vertice1, vertice2));
        assertNotNull(this.miGrafoND.buscarArista(vertice2, vertice3));
    }

    /**
     * Prueba de Metodo esta de la clase GrafoND.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
        Object verticeA = 87 ;
        Object verticeB = 15 ;
        Object verticeC = 33 ;
        Object verticeD = 98 ;
        Object verticeE = 67 ;
        Object verticeF = 76 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeE);
        assertTrue(this.miGrafoND.esta((verticeA)));
        assertTrue(this.miGrafoND.esta((verticeB)));
        assertTrue(this.miGrafoND.esta((verticeC)));
        assertFalse(this.miGrafoND.esta((verticeD)));
        assertTrue(this.miGrafoND.esta((verticeE)));
        assertFalse(this.miGrafoND.esta((verticeF)));
    }

    /**
     * Prueba de Metodo getMatrizAdyacencia de la clase GrafoND.
     */
    @Test
    public void testGetMatrizAdyacencia() {
        System.out.println("Prueba... getMatrizAdyacencia()");
        Object verticeA = 87 ;
        Object verticeB = 15 ;
        Object verticeC = 33 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);        
        Object[][] expResult = new Object[4][4];
        expResult[1][1]=0;  expResult[1][2]=1;  expResult[1][3]=1;
        expResult[2][1]=1;  expResult[2][2]=0;  expResult[2][3]=0;
        expResult[3][1]=1;  expResult[3][2]=0;  expResult[3][3]=0;        
        Object[][] result = this.miGrafoND.getMatrizAdyacencia();
        for(int i=1; i<4; i++)
            for(int j=1; j<4; j++)
                assertEquals(result[i][j].toString(),expResult[i][j].toString());
    }


    /**
     * Prueba de Metodo getMatrizIncidencia de la clase GrafoND.
     */
    @Test
    public void testGetMatrizIncidencia() {
        System.out.println("Prueba... getMatrizIncidencia()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);  
        Object[][] expResult = new Object[4][3];
        expResult[1][1]=1;  expResult[1][2]=1;  
        expResult[2][1]=1;  expResult[2][2]=0;  
        expResult[3][1]=0;  expResult[3][2]=1;  
        Object[][] result = this.miGrafoND.getMatrizIncidencia();
        for(int i=1; i<4; i++)
            for(int j=1; j<3; j++)
                assertEquals(result[i][j].toString(),expResult[i][j].toString());
    }

    /**
     * Prueba de Metodo esMultigrafo de la clase GrafoND.
     */
    @Test
    public void testEsMultigrafo() {
        System.out.println("Prueba... esMultigrafo()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        assertFalse(this.miGrafoND.esMultigrafo());
        this.miGrafoND.insertarArista(verticeA, verticeB);
        assertTrue(this.miGrafoND.esMultigrafo());
        
    }

    /**
     * Prueba de Metodo esPseudoGrafo de la clase GrafoND.
     */
    @Test
    public void testEsPseudoGrafo() {
        System.out.println("Prueba.. esPseudoGrafo()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        assertFalse(this.miGrafoND.esPseudoGrafo());
        this.miGrafoND.insertarArista(verticeC, verticeC);
        assertTrue(this.miGrafoND.esPseudoGrafo());
    }

    /**
     * Prueba de Metodo esGrafoSimple de la clase GrafoND.
     */
    @Test
    public void testEsGrafoSimple() {
        System.out.println("Prueba.. esGrafoSimple()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        assertTrue(this.miGrafoND.esGrafoSimple());
        this.miGrafoND.insertarArista(verticeC, verticeC);
        assertFalse(this.miGrafoND.esGrafoSimple());
    }

    /**
     * Prueba de Metodo esGrafoNulo de la clase GrafoND.
     */
    @Test
    public void testEsGrafoNulo() {
        System.out.println("Prueba... esGrafoNulo()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        assertEquals(this.miGrafoND.esGrafoNulo(),-1);
        this.miGrafoND.eliminarArista(verticeA, verticeB);
        this.miGrafoND.eliminarArista(verticeA, verticeC);
        assertEquals(this.miGrafoND.esGrafoNulo(),3);
    }

    /**
     * Prueba de Metodo esConexo de la clase GrafoND.
     */
    @Test
    public void testEsConexo() {
        System.out.println("Prueba... esConexo()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        assertFalse(this.miGrafoND.esConexo());
        this.miGrafoND.insertarArista(verticeA, verticeC);
        assertTrue(this.miGrafoND.esConexo());
    }


    /**
     * Prueba de Metodo esGrafoRegular de la clase GrafoND.
     */
    @Test
    public void testEsGrafoRegular() {
        System.out.println("Prueba... esGrafoRegular()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        assertFalse(this.miGrafoND.esGrafoRegular());
        this.miGrafoND.insertarArista(verticeB, verticeC);
        assertTrue(this.miGrafoND.esGrafoRegular());
    }

    /**
     * Prueba de Metodo esCompleto de la clase GrafoND.
     */
    @Test
    public void testEsCompleto() {
        System.out.println("Prueba... esCompleto()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        assertEquals(this.miGrafoND.esCompleto(),-1);
        this.miGrafoND.insertarArista(verticeB, verticeC);
        assertEquals(this.miGrafoND.esCompleto(),3);
        
    }

    /**
     * Prueba de Metodo esBipartito de la clase GrafoND.
     */
    @Test
    public void testEsBipartito() {
        System.out.println("Prueba... esBipartito()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        assertEquals(this.miGrafoND.esBipartito(),"El Grafo es Bipartito Completo: K[1,2]");
        Object verticeD = 45 ;
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        assertEquals(this.miGrafoND.esBipartito(),"El Grafo es Bipartito pero no es Completo!");
        this.miGrafoND.insertarArista(verticeA, verticeD);
        assertEquals(this.miGrafoND.esBipartito(),"El Grafo No es Bipartito");
    }

    /**
     * Prueba de Metodo esGrafoHamiltoniano de la clase GrafoND.
     */
    @Test
    public void testEsGrafoHamiltoniano() {
        System.out.println("Prueba... esGrafoHamiltoniano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        assertFalse(this.miGrafoND.esGrafoHamiltoniano());
        this.miGrafoND.insertarArista(verticeB, verticeD);
        assertTrue(this.miGrafoND.esGrafoHamiltoniano());
    }

    /**
     * Prueba de Metodo getCicloHamiltoniano de la clase GrafoND.
     */
    @Test
    public void testGetCicloHamiltoniano() {
        System.out.println("Prueba... getCicloHamiltoniano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeB, verticeD);

        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeB);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeC);
        ListaCD result = this.miGrafoND.getCicloHamiltoniano();
        
        for(int i=0; i<expResult.getTamanio(); i++){
            assertEquals(expResult.get(i).toString(),result.get(i).toString());
        }        
    }

    /**
     * Prueba de Metodo hayCaminoHamiltoniano de la clase GrafoND.
     */
    @Test
    public void testHayCaminoHamiltoniano() {
        System.out.println("Prueba... hayCaminoHamiltoniano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        assertTrue(this.miGrafoND.hayCaminoHamiltoniano());
    }

    /**
     * Prueba de Metodo getCaminoHamiltoniano de la clase GrafoND.
     */
    @Test
    public void testGetCaminoHamiltoniano() {
        System.out.println("Prueba... getCaminoHamiltoniano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);

        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeB);
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        ListaCD result = this.miGrafoND.getCaminoHamiltoniano();
        
        for(int i=0; i<expResult.getTamanio(); i++){
            assertEquals(expResult.get(i).toString(),result.get(i).toString());
        }        
    }

    /**
     * Prueba de Metodo esGrafoEuleriano de la clase GrafoND.
     */
    @Test
    public void testEsGrafoEuleriano() {
        System.out.println("Prueba... esGrafoEuleriano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeB, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        assertFalse(this.miGrafoND.esGrafoEuleriano());
        this.miGrafoND.insertarArista(verticeA, verticeD);
        assertTrue(this.miGrafoND.esGrafoEuleriano());
    }

    /**
     * Prueba de Metodo getCicloEuleriano de la clase GrafoND.
     */
    @Test
    public void testGetCicloEuleriano() {
        System.out.println("Prueba... getCicloEuleriano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeB, verticeD);

        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeB);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeA);
        ListaCD result = this.miGrafoND.getCicloEuleriano();
        
        for(int i=0; i<expResult.getTamanio(); i++){
            assertEquals(expResult.get(i).toString(),result.get(i).toString());
        }        
    }

    /**
     * Prueba de Metodo hayCaminoEuleriano de la clase GrafoND.
     */
    @Test
    public void testHayCaminoEuleriano() {
        System.out.println("Prueba... hayCaminoEuleriano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        assertTrue(this.miGrafoND.hayCaminoEuleriano());
    }

    /**
     * Prueba de Metodo getCaminoEuleriano de la clase GrafoND.
     */
    @Test
    public void testGetCaminoEuleriano() {
        System.out.println("Prueba... getCaminoEuleriano()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);

        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeB);
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        ListaCD result = this.miGrafoND.getCaminoEuleriano();
        
        for(int i=0; i<expResult.getTamanio(); i++){
            assertEquals(expResult.get(i).toString(),result.get(i).toString());
        }        
    }

    /**
     * Prueba de Metodo existeRutaEntre de la clase GrafoND.
     */
    @Test
    public void testExisteRutaEntre() {
        System.out.println("Prueba... existeRutaEntre()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        assertTrue(this.miGrafoND.existeRutaEntre(verticeA, verticeC));
        assertTrue(this.miGrafoND.existeRutaEntre(verticeB, verticeC));
        assertTrue(this.miGrafoND.existeRutaEntre(verticeA, verticeC));
    }

    /**
     * Prueba de Metodo getLongitudDeCamino de la clase GrafoND.
     */
    @Test
    public void testGetLongitudDeCamino() {
        System.out.println("Prueba... getLongitudDeCamino()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        assertEquals(this.miGrafoND.getLongitudDeCamino(verticeA, verticeC),1);
        assertEquals(this.miGrafoND.getLongitudDeCamino(verticeA, verticeD),2);
        assertEquals(this.miGrafoND.getLongitudDeCamino(verticeD, verticeB),3);
    }

    /**
     * Prueba de Metodo longRutaMinimaDijkstra de la clase GrafoND.
     */
    @Test
    public void testLongRutaMinimaDijkstra() {
        System.out.println("Prueba... longRutaMinimaDijkstra()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarAristaP(verticeA, verticeB, 6);
        this.miGrafoND.insertarAristaP(verticeA, verticeC, 23);
        this.miGrafoND.insertarAristaP(verticeC, verticeD, 90);
        this.miGrafoND.insertarAristaP(verticeD, verticeB, 12);
        assertEquals(this.miGrafoND.longRutaMinimaDijkstra(verticeA, verticeD), 18);
        assertEquals(this.miGrafoND.longRutaMinimaDijkstra(verticeD, verticeC), 41);
    }


    /**
     * Prueba de Metodo rutaMasCorta de la clase GrafoND.
     */
    @Test
    public void testRutaMasCorta() {
        System.out.println("Prueba... rutaMasCorta()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeD, verticeB);
        assertEquals(this.miGrafoND.rutaMasCorta(verticeA, verticeC),2);
        assertEquals(this.miGrafoND.rutaMasCorta(verticeB, verticeC),3);
    }

    /**
     * Prueba de Metodo getBEP de la clase GrafoND.
     */
    @Test
    public void testGetBEP_GenericType() {
        System.out.println("Prueba... getBEP()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        Object verticeE = 911 ;
        
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarVertice(verticeE);
        
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeD, verticeE);
        
        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeE);
        ListaCD result = this.miGrafoND.getBEP(verticeA.toString());
        for(int i=0; i<result.getTamanio();i++)
            assertEquals(result.get(i).toString(),expResult.get(i).toString());
    }

    /**
     * Prueba de Metodo getBEA de la clase GrafoND.
     */
    @Test
    public void testGetBEA_GenericType() {
        System.out.println("Prueba... getBEA()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        Object verticeE = 911 ;
        
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarVertice(verticeE);
        
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeD, verticeE);
        
        ListaCD expResult = new ListaCD();
        expResult.insertarAlFinal(verticeA);
        expResult.insertarAlFinal(verticeB);
        expResult.insertarAlFinal(verticeC);
        expResult.insertarAlFinal(verticeD);
        expResult.insertarAlFinal(verticeE);
        ListaCD result = this.miGrafoND.getBEA(verticeA.toString());
        for(int i=0; i<result.getTamanio();i++)
            assertEquals(result.get(i).toString(),expResult.get(i).toString());
    }

    /**
     * Prueba de Metodo sonVerticesAdyacentes de la clase GrafoND.
     */
    @Test
    public void testSonVerticesAdyacentes() {
        System.out.println("Prueba... sonVerticesAdyacentes()");
        Object verticeA = 19 ;
        Object verticeB = 52 ;
        Object verticeC = 320 ;
        Object verticeD = 39 ;
        Object verticeE = 911 ;
        
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarVertice(verticeE);
        
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeD, verticeE);
        
        assertTrue(this.miGrafoND.sonVerticesAdyacentes(verticeA, verticeB));
        assertFalse(this.miGrafoND.sonVerticesAdyacentes(verticeC, verticeB));
        assertTrue(this.miGrafoND.sonVerticesAdyacentes(verticeC, verticeD));
        assertFalse(this.miGrafoND.sonVerticesAdyacentes(verticeD, verticeB));
        assertTrue(this.miGrafoND.sonVerticesAdyacentes(verticeD, verticeE));
    }

    /**
     * Prueba de Metodo getGradoSalidaVert de la clase GrafoND.
     */
    @Test
    public void testGetGradoSalidaVert() {
        System.out.println("Prueba.. getGradoSalidaVert()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarVertice(verticeE);
        
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeD, verticeE);
        
        assertEquals(this.miGrafoND.getGradoSalidaVert(verticeA),2);
        assertEquals(this.miGrafoND.getGradoSalidaVert(verticeB),1);
        assertEquals(this.miGrafoND.getGradoSalidaVert(verticeC),2);
        assertEquals(this.miGrafoND.getGradoSalidaVert(verticeD),2);
        
    }

    /**
     * Prueba de Metodo getGradoEntradaVert de la clase GrafoND.
     */
    @Test
    public void testGetGradoEntradaVert() {
        System.out.println("Prueba... getGradoEntradaVert()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarVertice(verticeE);
        
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeD, verticeE);
        
        assertEquals(this.miGrafoND.getGradoEntradaVert(verticeA),2);
        assertEquals(this.miGrafoND.getGradoEntradaVert(verticeB),1);
        assertEquals(this.miGrafoND.getGradoEntradaVert(verticeC),2);
        assertEquals(this.miGrafoND.getGradoEntradaVert(verticeD),2);
    }

    /**
     * Prueba de Metodo esVerticeAislado de la clase GrafoND.
     */
    @Test
    public void testEsVerticeAislado() {
        System.out.println("Prueba... esVerticeAislado()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarVertice(verticeE);
        
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        
        assertFalse(this.miGrafoND.esVerticeAislado(verticeA));
        assertFalse(this.miGrafoND.esVerticeAislado(verticeB));
        assertFalse(this.miGrafoND.esVerticeAislado(verticeC));
        assertFalse(this.miGrafoND.esVerticeAislado(verticeD));
        assertTrue(this.miGrafoND.esVerticeAislado(verticeE));
    }

    /**
     * Prueba de Metodo getPeso de la clase GrafoND.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miGrafoND.insertarVertice(verticeA);
        assertEquals(this.miGrafoND.getPeso(),1);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        assertEquals(this.miGrafoND.getPeso(),3);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarVertice(verticeE);
        assertEquals(this.miGrafoND.getPeso(),5);        
    }

    /**
     * Prueba de Metodo getListaGrados de la clase GrafoND.
     */
    @Test
    public void testGetListaGrados() {
        System.out.println("Prueba... getListaGrados()");
        Object verticeA = 45 ;
        Object verticeB = 78 ;
        Object verticeC = 30 ;
        Object verticeD = 359 ;
        Object verticeE = 91 ;
        
        this.miGrafoND.insertarVertice(verticeA);
        this.miGrafoND.insertarVertice(verticeB);
        this.miGrafoND.insertarVertice(verticeC);
        this.miGrafoND.insertarVertice(verticeD);
        this.miGrafoND.insertarVertice(verticeE);
        
        this.miGrafoND.insertarArista(verticeA, verticeB);
        this.miGrafoND.insertarArista(verticeA, verticeC);
        this.miGrafoND.insertarArista(verticeC, verticeD);
        this.miGrafoND.insertarArista(verticeD, verticeE);
        
        int expResult[] = {2,1,2,2,1};
        int result[] = this.miGrafoND.getListaGrados();
        for(int i=0; i<result.length;i++)
            assertEquals(result[i],expResult[i]);
    }

}