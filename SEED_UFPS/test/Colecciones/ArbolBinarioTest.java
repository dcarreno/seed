/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ArbolBinario;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Arbol Binario.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolBinarioTest extends TestCase{
    
    private ArbolBinario miArbol;
    
    public ArbolBinarioTest() {
        super();
    }
    
    @Override
    public  void setUp() {
        this.miArbol= new ArbolBinario();
    }
   
    /**
     * Prueba Metodo getObjRaiz(), de la clase ArbolBinario.
     */
    @Test
    public void testGetObjRaiz() {        
        System.out.println("Prueba... getObjRaiz()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoIzq(null,23);
        assertNotNull(this.miArbol.getRaiz());
        assertEquals(this.miArbol.getObjRaiz(),8);
    }

    /**
     *  Prueba Metodo insertarHijoIzq(), de la clase ArbolBinario.
     */
    @Test
    public void testInsertarHijoIzq() {
        System.out.println("Prueba... insertarHijoIzq()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.imprime();
        String cad="8-6-7-12-23-";
        assertNotNull(this.miArbol.getRaiz().getIzq().getInfo());
        assertEquals(cad, this.miArbol.preOrden_Iterativo());
    }

    /**
     * Prueba Metodo insertarHijoDer(), de la clase ArbolBinario.
     */
    @Test
    public void testInsertarHijoDer() {
        System.out.println("Prueba... insertarHijoDer()");
        this.miArbol.insertarHijoDer(null, 3);
        this.miArbol.insertarHijoDer(3,6);
        this.miArbol.insertarHijoDer(6,23);
        this.miArbol.insertarHijoDer(23,7);
        this.miArbol.insertarHijoDer(7,12);
        this.miArbol.insertarHijoDer(12,20);
        String cad="3-6-23-7-12-20-";
        assertFalse(this.miArbol.getRaiz().getDer().equals(6));
        assertEquals(cad, this.miArbol.preOrden_Iterativo());
    }

    /**
     * Prueba Metodo eliminar(), de la clase ArbolBinario.
     */
    @Test
    public void testEliminar() {        
        System.out.println("Prueba... eliminar()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        assertTrue(this.miArbol.eliminar(6));
        assertTrue(this.miArbol.eliminar(17));
        assertTrue(this.miArbol.eliminar(20));
        String cad="3-7-23-27-12-8-";
        assertEquals(cad, this.miArbol.inOrden_Iterativo());
    }

    /**
     * Prueba Metodo esta(), de la clase ArbolBinario.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        assertNotNull(this.miArbol.getRaiz().getDer());
        assertFalse(this.miArbol.esta(15));
        assertTrue(this.miArbol.esta(34));
    }

    /**
     * Prueba Metodo setDato(), de la clase ArbolBinario.
     */
    @Test
    public void testSetDato() {
        System.out.println("Prueba... setDato");
       this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoDer(8,50);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        assertFalse(this.miArbol.setDato(8, 23));
        assertTrue(this.miArbol.setDato(8, 10));
    }

    /**
     * Prueba Metodo getHojas(), de la clase ArbolBinario.
     */
    @Test
    public void testGetHojas() {
        System.out.println("Prueba... getHojas()");
       this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
         String cad="3-23-20-34-";
         String cad2="";
          assertNotNull(this.miArbol.getHojas());
           Iterator<Integer> it= this.miArbol.getHojas();
            while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo contarHojas(), de la clase ArbolBinario.
     */
    @Test
    public void testContarHojas() {
        System.out.println("Prueba... contarHojas()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoDer(8,50);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
         assertNotNull(this.miArbol.getHojas());
         assertTrue(this.miArbol.contarHojas()==5);
    }

    /**
     *  Prueba Metodo preOrden(), de la clase ArbolBinario.
     */
    @Test
    public void testPreOrden() {
        System.out.println("Prueba... preOrden()");
         this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
         String cad2="";
         assertNotNull(this.miArbol.preOrden());
         String cad="8-6-7-3-12-23-20-34-";
          Iterator<Integer> it= this.miArbol.preOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo inOrden(), de la clase ArbolBinario.
     */
    @Test
    public void testInOrden() {
        System.out.println("Prueba... inOrden()");
         this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
         String cad="3-7-6-23-12-20-8-34-";
         assertNotNull(this.miArbol.inOrden());
         String cad2="";
          Iterator<Integer> it= this.miArbol.inOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo posOrden(), de la clase ArbolBinario.
     */
    @Test
    public void testPosOrden() {
        System.out.println("Prueba... posOrden()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        String cad="3-7-23-20-12-6-34-8-";
         assertNotNull(this.miArbol.postOrden());
         String cad2="";
         Iterator<Integer> it= this.miArbol.postOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     *Prueba Metodo preOrden_Iterativo(), de la clase ArbolBinario.
     */
    @Test
    public void testPreOrden_Iterativo() {
        System.out.println("Prueba...  preOrden_Iterativo()");
         this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        String cad="8-6-7-3-12-23-20-34-";
         assertNotNull(this.miArbol.preOrden_Iterativo());
         String cad2=this.miArbol.preOrden_Iterativo();
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo inOrden_Iterativo(), de la clase ArbolBinario.
     */
    @Test
    public void testInOrden_Iterativo() {
       System.out.println("Prueba...  inOrden_Iterativo()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        String cad="3-7-6-23-12-20-8-34-";
         assertNotNull(this.miArbol.inOrden_Iterativo());
         String cad2=this.miArbol.inOrden_Iterativo();
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo impNiveles(), de la clase ArbolBinario.
     */
    @Test
    public void testImpNiveles() {
        System.out.println("Prueba... impNiveles()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        String cad="8-6-34-7-12-3-23-20-";
        String cad2="";
         Iterator<Integer> it= this.miArbol.impNiveles();
          assertNotNull(it);
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo getPeso(), de la clase ArbolBinario.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
         this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoDer(8,50);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        assertTrue(this.miArbol.getPeso()==10);
    }

    /**
     * Prueba Metodo esVacio(), de la clase ArbolBinario.
     */
    @Test
    public void testEsVacio() {
        System.out.println("Prueba... esVacio()");
         this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoDer(8,50);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        assertFalse(this.miArbol.esVacio());
    }

    /**
     * Prueba Metodo getAltura(), de la clase ArbolBinario.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
       this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoDer(8,50);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        assertTrue(this.miArbol.getAltura()==5);
    }

    /**
     * Prueba Metodo getGrado(), de la clase ArbolBinario.
     */
    @Test
    public void testGetGrado() {
        System.out.println("Prueba... getGrado()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoDer(8,50);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
        assertNotNull(this.miArbol.getRaiz());
        assertTrue(this.miArbol.getAltura()==5);
    }

    /**
     *  Prueba Metodo esCompleto(), de la clase ArbolBinario.
     */
    @Test
    public void testEsCompleto() {
        System.out.println("Prueba... esCompleto()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,50);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(50,23);
        this.miArbol.insertarHijoDer(50,17);
        assertTrue(this.miArbol.esCompleto());
    }

    /**
     *  Prueba Metodo estaLleno(), de la clase ArbolBinario.
     */
    @Test
    public void testEstaLleno() {
        System.out.println("Prueba... estaLleno()");
         this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(7,3);
        this.miArbol.insertarHijoDer(12,20);
      assertFalse( this.miArbol.estaLleno());
    }

    /**
     *  Prueba Metodo podar(), de la clase ArbolBinario.
     */
    @Test
    public void testPodar() {
        System.out.println("Prueba... podar()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(34,3);
        this.miArbol.insertarHijoDer(34,20);
         this.miArbol.insertarHijoIzq(23,11);
        this.miArbol.insertarHijoDer(12,37);
        this.miArbol.insertarHijoDer(7,91);
        int numhoja= this.miArbol.contarHojas();
        this.miArbol.podar();
        assertFalse(numhoja==this.miArbol.contarHojas());
        assertTrue(this.miArbol.contarHojas()==3);
    }

    /**
     *  Prueba Metodo luca(), de la clase ArbolBinario.
     */
    @Test
    public void testLuca() {
        System.out.println("Prueba... Luca()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(34,3);
        this.miArbol.insertarHijoDer(34,20);
         this.miArbol.insertarHijoIzq(23,11);
        this.miArbol.insertarHijoDer(12,37);
        this.miArbol.insertarHijoDer(7,91);
        String l=this.miArbol.Luca();
        String cad="aaababbaaabbabbabbaabbabb";
        assertNotNull(l);
       assertEquals(cad,l);
    }

    /**
     * Prueba Metodo esIgual(), de la clase ArbolBinario.
     */
    @Test
    public void testEsIgual() {
        System.out.println("Prueba... esIgual()");
        this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(34,3);
        this.miArbol.insertarHijoDer(34,20);
         this.miArbol.insertarHijoIzq(23,11);
        this.miArbol.insertarHijoDer(12,37);
        this.miArbol.insertarHijoDer(7,91);
        ArbolBinario n=new ArbolBinario();
        n.insertarHijoIzq(null, 8);
        n.insertarHijoIzq(8,6);
        n.insertarHijoDer(8,34);
        n.insertarHijoIzq(6,7);
        n.insertarHijoDer(6,12);
        n.insertarHijoIzq(12,23);
        n.insertarHijoIzq(23,17);
        n.insertarHijoDer(23,27);
        n.insertarHijoIzq(34,3);
        n.insertarHijoDer(34,20);
        n.insertarHijoIzq(23,11);
        n.insertarHijoDer(12,37);
        n.insertarHijoDer(7,91);
        assertTrue(this.miArbol.esIgual(n));
    }

    /**
     * Prueba Metodo esIsomorfo(), de la clase ArbolBinario.
     */
    @Test
    public void testEsIsomorfo() {
        System.out.println("Prueba... esIsomorfo()");
       this.miArbol.insertarHijoIzq(null, 8);
        this.miArbol.insertarHijoIzq(8,6);
        this.miArbol.insertarHijoDer(8,34);
        this.miArbol.insertarHijoIzq(6,7);
        this.miArbol.insertarHijoDer(6,12);
        this.miArbol.insertarHijoIzq(12,23);
        this.miArbol.insertarHijoIzq(23,17);
        this.miArbol.insertarHijoDer(23,27);
        this.miArbol.insertarHijoIzq(34,3);
        this.miArbol.insertarHijoDer(34,20);
         this.miArbol.insertarHijoIzq(23,11);
        this.miArbol.insertarHijoDer(12,37);
        this.miArbol.insertarHijoDer(7,91);
        ArbolBinario n=new ArbolBinario();
        n.insertarHijoIzq(null, 9);
        n.insertarHijoIzq(9,10);
        n.insertarHijoDer(9,6);
        n.insertarHijoIzq(10,8);
        n.insertarHijoDer(10,12);
        n.insertarHijoDer(8,7);
        n.insertarHijoIzq(12,1);
        n.insertarHijoDer(12,2);
        n.insertarHijoIzq(1,33);
        n.insertarHijoDer(1,24);
        n.insertarHijoIzq(6,11);
        n.insertarHijoDer(6,12);
        assertTrue(this.miArbol.esIsomorfo(n));
    }


}