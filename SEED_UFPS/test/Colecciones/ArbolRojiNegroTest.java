/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.NodoRN;
import Colecciones.ArbolRojiNegro;
import java.util.Iterator;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para Arbol RojiNegro.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolRojiNegroTest extends TestCase{
    
    private ArbolRojiNegro miARN;
    
    public ArbolRojiNegroTest() {
        super();
    }
    
    public void setUp() {
        this.miARN= new ArbolRojiNegro();
    }

    /**
     * Prueba Metodo getObjRaiz(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testGetObjRaiz() {
        System.out.println("Prueba... getObjRaiz()");
        this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        assertNotNull(this.miARN.getRaiz());
        assertEquals(this.miARN.getObjRaiz(),23);
    }

    /**
     * Prueba Metodo insertar(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testInsertarRN() {
        System.out.println("Prueba... insertar()");
       assertTrue(this.miARN.insertar(23));
        assertTrue(this.miARN.insertar(7));
        assertTrue(this.miARN.insertar(25));
        assertTrue(this.miARN.insertar(3));
        assertTrue(this.miARN.insertar(18));
        assertTrue(this.miARN.insertar(24));
        assertTrue(this.miARN.insertar(30));
        assertTrue(this.miARN.insertar(1));
        assertTrue(this.miARN.insertar(5));
        assertTrue(this.miARN.insertar(8));
        assertTrue(this.miARN.insertar(19));
        assertTrue(this.miARN.insertar(28));
        assertTrue(this.miARN.insertar(35));
        this.miARN.imprime();
    }

    /**
     * Prueba Metodo eliminar(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
        this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
        assertFalse(this.miARN.eliminar(6));
        assertTrue(this.miARN.eliminar(7));
        assertTrue(this.miARN.eliminar(30));
    }

    /**
     * Prueba Metodo esta(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
       this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
        assertNotNull(this.miARN.getRaiz().getDer());
        assertFalse(this.miARN.esta(34));
        assertTrue(this.miARN.esta(23));
    }

    /**
     *  Prueba Metodo getHojas(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testGetHojas() {
       System.out.println("Prueba... getHojas()");
       this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
        this.miARN.insertar(0);
         String cad="0-5-8-19-24-28-35-";
         String cad2="";
          assertNotNull(this.miARN.getHojas());
           Iterator<Integer> it= this.miARN.getHojas();
            while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     *  Prueba Metodo perOrden(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testPreOrden() {
        System.out.println("Prueba... preOrden()");
       this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
         this.miARN.insertar(0);
         String cad="23-7-3-1-0-5-18-8-19-25-24-30-28-35-";
        String cad2="";
         assertNotNull(this.miARN.preOrden());
          Iterator<Integer> it= this.miARN.preOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
        assertTrue( ((NodoRN)this.miARN.getRaiz()).getColor()==1);
    }

    /**
     * Prueba Metodo inOrden(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testInOrden() {
        System.out.println("Prueba... inOrden()");
        this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
        this.miARN.insertar(0);
         String cad="0-1-3-5-7-8-18-19-23-24-25-28-30-35-";
         assertNotNull(this.miARN.inOrden());
         String cad2="";
          Iterator<Integer> it= this.miARN.inOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     * Prueba Metodo postOrden(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testPostOrden() {
        System.out.println("Prueba... postOrden()");
        this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
        this.miARN.insertar(0);
         String cad="0-1-5-3-8-19-18-7-24-28-35-30-25-23-";
         assertNotNull(this.miARN.postOrden());
         String cad2="";
         Iterator<Integer> it= this.miARN.postOrden();
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     *  Prueba Metodo impNiveles(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testImpNiveles() {
        System.out.println("Prueba... impNiveles()");
      this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
        this.miARN.insertar(0);
        String cad="23-7-25-3-18-24-30-1-5-8-19-28-35-0-";
        String cad2="";
         Iterator<Integer> it= this.miARN.impNiveles();
          assertNotNull(it);
          while(it.hasNext())
                cad2+=it.next().toString()+"-";
        assertEquals(cad, cad2);
    }

    /**
     *  Prueba Metodo getPeso(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
      this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
        this.miARN.insertar(0);
        assertFalse(this.miARN.getPeso()==0);
        assertTrue(this.miARN.getPeso()==14);
    }

    /**
     *  Prueba Metodo esVacio(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testEsVacio() {
        System.out.println("Prueba... esVacio()");
        assertTrue(this.miARN.esVacio());
    }

    /**
     *  Prueba Metodo getAltura(), de la clase ArbolRojiNegro.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
        this.miARN.insertar(23);
        this.miARN.insertar(7);
        this.miARN.insertar(25);
        this.miARN.insertar(3);
        this.miARN.insertar(18);
        this.miARN.insertar(24);
        this.miARN.insertar(30);
        this.miARN.insertar(1);
        this.miARN.insertar(5);
        this.miARN.insertar(8);
        this.miARN.insertar(19);
        this.miARN.insertar(28);
        this.miARN.insertar(35);
        this.miARN.insertar(0);
        assertTrue(this.miARN.getAltura()==5);
    }

}