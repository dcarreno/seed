/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ArbolEneario;
import Colecciones.ListaCD;
import java.util.Iterator;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ArbolEneario.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ArbolEnearioTest extends TestCase{
    
    private ArbolEneario miArbolEneario;
    
    public ArbolEnearioTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miArbolEneario = new ArbolEneario();
    }
    /**
     * Prueba de Metodo getObjRaiz de la clase ArbolEneario.
     */
    @Test
    public void testGetObjRaiz() {
        System.out.println("Prueba... getObjRaiz()");
        this.miArbolEneario.insertarHijo(null, 85);
        Object expResult = 85;
        Object result = this.miArbolEneario.getObjRaiz();
        assertEquals(expResult.toString(), result.toString());
        
        this.miArbolEneario.insertarHijo(85, 332);
        expResult = 85;
        result = this.miArbolEneario.getObjRaiz();
        assertEquals(expResult.toString(), result.toString());
        
        this.miArbolEneario.eliminar(85);
        expResult = 332;
        result = this.miArbolEneario.getObjRaiz();
        assertEquals(expResult.toString(), result.toString());
    }

    /**
     * Prueba de Metodo insertarHijo de la clase ArbolEneario.
     */
    @Test
    public void testInsertarHijo() {
        System.out.println("Prueba... insertarHijo()");
        assertTrue(this.miArbolEneario.insertarHijo(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHijo(85, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHijo(85, 13));
        assertTrue(this.miArbolEneario.insertarHijo(23, 19));
        assertTrue(this.miArbolEneario.insertarHijo(13, 70));
        assertTrue(this.miArbolEneario.esta(85));
        assertTrue(this.miArbolEneario.esta(13));
        assertTrue(this.miArbolEneario.esta(69));
        assertFalse(this.miArbolEneario.esta(21));
    }

    /**
     * Prueba de Metodo insertarHermano de la clase ArbolEneario.
     */
    @Test
    public void testInsertarHermano() {
        System.out.println("Prueba... insertarHermano()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        assertTrue(this.miArbolEneario.esta(85));
        assertFalse(this.miArbolEneario.esta(79));
        assertTrue(this.miArbolEneario.esta(23));
        assertTrue(this.miArbolEneario.esta(69));
        assertFalse(this.miArbolEneario.esta(21));
    }

    /**
     * Prueba de Metodo eliminar de la clase ArbolEneario.
     */
    @Test
    public void testEliminar() {
        System.out.println("Prueba... eliminar()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        assertTrue(this.miArbolEneario.esta(85));
        this.miArbolEneario.eliminar(85);
        assertFalse(this.miArbolEneario.esta(85));
        assertTrue(this.miArbolEneario.esta(23));
        this.miArbolEneario.eliminar(23);
        assertFalse(this.miArbolEneario.esta(23));
        assertTrue(this.miArbolEneario.esta(69));
        this.miArbolEneario.eliminar(69);
        assertFalse(this.miArbolEneario.esta(69));
    }

    /**
     * Prueba de Metodo esta de la clase ArbolEneario.
     */
    @Test
    public void testEsta() {
        System.out.println("Prueba... esta()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        assertTrue(this.miArbolEneario.esta(85));
        this.miArbolEneario.eliminar(85);
        assertFalse(this.miArbolEneario.esta(85));
        assertTrue(this.miArbolEneario.esta(23));
        this.miArbolEneario.eliminar(23);
        assertFalse(this.miArbolEneario.esta(23));
        assertTrue(this.miArbolEneario.esta(69));
        this.miArbolEneario.eliminar(69);
        assertFalse(this.miArbolEneario.esta(69));
    }

    /**
     * Prueba de Metodo getHijos de la clase ArbolEneario.
     */
    @Test
    public void testGetHijos() {
        System.out.println("Prueba... getHijos");
        assertTrue(this.miArbolEneario.insertarHijo(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHijo(85, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHijo(85, 13));
        assertTrue(this.miArbolEneario.insertarHijo(23, 19));
        assertTrue(this.miArbolEneario.insertarHijo(13, 70));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(13);
        l.insertarAlFinal(12);        
        l.insertarAlFinal(23);    
        
        Iterator it = this.miArbolEneario.getHijos(85);
        Iterator it2 = l.iterator();
        
        while(it.hasNext() && it2.hasNext())
            assertEquals(it.next().toString(),it2.next().toString());
    }

    /**
     * Prueba de Metodo getHojas de la clase ArbolEneario.
     */
    @Test
    public void testGetHojas() {
        System.out.println("Prueba... getHojas()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(69);
        l.insertarAlFinal(70);        
        l.insertarAlFinal(19);    
        l.insertarAlFinal(13);    
        
        Iterator it = this.miArbolEneario.getHojas();
        Iterator it2 = l.iterator();
        
        while(it.hasNext() && it2.hasNext())
            assertEquals(it.next().toString(),it2.next().toString());
        
    }

    /**
     * Prueba de Metodo contarHojas de la clase ArbolEneario.
     */
    @Test
    public void testContarHojas() {
        System.out.println("Prueba... contarHojas()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        assertEquals(this.miArbolEneario.contarHojas(),5);
    }

    /**
     * Prueba de Metodo preOrden de la clase ArbolEneario.
     */
    @Test
    public void testPreOrden() {
        System.out.println("Prueba... preOrden()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(85);
        l.insertarAlFinal(23);        
        l.insertarAlFinal(69);    
        l.insertarAlFinal(70);    
        l.insertarAlFinal(19);
        l.insertarAlFinal(13);        
        l.insertarAlFinal(12);    
        
        Iterator it = this.miArbolEneario.preOrden();
        Iterator it2 = l.iterator();
        
        while(it.hasNext() && it2.hasNext())
            assertEquals(it.next().toString(),it2.next().toString());
    }

    /**
     * Prueba de Metodo inOrden de la clase ArbolEneario.
     */
    @Test
    public void testInOrden() {
        System.out.println("Prueba... inOrden()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(69);
        l.insertarAlFinal(23);        
        l.insertarAlFinal(70);    
        l.insertarAlFinal(19);    
        l.insertarAlFinal(13);
        l.insertarAlFinal(85);        
        l.insertarAlFinal(12);    
        
        Iterator it = this.miArbolEneario.inOrden();
        Iterator it2 = l.iterator();
        
        while(it.hasNext() && it2.hasNext())
            assertEquals(it.next().toString(),it2.next().toString());
    }

    /**
     * Prueba de Metodo postOrden de la clase ArbolEneario.
     */
    @Test
    public void testPosOrden() {
        System.out.println("Prueba... postOrden()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(69);
        l.insertarAlFinal(70);        
        l.insertarAlFinal(19);    
        l.insertarAlFinal(13);    
        l.insertarAlFinal(23);
        l.insertarAlFinal(12);        
        l.insertarAlFinal(85);    
        
        Iterator it = this.miArbolEneario.postOrden();
        Iterator it2 = l.iterator();
        
        while(it.hasNext() && it2.hasNext())
            assertEquals(it.next().toString(),it2.next().toString());
    }

    /**
     * Prueba de Metodo impNiveles de la clase ArbolEneario.
     */
    @Test
    public void testImpNiveles() {
        System.out.println("Prueba... impNiveles()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        ListaCD l = new ListaCD();
        l.insertarAlFinal(85);
        l.insertarAlFinal(23);        
        l.insertarAlFinal(12);    
        l.insertarAlFinal(69);    
        l.insertarAlFinal(70);
        l.insertarAlFinal(19);        
        l.insertarAlFinal(13);    
        
        Iterator it = this.miArbolEneario.impNiveles();
        Iterator it2 = l.iterator();
        
        while(it.hasNext() && it2.hasNext())
            assertEquals(it.next().toString(),it2.next().toString());
    }

    /**
     * Prueba de Metodo getPeso de la clase ArbolEneario.
     */
    @Test
    public void testGetPeso() {
        System.out.println("Prueba... getPeso()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        assertEquals(this.miArbolEneario.getPeso(),7);
    }


    /**
     * Prueba de Metodo gordura de la clase ArbolEneario.
     */
    @Test
    public void testGordura() {
        System.out.println("Prueba... gordura()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        assertEquals(this.miArbolEneario.gordura(),4);
    }

    /**
     * Prueba de Metodo getAltura de la clase ArbolEneario.
     */
    @Test
    public void testGetAltura() {
        System.out.println("Prueba... getAltura()");
        assertTrue(this.miArbolEneario.insertarHermano(null, 85));
        assertTrue(this.miArbolEneario.insertarHijo(85, 23));
        assertTrue(this.miArbolEneario.insertarHermano(23, 12));
        assertTrue(this.miArbolEneario.insertarHijo(23, 69));
        assertTrue(this.miArbolEneario.insertarHermano(69, 13));
        assertTrue(this.miArbolEneario.insertarHermano(69, 19));
        assertTrue(this.miArbolEneario.insertarHermano(69, 70));
        
        assertEquals(this.miArbolEneario.getAltura(),3);
    }
}