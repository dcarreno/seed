/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Colecciones;

import Colecciones.ColaP;
import junit.framework.TestCase;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Implementacion Prueba de Unidad para ColaP.
 * @author Uriel García - Yulieth Pabón
 * @version 1.0
 */

public class ColaPTest extends TestCase {
    
    private ColaP miColaP;
    
    public ColaPTest() {
        super();
    }
    
    @Override
    public void setUp() {
        this.miColaP= new ColaP();
    }
    /**
     * Prueba Metodo enColar(), de la clase ColaP.
     */
    @Test
    public void testEnColar() {
        System.out.println("Prueba... enColar()");
        Object info = 0;
        this.miColaP.enColar(info,3);
        info = 1;
        this.miColaP.enColar(info,3);
        info = 2;
        this.miColaP.enColar(info,1);
        info = 3;
        this.miColaP.enColar(info,1);
        info = 4;
        this.miColaP.enColar(info,0);
        info = 5;
        this.miColaP.enColar(info,2);
        info = 6;
        this.miColaP.enColar(info,4);
        info = 7;
        this.miColaP.enColar(info,5);
        assertEquals(this.miColaP.deColar(),7);
    }

    /**
     * Prueba Metodo deColar(), de la clase ColaP.
     */
    @Test
    public void testDeColar() {
        System.out.println("Prueba... deColar()");
        Object info = 0;
        this.miColaP.enColar(info,2);
        info = 1;
        this.miColaP.enColar(info,1);
        info = 2;
        this.miColaP.enColar(info,0);
        info = 3;
        this.miColaP.enColar(info,9);
        info = 4;
        this.miColaP.enColar(info,3);
        info = 5;
        this.miColaP.enColar(info,1);
        info = 6;
        this.miColaP.enColar(info,0);
        
        this.miColaP.deColar();
        this.miColaP.deColar();
        this.miColaP.deColar();
        
        assertEquals(this.miColaP.deColar(),1);
    }

    /**
     * Prueba Metodo vaciar(), de la clase ColaP.
     */
    @Test
    public void testVaciar() {
        System.out.println("Prueba... vaciar()");
        Object info = 0;
        this.miColaP.enColar(info,2);
        info = 1;
        this.miColaP.enColar(info,1);
        info = 2;
        this.miColaP.enColar(info,0);
        info = 3;
        this.miColaP.enColar(info,9);
        info = 4;
        this.miColaP.enColar(info,3);
        info = 5;
        this.miColaP.enColar(info,1);
        info = 6;
        this.miColaP.enColar(info,0);
        info = 7;
        this.miColaP.enColar(info,5);
        info = 8;
        this.miColaP.enColar(info,4);
        info = 9;
        this.miColaP.enColar(info,3);
        info = 10;
        this.miColaP.enColar(info,1);
        info = 11;
        this.miColaP.enColar(info,0);
        info = 12;
        this.miColaP.enColar(info,7);
        info = 13;
        this.miColaP.enColar(info,2);
        this.miColaP.vaciar();
        assertTrue(this.miColaP.esVacia());
    }

    /**
     * Prueba Metodo getIni(), de la clase ColaP.
     */
    @Test
    public void testGetIni() {
        System.out.println("Prueba... getIni()");
        Object info = 0;
        this.miColaP.enColar(info,2);
        info = 1;
        this.miColaP.enColar(info,1);
        info = 2;
        this.miColaP.enColar(info,0);
        info = 3;
        this.miColaP.enColar(info,9);
        info = 4;
        this.miColaP.enColar(info,3);
        info = 5;
        this.miColaP.enColar(info,1);
        info = 6;
        this.miColaP.enColar(info,0);
        info = 7;
        this.miColaP.enColar(info,5);
        info = 8;
        this.miColaP.enColar(info,4);        
        assertEquals(this.miColaP.getInfoInicio(),3);
    }

    /**
     * Prueba Metodo getTamanio(), de la clase ColaP.
     */
    @Test
    public void testGetTamanio() {
        System.out.println("Prueba... getTamanio()");
        Object info = 0;
        this.miColaP.enColar(info,2);
        info = 1;
        this.miColaP.enColar(info,1);
        info = 2;
        this.miColaP.enColar(info,0);
        info = 3;
        this.miColaP.enColar(info,9);
        info = 4;
        this.miColaP.enColar(info,3);
        info = 5;
        this.miColaP.enColar(info,1);
        info = 6;
        this.miColaP.enColar(info,0);
        info = 7;
        this.miColaP.enColar(info,5);
        info = 8;
        this.miColaP.enColar(info,4);
        assertTrue(this.miColaP.getTamanio()==9);
    }

    /**
     * Prueba Metodo esVacia(), de la clase ColaP.
     */
    @Test
    public void testEsVacia() {
        System.out.println("Prueba... esVacia()");
        assertTrue(this.miColaP.esVacia());
    }

    /**
     * Prueba Metodo toString(), de la clase ColaP.
     */
    @Test
    public void testToString() {
        System.out.println("Prueba... toString()");
        Object info = 0;
        this.miColaP.enColar(info,2);
        info = 1;
        this.miColaP.enColar(info,1);
        info = 2;
        this.miColaP.enColar(info,0);
        info = 3;
        this.miColaP.enColar(info,9);
        info = 4;
        this.miColaP.enColar(info,3);
        info = 5;
        this.miColaP.enColar(info,1);
        info = 6;
        this.miColaP.enColar(info,0);
        info = 7;
        this.miColaP.enColar(info,5);        
        String cad="3_9->7_5->4_3->0_2->1_1->5_1->2_0->6_0->";
        assertEquals(cad,this.miColaP.toString());
    }
}